package com.degoos.orbisnoir.interfaces;

public interface Savable {

	String toJSON();
}
