package com.degoos.orbisnoir.interfaces;

import org.joml.Vector2f;

public interface Positionable {

	Vector2f getPosition();

	void setPosition(Vector2f position);

}
