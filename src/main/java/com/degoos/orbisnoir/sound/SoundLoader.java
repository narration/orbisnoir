package com.degoos.orbisnoir.sound;

import com.degoos.graphicengine2.Engine;
import com.degoos.orbisnoir.util.JarUtils;

public class SoundLoader {

	public static void load() {
		try {
			JarUtils.getFilesInsideFolder("sound", null).forEach((name, stream) -> {
				try {
					System.out.println("Loading sound sound/" + name + ".");
					Engine.getSoundManager().loadSound("sound/" + name);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			});
			JarUtils.getFilesInsideFolder("music", null).forEach((name, stream) -> {
				try {
					System.out.println("Loading sound music/" + name + ".");
					Engine.getSoundManager().loadSound("music/" + name);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
