package com.degoos.orbisnoir.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Cache {

	private Map<String, String> data;

	public Cache() {
		data = new ConcurrentHashMap<>();
	}

	public Cache(Map<String, String> map) {
		if (map instanceof ConcurrentHashMap)
			this.data = map;
		else
			data = new ConcurrentHashMap<>(map);
	}

	public void put(String key, Object value) {
		if (value == null) {
			data.remove(key);
		} else {
			data.put(key, value.toString());
		}
	}

	public String get(String key) {
		return data.get(key);
	}

	public boolean contains(String key) {
		return data.containsKey(key);
	}

	public void clear() {
		data.clear();
	}

	@Override
	public String toString() {
		return data.toString();
	}
}
