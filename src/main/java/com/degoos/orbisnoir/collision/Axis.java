package com.degoos.orbisnoir.collision;

import com.degoos.orbisnoir.util.VectorUtils;
import org.joml.Vector2f;

import java.awt.geom.Line2D;
import java.util.List;

public class Axis {

	private Vector2f normal;
	private Vector2f point;
	private Line2D line2D;

	public Axis(Vector2f pointA, Vector2f pointB) {
		point = new Vector2f(pointA).add(pointB).mul(0.5f);
		normal = new Vector2f(pointB).sub(pointA).normalize();
		float x = normal.x;
		normal.x = normal.y;
		normal.y = -x;
		line2D = new Line2D.Float(pointA.x, pointA.y, pointB.x, pointB.y);
	}

	public Vector2f getPoint() {
		return point;
	}

	public Vector2f getNormal() {
		return normal;
	}

	public Line2D getLine2D() {
		return line2D;
	}

	public ShapeProjection project(CollisionBox box) {
		List<Vector2f> vertices = box.getVertices();
		if (vertices.size() < 2) return null;

		Vector2f position = box.getPosition();
		Vector2f vertex = vertices.get(0);

		float min = VectorUtils.fastDot(normal.x, normal.y, vertex.x + position.x, vertex.y + position.y);
		float max = min;
		float p;
		for (int i = 1; i < vertices.size(); i++) {
			vertex = vertices.get(i);
			p = VectorUtils.fastDot(normal.x, normal.y, vertex.x + position.x, vertex.y + position.y);
			if (p < min) min = p;
			else if (p > max) max = p;
		}
		return new ShapeProjection(min, max, Math.abs(max - min));
	}
}
