package com.degoos.orbisnoir.collision;

import org.joml.Vector2f;

public class Collision {

	private Axis axis;
	private float distance;
	private Vector2f mtv;

	public Collision(Axis axis, float distance, Vector2f mtv) {
		this.axis = axis;
		this.distance = distance;
		this.mtv = mtv;
	}

	public Axis getAxis() {
		return axis;
	}

	public float getDistance() {
		return distance;
	}

	public Vector2f getMtv() {
		return mtv;
	}
}
