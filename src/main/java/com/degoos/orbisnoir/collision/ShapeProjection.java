package com.degoos.orbisnoir.collision;

public class ShapeProjection {

	private float min, max;
	private float distanceSquared;

	public ShapeProjection(float min, float max, float distanceSquared) {
		this.min = min;
		this.max = max;
		this.distanceSquared = distanceSquared;
	}

	public float getMin() {
		return min;
	}

	public float getMax() {
		return max;
	}

	public float getDistanceSquared() {
		return distanceSquared;
	}


	public float getOverlap(ShapeProjection projection) {
		if (min < projection.min) {
			return max - projection.min;
		} else return projection.max - min;
	}

	public boolean overlaps(ShapeProjection projection) {
		return getOverlap(projection) > 0;
	}
}
