package com.degoos.orbisnoir.collision;

import com.degoos.orbisnoir.util.VectorUtils;
import org.joml.Vector2f;

import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CollisionBox {

	private Vector2f position;
	private List<Vector2f> vertices;
	private List<Axis> axes;

	private Vector2f min, max, center;

	public CollisionBox(Vector2f position, Vector2f min, Vector2f max) {
		this(position, Arrays.asList(min, new Vector2f(max.x, min.y), max, new Vector2f(min.x, max.y)));
	}

	public CollisionBox(Vector2f position, List<Vector2f> vertices) {
		this.position = position;
		this.vertices = vertices;

		min = new Vector2f(Float.MAX_VALUE);
		max = new Vector2f(Float.MIN_VALUE);

		center = new Vector2f();

		vertices.forEach(target -> center.add(target));
		center.mul(1f / vertices.size());


		axes = new ArrayList<>(vertices.size());

		Vector2f vertex;
		for (int i = 0; i < vertices.size(); i++) {
			vertex = vertices.get(i);
			if (vertex.x < min.x) min.x = vertex.x;
			else if (vertex.x > max.x) max.x = vertex.x;

			if (vertex.y < min.y) min.y = vertex.y;
			else if (vertex.y > max.y) max.y = vertex.y;
			axes.add(new Axis(vertex, vertices.get((i + 1) % vertices.size())));
		}
	}

	public Vector2f getPosition() {
		return position;
	}

	public void setPosition(Vector2f position) {
		this.position = position;
	}

	public List<Vector2f> getVertices() {
		return vertices;
	}

	public List<Axis> getAxes() {
		return axes;
	}

	public Vector2f getMin() {
		return min;
	}

	public Vector2f getMax() {
		return max;
	}

	public Vector2f getCenter() {
		return new Vector2f(center).add(position);
	}

	public Collision collision(CollisionBox box) {

		List<Axis> axes = box.getAxes();

		float minOverlap = Float.MAX_VALUE;
		Axis minAxis = axes.get(0);

		ShapeProjection p1, p2;
		float overlap;
		for (Axis axis : axes) {
			p1 = axis.project(this);
			p2 = axis.project(box);
			overlap = p1.getOverlap(p2);
			if (overlap < 0) return null;
			if (overlap < minOverlap) {
				minOverlap = overlap;
				minAxis = axis;
			} else if (overlap == minOverlap) {
				if (new Vector2f(axis.getPoint()).add(box.position).distanceSquared(position) <
						new Vector2f(minAxis.getPoint()).add(box.position).distanceSquared(position)) {
					minAxis = axis;
				}
			}
		}

		Vector2f mtv = new Vector2f(minAxis.getNormal()).mul(minOverlap);

		if (VectorUtils.fastDot(position.x - box.position.x, position.y - box.position.y, mtv.x, mtv.y) < 0)
			mtv.mul(-1);

		return new Collision(minAxis, minOverlap, mtv);
	}

	public boolean collidesWithLine(Line2D.Float line2D) {
		Line2D.Float sub = new Line2D.Float(line2D.x1 - position.x, line2D.y1 - position.y,
				line2D.x2 - position.x, line2D.y2 - position.y);
		return axes.stream().anyMatch(target -> sub.intersectsLine(target.getLine2D()));
	}
}

