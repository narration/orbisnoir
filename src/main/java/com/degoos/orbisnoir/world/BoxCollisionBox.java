package com.degoos.orbisnoir.world;

import com.degoos.orbisnoir.collision.CollisionBox;
import org.joml.Vector2f;

import java.util.Arrays;
import java.util.List;

public class BoxCollisionBox extends CollisionBox {

	private static final List<Vector2f> vertices = Arrays.asList(new Vector2f(-0.5f), new Vector2f(-0.5f, 0.5f),
			new Vector2f(0.5f), new Vector2f(-0.5f, 0.5f));

	public BoxCollisionBox(Vector2f position) {
		super(new Vector2f(position.x + 0.5f, position.y + 0.5f), vertices);
	}
}
