package com.degoos.orbisnoir.world;

import com.degoos.graphicengine2.Engine;
import com.degoos.orbisnoir.enums.EnumBlockCollision;
import com.degoos.orbisnoir.game.map.level1.Level1Room1;
import com.degoos.orbisnoir.game.map.level1.Level1Room4;
import com.degoos.orbisnoir.game.map.level1.Level1Room7;
import com.degoos.orbisnoir.game.map.level1.Level1Room8;
import com.degoos.orbisnoir.game.map.level2.Level2Room1;
import com.degoos.orbisnoir.game.map.level3.Level3Room5;
import com.degoos.orbisnoir.game.map.level3.Level3Room6;
import com.degoos.orbisnoir.game.map.level5.Level5RoomBoss;
import com.degoos.orbisnoir.game.map.purgatorio.PurgatorioWorld;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptManager;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.util.JarUtils;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.jooq.tools.json.ParseException;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class WorldManager {

	public static Map<String, World> worlds;
	public static File worldsFolder;

	private static Map<String, Class<? extends World>> worldInstances;

	public static void load() {
		worlds = new HashMap<>();
		worldsFolder = new File("worlds");
		if (worldsFolder.isFile()) worldsFolder.delete();
		if (!worldsFolder.exists()) worldsFolder.mkdirs();
		worldInstances = new HashMap<>();
		loadDefaultInstances();
	}

	private static void loadDefaultInstances() {
		worldInstances.put("test", TestWorld.class);
		worldInstances.put("purgatorio", PurgatorioWorld.class);
		worldInstances.put("level1room1", Level1Room1.class);
		worldInstances.put("level1room4", Level1Room4.class);
		worldInstances.put("level1room7", Level1Room7.class);
		worldInstances.put("level1room8", Level1Room8.class);
		worldInstances.put("level2room1", Level2Room1.class);
		worldInstances.put("level3room5", Level3Room5.class);
		worldInstances.put("level3room6", Level3Room6.class);
		worldInstances.put("level5roomBoss", Level5RoomBoss.class);
	}


	public static World loadWorld(String name, Vector2f playerPosition) throws ParseException, IOException, ClassNotFoundException, InvocationTargetException,
			NoSuchMethodException, InstantiationException, IllegalAccessException, URISyntaxException {
		File folder = new File(worldsFolder, name);
		if (!folder.exists() || !folder.isDirectory()) {
			System.out.println("Folder doesn't exist. Loading from jar...");
			return loadFromJar(name, playerPosition);
		}
		World world = createWorldInstance(folder.getName(), new EntitySketchContainer(new File(folder, "entities.dat")), playerPosition, false);
		File chunkFolder = new File(folder, "chunks");
		if (!chunkFolder.isDirectory()) chunkFolder.delete();
		if (!chunkFolder.exists()) chunkFolder.mkdirs();
		for (File file : chunkFolder.listFiles()) {
			world.registerChunk(loadChunk(world, file));
		}
		return world;
	}

	private static World loadFromJar(String name, Vector2f playerPosition) throws IOException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException, URISyntaxException {
		InputStream stream = Engine.getResourceManager().getResourceInputStream("worlds/" + name + "/entities.dat");
		World world = createWorldInstance(name, new EntitySketchContainer(stream), playerPosition, true);

		JarUtils.getFilesInsideFolder("worlds/" + name + "/chunks", ".dat").forEach((n, in) -> {
			try {
				world.registerChunk(loadChunk(world, new ObjectInputStream(in)));
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});
		return world;
	}

	private static World createWorldInstance(String name, EntitySketchContainer entitySketchContainer, Vector2f playerPosition, boolean fromJar) throws NoSuchMethodException,
			IllegalAccessException, InvocationTargetException, InstantiationException {
		if (worldInstances.containsKey(name)) {
			System.out.println("Loading special world " + name + ".");
			return worldInstances.get(name).getConstructor(EntitySketchContainer.class, Vector2f.class, boolean.class)
					.newInstance(entitySketchContainer, playerPosition, fromJar);
		}
		return new World(name, entitySketchContainer, playerPosition, fromJar);
	}

	private static Chunk loadChunk(World world, File file) throws IOException, ClassNotFoundException, ParseException {
		return loadChunk(world, new ObjectInputStream(new FileInputStream(file)));
	}

	private static Chunk loadChunk(World world, ObjectInputStream inputStream) throws IOException, ClassNotFoundException, ParseException {
		int version = inputStream.readInt();
		Vector2i position = new Vector2i(inputStream.readInt(), inputStream.readInt());
		String palette = (String) inputStream.readObject();
		Chunk chunk = new Chunk(world, position, PaletteManager.loadPalette(palette), palette);

		for (int x = 0; x < 32; x++) {
			for (int y = 0; y < 32; y++) {
				boolean b = inputStream.readBoolean();
				if (b) {

					EnumBlockCollision collision;
					if (version > 0) collision = EnumBlockCollision.values()[inputStream.readInt()];
					else collision = EnumBlockCollision.LEVEL_0;

					Script script;
					if (version > 1) script = ScriptManager.getScriptUnsafe((String) inputStream.readObject());
					else script = null;

					chunk.boxes[x][y] = new Box(new Vector2i(x, y),
							chunk, new Vector2i(inputStream.readInt(), inputStream.readInt()), collision, script);
				}
			}
		}
		inputStream.close();

		return chunk;
	}

	public static void resetFolder(World world) {
		File folder = new File(worldsFolder, world.getName());
		if (folder.exists()) folder.delete();
	}

	public static void saveChunk(Chunk chunk) throws IOException {
		File folder = chunk.world.worldFolder;
		if (!folder.isDirectory()) folder.delete();
		if (!folder.exists()) folder.mkdirs();
		folder = new File(folder, "chunks");
		if (!folder.isDirectory()) folder.delete();
		if (!folder.exists()) folder.mkdirs();

		File file = new File(folder, chunk.position.x + "_" + chunk.position.y + ".dat");
		if (!file.isFile()) file.delete();
		if (!file.exists()) file.createNewFile();

		ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(file));

		//VERSION
		outputStream.writeInt(2);

		outputStream.writeInt(chunk.position.x);
		outputStream.writeInt(chunk.position.y);
		outputStream.writeObject(chunk.paletteName);

		Box box;
		for (int x = 0; x < 32; x++) {
			for (int y = 0; y < 32; y++) {
				box = chunk.boxes[x][y];
				if (box == null) outputStream.writeBoolean(false);
				else {
					outputStream.writeBoolean(true);
					outputStream.writeInt(box.collision.ordinal());
					outputStream.writeObject(box.script == null ? "null" : box.script.getName());
					outputStream.writeInt(box.paletteTexture.x);
					outputStream.writeInt(box.paletteTexture.y);
				}
			}
		}
		outputStream.close();
	}
}
