package com.degoos.orbisnoir.world;

import com.degoos.graphicengine2.object.texture.Palette;
import org.joml.Vector2i;

import java.util.NoSuchElementException;
import java.util.Objects;

public class Chunk {

	protected World world;
	protected Vector2i position;
	protected Palette palette;
	protected String paletteName;

	protected Box[][] boxes;

	public Chunk(World world, Vector2i position, Palette palette, String paletteName) {
		this.world = world;
		this.position = position;
		this.palette = palette;
		this.paletteName = paletteName;
		this.boxes = new Box[32][32];
	}

	public World getWorld() {
		return world;
	}

	public Vector2i getPosition() {
		return position;
	}

	public Palette getPalette() {
		return palette;
	}

	public String getPaletteName() {
		return paletteName;
	}

	public void setPalette(Palette palette, String paletteName) {
		this.palette = palette;
		this.paletteName = paletteName;
		for (Box[] boxList : boxes)
			for (Box box : boxList)
				if (box != null) box.refreshPalette();
	}

	public Box[][] getBoxes() {
		return boxes;
	}

	public Box getBox(Vector2i position) {
		Box box = boxes[position.x][position.y];
		if (box == null) throw new NoSuchElementException();
		return box;
	}

	public Box getBox(int x, int y) {
		return boxes[x][y];
	}

	public void registerBox(Box box) {
		boxes[box.chunkPosition.x][box.chunkPosition.y] = box;
		world.addGObject(box);
	}

	public void unregisterBox(Vector2i position) {
		Box box = getBox(position);
		if (box == null) return;
		boxes[position.x][position.y] = null;
		world.removeObject(box);
	}

	public boolean isEmpty() {
		int x = 0;
		int y = 0;
		boolean empty = true;
		while (empty && y < 32) {
			if (boxes[x][y] != null) empty = false;
			else {
				x++;
				if (x == 32) {
					x = 0;
					y++;
				}
			}
		}

		return empty;
	}

	@Override
	public String toString() {
		return "Chunk: [" + position.x + ", " + position.y + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Chunk chunk = (Chunk) o;
		return world.equals(chunk.world) &&
				position.equals(chunk.position);
	}

	@Override
	public int hashCode() {
		return Objects.hash(world, position);
	}
}
