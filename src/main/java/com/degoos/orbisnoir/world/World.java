package com.degoos.orbisnoir.world;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.object.Room;
import com.degoos.orbisnoir.cache.Cache;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.entity.player.PlayerController;
import com.degoos.orbisnoir.enums.EnumDirection;
import com.degoos.orbisnoir.gui.BlackScreen;
import com.degoos.orbisnoir.gui.button.WorldGUI;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptContainer;
import com.degoos.orbisnoir.script.ScriptParser;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.util.LoginUtils;
import com.degoos.orbisnoir.util.VectorUtils;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class World extends Room {

	protected File worldFolder;
	private boolean fromJar;

	protected Camera camera;
	protected Player player;
	protected BlackScreen blackScreen;
	protected ScriptContainer scriptContainer;
	protected EntitySketchContainer entitySketchContainer;
	protected WorldGUI gui;

	protected Map<Vector2i, Chunk> chunks;
	protected Map<UUID, Entity> entities;
	protected Cache cache;

	public World(String name, EntitySketchContainer entitySketchContainer, Vector2f playerPosition, boolean fromJar) {
		super(name);
		this.fromJar = fromJar;
		this.entitySketchContainer = entitySketchContainer;
		camera = new Camera(new Vector3f(playerPosition, 0), 0.15f);
		blackScreen = new BlackScreen();
		chunks = new ConcurrentHashMap<>();
		entities = new ConcurrentHashMap<>();
		cache = new Cache();

		scriptContainer = new ScriptContainer(this);
		gui = new WorldGUI(this);

		try {
			player = new Player(this, new Vector2f(playerPosition), new PlayerController(),
					EnumDirection.DOWN, PaletteManager.loadPalette("protagonist"));
			camera.setTarget(player);
			registerEntity(player);
		} catch (Exception e) {
			e.printStackTrace();
		}

		addGObject(blackScreen);
		loadFolder();
		entitySketchContainer.spawnAllSketches(this);
		new LoginUtils(name).start();
	}

	public Camera getCamera() {
		return camera;
	}

	public void setCamera(Camera camera) {
		this.camera = camera;
	}

	public Player getPlayer() {
		return player;
	}

	public BlackScreen getBlackScreen() {
		return blackScreen;
	}

	public ScriptContainer getScriptContainer() {
		return scriptContainer;
	}

	public WorldGUI getGUI() {
		return gui;
	}

	public Map<Vector2i, Chunk> getChunks() {
		return chunks;
	}

	public Map<UUID, Entity> getEntities() {
		return entities;
	}

	public File getWorldFolder() {
		return worldFolder;
	}

	public Cache getCache() {
		return cache;
	}

	public EntitySketchContainer getEntitySketchContainer() {
		return entitySketchContainer;
	}

	public void registerChunk(Chunk chunk) {
		chunks.put(chunk.position, chunk);
		for (Box[] boxes : chunk.boxes)
			for (Box box : boxes)
				if (box != null) addGObject(box);
	}

	public void unregisterChunk(Vector2i position) {
		for (Box[] boxes : chunks.remove(position).boxes)
			for (Box box : boxes)
				if (box != null) removeObject(box);
	}

	public Chunk getChunkByBlockPosition(Vector2i position) {
		return getChunk(VectorUtils.shiftRight(position, 5, new Vector2i()));
	}

	public Chunk getChunk(Vector2i position) {
		Chunk chunk = chunks.get(position);
		if (chunk == null) throw new NoSuchElementException();
		return chunk;
	}

	public Chunk getChunkOrNull(Vector2i position) {
		try {
			return getChunk(position);
		} catch (NoSuchElementException ex) {
			return null;
		}
	}

	public Box getBox(Vector2i position) {
		Vector2i chunkPosition = VectorUtils.shiftRight(position, 5, new Vector2i());
		Chunk chunk = chunks.get(chunkPosition);
		if (chunk == null) throw new NoSuchElementException("Chunk doesn't exist or it is not loaded");
		Vector2i boxPosition = new Vector2i(position).sub(VectorUtils.shiftLeft(chunkPosition, 5, chunkPosition));

		Box box = chunk.getBox(boxPosition);

		if (box == null) throw new NoSuchElementException("Box doesn't exist");
		return box;
	}

	public Box getBox(int x, int y) {
		return getBox(new Vector2i(x, y));
	}

	public void registerEntity(Entity entity) {
		entities.put(entity.getUuid(), entity);
		addGObject(entity.getRectangle());
	}

	public void unregisterEntity(UUID uuid) {
		Entity entity = entities.remove(uuid);
		if (entity != null) removeObject(entity.getRectangle());
	}

	public Entity getEntity(UUID uuid) {
		Entity entity = entities.get(uuid);
		if (entity == null) throw new NoSuchElementException("Entity doesn't exist!");
		return entity;
	}

	public Entity getEntity(String key) {
		Entity entity = entities.values().stream().filter(target -> key.equals(target.getName())).findAny().orElse(null);
		if (entity == null) throw new NoSuchElementException("Entity doesn't exist!");
		return entity;
	}

	private void loadFolder() {
		if (fromJar) return;
		File worldsFolder = new File("worlds");
		if (worldsFolder.isFile()) worldsFolder.delete();
		if (!worldsFolder.exists()) worldsFolder.mkdir();

		worldFolder = new File(worldsFolder, getName());
		if (worldFolder.isFile()) worldsFolder.delete();
		if (!worldFolder.exists()) worldsFolder.mkdir();
	}

	public void runInitScript() {
		try {

			if (fromJar) {
				InputStream stream = Engine.getResourceManager().getResourceInputStream("worlds/" + getName() + "/initScript.spt");
				Script script = ScriptParser.parseScript(stream, "init");
				getScriptContainer().runScript(script);
			} else {
				File file = new File(worldFolder, "initScript.spt");
				if (!file.isFile()) return;
				Script script = ScriptParser.parseScript(new FileInputStream(file), "init");
				getScriptContainer().runScript(script);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void tick(long dif) {
		List<Entity> entities = new ArrayList<>(this.entities.values());
		Collections.sort(entities, Comparator.comparingInt(Entity::getTickPriority));
		flushAndCheckSorts();
		entities.forEach(target -> target.tick(dif));
		tickObjects.forEach(target -> target.onTick(dif, this));
		flushAndCheckSorts();
		if (camera != null) camera.onTick(dif);
		entities.forEach(target -> target.tick2(dif));
		tickObjects.forEach(target -> target.onTick2(dif, this));
		flushAndCheckSorts();
	}

	@Override
	public void onKeyEvent(KeyEvent event) {
		entities.values().forEach(target -> target.keyEvent(event));
		super.onKeyEvent(event);
	}
}
