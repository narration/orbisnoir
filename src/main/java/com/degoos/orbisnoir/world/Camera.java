package com.degoos.orbisnoir.world;

import com.degoos.graphicengine2.Engine;
import com.degoos.orbisnoir.interfaces.Positionable;
import org.joml.Vector2f;
import org.joml.Vector3f;

public class Camera {

	private Vector3f position;
	private float boxSize;

	private Positionable target;
	private float targetBoxSize;

	private boolean blockFollowXMovement;


	//MOVE
	private Vector3f to;
	private long nanos, finalNanos;
	private Vector3f movementPerNano;
	private Runnable then;

	//ZOOM
	private boolean requiresResize;

	public Camera(Vector3f position, float boxSize) {
		this(position, boxSize, null);
	}

	public Camera(Vector3f position, float boxSize, Positionable target) {
		this.position = position;
		this.boxSize = boxSize;
		this.target = target;
		this.targetBoxSize = 1.7f;

		to = movementPerNano = null;
		nanos = finalNanos = 0;
		requiresResize = false;
		blockFollowXMovement = false;
	}

	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(Vector3f position) {
		this.position = position;
	}

	public float getBoxSize() {
		return boxSize;
	}

	public void setBoxSize(float boxSize) {
		this.boxSize = boxSize;
		requiresResize = true;
	}

	public Positionable getTarget() {
		return target;
	}

	public void setTarget(Positionable target) {
		this.target = target;
	}

	public float getTargetBoxSize() {
		return targetBoxSize;
	}

	public void setTargetBoxSize(float targetBoxSize) {
		this.targetBoxSize = targetBoxSize;
	}

	public boolean requiresResize() {
		return requiresResize;
	}

	public void setRequiresResize(boolean requiresResize) {
		this.requiresResize = requiresResize;
	}

	public boolean isBlockFollowXMovement() {
		return blockFollowXMovement;
	}

	public void setBlockFollowXMovement(boolean blockFollowXMovement) {
		this.blockFollowXMovement = blockFollowXMovement;
	}

	//region conversions

	public Vector3f toEnginePosition(Vector3f position) {
		Vector3f pos = new Vector3f(position).sub(this.position).mul(boxSize);
		pos.z = position.z;
		return pos;
	}

	public Vector3f toEnginePosition(Vector3f position, Vector3f dest) {
		dest.set(position).sub(this.position).mul(boxSize);
		dest.z = position.z;
		return dest;
	}

	public Vector3f toEnginePosition(Vector2f position, float z) {
		Vector3f pos = new Vector3f(position, 0).sub(this.position).mul(boxSize);
		pos.z = z;
		return pos;
	}

	public Vector3f toEnginePosition(Vector2f position, float z, Vector3f dest) {
		dest.set(position, 0).sub(this.position).mul(boxSize);
		dest.z = z;
		return dest;
	}

	public Vector3f toGamePosition(Vector3f position) {
		Vector3f pos = new Vector3f(position).div(boxSize).add(this.position);
		pos.z = position.z;
		return pos;
	}

	public Vector3f toGamePosition(Vector3f position, Vector3f dest) {
		dest.set(position).div(boxSize).add(this.position);
		dest.z = position.z;
		return dest;
	}

	public Vector3f toGamePosition(Vector2f position, float z) {
		Vector3f pos = new Vector3f(position, 0).div(boxSize).add(this.position);
		pos.z = z;
		return pos;
	}

	public Vector3f toGamePosition(Vector2f position, float z, Vector3f dest) {
		dest.set(position, 0).div(boxSize).add(this.position);
		dest.z = z;
		return dest;
	}

	public Box getPointedBox(World world) {
		Vector3f position = toGamePosition(Engine.getMousePosition(), 0);
		return world.getBox((int) Math.floor(position.x), (int) Math.floor(position.y));
	}

	//endregion

	public void move(Vector3f to, long nanos, Runnable then) {
		this.to = new Vector3f(to);
		this.finalNanos = nanos;
		this.nanos = 0;
		this.movementPerNano = new Vector3f(to).sub(position).mul(1f / finalNanos);
		this.then = then;
	}

	public void onTick(long dif) {
		if (target != null) {
			Vector2f targetPosition = target.getPosition();
			if (position.x - targetPosition.x > targetBoxSize && !blockFollowXMovement) position.x = targetPosition.x + targetBoxSize;
			if (position.x - targetPosition.x < -targetBoxSize && !blockFollowXMovement) position.x = targetPosition.x - targetBoxSize;
			if (position.y - targetPosition.y > targetBoxSize) position.y = targetPosition.y + targetBoxSize;
			if (position.y - targetPosition.y < -targetBoxSize) position.y = targetPosition.y - targetBoxSize;
		}

		if (to != null) {
			nanos += dif;
			if (nanos >= finalNanos) {
				position = to;
				movementPerNano = to = null;

				Runnable then = this.then;
				if (then != null) {
					this.then = null;
					then.run();
				}
			} else {
				position.add(new Vector3f(movementPerNano).mul(dif));
			}
		}
	}
}
