package com.degoos.orbisnoir.world;

import com.degoos.orbisnoir.game.entity.level3.boss.Level3Boss;
import org.joml.Vector2f;
import org.jooq.tools.json.ParseException;

import java.io.IOException;

public class TestWorld extends World {

	public TestWorld(EntitySketchContainer entitySketchContainer, Vector2f playerPosition, boolean fromJar) {
		super("test", entitySketchContainer, playerPosition, fromJar);
		try {
			registerEntity(new Level3Boss(this, new Vector2f(-30, 10)));
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
	}
}
