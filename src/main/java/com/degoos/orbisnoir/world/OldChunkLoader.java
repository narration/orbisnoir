package com.degoos.orbisnoir.world;

import com.degoos.graphicengine2.object.texture.Palette;
import com.degoos.orbisnoir.enums.EnumBlockCollision;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptManager;
import com.degoos.orbisnoir.texture.PaletteManager;
import org.joml.Vector2i;
import org.jooq.tools.json.JSONValue;
import org.jooq.tools.json.ParseException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

public class OldChunkLoader {


	public static Chunk load(InputStream inputStream, World world, Vector2i position) throws IOException, ParseException {
		Map map = (Map) JSONValue.parseWithException(new InputStreamReader(inputStream));
		inputStream.close();

		String paletteName = (String) map.get("palette");
		Palette palette = PaletteManager.loadPalette(paletteName);

		Chunk chunk = new Chunk(world, position, palette, paletteName);

		List boxes = (List) map.get("boxes");
		String boxName = null;

		try {
			for (Object o : boxes) {
				Map boxMap = (Map) o;

				int x = (int) (long) boxMap.get("x");
				int y = (int) (long) boxMap.get("y");

				boxName = x + ", " + y;

				int paletteIndex = (int) (long) boxMap.get("palette_index");

				Script script = ScriptManager.getScriptUnsafe((String) boxMap.getOrDefault("script", null));

				EnumBlockCollision collision = EnumBlockCollision.values()[(int) (long) boxMap.get("collision")];

				int size = palette.getPaletteSize().x;

				Box box = new Box(new Vector2i(x, y), chunk, new Vector2i(paletteIndex % size, paletteIndex / size),
						collision, script, world.getCamera());
				box.setScript(script);
				chunk.getBoxes()[x][y] = box;
			}
		} catch (Exception ex) {
			System.out.println("Error while loading a box! " + boxName);
			ex.printStackTrace();
		}


		return chunk;
	}

}
