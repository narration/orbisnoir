package com.degoos.orbisnoir.world;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Room;
import com.degoos.orbisnoir.cache.Cache;
import com.degoos.orbisnoir.collision.Collision;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.editor.collision.CollisionEditor;
import com.degoos.orbisnoir.editor.script.ScriptEditor;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.enums.EnumBlockCollision;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.util.VectorUtils;
import org.joml.Vector2i;
import org.joml.Vector3f;

import java.util.Objects;

public class Box extends Rectangle {

	protected Chunk chunk;
	protected Vector2i chunkPosition;
	protected Vector3f worldPosition;
	protected Vector2i mapPosition;

	protected EnumBlockCollision collision;
	protected CollisionBox collisionBox;
	protected Script script;

	protected Vector2i paletteTexture;

	private boolean selectedInEditor;

	public Box(Vector2i position, Chunk chunk, Vector2i paletteTexture, EnumBlockCollision collision, Script script) {
		this(position, chunk, paletteTexture, collision, script, chunk.world.camera);
	}

	public Box(Vector2i position, Chunk chunk, Vector2i paletteTexture, EnumBlockCollision collision, Script script, Camera camera) {
		super(new Vector3f(0), new Vector3f(0), new Vector3f(camera.getBoxSize()));
		this.chunk = chunk;
		this.chunkPosition = position;
		this.worldPosition = new Vector3f(VectorUtils.shiftLeft(chunk.position, 5, new Vector2i()).add(position), -0.8f);
		this.mapPosition = new Vector2i((int) worldPosition.x, (int) worldPosition.y);
		this.paletteTexture = paletteTexture;
		this.collisionBox = new BoxCollisionBox(VectorUtils.toVector2f(worldPosition));
		this.collision = collision;
		this.script = script;
		this.selectedInEditor = false;

		setKey("box_"+mapPosition.x+"_"+mapPosition.y);
		setPosition(chunk.world.camera.toEnginePosition(worldPosition));

		setVisible(true);
		refreshPalette();
	}

	public Chunk getChunk() {
		return chunk;
	}

	public Vector2i getChunkPosition() {
		return chunkPosition;
	}

	public Vector3f getWorldPosition() {
		return worldPosition;
	}

	public Vector2i getMapPosition() {
		return mapPosition;
	}

	public Vector2i getPaletteTexture() {
		return paletteTexture;
	}

	public void setPaletteTexture(Vector2i paletteTexture) {
		this.paletteTexture = paletteTexture;
		refreshPalette();
	}

	public EnumBlockCollision getCollision() {
		return collision;
	}

	public CollisionBox getCollisionBox() {
		return collisionBox;
	}

	public void setCollision(EnumBlockCollision collision) {
		this.collision = collision;
	}

	public Script getScript() {
		return script;
	}

	public void setScript(Script script) {
		this.script = script;
	}

	public boolean isSelectedInEditor() {
		return selectedInEditor;
	}

	public void setSelectedInEditor(boolean selectedInEditor) {
		this.selectedInEditor = selectedInEditor;
		if (selectedInEditor) setColor(new GColor(0, 1, 1));
		else setColor(GColor.WHITE);
	}

	public void refreshPalette() {
		try {
			setTexture(chunk.palette.getTexture(paletteTexture));
		} catch (ArrayIndexOutOfBoundsException ex) {
			ex.printStackTrace();
			setTexture(null);
		}
	}

	public void collide(Entity entity, Collision collision) {
	}

	public void onScript(Player player) {
		if (script == null) return;
		System.out.println("Running script "+script.getName());
		Cache cache = new Cache();
		cache.put("trigger", getKey());
		cache.put("trigger_type", "box");
		cache.put("player_position_x", player.getPosition().x);
		cache.put("player_position_y", player.getPosition().y);
		cache.put("camera_position_x", chunk.world.camera.getPosition().x);
		cache.put("camera_position_y", chunk.world.camera.getPosition().y);
		chunk.world.scriptContainer.runScript(script, cache);
	}

	@Override
	public void onTick2(long dif, Room room) {

		if (Engine.getRoom() instanceof CollisionEditor) setColor(collision.getColor());
		else if(Engine.getRoom() instanceof ScriptEditor) setColor(script == null ? GColor.WHITE : GColor.GREEN);
		else setColor(GColor.WHITE);

		setPosition(chunk.world.camera.toEnginePosition(worldPosition));

		Camera camera = chunk.world.camera;
		if (camera.requiresResize()) {
			setMax(new Vector3f(camera.getBoxSize()));
		}

		setVisible((Engine.getRenderArea().isInsideX(position.x)
				&& Engine.getRenderArea().isInsideY(position.y))
				|| (Engine.getRenderArea().isInsideX(position.x + max.x)
				&& Engine.getRenderArea().isInsideY(position.y + max.y))
				|| (Engine.getRenderArea().isInsideX(position.x)
				&& Engine.getRenderArea().isInsideY(position.y + max.y))
				|| (Engine.getRenderArea().isInsideX(position.x + max.x)
				&& Engine.getRenderArea().isInsideY(position.y)));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Box box = (Box) o;
		return Objects.equals(chunk, box.chunk) &&
				Objects.equals(chunkPosition, box.chunkPosition);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), chunk, chunkPosition);
	}
}
