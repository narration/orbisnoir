package com.degoos.orbisnoir.world;

import com.degoos.orbisnoir.entity.sketch.EntitySketch;
import com.degoos.orbisnoir.entity.sketch.EntitySketchManager;
import com.degoos.orbisnoir.entity.sketch.EntitySketchPackage;
import org.jooq.tools.json.JSONValue;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EntitySketchContainer {


	private File file;
	private InputStream stream;
	private List<EntitySketch> sketches;

	public EntitySketchContainer(File file) {
		this.file = file;
		try {
			this.stream = new FileInputStream(file);
		} catch (FileNotFoundException ex) {
			stream = null;
		}
		this.sketches = new ArrayList<>();
		loadSketches();
	}

	public EntitySketchContainer(InputStream stream) {
		this.file = null;
		this.stream = stream;
		this.sketches = new ArrayList<>();
		loadSketches();
	}

	public List<EntitySketch> getSketches() {
		return sketches;
	}

	public void spawnAllSketches(World world) {
		sketches.forEach(target -> world.registerEntity(target.toEntity(world)));
	}

	private void loadSketches() {
		if (stream == null) return;
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(stream));
			String line;
			while ((line = reader.readLine()) != null) parseLine(line);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void parseLine(String line) {
		try {
			Map json = (Map) JSONValue.parseWithException(line);
			Class<? extends EntitySketch> clazz = EntitySketchManager.getSketches().stream()
					.filter(target -> target.getName().equals(json.get("type"))).findAny()
					.map(EntitySketchPackage::getEntityClass).orElse(null);
			if (clazz == null) return;
			sketches.add(clazz.getConstructor(Map.class).newInstance(json));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void saveSketches() {
		if (file == null) return;
		if (file.exists()) file.delete();
		BufferedWriter writer = null;
		try {
			file.createNewFile();
			writer = new BufferedWriter(new FileWriter(file));
			for (EntitySketch sketch : sketches) writer.write(sketch.toJSON() + "\n");
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
