package com.degoos.orbisnoir.script;

import com.degoos.graphicengine2.Engine;
import com.degoos.orbisnoir.game.script.level1.Level1ScriptBoxFall;
import com.degoos.orbisnoir.util.FileUtils;
import com.degoos.orbisnoir.util.JarUtils;

import java.io.File;
import java.io.FileInputStream;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class ScriptManager {

	private static Map<String, Script> scripts;

	public static void load() {
		if(scripts == null) scripts = new ConcurrentHashMap<>();
		else scripts.clear();
		try {
			FileUtils.getFilesInsideFolder(new File("scripts"), ".spt").forEach((name, file) -> {
				try {
					if (name.toLowerCase().endsWith(".spt")) {
						String scriptName = name.substring(0, name.length() - 4);
						System.out.println("Loading script " + scriptName + "...");
						scripts.put(scriptName, ScriptParser.parseScript(new FileInputStream(file), scriptName));
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			});


			JarUtils.getFilesInsideFolder("scripts", ".spt").forEach((name, stream) -> {
				try {
					String scriptName = name.substring(0, name.length() - 4);
					System.out.println("Loading script " + scriptName + "...");
					scripts.put(scriptName, ScriptParser.parseScript(stream, scriptName));
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		loadSpecialScripts();
	}

	public static Map<String, Script> getScripts() {
		return scripts;
	}

	public static Optional<Script> getScript(String name) {
		return Optional.ofNullable(scripts.getOrDefault(name, null));
	}

	public static Script getScriptUnsafe(String name) {
		return name == null ? null : scripts.get(name);
	}


	public static void loadSpecialScripts() {
		scripts.put("level1/room7/special_box_fall", new Level1ScriptBoxFall());
	}

}
