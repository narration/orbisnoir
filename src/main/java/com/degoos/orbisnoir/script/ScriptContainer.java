package com.degoos.orbisnoir.script;

import com.degoos.orbisnoir.cache.Cache;
import com.degoos.orbisnoir.world.World;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class ScriptContainer {

	private World world;
	private List<ScriptExecuter> scripts;

	public ScriptContainer(World world) {
		this.world = world;
		this.scripts = new ArrayList<>();
	}

	public World getWorld() {
		return world;
	}

	public List<ScriptExecuter> getScripts() {
		return scripts;
	}

	public void stopAllScripts() {
		scripts.forEach(ScriptExecuter::stop);
		scripts.clear();
	}

	public ScriptExecuter runScript(Script script) {
		ScriptExecuter scriptExecuter = new ScriptExecuter(script, world);
		scripts.add(scriptExecuter);
		scriptExecuter.next();
		return scriptExecuter;
	}

	public ScriptExecuter runScript(Script script, Consumer<ScriptExecuter> onFinish) {
		ScriptExecuter scriptExecuter = new ScriptExecuter(script, world);
		scriptExecuter.setOnFinish(onFinish);
		scripts.add(scriptExecuter);
		scriptExecuter.next();
		return scriptExecuter;
	}

	public ScriptExecuter runScript(Script script, Consumer<ScriptExecuter> onFinish, Cache cache) {
		ScriptExecuter scriptExecuter = new ScriptExecuter(script, world, cache);
		scriptExecuter.setOnFinish(onFinish);

		scripts.add(scriptExecuter);
		scriptExecuter.next();
		return scriptExecuter;
	}


	public ScriptExecuter runScript(Script script, Map<String, String> cache) {
		ScriptExecuter scriptExecuter = new ScriptExecuter(script, world, new Cache(cache));
		scripts.add(scriptExecuter);
		scriptExecuter.next();
		return scriptExecuter;
	}

	public ScriptExecuter runScript(Script script, Cache cache) {
		ScriptExecuter scriptExecuter = new ScriptExecuter(script, world, cache);
		scripts.add(scriptExecuter);
		scriptExecuter.next();
		return scriptExecuter;
	}

	public void addScript(ScriptExecuter executer) {
		scripts.add(executer);
	}

	public void stopScript(ScriptExecuter executer) {
		executer.stop();
		scripts.remove(executer);
	}

	public void stopIf(Predicate<ScriptExecuter> executer) {
		scripts.stream().filter(executer).forEach(ScriptExecuter::stop);
		scripts.removeIf(executer);
	}
}
