package com.degoos.orbisnoir.script;

import com.degoos.orbisnoir.cache.Cache;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.world.World;

import java.util.UUID;
import java.util.function.Consumer;

public class ScriptExecuter {

	private UUID uuid;
	private Script script;
	private World world;
	private Cache cache;

	private ScriptExecuter parent;
	private Consumer<ScriptExecuter> onFinish;

	private boolean alive;
	private int step;

	public ScriptExecuter(Script script, World world) {
		this.uuid = UUID.randomUUID();
		this.script = script;
		this.world = world;
		this.cache = new Cache();

		this.parent = null;
		this.onFinish = null;
		this.alive = true;
		this.step = -1;
	}

	public ScriptExecuter(Script script, World world, Cache cache) {
		this.uuid = UUID.randomUUID();
		this.script = script;
		this.world = world;
		this.cache = cache;

		this.parent = null;
		this.onFinish = null;
		this.alive = true;
		this.step = -1;
	}

	public ScriptExecuter(Script script, World world, ScriptExecuter parent) {
		this.uuid = UUID.randomUUID();
		this.script = script;
		this.world = world;
		this.cache = parent == null ? new Cache() : parent.cache;

		this.parent = parent;
		this.onFinish = null;
		this.alive = true;
		this.step = -1;
	}

	public UUID getUuid() {
		return uuid;
	}

	public Script getScript() {
		return script;
	}

	public World getWorld() {
		return world;
	}

	public Cache getCache() {
		return cache;
	}

	public ScriptExecuter getParent() {
		return parent;
	}

	public boolean isAlive() {
		return alive;
	}

	public Consumer<ScriptExecuter> getOnFinish() {
		return onFinish;
	}

	public void setOnFinish(Consumer<ScriptExecuter> onFinish) {
		this.onFinish = onFinish;
	}

	public int getStep() {
		return step;
	}

	public void setStep(int step) {
		this.step = step;
	}

	public void next() {
		if (!alive) {
			return;
		}
		if (parent != null && !parent.isAlive()) {
			stop();
			return;
		}

		step++;

		if (step >= script.getSteps().size()) {
			stop();
			return;
		}

		ScriptStep scriptStep = script.getSteps().get(step);
		try {
			scriptStep.run0(this, this::next);
		} catch (Exception ex) {
			System.err.println("ERROR RUNNING LINE " + step + " (" + scriptStep + ") OF SCRIPT " + getRootName() + ":");
			ex.printStackTrace();
		}
	}

	private String getRootName() {
		StringBuilder builder = new StringBuilder(script.getName());
		ScriptExecuter executer = this;
		while (executer.parent != null) {
			executer = executer.parent;
			builder.append(" -> ").append(executer.getScript().getName());
		}
		return builder.toString();
	}

	public void stop() {
		alive = false;
		if (onFinish != null)
			onFinish.accept(this);
	}
}
