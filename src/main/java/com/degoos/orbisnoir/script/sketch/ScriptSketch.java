package com.degoos.orbisnoir.script.sketch;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.step.ScriptStep;

import java.util.ArrayList;
import java.util.List;

public abstract class ScriptSketch<T extends ScriptStep> {

	private String name;

	public ScriptSketch(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public T parse(String line) {
		return parse(line, new ArrayList<>(), null);
	}

	public abstract T parse(String line, List<ScriptStep> forcedSteps, Script follow);

}
