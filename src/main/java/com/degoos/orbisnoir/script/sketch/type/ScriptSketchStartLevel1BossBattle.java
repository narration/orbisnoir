package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepStartLevel1BossBattle;

import java.util.List;

public class ScriptSketchStartLevel1BossBattle extends ScriptSketch<ScriptStepStartLevel1BossBattle> {

	public ScriptSketchStartLevel1BossBattle() {
		super("start-level-1-boss-battle");
	}

	@Override
	public ScriptStepStartLevel1BossBattle parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepStartLevel1BossBattle(forcedSteps, follow);
	}
}
