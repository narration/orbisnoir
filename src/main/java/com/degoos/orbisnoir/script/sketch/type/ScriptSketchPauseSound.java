package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepPauseSound;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchPauseSound extends ScriptSketch<ScriptStepPauseSound> {

	public ScriptSketchPauseSound() {
		super("pause-sound");
	}

	@Override
	public ScriptStepPauseSound parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepPauseSound(forcedSteps, follow, (StringValueParser)
				ValueParserParser.parseLine(line, EnumValueParser.STRING).get(0));
	}
}
