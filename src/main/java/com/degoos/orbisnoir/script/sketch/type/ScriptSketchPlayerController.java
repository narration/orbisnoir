package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepPlayerController;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchPlayerController extends ScriptSketch<ScriptStepPlayerController> {

	public ScriptSketchPlayerController() {
		super("player-controller");
	}

	@Override
	public ScriptStepPlayerController parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepPlayerController(forcedSteps, follow, (BooleanValueParser)
				ValueParserParser.parseLine(line, EnumValueParser.BOOLEAN).get(0));
	}
}
