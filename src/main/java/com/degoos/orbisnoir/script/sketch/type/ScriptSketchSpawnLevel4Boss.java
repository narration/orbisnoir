package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepSpawnLevel4Boss;

import java.util.List;

public class ScriptSketchSpawnLevel4Boss extends ScriptSketch<ScriptStepSpawnLevel4Boss> {

	public ScriptSketchSpawnLevel4Boss() {
		super("spawn-level-4-boss");
	}

	@Override
	public ScriptStepSpawnLevel4Boss parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepSpawnLevel4Boss(forcedSteps, follow);
	}
}
