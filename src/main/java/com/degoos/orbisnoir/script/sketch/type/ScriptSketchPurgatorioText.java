package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepPurgatorioText;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchPurgatorioText extends ScriptSketch<ScriptStepPurgatorioText> {

	public ScriptSketchPurgatorioText() {
		super("purgatorio-text");
	}

	@Override
	public ScriptStepPurgatorioText parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepPurgatorioText(forcedSteps, follow, (StringValueParser)
				ValueParserParser.parseLine(line, EnumValueParser.STRING).get(0));
	}
}
