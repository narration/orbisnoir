package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepLevel1BossArrow;

import java.util.List;

public class ScriptSketchLevel1BossArrow extends ScriptSketch<ScriptStepLevel1BossArrow> {

	public ScriptSketchLevel1BossArrow() {
		super("level-1-boss-arrow");
	}

	@Override
	public ScriptStepLevel1BossArrow parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepLevel1BossArrow(forcedSteps, follow);
	}
}
