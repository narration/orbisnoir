package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepMoveComplex;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.*;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchMoveComplex extends ScriptSketch<ScriptStepMoveComplex> {

	public ScriptSketchMoveComplex() {
		super("move-complex");
	}

	@Override
	public ScriptStepMoveComplex parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		List<ValueParser> parsers = ValueParserParser.parseLine(line,
				EnumValueParser.STRING, EnumValueParser.VECTOR_2, EnumValueParser.LONG,  EnumValueParser.BOOLEAN);
		//KEY TO MILLIS RELATIVE
		return new ScriptStepMoveComplex(forcedSteps, follow,
				(StringValueParser) parsers.get(0), (BooleanValueParser) parsers.get(3),
				(Vector2ValueParser) parsers.get(1), (LongValueParser) parsers.get(2));
	}
}
