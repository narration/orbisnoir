package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepStopAllSounds;

import java.util.List;

public class ScriptSketchStopAllSounds extends ScriptSketch<ScriptStepStopAllSounds> {

	public ScriptSketchStopAllSounds() {
		super("stop-all-sounds");
	}

	@Override
	public ScriptStepStopAllSounds parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepStopAllSounds(forcedSteps, follow);
	}
}
