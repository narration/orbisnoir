package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepRectangle;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.*;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.awt.*;
import java.util.List;

public class
ScriptSketchRectangle extends ScriptSketch<ScriptStepRectangle> {

	public ScriptSketchRectangle() {
		super("rectangle");
	}

	@Override
	public ScriptStepRectangle parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		List<ValueParser> parsers = ValueParserParser.parseLine(line,
				EnumValueParser.STRING, EnumValueParser.BOOLEAN, EnumValueParser.FLOAT,
				EnumValueParser.FLOAT, EnumValueParser.VECTOR_2, EnumValueParser.VECTOR_2,
				EnumValueParser.VECTOR_2, EnumValueParser.COLOR, EnumValueParser.FLOAT,
				EnumValueParser.FLOAT);


		StringValueParser key = (StringValueParser) parsers.get(0);
		BooleanValueParser visible = (BooleanValueParser) parsers.get(1);
		FloatValueParser drawPriority = (FloatValueParser) parsers.get(2);
		FloatValueParser tickPriority = (FloatValueParser) parsers.get(3);
		Vector2ValueParser origin = (Vector2ValueParser) parsers.get(4);
		Vector2ValueParser min = (Vector2ValueParser) parsers.get(5);
		Vector2ValueParser max = (Vector2ValueParser) parsers.get(6);
		ColorValueParser color;
		FloatValueParser opacity;
		FloatValueParser rotation;
		if (parsers.size() > 7)
			color = (ColorValueParser) parsers.get(7);
		else color = new ColorValueParser(Color.BLACK);
		if (parsers.size() > 8)
			opacity = (FloatValueParser) parsers.get(8);
		else opacity = new FloatValueParser(1);
		if (parsers.size() > 9)
			rotation = (FloatValueParser) parsers.get(9);
		else rotation = new FloatValueParser(0);

		return new ScriptStepRectangle(forcedSteps, follow,
				key, visible, drawPriority, tickPriority, origin, min, max, color, opacity, rotation);
	}
}
