package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepOrigin;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.parser.ValueParser;
import com.degoos.orbisnoir.script.value.parser.Vector2ValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchOrigin extends ScriptSketch<ScriptStepOrigin> {

	public ScriptSketchOrigin() {
		super("origin");
	}

	@Override
	public ScriptStepOrigin parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		List<ValueParser> parsers = ValueParserParser.parseLine(line,
				EnumValueParser.STRING, EnumValueParser.BOOLEAN, EnumValueParser.VECTOR_2);
		return new ScriptStepOrigin(forcedSteps, follow,
				(StringValueParser) parsers.get(0), (BooleanValueParser) parsers.get(1), (Vector2ValueParser) parsers.get(2));
	}
}
