package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepMoveCamera;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;
import com.degoos.orbisnoir.script.value.parser.LongValueParser;
import com.degoos.orbisnoir.script.value.parser.ValueParser;
import com.degoos.orbisnoir.script.value.parser.Vector2ValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchMoveCamera extends ScriptSketch<ScriptStepMoveCamera> {

	public ScriptSketchMoveCamera() {
		super("move-camera");
	}

	@Override
	public ScriptStepMoveCamera parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		List<ValueParser> parsers = ValueParserParser.parseLine(line,
				EnumValueParser.BOOLEAN, EnumValueParser.VECTOR_2, EnumValueParser.LONG);
		return new ScriptStepMoveCamera(forcedSteps, follow,
				(BooleanValueParser) parsers.get(0), (Vector2ValueParser) parsers.get(1),
				parsers.size() > 2 ? (LongValueParser) parsers.get(2) : new LongValueParser(0));
	}
}
