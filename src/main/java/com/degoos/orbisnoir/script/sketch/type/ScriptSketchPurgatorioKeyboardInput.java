package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepPurgatorioKeyboardInput;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchPurgatorioKeyboardInput extends ScriptSketch<ScriptStepPurgatorioKeyboardInput> {

	public ScriptSketchPurgatorioKeyboardInput() {
		super("purgatorio-keyboard-input");
	}

	@Override
	public ScriptStepPurgatorioKeyboardInput parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepPurgatorioKeyboardInput(forcedSteps, follow, (StringValueParser)
				ValueParserParser.parseLine(line, EnumValueParser.STRING).get(0));
	}
}
