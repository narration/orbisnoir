package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepTurboButton;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchTurboButton extends ScriptSketch<ScriptStepTurboButton> {

	public ScriptSketchTurboButton() {
		super("turbo-button");
	}

	@Override
	public ScriptStepTurboButton parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepTurboButton(forcedSteps, follow, (BooleanValueParser)
				ValueParserParser.parseLine(line, EnumValueParser.BOOLEAN).get(0));
	}
}
