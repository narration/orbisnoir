package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepPlaySound;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.*;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchPlaySound extends ScriptSketch<ScriptStepPlaySound> {

	public ScriptSketchPlaySound() {
		super("play-sound");
	}

	@Override
	public ScriptStepPlaySound parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		List<ValueParser> parsers = ValueParserParser.parseLine(line,
				EnumValueParser.STRING, EnumValueParser.STRING,
				EnumValueParser.FLOAT, EnumValueParser.FLOAT,
				EnumValueParser.FLOAT,
				EnumValueParser.BOOLEAN,
				EnumValueParser.BOOLEAN,
				EnumValueParser.LONG);
		return new ScriptStepPlaySound(forcedSteps, follow,
				(StringValueParser) parsers.get(0), (StringValueParser) parsers.get(1),
				(FloatValueParser) parsers.get(2), (FloatValueParser) parsers.get(3),
				(FloatValueParser) parsers.get(4),
				parsers.size() > 5 ? (BooleanValueParser) parsers.get(5) : new BooleanValueParser(false),
				parsers.size() > 6 ? (BooleanValueParser) parsers.get(6) : new BooleanValueParser(true),
				parsers.size() > 7 ? (LongValueParser) parsers.get(7) : new LongValueParser(0));
	}
}
