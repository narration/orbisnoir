package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepImageRotation;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.FloatValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.parser.ValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchImageRotation extends ScriptSketch<ScriptStepImageRotation> {

	public ScriptSketchImageRotation() {
		super("image-rotation");
	}

	@Override
	public ScriptStepImageRotation parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		List<ValueParser> parsers = ValueParserParser.parseLine(line,
				EnumValueParser.STRING, EnumValueParser.FLOAT);
		return new ScriptStepImageRotation(forcedSteps, follow,
				(StringValueParser) parsers.get(0), (FloatValueParser) parsers.get(1));
	}
}
