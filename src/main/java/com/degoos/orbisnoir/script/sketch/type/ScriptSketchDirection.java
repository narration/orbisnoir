package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepDirection;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.parser.ValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchDirection extends ScriptSketch<ScriptStepDirection> {

	public ScriptSketchDirection() {
		super("direction");
	}

	@Override
	public ScriptStepDirection parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		List<ValueParser> parsers = ValueParserParser.parseLine(line,
				EnumValueParser.STRING, EnumValueParser.STRING);
		return new ScriptStepDirection(forcedSteps, follow,
				(StringValueParser) parsers.get(0), (StringValueParser) parsers.get(1));
	}
}
