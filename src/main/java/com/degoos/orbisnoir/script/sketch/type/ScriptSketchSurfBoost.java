package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepSurfBoost;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.IntValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchSurfBoost extends ScriptSketch<ScriptStepSurfBoost> {

	public ScriptSketchSurfBoost() {
		super("surf-boost");
	}

	@Override
	public ScriptStepSurfBoost parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepSurfBoost(forcedSteps, follow, (IntValueParser)
				ValueParserParser.parseLine(line, EnumValueParser.INT).get(0));
	}
}
