package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepMove;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.parser.ValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchMove extends ScriptSketch<ScriptStepMove> {

	public ScriptSketchMove() {
		super("move");
	}

	@Override
	public ScriptStepMove parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		List<ValueParser> parsers = ValueParserParser.parseLine(line,
				EnumValueParser.STRING, EnumValueParser.BOOLEAN, EnumValueParser.BOOLEAN, EnumValueParser.STRING);
		return new ScriptStepMove(forcedSteps, follow,
				(StringValueParser) parsers.get(0), (StringValueParser) parsers.get(3),
				(BooleanValueParser) parsers.get(1), (BooleanValueParser) parsers.get(2));
	}
}
