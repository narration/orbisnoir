package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepContinueSound;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchContinueSound extends ScriptSketch<ScriptStepContinueSound> {

	public ScriptSketchContinueSound() {
		super("continue-sound");
	}

	@Override
	public ScriptStepContinueSound parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepContinueSound(forcedSteps, follow, (StringValueParser)
				ValueParserParser.parseLine(line, EnumValueParser.STRING).get(0));
	}
}
