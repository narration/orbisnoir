package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepSetPaletteIndex;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.IntValueParser;
import com.degoos.orbisnoir.script.value.parser.ValueParser;
import com.degoos.orbisnoir.script.value.parser.Vector2ValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchSetPaletteIndex extends ScriptSketch<ScriptStepSetPaletteIndex> {

	public ScriptSketchSetPaletteIndex() {
		super("set-palette-index");
	}

	@Override
	public ScriptStepSetPaletteIndex parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		List<ValueParser> parsers = ValueParserParser.parseLine(line,
				EnumValueParser.VECTOR_2, EnumValueParser.INT, EnumValueParser.INT);
		return new ScriptStepSetPaletteIndex(forcedSteps, follow,
				(Vector2ValueParser) parsers.get(0), (IntValueParser) parsers.get(1),
				parsers.size() > 2 ? (IntValueParser) parsers.get(2) : null);
	}
}
