package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepMainMenu;

import java.util.List;

public class ScriptSketchMainMenu extends ScriptSketch<ScriptStepMainMenu> {

	public ScriptSketchMainMenu() {
		super("main-menu");
	}

	@Override
	public ScriptStepMainMenu parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepMainMenu(forcedSteps, follow);
	}
}
