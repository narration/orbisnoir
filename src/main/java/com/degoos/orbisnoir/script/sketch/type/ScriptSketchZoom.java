package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepZoom;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.FloatValueParser;
import com.degoos.orbisnoir.script.value.parser.LongValueParser;
import com.degoos.orbisnoir.script.value.parser.ValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchZoom extends ScriptSketch<ScriptStepZoom> {

	public ScriptSketchZoom() {
		super("zoom");
	}

	@Override
	public ScriptStepZoom parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		List<ValueParser> parsers = ValueParserParser.parseLine(line,
				EnumValueParser.FLOAT, EnumValueParser.LONG);

		return new ScriptStepZoom(forcedSteps, follow,
				(FloatValueParser) parsers.get(0),
				(LongValueParser) parsers.get(1));
	}
}
