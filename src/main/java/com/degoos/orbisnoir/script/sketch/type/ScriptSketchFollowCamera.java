package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepFollowCamera;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class
ScriptSketchFollowCamera extends ScriptSketch<ScriptStepFollowCamera> {

	public ScriptSketchFollowCamera() {
		super("follow-camera");
	}

	@Override
	public ScriptStepFollowCamera parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepFollowCamera(forcedSteps, follow, (StringValueParser)
				ValueParserParser.parseLine(line, EnumValueParser.STRING).get(0));
	}
}
