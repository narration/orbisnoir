package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepWait;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.LongValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchWait extends ScriptSketch<ScriptStepWait> {

	public ScriptSketchWait() {
		super("wait");
	}

	@Override
	public ScriptStepWait parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepWait(forcedSteps, follow, (LongValueParser)
				ValueParserParser.parseLine(line, EnumValueParser.LONG).get(0));
	}
}
