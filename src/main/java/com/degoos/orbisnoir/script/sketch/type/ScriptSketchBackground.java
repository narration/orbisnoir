package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepBackground;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.ColorValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchBackground extends ScriptSketch<ScriptStepBackground> {

	public ScriptSketchBackground() {
		super("background");
	}

	@Override
	public ScriptStepBackground parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepBackground(forcedSteps, follow, (ColorValueParser)
				ValueParserParser.parseLine(line, EnumValueParser.COLOR).get(0));
	}
}
