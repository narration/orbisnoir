package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepWarp;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.parser.ValueParser;
import com.degoos.orbisnoir.script.value.parser.Vector2ValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchWarp extends ScriptSketch<ScriptStepWarp> {

	public ScriptSketchWarp() {
		super("warp");
	}

	@Override
	public ScriptStepWarp parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		List<ValueParser> parsers = ValueParserParser.parseLine(line,
				EnumValueParser.STRING, EnumValueParser.STRING, EnumValueParser.VECTOR_2);

		return new ScriptStepWarp(forcedSteps, follow,
				(StringValueParser) parsers.get(0),
				(StringValueParser) parsers.get(1),
				(Vector2ValueParser) parsers.get(2));
	}
}
