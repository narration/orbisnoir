package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepAddHotbarElement;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.IntValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.parser.ValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchAddHotbarElement extends ScriptSketch<ScriptStepAddHotbarElement> {

	public ScriptSketchAddHotbarElement() {
		super("add-hotbar-element");
	}

	@Override
	public ScriptStepAddHotbarElement parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		List<ValueParser> parsers = ValueParserParser.parseLine(line,
				EnumValueParser.STRING, EnumValueParser.STRING, EnumValueParser.STRING, EnumValueParser.INT);

		return new ScriptStepAddHotbarElement(forcedSteps, follow,
				(StringValueParser) parsers.get(0),
				(StringValueParser) parsers.get(1),
				(StringValueParser) parsers.get(2),
				parsers.size() > 3 ? (IntValueParser) parsers.get(3) : new IntValueParser(0));
	}
}
