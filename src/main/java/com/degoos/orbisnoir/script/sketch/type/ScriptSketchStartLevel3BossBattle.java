package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepStartLevel1BossBattle;
import com.degoos.orbisnoir.script.step.type.ScriptStepStartLevel3BossBattle;

import java.util.List;

public class ScriptSketchStartLevel3BossBattle extends ScriptSketch<ScriptStepStartLevel3BossBattle> {

	public ScriptSketchStartLevel3BossBattle() {
		super("start-level-3-boss-battle");
	}

	@Override
	public ScriptStepStartLevel3BossBattle parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepStartLevel3BossBattle(forcedSteps, follow);
	}
}
