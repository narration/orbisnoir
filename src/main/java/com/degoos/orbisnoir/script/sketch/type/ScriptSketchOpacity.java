package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepOpacity;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.FloatValueParser;
import com.degoos.orbisnoir.script.value.parser.LongValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.parser.ValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchOpacity extends ScriptSketch<ScriptStepOpacity> {

	public ScriptSketchOpacity() {
		super("opacity");
	}

	@Override
	public ScriptStepOpacity parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		List<ValueParser> parsers = ValueParserParser.parseLine(line,
				EnumValueParser.STRING, EnumValueParser.FLOAT, EnumValueParser.LONG);
		if (parsers.size() < 3)
			parsers.add(new LongValueParser(0));
		return new ScriptStepOpacity(forcedSteps, follow,
				(StringValueParser) parsers.get(0), (FloatValueParser) parsers.get(1), (LongValueParser) parsers.get(2));
	}
}
