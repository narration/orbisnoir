package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepSmallText;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;
import com.degoos.orbisnoir.script.value.parser.IntValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.parser.ValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;
import com.degoos.orbisnoir.util.Container;

import java.util.ArrayList;
import java.util.List;

public class ScriptSketchSmallText extends ScriptSketch<ScriptStepSmallText> {

	public ScriptSketchSmallText() {
		super("small-text");
	}

	@Override
	public ScriptStepSmallText parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		StringValueParser key = null;

		List<ValueParser> list;
		if (line.toLowerCase().startsWith("-k ")) {
			line = line.substring(3);
			list = new ArrayList<>();
			line = ValueParserParser.parse(line, list, new Container<>(null), EnumValueParser.STRING, false);
			key = (StringValueParser) list.get(0);
		}
		list = ValueParserParser.parseLine(line, EnumValueParser.BOOLEAN, EnumValueParser.STRING, EnumValueParser.INT, EnumValueParser.STRING);

		return new ScriptStepSmallText(forcedSteps, follow, key,
				(StringValueParser) list.get(3),
				(StringValueParser) list.get(1),
				(BooleanValueParser) list.get(0), (IntValueParser) list.get(2));
	}
}
