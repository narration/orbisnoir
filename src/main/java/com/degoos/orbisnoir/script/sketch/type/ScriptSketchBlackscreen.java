package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepBlackscreen;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.IntValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchBlackscreen extends ScriptSketch<ScriptStepBlackscreen> {

	public ScriptSketchBlackscreen() {
		super("blackscreen");
	}

	@Override
	public ScriptStepBlackscreen parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepBlackscreen(forcedSteps, follow, (IntValueParser)
				ValueParserParser.parseLine(line, EnumValueParser.INT).get(0));
	}
}
