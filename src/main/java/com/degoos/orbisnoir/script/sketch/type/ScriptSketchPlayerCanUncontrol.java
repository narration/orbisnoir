package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepPlayerCanUncontrol;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchPlayerCanUncontrol extends ScriptSketch<ScriptStepPlayerCanUncontrol> {

	public ScriptSketchPlayerCanUncontrol() {
		super("player-can-uncontrol");
	}

	@Override
	public ScriptStepPlayerCanUncontrol parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepPlayerCanUncontrol(forcedSteps, follow, (BooleanValueParser)
				ValueParserParser.parseLine(line, EnumValueParser.BOOLEAN).get(0));
	}
}
