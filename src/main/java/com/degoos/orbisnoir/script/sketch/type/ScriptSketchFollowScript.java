package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepFollowScript;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchFollowScript extends ScriptSketch<ScriptStepFollowScript> {

	public ScriptSketchFollowScript() {
		super("follow-script");
	}

	@Override
	public ScriptStepFollowScript parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepFollowScript(forcedSteps, follow, (StringValueParser)
				ValueParserParser.parseLine(line, EnumValueParser.STRING).get(0));
	}
}
