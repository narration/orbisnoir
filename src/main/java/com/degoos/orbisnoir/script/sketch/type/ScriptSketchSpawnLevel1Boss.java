package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepSpawnLevel1Boss;

import java.util.List;

public class ScriptSketchSpawnLevel1Boss extends ScriptSketch<ScriptStepSpawnLevel1Boss> {

	public ScriptSketchSpawnLevel1Boss() {
		super("spawn-level-1-boss");
	}

	@Override
	public ScriptStepSpawnLevel1Boss parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepSpawnLevel1Boss(forcedSteps, follow);
	}
}
