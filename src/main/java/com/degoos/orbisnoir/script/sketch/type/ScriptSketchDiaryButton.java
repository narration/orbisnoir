package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepDiaryButton;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchDiaryButton extends ScriptSketch<ScriptStepDiaryButton> {

	public ScriptSketchDiaryButton() {
		super("diary-button");
	}

	@Override
	public ScriptStepDiaryButton parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepDiaryButton(forcedSteps, follow, (BooleanValueParser)
				ValueParserParser.parseLine(line, EnumValueParser.BOOLEAN).get(0));
	}
}
