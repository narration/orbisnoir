package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepMoveChain;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.parser.ValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.ArrayList;
import java.util.List;

public class ScriptSketchMoveChain extends ScriptSketch<ScriptStepMoveChain> {

	public ScriptSketchMoveChain() {
		super("move-chain");
	}

	@Override
	public ScriptStepMoveChain parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		List<ValueParser> parsers = ValueParserParser.parseLine(EnumValueParser.STRING, line,
				EnumValueParser.STRING, EnumValueParser.BOOLEAN, EnumValueParser.BOOLEAN);
		List<StringValueParser> stringParsers = new ArrayList<>();

		for (int i = 3; i < parsers.size(); i++) {
			stringParsers.add((StringValueParser) parsers.get(i));
		}


		return new ScriptStepMoveChain(forcedSteps, follow,
				(StringValueParser) parsers.get(0),
				(BooleanValueParser) parsers.get(1), (BooleanValueParser) parsers.get(2), stringParsers);
	}
}
