package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepRunButton;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchRunButton extends ScriptSketch<ScriptStepRunButton> {

	public ScriptSketchRunButton() {
		super("run-button");
	}

	@Override
	public ScriptStepRunButton parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepRunButton(forcedSteps, follow, (BooleanValueParser)
				ValueParserParser.parseLine(line, EnumValueParser.BOOLEAN).get(0));
	}
}
