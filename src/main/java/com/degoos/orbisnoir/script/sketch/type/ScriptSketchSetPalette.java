package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepSetPalette;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.parser.ValueParser;
import com.degoos.orbisnoir.script.value.parser.Vector2ValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchSetPalette extends ScriptSketch<ScriptStepSetPalette> {

	public ScriptSketchSetPalette() {
		super("set-palette");
	}

	@Override
	public ScriptStepSetPalette parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		List<ValueParser> parsers = ValueParserParser.parseLine(line,
				EnumValueParser.VECTOR_2, EnumValueParser.STRING);
		return new ScriptStepSetPalette(forcedSteps, follow,
				(Vector2ValueParser) parsers.get(0), (StringValueParser) parsers.get(1));
	}
}
