package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepStopSound;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.IntValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.parser.ValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchStopSound extends ScriptSketch<ScriptStepStopSound> {

	public ScriptSketchStopSound() {
		super("stop-sound");
	}

	@Override
	public ScriptStepStopSound parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		List<ValueParser> parsers = ValueParserParser.parseLine(line,
				EnumValueParser.STRING, EnumValueParser.INT);

		return new ScriptStepStopSound(forcedSteps, follow,
				(StringValueParser) parsers.get(0),
				parsers.size() > 1 ? (IntValueParser) parsers.get(1) : new IntValueParser(0));
	}
}
