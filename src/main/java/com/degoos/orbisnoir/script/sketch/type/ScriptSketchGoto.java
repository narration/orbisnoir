package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepGoto;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.IntValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchGoto extends ScriptSketch<ScriptStepGoto> {

	public ScriptSketchGoto() {
		super("goto");
	}

	@Override
	public ScriptStepGoto parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepGoto(forcedSteps, follow, (IntValueParser)
				ValueParserParser.parseLine(line, EnumValueParser.INT).get(0));
	}
}
