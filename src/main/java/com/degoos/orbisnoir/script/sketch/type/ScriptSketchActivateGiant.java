package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepActivateGiant;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchActivateGiant extends ScriptSketch<ScriptStepActivateGiant> {

	public ScriptSketchActivateGiant() {
		super("activate-giant");
	}

	@Override
	public ScriptStepActivateGiant parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepActivateGiant(forcedSteps, follow, (StringValueParser)
				ValueParserParser.parseLine(line, EnumValueParser.STRING).get(0));
	}
}
