package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepRepeat;

import java.util.List;

public class ScriptSketchRepeat extends ScriptSketch<ScriptStepRepeat> {

	public ScriptSketchRepeat() {
		super("repeat");
	}

	@Override
	public ScriptStepRepeat parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepRepeat(forcedSteps, follow);
	}
}
