package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepText;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;
import com.degoos.orbisnoir.script.value.parser.IntValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.parser.ValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;
import com.degoos.orbisnoir.script.value.redirector.StaticValueRedirector;
import com.degoos.orbisnoir.util.Container;

import java.util.ArrayList;
import java.util.List;

public class ScriptSketchText extends ScriptSketch<ScriptStepText> {

	public ScriptSketchText() {
		super("text");
	}

	@Override
	public ScriptStepText parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		StringValueParser key = null;
		BooleanValueParser canBeClosed;

		List<ValueParser> list;
		if (line.toLowerCase().startsWith("-c ")) {
			line = line.substring(3);
			canBeClosed = new BooleanValueParser(new StaticValueRedirector("false"));
		} else canBeClosed = new BooleanValueParser(new StaticValueRedirector("true"));
		if (line.toLowerCase().startsWith("-k ")) {
			line = line.substring(3);
			list = new ArrayList<>();
			line = ValueParserParser.parse(line, list, new Container<>(null), EnumValueParser.STRING, false);
			key = (StringValueParser) list.get(0);
		}
		list = ValueParserParser.parseLine(line, EnumValueParser.BOOLEAN, EnumValueParser.STRING, EnumValueParser.INT, EnumValueParser.STRING);

		return new ScriptStepText(forcedSteps, follow, key,
				(StringValueParser) list.get(3),
				(StringValueParser) list.get(1),
				(BooleanValueParser) list.get(0),
				canBeClosed, (IntValueParser) list.get(2));
	}
}
