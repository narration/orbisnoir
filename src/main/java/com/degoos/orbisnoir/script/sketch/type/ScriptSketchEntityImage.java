package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepEntityImage;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.*;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchEntityImage extends ScriptSketch<ScriptStepEntityImage> {

	public ScriptSketchEntityImage() {
		super("entity-image");
	}

	@Override
	public ScriptStepEntityImage parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		List<ValueParser> parsers = ValueParserParser.parseLine(line,
				EnumValueParser.STRING,
				EnumValueParser.VECTOR_2,
				EnumValueParser.VECTOR_2,
				EnumValueParser.VECTOR_2,
				EnumValueParser.STRING,
				EnumValueParser.COLOR,
				EnumValueParser.FLOAT,
				EnumValueParser.FLOAT,
				EnumValueParser.FLOAT,
				EnumValueParser.VECTOR_2,
				EnumValueParser.VECTOR_2,
				EnumValueParser.STRING,
				EnumValueParser.STRING,
				EnumValueParser.STRING,
				EnumValueParser.STRING,
				EnumValueParser.INT);

		IntValueParser paletteIndex;
		if (parsers.size() > 15)
			paletteIndex = (IntValueParser) parsers.get(15);
		else paletteIndex = new IntValueParser(0);

		return new ScriptStepEntityImage(forcedSteps, follow,
				(StringValueParser) parsers.get(0),
				(Vector2ValueParser) parsers.get(1),
				(Vector2ValueParser) parsers.get(2),
				(Vector2ValueParser) parsers.get(3),
				(Vector2ValueParser) parsers.get(9),
				(Vector2ValueParser) parsers.get(10),
				(StringValueParser) parsers.get(4),
				(ColorValueParser) parsers.get(5),
				(FloatValueParser) parsers.get(6),
				(FloatValueParser) parsers.get(7),
				(FloatValueParser) parsers.get(8),
				(StringValueParser) parsers.get(11),
				(StringValueParser) parsers.get(12),
				(StringValueParser) parsers.get(13),
				(StringValueParser) parsers.get(14),
				paletteIndex);
	}
}
