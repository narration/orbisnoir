package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepSave;

import java.util.List;

public class ScriptSketchSave extends ScriptSketch<ScriptStepSave> {

	public ScriptSketchSave() {
		super("save");
	}

	@Override
	public ScriptStepSave parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepSave(forcedSteps, follow);
	}
}
