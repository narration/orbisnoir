package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepRemoveHotbarElement;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.parser.ValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchRemoveHotbarElement extends ScriptSketch<ScriptStepRemoveHotbarElement> {

	public ScriptSketchRemoveHotbarElement() {
		super("remove-hotbar-element");
	}

	@Override
	public ScriptStepRemoveHotbarElement parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		List<ValueParser> parsers = ValueParserParser.parseLine(line, EnumValueParser.STRING);

		return new ScriptStepRemoveHotbarElement(forcedSteps, follow, (StringValueParser) parsers.get(0));
	}
}
