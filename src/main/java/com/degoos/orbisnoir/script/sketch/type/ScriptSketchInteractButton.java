package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepInteractButton;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchInteractButton extends ScriptSketch<ScriptStepInteractButton> {

	public ScriptSketchInteractButton() {
		super("interact-button");
	}

	@Override
	public ScriptStepInteractButton parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		return new ScriptStepInteractButton(forcedSteps, follow, (BooleanValueParser)
				ValueParserParser.parseLine(line, EnumValueParser.BOOLEAN).get(0));
	}
}
