package com.degoos.orbisnoir.script.sketch.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepTickPriority;
import com.degoos.orbisnoir.script.value.ValueParserParser;
import com.degoos.orbisnoir.script.value.parser.DoubleValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.parser.ValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;

import java.util.List;

public class ScriptSketchTickPriority extends ScriptSketch<ScriptStepTickPriority> {

	public ScriptSketchTickPriority() {
		super("tick-priority");
	}

	@Override
	public ScriptStepTickPriority parse(String line, List<ScriptStep> forcedSteps, Script follow) {
		List<ValueParser> parsers = ValueParserParser.parseLine(line,
				EnumValueParser.STRING, EnumValueParser.DOUBLE);
		return new ScriptStepTickPriority(forcedSteps, follow,
				(StringValueParser) parsers.get(0), (DoubleValueParser) parsers.get(1));
	}
}
