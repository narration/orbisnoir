package com.degoos.orbisnoir.script;

import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepIf;
import com.degoos.orbisnoir.script.step.type.ScriptStepWhile;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ScriptParser {

	public static ScriptParserMap SCRIPT_PARSER_MAP = new ScriptParserMap();

	public static Script parseScript(InputStream inputStream, String name) {
		return new Script(name, inputStream);
	}

	public static List<ScriptStep> parseSteps(InputStream inputStream) {
		return parseSteps(new Scanner(inputStream, "UTF-8"), null);
	}

	public static List<ScriptStep> parseSteps(Scanner scanner, ScriptStep stepParent) {
		List<ScriptStep> steps = new ArrayList<>();

		ScriptStep aux, lastStep = null, lastForcedStep = null;
		String line, command;
		int index;
		ScriptSketch sketch;

		while (scanner.hasNextLine()) {
			line = scanner.nextLine().trim();

			if (line.equals("end")) return steps;
			else if (line.equals("follow") && lastForcedStep != null) {
				lastForcedStep.setFollow(new Script("follow", parseSteps(scanner, lastForcedStep)));
				continue;
			} else if (line.equals("else") && stepParent instanceof ScriptStepIf) {
				((ScriptStepIf) stepParent).setElseScript(new Script("else", parseSteps(scanner, null)));
				return steps;
			}

			index = line.indexOf(' ');
			if (index == -1) {
				command = line;
				line = "";
			} else {
				command = line.substring(0, index);
				line = line.substring(index + 1);
			}

			sketch = SCRIPT_PARSER_MAP.get(command);
			if (sketch == null) continue;

			boolean forced = line.startsWith("-f ");
			if (!forced) {
				forced = line.equals("-f");
				if (forced) line = "";
			} else line = line.substring(3);

			if (forced && lastStep != null) {
				aux = parseSafe(sketch, line);
				if (aux == null) continue;
				lastStep.getForcedSteps().add(lastForcedStep = aux);
			} else {
				aux = parseSafe(sketch, line);
				if (aux == null) continue;
				steps.add(lastStep = aux);
				lastForcedStep = null;

				if (aux instanceof ScriptStepIf) {
					((ScriptStepIf) aux).setIfScript(new Script("if", parseSteps(scanner, aux)));
				} else if (aux instanceof ScriptStepWhile) {
					((ScriptStepWhile) aux).setWhileScript(new Script("while", parseSteps(scanner, aux)));
				}
			}
		}

		return steps;
	}

	private static ScriptStep parseSafe(ScriptSketch sketch, String line) {
		try {
			return sketch.parse(line);
		} catch (Exception ex) {
			System.err.println(sketch.getName() + " " + line);
			return null;
		}
	}


}
