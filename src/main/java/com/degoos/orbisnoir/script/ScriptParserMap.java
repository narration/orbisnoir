package com.degoos.orbisnoir.script;

import com.degoos.orbisnoir.script.sketch.ScriptSketch;
import com.degoos.orbisnoir.script.sketch.type.*;

import java.util.HashMap;

public class ScriptParserMap extends HashMap<String, ScriptSketch> {

	public ScriptParserMap() {
		put(new ScriptSketchAddHotbarElement());
		put(new ScriptSketchBackground());
		put(new ScriptSketchBlackscreen());
		put(new ScriptSketchColor());
		put(new ScriptSketchContinueButton());
		put(new ScriptSketchContinueSound());
		put(new ScriptSketchControlEntity());
		put(new ScriptSketchDelete());
		put(new ScriptSketchDiaryButton());
		put(new ScriptSketchDirection());
		put(new ScriptSketchDrawPriority());
		put(new ScriptSketchEntityImage());
		put(new ScriptSketchFollowCamera());
		put(new ScriptSketchFollowScript());
		put(new ScriptSketchGoto());
		put(new ScriptSketchIf());
		put(new ScriptSketchImageAngularVelocity());
		put(new ScriptSketchImageRotation());
		put(new ScriptSketchInteractButton());
		put(new ScriptSketchMainMenu());
		put(new ScriptSketchMove());
		put(new ScriptSketchMoveButton());
		put(new ScriptSketchMoveCamera());
		put(new ScriptSketchMoveChain());
		put(new ScriptSketchMoveComplex());
		put(new ScriptSketchOpacity());
		put(new ScriptSketchOrigin());
		put(new ScriptSketchPauseSound());
		put(new ScriptSketchPlayerCanUncontrol());
		put(new ScriptSketchPlayerController());
		put(new ScriptSketchPlayerSurf());
		put(new ScriptSketchPlaySound());
		put(new ScriptSketchPurgatorioKeyboardInput());
		put(new ScriptSketchPurgatorioText());
		put(new ScriptSketchRectangle());
		put(new ScriptSketchRemoveBlackscreen());
		put(new ScriptSketchRemoveHotbarElement());
		put(new ScriptSketchRepeat());
		put(new ScriptSketchRunButton());
		put(new ScriptSketchRunScript());
		put(new ScriptSketchSave());
		put(new ScriptSketchSet());
		put(new ScriptSketchSetMoving());
		put(new ScriptSketchSetPalette());
		put(new ScriptSketchSetPaletteIndex());
		put(new ScriptSketchSetScript());
		put(new ScriptSketchShieldButton());
		put(new ScriptSketchSmallText());
		put(new ScriptSketchStopAllSounds());
		put(new ScriptSketchStopScript());
		put(new ScriptSketchStopSound());
		put(new ScriptSketchSurfBoost());
		put(new ScriptSketchTeleport());
		put(new ScriptSketchText());
		put(new ScriptSketchTexture());
		put(new ScriptSketchTickPriority());
		put(new ScriptSketchTurboButton());
		put(new ScriptSketchWait());
		put(new ScriptSketchWarp());
		put(new ScriptSketchWhile());
		put(new ScriptSketchZoom());

		put(new ScriptSketchActivateGiant());
		put(new ScriptSketchLevel1BossArrow());
		put(new ScriptSketchSpawnLevel1Boss());
		put(new ScriptSketchStartLevel1BossBattle());
		put(new ScriptSketchStartLevel3BossBattle());
		put(new ScriptSketchSpawnLevel4Boss());
	}

	public void put(ScriptSketch sketch) {
		put(sketch.getName(), sketch);
	}
}
