package com.degoos.orbisnoir.script;

import com.degoos.orbisnoir.script.step.ScriptStep;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class Script {

	private String name;
	private List<ScriptStep> steps;


	public Script(String name, List<ScriptStep> steps) {
		this.name = name;
		this.steps = steps == null ? new ArrayList<>() : steps;
	}

	public Script(String name, InputStream input) {
		this.name = name;
		this.steps = ScriptParser.parseSteps(input);
	}

	public String getName() {
		return name;
	}

	public List<ScriptStep> getSteps() {
		return steps;
	}

	public Script addStep (ScriptStep step) {
		steps.add(step);
		return this;
	}
}
