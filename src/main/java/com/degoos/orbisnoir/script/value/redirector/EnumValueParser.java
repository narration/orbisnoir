package com.degoos.orbisnoir.script.value.redirector;

import com.degoos.orbisnoir.script.value.parser.*;

public enum EnumValueParser {

	INT(IntValueParser.class),
	LONG(LongValueParser.class),
	FLOAT(FloatValueParser.class),
	DOUBLE(DoubleValueParser.class),
	BOOLEAN(BooleanValueParser.class),
	STRING(StringValueParser.class),
	COLOR(ColorValueParser.class),
	VECTOR_2(Vector2ValueParser.class);

	private Class<? extends ValueParser> clazz;

	EnumValueParser(Class<? extends ValueParser> clazz) {
		this.clazz = clazz;
	}

	public Class<? extends ValueParser> getParserClass() {
		return clazz;
	}

	public ValueParser toParser(ValueRedirector redirector) {
		try {
			return clazz.getConstructor(ValueRedirector.class).newInstance(redirector);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}

