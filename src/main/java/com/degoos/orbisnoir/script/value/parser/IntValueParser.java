package com.degoos.orbisnoir.script.value.parser;

import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.value.MathParser;
import com.degoos.orbisnoir.script.value.redirector.StaticValueRedirector;
import com.degoos.orbisnoir.script.value.redirector.ValueRedirector;

public class IntValueParser implements ValueParser<Integer> {

	private ValueRedirector redirector;

	public IntValueParser(ValueRedirector redirector) {
		this.redirector = redirector;
	}

	public IntValueParser(int i) {
		this.redirector = new StaticValueRedirector(i);
	}

	public ValueRedirector getRedirector() {
		return redirector;
	}

	public void setRedirector(ValueRedirector redirector) {
		this.redirector = redirector;
	}

	public Integer getValue(ScriptExecuter executer) {
		return MathParser.parse(executer, redirector.getValue(executer)).intValue();
	}
}
