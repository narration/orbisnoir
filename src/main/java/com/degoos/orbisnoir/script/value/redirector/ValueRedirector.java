package com.degoos.orbisnoir.script.value.redirector;

import com.degoos.orbisnoir.script.ScriptExecuter;

public interface ValueRedirector {

	String getValue(ScriptExecuter executer);

}
