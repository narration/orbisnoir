package com.degoos.orbisnoir.script.value.parser;

import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.value.ConditionParser;
import com.degoos.orbisnoir.script.value.redirector.StaticValueRedirector;
import com.degoos.orbisnoir.script.value.redirector.ValueRedirector;

public class BooleanValueParser implements ValueParser<Boolean> {

	private ValueRedirector redirector;

	public BooleanValueParser(ValueRedirector redirector) {
		this.redirector = redirector;
	}

	public BooleanValueParser(boolean b) {
		this.redirector = new StaticValueRedirector(b);
	}

	public ValueRedirector getRedirector() {
		return redirector;
	}

	public void setRedirector(ValueRedirector redirector) {
		this.redirector = redirector;
	}

	public Boolean getValue(ScriptExecuter executer) {
		return ConditionParser.parse(executer, redirector.getValue(executer));
	}
}
