package com.degoos.orbisnoir.script.value.parser;

import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.value.MathParser;
import com.degoos.orbisnoir.script.value.redirector.StaticValueRedirector;
import com.degoos.orbisnoir.script.value.redirector.ValueRedirector;

public class FloatValueParser implements ValueParser<Float> {

	private ValueRedirector redirector;

	public FloatValueParser(ValueRedirector redirector) {
		this.redirector = redirector;
	}

	public FloatValueParser(float f) {
		this.redirector = new StaticValueRedirector(f);
	}

	public ValueRedirector getRedirector() {
		return redirector;
	}

	public void setRedirector(ValueRedirector redirector) {
		this.redirector = redirector;
	}

	public Float getValue(ScriptExecuter executer) {
		return MathParser.parse(executer, redirector.getValue(executer)).floatValue();
	}
}
