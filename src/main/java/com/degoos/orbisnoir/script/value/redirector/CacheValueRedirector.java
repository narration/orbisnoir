package com.degoos.orbisnoir.script.value.redirector;

import com.degoos.orbisnoir.Game;
import com.degoos.orbisnoir.script.ScriptExecuter;

public class CacheValueRedirector implements ValueRedirector {

	private String key;

	public CacheValueRedirector(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue(ScriptExecuter executer) {
		return Game.getCache().get(key);
	}

}
