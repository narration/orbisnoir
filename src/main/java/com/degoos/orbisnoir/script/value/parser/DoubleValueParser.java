package com.degoos.orbisnoir.script.value.parser;

import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.value.MathParser;
import com.degoos.orbisnoir.script.value.redirector.StaticValueRedirector;
import com.degoos.orbisnoir.script.value.redirector.ValueRedirector;

public class DoubleValueParser implements ValueParser<Double> {

	private ValueRedirector redirector;

	public DoubleValueParser(ValueRedirector redirector) {
		this.redirector = redirector;
	}

	public DoubleValueParser(double d) {
		this.redirector = new StaticValueRedirector(d);
	}

	public ValueRedirector getRedirector() {
		return redirector;
	}

	public void setRedirector(ValueRedirector redirector) {
		this.redirector = redirector;
	}

	public Double getValue(ScriptExecuter executer) {
		return MathParser.parse(executer, redirector.getValue(executer));
	}
}
