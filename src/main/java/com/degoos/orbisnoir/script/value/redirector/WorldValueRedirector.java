package com.degoos.orbisnoir.script.value.redirector;

import com.degoos.orbisnoir.script.ScriptExecuter;

public class WorldValueRedirector implements ValueRedirector {

	private String key;

	public WorldValueRedirector(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue(ScriptExecuter executer) {
		return executer.getWorld().getCache().get(key);
	}

}
