package com.degoos.orbisnoir.script.value.redirector;

import com.degoos.orbisnoir.script.ScriptExecuter;

public class ExistsScriptValueRedirector implements ValueRedirector {

	private String key;

	public ExistsScriptValueRedirector(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue(ScriptExecuter executer) {
		return String.valueOf(executer.getCache().contains(key));
	}

}
