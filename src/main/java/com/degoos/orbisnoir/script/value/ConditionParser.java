package com.degoos.orbisnoir.script.value;

import com.degoos.orbisnoir.script.ScriptExecuter;

import java.util.ArrayList;
import java.util.List;

public class ConditionParser {
	public static boolean parse(ScriptExecuter executer, String string) {
		return parse(executer, string, true);
	}

	public static boolean parse(ScriptExecuter executer, String string, boolean replace) {
		if (string.equalsIgnoreCase("true") || string.equalsIgnoreCase("false"))
			return Boolean.valueOf(string);
		if (string.equalsIgnoreCase("!true") || string.equalsIgnoreCase("!false"))
			return !Boolean.valueOf(string.substring(1));

		string = replace ? PlaceholderParser.parse(executer, string) + " " : string + " ";
		StringBuilder aux = new StringBuilder(string.length());
		char c;
		int index;
		List<Condition> conditions = new ArrayList<>();

		for (int i = 0; i < string.length() - 1; i++) {
			c = string.charAt(i);
			if (c == ' ') continue;
			if (c == '(') {
				index = MathParser.getLastIndex(i + 1, string);
				aux.append(parse(executer, string.substring(i + 1, index)));
				i = index;
			} else if ((c == '&' && string.charAt(i + 1) == '&') || (c == '|' && string.charAt(i + 1) == '|')) {
				conditions.add(new Condition(aux.toString(), c));
				aux = new StringBuilder(string.length() - i);
				i++;
			} else aux.append(c);
		}
		conditions.add(new Condition(aux.toString(), 'i'));

		List<Condition> ors = new ArrayList<>();

		Condition condition, nextCondition;
		boolean isTrue = true;
		for (int i = 0; i < conditions.size() - 1; i++) {
			condition = conditions.get(i);
			if (condition.operation == '&') {
				nextCondition = conditions.get(i + 1);
				if (!isTrue) {
					nextCondition.calculated = true;
					nextCondition.value = false;
				} else {
					if (!condition.calculated) condition.calculate();
					if (!condition.value) {
						nextCondition.calculated = true;
						nextCondition.value = false;
						isTrue = false;
					} else {
						if (!nextCondition.calculated) nextCondition.calculate();
						nextCondition.value = condition.value && nextCondition.value;
					}
				}
			} else {
				ors.add(condition);
				isTrue = true;
			}
		}
		condition = conditions.get(conditions.size() - 1);
		if (!condition.calculated) condition.calculate();
		ors.add(condition);

		for (int i = 0; i < ors.size() - 1; i++) {
			condition = ors.get(i);
			nextCondition = ors.get(i + 1);
			nextCondition.value = condition.value || nextCondition.value;

		}
		condition = ors.get(ors.size() - 1);
		if(!condition.calculated) condition.calculate();
		return condition.value;
	}


	private static class Condition {

		String data;
		boolean value, calculated;
		char operation;

		public Condition(String value, char operation) {
			data = value;
			this.calculated = false;
			this.operation = operation;
		}

		void calculate() {
			calculated = true;
			value = getValue(data);
		}

		boolean getValue(String s) {
			boolean inverse = false;
			if (s.startsWith("!")) {
				inverse = true;
				s = s.substring(1);
			}
			if (s.equalsIgnoreCase("true") || s.equalsIgnoreCase("false"))
				return inverse ? !Boolean.valueOf(s) : Boolean.valueOf(s);

			String[] sl;
			boolean onlyEquals, equals, lower;

			if (s.contains("==")) {
				sl = s.split("==");
				onlyEquals = true;
				equals = false;
				lower = false;
			} else if (s.contains("!=")) {
				sl = s.split("!=");
				onlyEquals = true;
				equals = false;
				lower = false;
				inverse = !inverse;
			} else if (s.contains("<=")) {
				sl = s.split("<=");
				equals = true;
				lower = true;
				onlyEquals = false;
			} else if (s.contains(">=")) {
				sl = s.split(">=");
				equals = true;
				lower = false;
				onlyEquals = false;
			} else if (s.contains("<")) {
				sl = s.split("<");
				equals = false;
				lower = true;
				onlyEquals = false;
			} else if (s.contains(">")) {
				sl = s.split(">");
				equals = false;
				lower = false;
				onlyEquals = false;
			} else return false;

			if (inverse) {
				lower = !lower;
				equals = !equals;
			}

			if (sl.length != 2) return false;
			double d1 = MathParser.isDouble(sl[0]);
			double d2 = MathParser.isDouble(sl[1]);

			if (Double.isNaN(d1) || Double.isNaN(d2)) {
				try {
					d1 = MathParser.parse(null, sl[0], false);
					d2 = MathParser.parse(null, sl[1], false);
				} catch (Exception ex) {
				}
			}

			if (!Double.isNaN(d1) && !Double.isNaN(d2)) {
				boolean condition = d1 < d2;
				if (onlyEquals) return inverse ? d1 != d2 : d1 == d2;
				if (lower) return condition || (equals && d1 == d2);
				else return !condition || (equals && d1 == d2);
			}
			int condition = sl[0].compareTo(sl[1]);

			if (onlyEquals) {
				return inverse == (condition != 0);
			} else {
				if (lower) {
					if (equals) return condition <= 0;
					else return condition < 0;
				} else {
					if (equals) return condition >= 0;
					else return condition > 0;
				}
			}
		}
	}

}
