package com.degoos.orbisnoir.script.value.parser;

import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.value.MathParser;
import com.degoos.orbisnoir.script.value.redirector.StaticValueRedirector;
import com.degoos.orbisnoir.script.value.redirector.ValueRedirector;
import org.joml.Vector2f;

public class Vector2ValueParser implements ValueParser<Vector2f> {

	private ValueRedirector xRedirector, yRedirector;

	public Vector2ValueParser(ValueRedirector xRedirector, ValueRedirector yRedirector) {
		this.xRedirector = xRedirector;
		this.yRedirector = yRedirector;
	}

	public Vector2ValueParser(float x, float y) {
		this.xRedirector = new StaticValueRedirector(x);
		this.yRedirector = new StaticValueRedirector(y);
	}

	public ValueRedirector getxRedirector() {
		return xRedirector;
	}

	public void setxRedirector(ValueRedirector xRedirector) {
		this.xRedirector = xRedirector;
	}

	public ValueRedirector getyRedirector() {
		return yRedirector;
	}

	public void setyRedirector(ValueRedirector yRedirector) {
		this.yRedirector = yRedirector;
	}

	public Vector2f getValue(ScriptExecuter executer) {
		return new Vector2f(MathParser.parse(executer, xRedirector.getValue(executer)).floatValue(),
				MathParser.parse(executer, yRedirector.getValue(executer)).floatValue());
	}
}
