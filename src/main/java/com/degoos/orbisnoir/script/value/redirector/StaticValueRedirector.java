package com.degoos.orbisnoir.script.value.redirector;

import com.degoos.orbisnoir.script.ScriptExecuter;

public class StaticValueRedirector implements ValueRedirector {

	private String value;

	public StaticValueRedirector(String value) {
		this.value = value;
	}

	public StaticValueRedirector(Object o) {
		this.value = o.toString();
	}

	public String getValue(ScriptExecuter executer) {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
