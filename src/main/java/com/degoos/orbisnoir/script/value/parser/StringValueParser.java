package com.degoos.orbisnoir.script.value.parser;

import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.value.PlaceholderParser;
import com.degoos.orbisnoir.script.value.redirector.StaticValueRedirector;
import com.degoos.orbisnoir.script.value.redirector.ValueRedirector;

public class StringValueParser implements ValueParser<String> {

	private ValueRedirector redirector;

	public StringValueParser(ValueRedirector redirector) {
		this.redirector = redirector;
	}

	public StringValueParser(String string) {
		this.redirector = new StaticValueRedirector(string);
	}

	public ValueRedirector getRedirector() {
		return redirector;
	}

	public void setRedirector(ValueRedirector redirector) {
		this.redirector = redirector;
	}

	public String getValue(ScriptExecuter executer) {
		return PlaceholderParser.parse(executer, redirector.getValue(executer));
	}

	public <T extends Enum<T>> T getValue(ScriptExecuter executer, Class<T> enumClass) {
		return Enum.valueOf(enumClass, getValue(executer).toUpperCase());
	}

	public <T extends Enum<T>> T getValue(ScriptExecuter executer, Class<T> enumClass, T def) {
		try {
			return Enum.valueOf(enumClass, getValue(executer).toUpperCase());
		} catch (Exception ex) {
			return def;
		}
	}
}
