package com.degoos.orbisnoir.script.value.parser;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.value.MathParser;
import com.degoos.orbisnoir.script.value.redirector.StaticValueRedirector;
import com.degoos.orbisnoir.script.value.redirector.ValueRedirector;

import java.awt.*;

public class ColorValueParser implements ValueParser<GColor> {

	private ValueRedirector redRedirector, greenRedirector, blueRedirector;

	public ColorValueParser(ValueRedirector redRedirector, ValueRedirector greenRedirector, ValueRedirector blueRedirector) {
		this.redRedirector = redRedirector;
		this.greenRedirector = greenRedirector;
		this.blueRedirector = blueRedirector;
	}

	public ColorValueParser(Color color) {
		this.redRedirector = new StaticValueRedirector(color.getRed());
		this.greenRedirector = new StaticValueRedirector(color.getGreen());
		this.blueRedirector = new StaticValueRedirector(color.getBlue());
	}

	public ValueRedirector getRedRedirector() {
		return redRedirector;
	}

	public void setRedRedirector(ValueRedirector redRedirector) {
		this.redRedirector = redRedirector;
	}

	public ValueRedirector getGreenRedirector() {
		return greenRedirector;
	}

	public void setGreenRedirector(ValueRedirector greenRedirector) {
		this.greenRedirector = greenRedirector;
	}

	public ValueRedirector getBlueRedirector() {
		return blueRedirector;
	}

	public void setBlueRedirector(ValueRedirector blueRedirector) {
		this.blueRedirector = blueRedirector;
	}

	public GColor getValue(ScriptExecuter executer) {
		return new GColor(MathParser.parse(executer, redRedirector.getValue(executer)).floatValue() / 255,
				MathParser.parse(executer, greenRedirector.getValue(executer)).floatValue() / 255,
				MathParser.parse(executer, blueRedirector.getValue(executer)).floatValue() / 255);
	}
}
