package com.degoos.orbisnoir.script.value;

import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.value.parser.ValueParser;
import com.degoos.orbisnoir.script.value.redirector.EnumValueParser;
import com.degoos.orbisnoir.util.Container;

import java.util.ArrayList;
import java.util.List;

public class PlaceholderParser {

	public static String parse(ScriptExecuter executer, String string) {
		if (string == null) return null;
		int index = 0;
		StringBuilder builder = new StringBuilder();
		while (index < string.length()) {
			char c = string.charAt(index);
			if (c == '{') {
				index = replace(executer, string, index + 1, builder);
			} else {
				builder.append(c);
				index++;
			}
		}
		return builder.toString();
	}

	private static int replace(ScriptExecuter executer, String string, int index, StringBuilder builder) {
		StringBuilder placeholder = new StringBuilder("{");
		char c = ' ';
		while (c != '}' && index < string.length()) {
			c = string.charAt(index);
			placeholder.append(c);
			index++;
		}
		if (placeholder.toString().endsWith("}")) {
			List<ValueParser> list = new ArrayList<>();
			ValueParserParser.parse(placeholder.toString(), list, new Container<>(null), EnumValueParser.STRING, true);
			if (!list.isEmpty()) builder.append(list.get(0).getValue(executer));
			else builder.append(placeholder);
		}
		return index;
	}
}
