package com.degoos.orbisnoir.script.value.redirector;

import com.degoos.orbisnoir.Game;
import com.degoos.orbisnoir.script.ScriptExecuter;

public class ExistsCacheValueRedirector implements ValueRedirector {

	private String key;

	public ExistsCacheValueRedirector(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue(ScriptExecuter executer) {
		return String.valueOf(Game.getCache().contains(key));
	}

}
