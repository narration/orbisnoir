package com.degoos.orbisnoir.script.value.parser;

import com.degoos.orbisnoir.script.ScriptExecuter;

public interface ValueParser<T> {

	T getValue(ScriptExecuter executer);

}
