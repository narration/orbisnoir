package com.degoos.orbisnoir.script.value;

import com.degoos.orbisnoir.script.value.parser.ColorValueParser;
import com.degoos.orbisnoir.script.value.parser.ValueParser;
import com.degoos.orbisnoir.script.value.parser.Vector2ValueParser;
import com.degoos.orbisnoir.script.value.redirector.*;
import com.degoos.orbisnoir.util.Container;

import java.util.ArrayList;
import java.util.List;

public class ValueParserParser {

	public static List<ValueParser> parseLine(String line, EnumValueParser... expected) {
		return parseLine(null, line, expected);
	}

	public static List<ValueParser> parseLine(EnumValueParser exceed, String line, EnumValueParser... expected) {
		List<ValueParser> parsers = new ArrayList<>();
		if (expected.length == 0 && exceed == null) return parsers;
		line = line.trim();
		int index = 0;
		Container<ValueRedirector> container = new Container<>(null);
		do {
			line = parse(line, parsers, container, index < expected.length ? expected[index] : exceed,
					expected.length - 1 == index && exceed == null).trim();
			index++;
		} while (!line.isEmpty() && (exceed != null || index < expected.length));
		return parsers;
	}

	public static String parse(String line, List<ValueParser> parsers, Container<ValueRedirector> container, EnumValueParser expected, boolean finalParse) {

		if (expected == EnumValueParser.COLOR) {
			ValueRedirector r, g, b;
			line = getRedirector(line, container, false);
			r = container.getValue();
			line = getRedirector(line, container, false);
			g = container.getValue();
			line = getRedirector(line, container, finalParse);
			b = container.getValue();
			parsers.add(new ColorValueParser(r, g, b));
			return line;
		} else if (expected == EnumValueParser.VECTOR_2) {
			ValueRedirector x, y;
			line = getRedirector(line, container, false);
			x = container.getValue();
			line = getRedirector(line, container, finalParse);
			y = container.getValue();

			parsers.add(new Vector2ValueParser(x, y));
			return line;
		}

		container.setValue(null);
		line = getRedirector(line, container, finalParse);
		if (!container.isEmpty())
			parsers.add(expected.toParser(container.getValue()));
		return line;
	}

	public static String getRedirector(String line, Container<ValueRedirector> container, boolean finalValue) {
		int to;
		int from = 0;
		int offSet = 1;

		if (finalValue) {
			to = line.length();
			offSet = 0;
		} else {
			if (line.startsWith("\"")) {
				to = line.indexOf('"', 1);
				if (to == -1) {
					line = line.substring(1);
					to = line.indexOf(' ');
				} else {
					from = 1;
					offSet = 2;
				}
			} else {
				to = line.indexOf(' ');
			}
			if (to == -1) to = line.length();
		}

		String param = line.substring(from, to);

		ValueRedirector redirector;
		if (param.charAt(0) == '{' && param.charAt(param.length() - 1) == '}') {
			String[] sl = param.substring(1, param.length() - 1).split(";");
			if (sl.length != 2) redirector = new StaticValueRedirector(param);
			else {
				switch (sl[0].toLowerCase()) {
					case "save":
						redirector = new SaveValueRedirector(sl[1]);
						break;
					case "cache":
						redirector = new CacheValueRedirector(sl[1]);
						break;
					case "world":
						redirector = new WorldValueRedirector(sl[1]);
						break;
					case "script":
						redirector = new ScriptValueRedirector(sl[1]);
						break;
					case "exists_save":
						redirector = new ExistsSaveValueRedirector(sl[1]);
						break;
					case "exists_cache":
						redirector = new ExistsCacheValueRedirector(sl[1]);
						break;
					case "exists_world":
						redirector = new ExistsWorldValueRedirector(sl[1]);
						break;
					case "exists_script":
						redirector = new ExistsScriptValueRedirector(sl[1]);
						break;
					case "exists_hotbar":
						redirector = new ExistsHotbarValueRedirector(sl[1]);
						break;
					default:
						redirector = new StaticValueRedirector(param);
				}
			}
		} else {
			redirector = new StaticValueRedirector(param);
		}
		container.setValue(redirector);
		return substringOrEmpty(line, to + offSet);
	}


	private static String substringOrEmpty(String string, int from) {
		try {
			return string.substring(from);
		} catch (Exception ex) {
			return "";
		}
	}
}
