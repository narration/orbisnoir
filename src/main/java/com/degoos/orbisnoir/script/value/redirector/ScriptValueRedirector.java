package com.degoos.orbisnoir.script.value.redirector;

import com.degoos.orbisnoir.script.ScriptExecuter;

public class ScriptValueRedirector implements ValueRedirector {

	private String key;

	public ScriptValueRedirector(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue(ScriptExecuter executer) {
		return executer.getCache().get(key);
	}

}
