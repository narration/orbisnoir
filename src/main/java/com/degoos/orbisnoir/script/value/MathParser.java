package com.degoos.orbisnoir.script.value;

import com.degoos.orbisnoir.script.ScriptExecuter;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;


public class MathParser {

	public static Double parse(ScriptExecuter executer, String string) {
		return parse(executer, string, true);
	}

	public static Double parse(ScriptExecuter executer, String string, boolean replace) {
		if (string == null || string.isEmpty()) throw new RuntimeException();
		double d = isDouble(string);
		if (!Double.isNaN(d)) return d;
		if (replace)
			string = PlaceholderParser.parse(executer, string);
		Expression expression = new ExpressionBuilder(string).build();
		return expression.evaluate();
	}

	static int getLastIndex(int i, String string) {
		int current = 1;
		char c;
		while (current > 0 && i < string.length()) {
			c = string.charAt(i);
			if (c == '(') current++;
			else if (c == ')') current--;

			if (current > 0) i++;
		}
		return i;
	}


	public static double isDouble(String string) {
		try {
			return Double.valueOf(string);
		} catch (Exception ex) {
			return Double.NaN;
		}
	}

}
