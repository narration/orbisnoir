package com.degoos.orbisnoir.script.value.parser;

import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.value.MathParser;
import com.degoos.orbisnoir.script.value.redirector.StaticValueRedirector;
import com.degoos.orbisnoir.script.value.redirector.ValueRedirector;

public class LongValueParser implements ValueParser<Long> {

	private ValueRedirector redirector;

	public LongValueParser(ValueRedirector redirector) {
		this.redirector = redirector;
	}

	public LongValueParser(long l) {
		this.redirector = new StaticValueRedirector(l);
	}

	public ValueRedirector getRedirector() {
		return redirector;
	}

	public void setRedirector(ValueRedirector redirector) {
		this.redirector = redirector;
	}

	public Long getValue(ScriptExecuter executer) {
		return MathParser.parse(executer, redirector.getValue(executer)).longValue();
	}
}
