package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.Game;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.MathParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;

public class ScriptStepSet extends ScriptStep {

	private StringValueParser cache, key, value;

	public ScriptStepSet(List<ScriptStep> forcedSteps, Script follow, StringValueParser cache, StringValueParser key, StringValueParser value) {
		super(forcedSteps, follow);
		this.cache = cache;
		this.key = key;
		this.value = value;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		try {
			String value = this.value.getValue(executer);
			double d = getValue(executer, value);
			if (!Double.isNaN(d)) value = String.valueOf(d);
			if (value.endsWith(".0")) value = value.substring(0, value.length() - 2);

			switch (cache.getValue(executer).toLowerCase()) {
				case "save":
					Game.getSave().injectDataToField(key.getValue(executer), value);
					break;
				case "cache":
					Game.getCache().put(key.getValue(executer), value);
					break;
				case "world":
					executer.getWorld().getCache().put(key.getValue(executer), value);
					break;
				case "script":
					executer.getCache().put(key.getValue(executer), value);
					break;
				default:
					throw new RuntimeException();
			}

			if (then != null) then.run();
		} catch (NoSuchFieldException ex) {
			throw new RuntimeException(ex);
		}
	}

	private Double getValue(ScriptExecuter executer, String string) {
		try {
			return MathParser.parse(executer, string, true);
		} catch (Exception ex) {
			return Double.NaN;
		}
	}
}
