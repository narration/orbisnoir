package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.entity.DirectionalEntity;
import com.degoos.orbisnoir.enums.EnumDirection;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;

public class ScriptStepDirection extends ScriptStep {

	private StringValueParser key;
	private StringValueParser direction;

	public ScriptStepDirection(List<ScriptStep> forcedSteps, Script follow, StringValueParser key,
	                           StringValueParser direction) {
		super(forcedSteps, follow);
		this.key = key;
		this.direction = direction;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		String key = this.key.getValue(executer);
		EnumDirection direction = this.direction.getValue(executer, EnumDirection.class);
		executer.getWorld().getEntities().values()
				.stream().filter(target -> target instanceof DirectionalEntity &&
				key.equals(target.getName()))
				.forEach(target -> ((DirectionalEntity) target).setDirection(direction));
		if (then != null) then.run();
	}
}
