package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.ScriptManager;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;
import java.util.NoSuchElementException;

public class ScriptStepSetScript extends ScriptStep {


	private StringValueParser key, script;

	public ScriptStepSetScript(List<ScriptStep> forcedSteps, Script follow, StringValueParser key, StringValueParser script) {
		super(forcedSteps, follow);
		this.key = key;
		this.script = script;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		try {
			Entity entity = executer.getWorld().getEntity(key.getValue(executer));

			entity.setScript(ScriptManager.getScriptUnsafe(script.getValue(executer)));
		} catch (NoSuchElementException ignore) {
		}

		if (then != null) then.run();
	}
}
