package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.ScriptManager;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;

public class ScriptStepRunScript extends ScriptStep {

	private StringValueParser key;

	public ScriptStepRunScript(List<ScriptStep> forcedSteps, Script follow, StringValueParser key) {
		super(forcedSteps, follow);
		this.key = key;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		ScriptManager.getScript(key.getValue(executer)).ifPresent(target -> {
			ScriptExecuter ex = new ScriptExecuter(target, executer.getWorld());
			executer.getWorld().getScriptContainer().getScripts().add(ex);
			ex.next();
		});
		if(then != null) then.run();
	}
}
