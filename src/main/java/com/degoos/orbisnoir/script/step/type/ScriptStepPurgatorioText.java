package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.game.entity.purgatorio.PurgatorioText;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;

public class ScriptStepPurgatorioText extends ScriptStep {

	private StringValueParser text;

	public ScriptStepPurgatorioText(List<ScriptStep> forcedSteps, Script follow, StringValueParser text) {
		super(forcedSteps, follow);
		this.text = text;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		PurgatorioText textBox = new PurgatorioText(executer.getWorld(), text.getValue(executer));
		textBox.setKey(null);
		textBox.setThen(then);
		executer.getWorld().addGObject(textBox);
	}
}