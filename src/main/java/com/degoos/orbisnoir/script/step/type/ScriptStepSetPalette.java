package com.degoos.orbisnoir.script.step.type;

import com.degoos.graphicengine2.object.texture.Palette;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.parser.Vector2ValueParser;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.world.Chunk;
import org.joml.Vector2f;
import org.joml.Vector2i;

import java.util.List;
import java.util.NoSuchElementException;

public class ScriptStepSetPalette extends ScriptStep {


	private Vector2ValueParser chunk;
	private StringValueParser palette;

	public ScriptStepSetPalette(List<ScriptStep> forcedSteps, Script follow, Vector2ValueParser chunk, StringValueParser palette) {
		super(forcedSteps, follow);
		this.chunk = chunk;
		this.palette = palette;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		Vector2f pos = chunk.getValue(executer);
		String name = palette.getValue(executer);
		try {
			Chunk chunk = executer.getWorld().getChunk(new Vector2i((int) Math.floor(pos.x), (int) Math.floor(pos.y)));
			Palette palette = PaletteManager.loadPalette(name);
			chunk.setPalette(palette, name);
		} catch (NoSuchElementException ignore) {
		} catch (Exception ex) {
			System.err.println("Error loading palette " + name);
		}
		if (then != null) then.run();
	}
}
