package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.entity.MovableEntity;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;
import java.util.Optional;

public class ScriptStepSetMoving extends ScriptStep {

	private StringValueParser key;
	private BooleanValueParser move, run;

	public ScriptStepSetMoving(List<ScriptStep> forcedSteps, Script follow, StringValueParser key,
							   BooleanValueParser move, BooleanValueParser run) {
		super(forcedSteps, follow);
		this.key = key;
		this.move = move;
		this.run = run;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		String key = this.key.getValue(executer);
		Optional<MovableEntity> npc = executer.getWorld().getEntities().values()
				.stream().filter(target -> target instanceof MovableEntity && key.equals(target.getName()))
				.map(target -> (MovableEntity) target).findAny();
		if (npc.isPresent()) {
			npc.get().setMoving(move.getValue(executer));
			npc.get().setRunning(run.getValue(executer));
		}
		if (then != null) then.run();
	}
}
