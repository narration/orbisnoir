package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.handler.ZoomHandler;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.FloatValueParser;
import com.degoos.orbisnoir.script.value.parser.LongValueParser;

import java.util.List;

public class ScriptStepZoom extends ScriptStep {

	private FloatValueParser boxSize;
	private LongValueParser millis;


	public ScriptStepZoom(List<ScriptStep> forcedSteps, Script follow, FloatValueParser boxSize, LongValueParser millis) {
		super(forcedSteps, follow);
		this.boxSize = boxSize;
		this.millis = millis;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		long millis = this.millis.getValue(executer);
		if (millis > 0)
			executer.getWorld().addGObject(new ZoomHandler(boxSize.getValue(executer), millis, then, executer.getWorld().getCamera()));
		else {
			executer.getWorld().getCamera().setBoxSize(boxSize.getValue(executer));
			if (then != null) then.run();
		}
	}
}
