package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;

import java.util.List;

public class ScriptStepRepeat extends ScriptStep {


	public ScriptStepRepeat(List<ScriptStep> forcedSteps, Script follow) {
		super(forcedSteps, follow);
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		while (executer.getParent() != null) {
			executer = executer.getParent();
		}
		executer.setStep(-1);
		executer.next();
	}
}
