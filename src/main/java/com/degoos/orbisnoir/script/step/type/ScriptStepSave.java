package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.Game;
import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;

import java.util.List;

public class ScriptStepSave extends ScriptStep {


	public ScriptStepSave(List<ScriptStep> forcedSteps, Script follow) {
		super(forcedSteps, follow);
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		Player player = executer.getWorld().getPlayer();
		Game.getSave().player_position = player.getPosition();
		Game.getSave().player_room = player.getWorld().getName();

		Game.getSave().save();
		if (then != null) then.run();
	}
}
