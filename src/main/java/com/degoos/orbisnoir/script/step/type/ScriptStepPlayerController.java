package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.entity.player.PlayerController;
import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;

import java.util.List;

public class ScriptStepPlayerController extends ScriptStep {

	private BooleanValueParser enabled;

	public ScriptStepPlayerController(List<ScriptStep> forcedSteps, Script follow, BooleanValueParser enabled) {
		super(forcedSteps, follow);
		this.enabled = enabled;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		Player player = executer.getWorld().getPlayer();
		if (player.getController() instanceof PlayerController)
			((PlayerController) player.getController()).setCanBeControlled(enabled.getValue(executer));
		if (then != null) then.run();
	}
}
