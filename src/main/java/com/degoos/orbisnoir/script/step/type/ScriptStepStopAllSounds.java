package com.degoos.orbisnoir.script.step.type;

import com.degoos.graphicengine2.Engine;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;

import java.util.List;

public class ScriptStepStopAllSounds extends ScriptStep {


	public ScriptStepStopAllSounds(List<ScriptStep> forcedSteps, Script follow) {
		super(forcedSteps, follow);
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		Engine.getSoundManager().cleanUpAllSoundSources();
		if (then != null) then.run();
	}
}
