package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.FloatValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;

public class ScriptStepImageRotation extends ScriptStep {

	private StringValueParser key;
	private FloatValueParser rotation;

	public ScriptStepImageRotation(List<ScriptStep> forcedSteps, Script follow, StringValueParser key,
								   FloatValueParser rotation) {
		super(forcedSteps, follow);
		this.key = key;
		this.rotation = rotation;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		String key = this.key.getValue(executer);
		float rotationValue = rotation.getValue(executer);
		executer.getWorld().getEntities().values()
				.stream().filter(target -> key.equals(target.getName()))
				.forEach(target -> target.getRectangle().setRotation(rotationValue));
		if (then != null) then.run();
	}
}
