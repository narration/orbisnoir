package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;
import java.util.NoSuchElementException;

public class ScriptStepFollowCamera extends ScriptStep {

	private StringValueParser key;

	public ScriptStepFollowCamera(List<ScriptStep> forcedSteps, Script follow, StringValueParser key) {
		super(forcedSteps, follow);
		this.key = key;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		try {
			executer.getWorld().getCamera().setTarget(executer.getWorld().getEntity(key.getValue(executer)));
		} catch (
				NoSuchElementException ex) {
			executer.getWorld().getCamera().setTarget(null);
		}
		if (then != null) then.run();
	}
}
