package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.gui.button.ShieldButton;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;

import java.util.List;

public class ScriptStepShieldButton extends ScriptStep {

	private BooleanValueParser enabled;

	public ScriptStepShieldButton(List<ScriptStep> forcedSteps, Script follow, BooleanValueParser enabled) {
		super(forcedSteps, follow);
		this.enabled = enabled;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		ShieldButton button = executer.getWorld().getGUI().getShieldButton();
		if (enabled.getValue(executer))
			button.appear();
		else button.disappear();
		if (then != null) then.run();
	}
}
