package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.IntValueParser;

import java.util.List;

public class ScriptStepRemoveBlackscreen extends ScriptStep {

	private IntValueParser millis;

	public ScriptStepRemoveBlackscreen(List<ScriptStep> forcedSteps, Script follow, IntValueParser millis) {
		super(forcedSteps, follow);
		this.millis = millis;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		executer.getWorld().getBlackScreen().setOpacity(0, millis.getValue(executer) * 1000000L, then);
	}
}
