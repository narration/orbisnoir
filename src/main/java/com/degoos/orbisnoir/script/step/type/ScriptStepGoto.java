package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.IntValueParser;

import java.util.List;

public class ScriptStepGoto extends ScriptStep {

	private IntValueParser to;

	public ScriptStepGoto(List<ScriptStep> forcedSteps, Script follow, IntValueParser to) {
		super(forcedSteps, follow);
		this.to = to;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		executer.setStep(to.getValue(executer) - 1);
		if (then != null) then.run();
	}
}
