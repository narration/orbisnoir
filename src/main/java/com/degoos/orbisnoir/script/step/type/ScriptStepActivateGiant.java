package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.game.entity.level3.GiantWarrior;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;
import java.util.NoSuchElementException;

public class ScriptStepActivateGiant extends ScriptStep {

	private StringValueParser key;

	public ScriptStepActivateGiant(List<ScriptStep> forcedSteps, Script follow, StringValueParser key) {
		super(forcedSteps, follow);
		this.key = key;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		String key = this.key.getValue(executer);
		try {
			Entity entity = executer.getWorld().getEntity(key);
			if (entity instanceof GiantWarrior) ((GiantWarrior) entity).setActive(true);
		} catch (NoSuchElementException ex) {
		}
		if (then != null) then.run();
	}
}
