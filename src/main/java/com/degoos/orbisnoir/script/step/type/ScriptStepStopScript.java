package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;

public class ScriptStepStopScript extends ScriptStep {

	private StringValueParser key;

	public ScriptStepStopScript(List<ScriptStep> forcedSteps, Script follow, StringValueParser key) {
		super(forcedSteps, follow);
		this.key = key;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		String key = this.key.getValue(executer);
		executer.getWorld().getScriptContainer().stopIf(target -> target.getScript().getName().equals(key));
		if (then != null) then.run();
	}
}
