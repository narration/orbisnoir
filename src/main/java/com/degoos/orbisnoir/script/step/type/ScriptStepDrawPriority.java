package com.degoos.orbisnoir.script.step.type;

import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Text;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.FloatValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;
import java.util.NoSuchElementException;

public class ScriptStepDrawPriority extends ScriptStep {

	private StringValueParser key;
	private FloatValueParser priority;

	public ScriptStepDrawPriority(List<ScriptStep> forcedSteps, Script follow, StringValueParser key,
	                              FloatValueParser priority) {
		super(forcedSteps, follow);
		this.key = key;
		this.priority = priority;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		String key = this.key.getValue(executer);
		float priority = this.priority.getValue(executer);
		try {
			Entity entity = executer.getWorld().getEntity(key);
			entity.getRectangle().getPosition().z = priority;
			entity.getRectangle().setRequiresRecalculation(true);
		} catch (NoSuchElementException ex) {
			executer.getWorld().getGObject(key).ifPresent(target -> {
				if (target instanceof Rectangle) {
					((Rectangle) target).getPosition().z = priority;
					((Rectangle) target).setRequiresRecalculation(true);
				} else if (target instanceof Text) {
					((Text) target).getPosition().z = priority;
					((Text) target).setRequiresRecalculation(true);
				}
			});
		}
		if (then != null) then.run();
	}
}
