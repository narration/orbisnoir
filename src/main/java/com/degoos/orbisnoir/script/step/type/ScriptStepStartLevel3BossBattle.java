package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.game.entity.level3.boss.Level3Boss;
import com.degoos.orbisnoir.game.entity.level4.boss.Level4Boss;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;

import java.util.List;
import java.util.NoSuchElementException;

public class ScriptStepStartLevel3BossBattle extends ScriptStep {


	public ScriptStepStartLevel3BossBattle(List<ScriptStep> forcedSteps, Script follow) {
		super(forcedSteps, follow);
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {

		try {
			Entity entity = executer.getWorld().getEntity("boss");
			if (entity instanceof Level3Boss)
				((Level3Boss) entity).setActive(true);
		} catch (NoSuchElementException ignore) {
		}
		if (then != null) then.run();
	}
}
