package com.degoos.orbisnoir.script.step.type;

import com.degoos.graphicengine2.object.GObject;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.object.texture.TextureAbsImpl;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.IntValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.texture.TextureManager;

import java.util.List;
import java.util.NoSuchElementException;

public class ScriptStepTexture extends ScriptStep {

	private StringValueParser key, option, value;
	private IntValueParser paletteIndex;

	public ScriptStepTexture(List<ScriptStep> forcedSteps, Script follow, StringValueParser key,
	                         StringValueParser option, StringValueParser value, IntValueParser paletteIndex) {
		super(forcedSteps, follow);
		this.key = key;
		this.option = option;
		this.value = value;
		this.paletteIndex = paletteIndex;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		String key = this.key.getValue(executer);
		try {
			Entity entity = executer.getWorld().getEntity(key);
			ITexture texture = getImage(executer);
			executer.getWorld().addGObject((TextureAbsImpl) texture);
			entity.getRectangle().setTexture(texture);
		} catch (NoSuchElementException ex) {
			GObject object = executer.getWorld().getGObject(key).orElse(null);
			if (object instanceof Rectangle) {
				ITexture texture = getImage(executer);
				executer.getWorld().addGObject((TextureAbsImpl) texture);
				((Rectangle) object).setTexture(texture);
			}
		}
		if (then != null) then.run();
	}

	public ITexture getImage(ScriptExecuter executer) {
		try {
			switch (option.getValue(executer)) {
				case "null":
					return null;
				case "palette":
					return PaletteManager.loadPalette(value.getValue(executer)).getTexture(paletteIndex.getValue(executer));
				case "palette-animation":
					return PaletteManager.loadPalette(value.getValue(executer)).getAnimation(paletteIndex.getValue(executer));
				case "image":
					return TextureManager.loadImage(value.getValue(executer));
				case "animation":
					return TextureManager.loadAnimation(value.getValue(executer));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new NullPointerException();
		}
		return null;
	}

}
