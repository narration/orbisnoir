package com.degoos.orbisnoir.script.step.type;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.sound.SoundSource;
import com.degoos.orbisnoir.handler.VolumeHandler;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.IntValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;
import java.util.Set;

public class ScriptStepStopSound extends ScriptStep {

	private StringValueParser key;
	private IntValueParser millis;

	public ScriptStepStopSound(List<ScriptStep> forcedSteps, Script follow, StringValueParser key, IntValueParser millis) {
		super(forcedSteps, follow);
		this.key = key;
		this.millis = millis;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		boolean executeThen = true;
		String key = this.key.getValue(executer);
		int millis = this.millis.getValue(executer);

		if (millis == 0) Engine.getSoundManager().cleanUpSoundSources(key);
		else {
			Set<SoundSource> sources = Engine.getSoundManager().getSourcesUnsafe(key);
			if (sources == null || sources.isEmpty()) return;
			for (SoundSource soundSource : sources) {
				executer.getWorld().addGObject(new VolumeHandler(executer, soundSource, 0, millis * 1000000L,
						true, executeThen ? then : null));
				executeThen = false;
			}
		}
		if (executeThen && then != null) then.run();
	}
}
