package com.degoos.orbisnoir.script.step.type;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.sound.SoundSource;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;

public class ScriptStepContinueSound extends ScriptStep {

	private StringValueParser key;

	public ScriptStepContinueSound(List<ScriptStep> forcedSteps, Script follow, StringValueParser key) {
		super(forcedSteps, follow);
		this.key = key;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		Engine.getSoundManager().getSource(key.getValue(executer)).ifPresent(SoundSource::play);
		if (then != null) then.run();
	}
}
