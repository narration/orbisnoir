package com.degoos.orbisnoir.script.step.type;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.sound.SoundSource;
import com.degoos.orbisnoir.handler.VolumeHandler;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;
import com.degoos.orbisnoir.script.value.parser.FloatValueParser;
import com.degoos.orbisnoir.script.value.parser.LongValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import org.joml.Vector3f;

import java.util.List;
import java.util.Optional;

public class ScriptStepPlaySound extends ScriptStep {

	private StringValueParser key;
	private StringValueParser url;
	private FloatValueParser volume, pitch, balance;
	private BooleanValueParser loop;
	private BooleanValueParser playIfPresent;
	private LongValueParser millis;

	public ScriptStepPlaySound(List<ScriptStep> forcedSteps, Script follow,
							   StringValueParser key, StringValueParser url,
							   FloatValueParser volume, FloatValueParser pitch,
							   FloatValueParser balance, BooleanValueParser loop,
							   BooleanValueParser playIfPresent, LongValueParser millis) {
		super(forcedSteps, follow);
		this.key = key;
		this.url = url;
		this.volume = volume;
		this.pitch = pitch;
		this.balance = balance;
		this.loop = loop;
		this.playIfPresent = playIfPresent;
		this.millis = millis;
	}

	public ScriptStepPlaySound(List<ScriptStep> forcedSteps, Script follow,
							   String key, String url, float volume, float pitch, float balance,
							   boolean loop, boolean playIfPresent, long millis) {
		super(forcedSteps, follow);
		this.key = new StringValueParser(key);
		this.url = new StringValueParser(url);
		this.volume = new FloatValueParser(volume);
		this.pitch = new FloatValueParser(pitch);
		this.balance = new FloatValueParser(balance);
		this.loop = new BooleanValueParser(loop);
		this.playIfPresent = new BooleanValueParser(playIfPresent);
		this.millis = new LongValueParser(millis);
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		String key = this.key.getValue(executer);
		Optional<SoundSource> optional = Engine.getSoundManager().getSource(key);
		if (optional.isPresent()) {
			if (!optional.get().isPlaying()) {
				optional.get().cleanup();
				optional = Optional.empty();
			}
		}

		if (playIfPresent.getValue(executer) || !optional.isPresent() || !optional.get().isPlaying()) {
			long millis = this.millis.getValue(executer);
			if (millis <= 0) {
				createSoundSource(executer, key, volume.getValue(executer));
				if (then != null) then.run();
			} else {
				SoundSource source = createSoundSource(executer, key, 0);
				executer.getWorld().addGObject(new VolumeHandler(executer, source, volume.getValue(executer),
						millis * 1000000, false, then));
			}
		} else if (then != null) then.run();
	}

	private SoundSource createSoundSource(ScriptExecuter executer, String key, float volume) {
		SoundSource source = Engine.getSoundManager().createSoundSource(url.getValue(executer), key, loop.getValue(executer), true);
		source.setGain(volume);
		source.setPitch(pitch.getValue(executer));
		source.setPosition(new Vector3f(balance.getValue(executer), 0, 0));
		source.play();
		return source;
	}
}
