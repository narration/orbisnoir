package com.degoos.orbisnoir.script.step.type;

import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.orbisnoir.gui.text.Textbox;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;
import com.degoos.orbisnoir.script.value.parser.IntValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.texture.PaletteManager;

import java.util.List;

public class ScriptStepText extends ScriptStep {

	private StringValueParser key, text, paletteName;
	private BooleanValueParser executeMoveAnimation, canBeClosed;
	private IntValueParser paletteIndex;

	public ScriptStepText(List<ScriptStep> forcedSteps, Script follow, StringValueParser key, StringValueParser text, StringValueParser paletteName,
	                      BooleanValueParser executeMoveAnimation, BooleanValueParser canBeClosed, IntValueParser paletteIndex) {
		super(forcedSteps, follow);
		this.key = key;
		this.text = text;
		this.paletteName = paletteName;
		this.executeMoveAnimation = executeMoveAnimation;
		this.canBeClosed = canBeClosed;
		this.paletteIndex = paletteIndex;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		Textbox textbox = new Textbox(executer.getWorld(), text.getValue(executer), executeMoveAnimation.getValue(executer), getImage(executer));
		textbox.setThen(then);
		textbox.setKey(key == null ? null : key.getValue(executer));
		textbox.setCanBeClosed(canBeClosed.getValue(executer));
		executer.getWorld().addGObject(textbox);
	}

	public ITexture getImage(ScriptExecuter executer) {
		try {
			return PaletteManager.loadPalette(paletteName.getValue(executer))
					.getTexture(paletteIndex.getValue(executer));
		} catch (Exception ex) {
			return null;
		}
	}
}
