package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.game.entity.level1.boss.Level1Boss;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import org.joml.Vector2f;
import org.jooq.tools.json.ParseException;

import java.io.IOException;
import java.util.List;

public class ScriptStepSpawnLevel1Boss extends ScriptStep {


	public ScriptStepSpawnLevel1Boss(List<ScriptStep> forcedSteps, Script follow) {
		super(forcedSteps, follow);
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		try {
			Level1Boss boss = new Level1Boss(executer.getWorld(),
					executer.getWorld().getPlayer(), new Vector2f(17, 15), 10);
			boss.setPosition(new Vector2f(17, 21));
			boss.getRectangle().setTexture(boss.getDirectionablePalette().getPalette().getTexture(9, 1));
			executer.getWorld().registerEntity(boss);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (then != null) then.run();
	}
}
