package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;

import java.util.List;

public class ScriptStepWhile extends ScriptStep {

	private Script whileScript;
	private BooleanValueParser condition;

	public ScriptStepWhile(List<ScriptStep> forcedSteps, Script follow, BooleanValueParser condition) {
		super(forcedSteps, follow);
		whileScript = null;
		this.condition = condition;
	}

	public Script getWhileScript() {
		return whileScript;
	}

	public void setWhileScript(Script whileScript) {
		this.whileScript = whileScript;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		if (condition.getValue(executer)) {
			followScript(executer, whileScript, () -> wait(executer.getWorld(), 1, () -> run(executer, then)));
		} else if (then != null) then.run();
	}
}
