package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;

public class ScriptStepRun extends ScriptStep {

	private Runnable runnable;

	public ScriptStepRun(Runnable runnable) {
		super(null, null);
		this.runnable = runnable;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		runnable.run();
		then.run();
	}
}
