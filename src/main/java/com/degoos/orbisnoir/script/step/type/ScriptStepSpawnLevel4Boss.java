package com.degoos.orbisnoir.script.step.type;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.sound.SoundSource;
import com.degoos.orbisnoir.game.entity.level4.boss.Level4Boss;
import com.degoos.orbisnoir.handler.VolumeHandler;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import org.joml.Vector2f;

import java.util.List;

public class ScriptStepSpawnLevel4Boss extends ScriptStep {


	public ScriptStepSpawnLevel4Boss(List<ScriptStep> forcedSteps, Script follow) {
		super(forcedSteps, follow);
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {

		SoundSource soundSource = Engine.getSoundManager().createSoundSource("music/level_4_boss.ogg", "background", true, false);
		soundSource.play();
		soundSource.setGain(0);

		Level4Boss boss = new Level4Boss(executer.getWorld(), new Vector2f(17, 25), soundSource);
		executer.getWorld().registerEntity(boss);

		executer.getWorld().addGObject(new VolumeHandler(null, soundSource, 0.5f, 6 * 1000000000L, false, null));
		boss.appear();
		if (then != null) then.run();
	}
}
