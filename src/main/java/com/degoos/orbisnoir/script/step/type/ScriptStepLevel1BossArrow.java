package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.game.entity.level1.boss.Level1Boss;
import com.degoos.orbisnoir.game.entity.level1.boss.Level1BossArrow;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

import java.util.List;
import java.util.NoSuchElementException;

public class ScriptStepLevel1BossArrow extends ScriptStep {


	public ScriptStepLevel1BossArrow(List<ScriptStep> forcedSteps, Script follow) {
		super(forcedSteps, follow);
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		World world = executer.getWorld();

		try {
			Entity entity = world.getEntity("boss");
			if (entity instanceof Level1Boss) {
				world.registerEntity(new Level1BossArrow(world, new Vector2f(entity.getPosition()).add(0.5f, 0),
						new Vector2f(0, -10 / 1000000000f), (Level1Boss) entity));
			}
		} catch (NoSuchElementException ignore) {
		}
		if (then != null) then.run();
	}
}
