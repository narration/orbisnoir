package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.entity.MovableEntity;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;
import java.util.NoSuchElementException;

public class ScriptStepControlEntity extends ScriptStep {

	private StringValueParser key;

	public ScriptStepControlEntity(List<ScriptStep> forcedSteps, Script follow, StringValueParser key) {
		super(forcedSteps, follow);
		this.key = key;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		try {
			Entity entity = executer.getWorld().getEntity(key.getValue(executer));
			if (entity instanceof MovableEntity)
				executer.getWorld().getPlayer().setControlling((MovableEntity) entity);
			else executer.getWorld().getPlayer().setControlling(null);
		} catch (NoSuchElementException ex) {
			executer.getWorld().getPlayer().setControlling(null);
		}
		if (then != null) then.run();
	}
}
