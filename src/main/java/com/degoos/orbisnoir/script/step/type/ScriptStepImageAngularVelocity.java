package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.entity.image.ImageEntity;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.DoubleValueParser;
import com.degoos.orbisnoir.script.value.parser.FloatValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;

public class ScriptStepImageAngularVelocity extends ScriptStep {

	private StringValueParser key;
	private FloatValueParser angularVelocity;

	public ScriptStepImageAngularVelocity(List<ScriptStep> forcedSteps, Script follow, StringValueParser key,
	                                      FloatValueParser angularVelocity) {
		super(forcedSteps, follow);
		this.key = key;
		this.angularVelocity = angularVelocity;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		String key = this.key.getValue(executer);
		float velocity = angularVelocity.getValue(executer);
		executer.getWorld().getEntities().values()
				.stream().filter(target -> target instanceof ImageEntity && key.equals(target.getName()))
				.forEach(target -> ((ImageEntity) target).setAngularVelocity(velocity));
		if (then != null) then.run();
	}
}
