package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.ColorValueParser;

import java.util.List;

public class ScriptStepBackground extends ScriptStep {

	private ColorValueParser color;

	public ScriptStepBackground(List<ScriptStep> forcedSteps, Script follow, ColorValueParser color) {
		super(forcedSteps, follow);
		this.color = color;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		executer.getWorld().setBackground(color.getValue(executer));
		if (then != null) then.run();
	}
}
