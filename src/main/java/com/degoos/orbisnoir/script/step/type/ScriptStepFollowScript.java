package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.ScriptManager;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;

public class ScriptStepFollowScript extends ScriptStep {

	private StringValueParser key;

	public ScriptStepFollowScript(List<ScriptStep> forcedSteps, Script follow, StringValueParser key) {
		super(forcedSteps, follow);
		this.key = key;
	}

	public ScriptStepFollowScript(String script) {
		super(null, null);
		this.key = new StringValueParser(script);
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		Script script = ScriptManager.getScriptUnsafe(key.getValue(executer));
		if (script != null) {
			followScript(executer, script, then);
		} else if (then != null) then.run();
	}
}
