package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;

import java.util.List;

public class ScriptStepIf extends ScriptStep {

	private Script ifScript, elseScript;
	private BooleanValueParser condition;

	public ScriptStepIf(List<ScriptStep> forcedSteps, Script follow, BooleanValueParser condition) {
		super(forcedSteps, follow);
		ifScript = elseScript = null;
		this.condition = condition;
	}

	public Script getIfScript() {
		return ifScript;
	}

	public void setIfScript(Script ifScript) {
		this.ifScript = ifScript;
	}

	public Script getElseScript() {
		return elseScript;
	}

	public void setElseScript(Script elseScript) {
		this.elseScript = elseScript;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		if (condition.getValue(executer)) {
			followScript(executer, ifScript, then);
		} else if (elseScript != null) {
			followScript(executer, elseScript, then);
		} else if (then != null) then.run();
	}
}
