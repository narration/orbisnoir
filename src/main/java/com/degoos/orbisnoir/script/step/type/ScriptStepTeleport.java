package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.parser.Vector2ValueParser;

import java.util.List;
import java.util.NoSuchElementException;

public class ScriptStepTeleport extends ScriptStep {

	private StringValueParser key;
	private BooleanValueParser relative;
	private Vector2ValueParser position;

	public ScriptStepTeleport(List<ScriptStep> forcedSteps, Script follow, StringValueParser key, BooleanValueParser relative, Vector2ValueParser position) {
		super(forcedSteps, follow);
		this.key = key;
		this.relative = relative;
		this.position = position;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		try {
			Entity entity = executer.getWorld().getEntity(key.getValue(executer));
			entity.setPosition(relative.getValue(executer) ?
					entity.getPosition().add(position.getValue(executer)) : position.getValue(executer));
		} catch (NoSuchElementException ignore) {
		}
		if (then != null) then.run();
	}
}
