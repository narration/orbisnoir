package com.degoos.orbisnoir.script.step.type;

import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.image.ImageEntity;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.ScriptManager;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.*;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.texture.TextureManager;
import org.joml.Vector2f;

import java.util.List;

public class ScriptStepEntityImage extends ScriptStep {

	private StringValueParser key;
	private Vector2ValueParser position, minSize, maxSize, minCollisionBox, maxCollisionBox;
	private StringValueParser script;
	private ColorValueParser color;
	private FloatValueParser opacity;
	private FloatValueParser rotation, angularVelocity;
	private StringValueParser collisionType, drawPriorityType;
	private StringValueParser imageType, image;
	private IntValueParser paletteIndex;

	public ScriptStepEntityImage(List<ScriptStep> forcedSteps, Script follow,
	                             StringValueParser key, Vector2ValueParser position,
	                             Vector2ValueParser minSize, Vector2ValueParser maxSize,
	                             Vector2ValueParser minCollisionBox, Vector2ValueParser maxCollisionBox,
	                             StringValueParser script, ColorValueParser color,
	                             FloatValueParser opacity, FloatValueParser rotation,
	                             FloatValueParser angularVelocity, StringValueParser collisionType,
	                             StringValueParser drawPriorityType, StringValueParser imageType,
	                             StringValueParser image, IntValueParser paletteIndex) {
		super(forcedSteps, follow);
		this.key = key;
		this.position = position;
		this.minSize = minSize;
		this.maxSize = maxSize;
		this.minCollisionBox = minCollisionBox;
		this.maxCollisionBox = maxCollisionBox;
		this.script = script;
		this.color = color;
		this.opacity = opacity;
		this.rotation = rotation;
		this.angularVelocity = angularVelocity;
		this.collisionType = collisionType;
		this.drawPriorityType = drawPriorityType;
		this.imageType = imageType;
		this.image = image;
		this.paletteIndex = paletteIndex;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		ImageEntity image = new ImageEntity(executer.getWorld(),
				position.getValue(executer),
				color.getValue(executer),
				opacity.getValue(executer),
				new Area(minSize.getValue(executer), maxSize.getValue(executer)),
				drawPriorityType.getValue(executer, EnumEntityDrawPriority.class, EnumEntityDrawPriority.RELATIVE),
				new CollisionBox(new Vector2f(0), minCollisionBox.getValue(executer), maxCollisionBox.getValue(executer)),
				collisionType.getValue(executer, EnumEntityCollision.class, EnumEntityCollision.PASS),
				null,
				ScriptManager.getScriptUnsafe(script.getValue(executer)),
				getImage(executer),
				rotation.getValue(executer),
				angularVelocity.getValue(executer));
		image.setName(key.getValue(executer));
		executer.getWorld().registerEntity(image);
		if (then != null) then.run();
	}

	private ITexture getImage(ScriptExecuter executer) {
		try {
			switch (imageType.getValue(executer)) {
				case "null":
					return null;
				case "palette":
					return PaletteManager.loadPalette(image.getValue(executer)).getTexture(paletteIndex.getValue(executer));
				case "palette-animation":
					return PaletteManager.loadPalette(image.getValue(executer)).getAnimation(paletteIndex.getValue(executer));
				case "image":
					return TextureManager.loadImage(image.getValue(executer));
				case "animation":
					return TextureManager.loadAnimation(image.getValue(executer));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
		return null;
	}
}
