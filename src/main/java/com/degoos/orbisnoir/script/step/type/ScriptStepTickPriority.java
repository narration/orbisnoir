package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.DoubleValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;

public class ScriptStepTickPriority extends ScriptStep {

	private StringValueParser key;
	private DoubleValueParser priority;

	public ScriptStepTickPriority(List<ScriptStep> forcedSteps, Script follow, StringValueParser key,
	                              DoubleValueParser priority) {
		super(forcedSteps, follow);
		this.key = key;
		this.priority = priority;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		executer.getWorld().getGObject(key.getValue(executer))
				.ifPresent(target -> target.setTickPriority(priority.getValue(executer)));
		if (then != null) then.run();
	}
}
