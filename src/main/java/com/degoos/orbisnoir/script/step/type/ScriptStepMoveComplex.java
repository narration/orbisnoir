package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.handler.ComplexMoveHandler;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;
import com.degoos.orbisnoir.script.value.parser.LongValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.parser.Vector2ValueParser;
import org.joml.Vector2f;

import java.util.List;
import java.util.NoSuchElementException;

public class ScriptStepMoveComplex extends ScriptStep {

	private StringValueParser key;
	private BooleanValueParser relative;
	private Vector2ValueParser to;
	private LongValueParser millis;

	public ScriptStepMoveComplex(List<ScriptStep> forcedSteps, Script follow, StringValueParser key, BooleanValueParser relative, Vector2ValueParser to, LongValueParser millis) {
		super(forcedSteps, follow);
		this.key = key;
		this.relative = relative;
		this.to = to;
		this.millis = millis;
	}

	public ScriptStepMoveComplex(List<ScriptStep> forcedSteps, Script follow,
	                             String key, Boolean relative, Vector2f to, long millis) {
		super(forcedSteps, follow);
		this.key = new StringValueParser(key);
		this.relative = new BooleanValueParser(relative);
		this.to = new Vector2ValueParser(to.x, to.y);
		this.millis = new LongValueParser(millis);
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		try {
			Entity entity = executer.getWorld().getEntity(key.getValue(executer));
			if (entity == null) {
				if (then != null) then.run();
				return;
			}
			executer.getWorld().addGObject(new ComplexMoveHandler(entity, relative.getValue(executer) ?
							new Vector2f(entity.getPosition()).add(to.getValue(executer)) : to.getValue(executer),
					millis.getValue(executer),
					then));
		} catch (NoSuchElementException ex) {
			if (then != null) then.run();
		}
	}
}
