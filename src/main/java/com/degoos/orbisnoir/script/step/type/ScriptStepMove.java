package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.entity.MovableEntity;
import com.degoos.orbisnoir.enums.EnumDirection;
import com.degoos.orbisnoir.handler.MoveHandler;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;
import java.util.Optional;

public class ScriptStepMove extends ScriptStep {

	private StringValueParser key;
	private StringValueParser direction;
	private BooleanValueParser run, collide;

	public ScriptStepMove(List<ScriptStep> forcedSteps, Script follow, StringValueParser key, StringValueParser direction,
	                      BooleanValueParser run, BooleanValueParser collide) {
		super(forcedSteps, follow);
		this.key = key;
		this.direction = direction;
		this.run = run;
		this.collide = collide;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		String key = this.key.getValue(executer);
		Optional<MovableEntity> npc = executer.getWorld().getEntities().values()
				.stream().filter(target -> target instanceof MovableEntity && key.equals(target.getName()))
				.map(target -> (MovableEntity) target).findAny();
		if (npc.isPresent())
			executer.getWorld().addGObject(new MoveHandler(npc.get(),
					direction.getValue(executer, EnumDirection.class),
					run.getValue(executer),
					collide.getValue(executer),
					then));
		else if (then != null) then.run();
	}
}
