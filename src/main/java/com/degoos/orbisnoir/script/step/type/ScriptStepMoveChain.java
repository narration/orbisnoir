package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.entity.MovableEntity;
import com.degoos.orbisnoir.enums.EnumDirection;
import com.degoos.orbisnoir.handler.MoveHandler;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;
import java.util.Optional;

public class ScriptStepMoveChain extends ScriptStep {

	private StringValueParser key;
	private BooleanValueParser run, collide;
	private List<StringValueParser> directions;

	public ScriptStepMoveChain(List<ScriptStep> forcedSteps, Script follow, StringValueParser key, BooleanValueParser run,
	                           BooleanValueParser collide, List<StringValueParser> directions) {
		super(forcedSteps, follow);
		this.key = key;
		this.run = run;
		this.collide = collide;
		this.directions = directions;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		String key = this.key.getValue(executer);
		Optional<MovableEntity> npc = executer.getWorld().getEntities().values()
				.stream().filter(target -> target instanceof MovableEntity && key.equals(target.getName()))
				.map(target -> (MovableEntity) target).findAny();
		if (npc.isPresent()) {
			Runnable runnable = then;
			for (int i = directions.size() - 1; i >= 0; i--) {
				int finalI = i;
				Runnable finalRunnable = runnable;
				runnable = () -> executer.getWorld().addGObject(new MoveHandler(npc.get(),
						directions.get(finalI).getValue(executer, EnumDirection.class),
						run.getValue(executer),
						collide.getValue(executer),
						finalRunnable));
			}
			runnable.run();
		} else if (then != null) then.run();
	}
}
