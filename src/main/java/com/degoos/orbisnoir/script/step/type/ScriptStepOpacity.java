package com.degoos.orbisnoir.script.step.type;

import com.degoos.graphicengine2.object.GObject;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Text;
import com.degoos.orbisnoir.handler.OpacityHandler;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.FloatValueParser;
import com.degoos.orbisnoir.script.value.parser.LongValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

public class ScriptStepOpacity extends ScriptStep {

	private StringValueParser key;
	private FloatValueParser opacity;
	private LongValueParser millis;

	public ScriptStepOpacity(List<ScriptStep> forcedSteps, Script follow, StringValueParser key, FloatValueParser opacity, LongValueParser millis) {
		super(forcedSteps, follow);
		this.key = key;
		this.opacity = opacity;
		this.millis = millis;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		String key = this.key.getValue(executer).trim();
		long millis = this.millis.getValue(executer);
		try {
			Rectangle rectangle = executer.getWorld().getEntity(key).getRectangle();
			if (millis == 0) {
				rectangle.setOpacity(opacity.getValue(executer));
				if (then != null) then.run();
			} else executer.getWorld().addGObject(new OpacityHandler(executer,
					rectangle, millis * 1000000, opacity.getValue(executer), then));
		} catch (NoSuchElementException ex) {
			Optional<GObject> object = executer.getWorld().getGObject(key);
			object.ifPresent(target -> {
				if (millis == 0) {
					if (target instanceof Rectangle) {
						((Rectangle) target).setOpacity(opacity.getValue(executer));
					} else if (target instanceof Text) {
						((Text) target).setOpacity(opacity.getValue(executer));
					}
					if (then != null) then.run();
				} else {
					executer.getWorld().addGObject(new OpacityHandler(executer,
							target, millis * 1000000, opacity.getValue(executer), then));
				}
			});
			if(!object.isPresent()) {
				if (then != null) then.run();
			}
		}
	}
}
