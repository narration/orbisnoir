package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.game.entity.level1.boss.Level1Boss;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;

import java.util.List;
import java.util.NoSuchElementException;

public class ScriptStepStartLevel1BossBattle extends ScriptStep {


	public ScriptStepStartLevel1BossBattle(List<ScriptStep> forcedSteps, Script follow) {
		super(forcedSteps, follow);
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {

		try {
			Entity entity = executer.getWorld().getEntity("boss");
			if (entity instanceof Level1Boss)
				((Level1Boss) entity).startBattle();
		} catch (NoSuchElementException ignore) {
		}
		if (then != null) then.run();
	}
}
