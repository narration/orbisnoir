package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;
import com.degoos.orbisnoir.script.value.parser.LongValueParser;
import com.degoos.orbisnoir.script.value.parser.Vector2ValueParser;
import org.joml.Vector3f;

import java.util.List;

public class ScriptStepMoveCamera extends ScriptStep {

	private BooleanValueParser relative;
	private Vector2ValueParser to;
	private LongValueParser millis;

	public ScriptStepMoveCamera(List<ScriptStep> forcedSteps, Script follow, BooleanValueParser relative, Vector2ValueParser to, LongValueParser millis) {
		super(forcedSteps, follow);
		this.relative = relative;
		this.to = to;
		this.millis = millis;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		Vector3f pos = new Vector3f(to.getValue(executer), 0);
		if (millis.getValue(executer) <= 0) {
			executer.getWorld().getCamera().setPosition(relative.getValue(executer) ?
					executer.getWorld().getCamera().getPosition().add(pos) : pos);
			if (then != null) then.run();
		} else {
			executer.getWorld().getCamera().move(
					relative.getValue(executer) ? pos.add(executer.getWorld().getCamera().getPosition()) : pos,
					millis.getValue(executer) * 1000000, then);
		}
	}
}
