package com.degoos.orbisnoir.script.step.type;

import com.degoos.graphicengine2.object.GObject;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;
import java.util.NoSuchElementException;

public class ScriptStepDelete extends ScriptStep {

	private StringValueParser key;

	public ScriptStepDelete(List<ScriptStep> forcedSteps, Script follow, StringValueParser key) {
		super(forcedSteps, follow);
		this.key = key;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		String key = this.key.getValue(executer);
		try {
			executer.getWorld().getEntity(key).delete();
		} catch (NoSuchElementException ex) {
			executer.getWorld().getGObject(key).ifPresent(GObject::delete);
		}
		if (then != null) then.run();
	}
}
