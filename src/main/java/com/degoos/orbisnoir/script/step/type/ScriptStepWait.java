package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.LongValueParser;

import java.util.ArrayList;
import java.util.List;

public class ScriptStepWait extends ScriptStep {

	private LongValueParser millis;

	public ScriptStepWait(List<ScriptStep> forcedSteps, Script follow, LongValueParser millis) {
		super(forcedSteps, follow);
		this.millis = millis;
	}

	public ScriptStepWait(long millis) {
		super(new ArrayList<>(), null);
		this.millis = new LongValueParser(millis);
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		wait(executer.getWorld(), millis.getValue(executer), then);
	}
}
