package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.IntValueParser;

import java.util.List;

public class ScriptStepSurfBoost extends ScriptStep {

	private IntValueParser millis;

	public ScriptStepSurfBoost(List<ScriptStep> forcedSteps, Script follow, IntValueParser millis) {
		super(forcedSteps, follow);
		this.millis = millis;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		Player player = executer.getWorld().getPlayer();
		player.setSurfBoost(millis.getValue(executer) * 1000000);
		if (then != null) then.run();
	}
}
