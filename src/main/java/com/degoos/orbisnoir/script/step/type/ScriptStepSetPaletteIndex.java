package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.IntValueParser;
import com.degoos.orbisnoir.script.value.parser.Vector2ValueParser;
import com.degoos.orbisnoir.world.Box;
import org.joml.Vector2f;
import org.joml.Vector2i;

import java.util.List;

public class ScriptStepSetPaletteIndex extends ScriptStep {


	private Vector2ValueParser block;
	private IntValueParser index, yIndex;

	public ScriptStepSetPaletteIndex(List<ScriptStep> forcedSteps, Script follow, Vector2ValueParser block,
	                                 IntValueParser index, IntValueParser yIndex) {
		super(forcedSteps, follow);
		this.block = block;
		this.index = index;
		this.yIndex = yIndex;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		Vector2f pos = block.getValue(executer);
		try {
			Box box = executer.getWorld().getBox(new Vector2i((int) Math.floor(pos.x), (int) Math.floor(pos.y)));
			if (yIndex == null) {
				int size = box.getChunk().getPalette().getPaletteSize().x;
				int index = this.index.getValue(executer);
				box.setPaletteTexture(new Vector2i(index % size, index / size));
			} else box.setPaletteTexture(new Vector2i(index.getValue(executer), yIndex.getValue(executer)));
		} catch (Exception ignore) {
		}
		if (then != null) then.run();
	}
}
