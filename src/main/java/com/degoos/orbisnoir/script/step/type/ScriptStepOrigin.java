package com.degoos.orbisnoir.script.step.type;

import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Text;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.BooleanValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.parser.Vector2ValueParser;
import org.joml.Vector3f;

import java.util.List;

public class ScriptStepOrigin extends ScriptStep {

	private StringValueParser key;
	private BooleanValueParser relative;
	private Vector2ValueParser origin;

	public ScriptStepOrigin(List<ScriptStep> forcedSteps, Script follow, StringValueParser key, BooleanValueParser relative, Vector2ValueParser origin) {
		super(forcedSteps, follow);
		this.key = key;
		this.relative = relative;
		this.origin = origin;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		Vector3f value = new Vector3f(origin.getValue(executer), 0);
		executer.getWorld().getGObject(key.getValue(executer)).ifPresent(target -> {
			if (target instanceof Rectangle)
				((Rectangle) target).setPosition(relative.getValue(executer) ?
						((Rectangle) target).getPosition().add(value) : value);
			if (target instanceof Text)
				((Text) target).setPosition(relative.getValue(executer) ?
						((Text) target).getPosition().add(value) : value);
		});
		if (then != null) then.run();
	}
}
