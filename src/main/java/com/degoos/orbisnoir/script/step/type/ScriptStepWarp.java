package com.degoos.orbisnoir.script.step.type;

import com.degoos.graphicengine2.Engine;
import com.degoos.orbisnoir.enums.EnumDirection;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.script.value.parser.Vector2ValueParser;
import com.degoos.orbisnoir.world.World;
import com.degoos.orbisnoir.world.WorldManager;

import java.util.List;

public class ScriptStepWarp extends ScriptStep {

	private StringValueParser room, direction;
	private Vector2ValueParser position;

	public ScriptStepWarp(List<ScriptStep> forcedSteps, Script follow, StringValueParser room, StringValueParser direction, Vector2ValueParser position) {
		super(forcedSteps, follow);
		this.room = room;
		this.direction = direction;
		this.position = position;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		try {
			World world = WorldManager.loadWorld(room.getValue(executer), position.getValue(executer));
			world.getPlayer().setDirection(direction.getValue(executer, EnumDirection.class));
			if (Engine.getRoom() instanceof World)
				((World) Engine.getRoom()).getScriptContainer().stopAllScripts();


			if (executer.getWorld().getGUI().getDiaryButton().isActive())
				world.getGUI().getDiaryButton().appear();
			Engine.setRoom(world);
			world.runInitScript();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (then != null) then.run();
	}
}
