package com.degoos.orbisnoir.script.step;

import com.degoos.orbisnoir.handler.Waiter;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.world.World;

import java.util.ArrayList;
import java.util.List;

public abstract class ScriptStep {

	private List<ScriptStep> forcedSteps;
	private Script follow;

	public ScriptStep(List<ScriptStep> forcedSteps, Script follow) {
		this.forcedSteps = forcedSteps == null ? new ArrayList<>() : forcedSteps;
		this.follow = follow;
	}

	public List<ScriptStep> getForcedSteps() {
		return forcedSteps;
	}

	public Script getFollow() {
		return follow;
	}

	public void setFollow(Script follow) {
		this.follow = follow;
	}

	public void run0(ScriptExecuter executer, Runnable then) {
		run(executer, then);
		forcedSteps.forEach(target -> target.run0(executer, null));
		if (follow != null) new ScriptExecuter(follow, executer.getWorld(), executer).next();
	}

	public void followScript(ScriptExecuter executer, Script script, Runnable then) {
		ScriptExecuter ex = new ScriptExecuter(script, executer.getWorld(), executer);
		ex.setOnFinish(then == null ? null : e -> then.run());
		ex.next();
	}

	public abstract void run(ScriptExecuter executer, Runnable then);

	public void wait(World world, long millis, Runnable then) {
		world.addGObject(new Waiter(millis, then == null ? null : waiter -> then.run()));
	}
}
