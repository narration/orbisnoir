package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;

public class ScriptStepRemoveHotbarElement extends ScriptStep {

	private StringValueParser key;

	public ScriptStepRemoveHotbarElement(List<ScriptStep> forcedSteps, Script follow, StringValueParser key) {
		super(forcedSteps, follow);
		this.key = key;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		executer.getWorld().getPlayer().getHotbar().removeElement(key.getValue(executer));
		if (then != null) then.run();
	}
}
