package com.degoos.orbisnoir.script.step.type;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Text;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.ColorValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;
import java.util.NoSuchElementException;

public class ScriptStepColor extends ScriptStep {

	private StringValueParser key;
	private ColorValueParser color;

	public ScriptStepColor(List<ScriptStep> forcedSteps, Script follow, StringValueParser key,
	                       ColorValueParser color) {
		super(forcedSteps, follow);
		this.key = key;
		this.color = color;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		String key = this.key.getValue(executer);
		GColor color = this.color.getValue(executer);

		try {
			Entity entity = executer.getWorld().getEntity(key);
			entity.getRectangle().setColor(color);
		} catch (NoSuchElementException ex) {
			executer.getWorld().getGObject(key).ifPresent(target -> {
				if (target instanceof Rectangle)
					((Rectangle) target).setColor(color);
				else if (target instanceof Text)
					((Text) target).setColor(color);

			});
		}
		if (then != null) then.run();
	}
}
