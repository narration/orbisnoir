package com.degoos.orbisnoir.script.step.type;

import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.*;
import org.joml.Vector3f;

import java.util.List;

public class ScriptStepRectangle extends ScriptStep {

	private StringValueParser key;
	private BooleanValueParser visible;
	private FloatValueParser drawPriority, tickPriority;
	private Vector2ValueParser origin, min, max;
	private ColorValueParser color;
	private FloatValueParser opacity, rotation;

	public ScriptStepRectangle(List<ScriptStep> forcedSteps, Script follow,
	                           StringValueParser key, BooleanValueParser visible,
	                           FloatValueParser drawPriority, FloatValueParser tickPriority,
	                           Vector2ValueParser origin, Vector2ValueParser min,
	                           Vector2ValueParser max, ColorValueParser color,
	                           FloatValueParser opacity, FloatValueParser rotation) {
		super(forcedSteps, follow);
		this.key = key;
		this.visible = visible;
		this.drawPriority = drawPriority;
		this.tickPriority = tickPriority;
		this.origin = origin;
		this.min = min;
		this.max = max;
		this.color = color;
		this.opacity = opacity;
		this.rotation = rotation;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		Rectangle rectangle = new Rectangle(visible.getValue(executer),
				tickPriority.getValue(executer),
				new Vector3f(origin.getValue(executer), drawPriority.getValue(executer)),
				new Vector3f(min.getValue(executer), 0),
				new Vector3f(max.getValue(executer), 0),
				color.getValue(executer),
				opacity.getValue(executer),
				rotation.getValue(executer), new Vector3f(0, 0, 1));
		rectangle.setKey(key.getValue(executer));
		executer.getWorld().addGObject(rectangle);
		if (then != null) then.run();
	}
}
