package com.degoos.orbisnoir.script.step.type;

import com.degoos.orbisnoir.game.entity.purgatorio.PurgatorioKeyboardInput;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;

import java.util.List;

public class ScriptStepPurgatorioKeyboardInput extends ScriptStep {

	private StringValueParser text;

	public ScriptStepPurgatorioKeyboardInput(List<ScriptStep> forcedSteps, Script follow, StringValueParser text) {
		super(forcedSteps, follow);
		this.text = text;
	}

	@Override
	public void run(ScriptExecuter executer, Runnable then) {
		PurgatorioKeyboardInput textBox = new PurgatorioKeyboardInput(executer.getWorld(), text.getValue(executer));
		textBox.setThen(then == null ? null : target -> then.run());
		executer.getWorld().addGObject(textBox);
	}
}