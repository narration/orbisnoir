package com.degoos.orbisnoir.util;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

public class VectorUtils {

	public static Vector2i shiftLeft(Vector2i vec, int shift, Vector2i dest) {
		dest.x = vec.x << shift;
		dest.y = vec.y << shift;
		return dest;
	}

	public static Vector2i shiftRight(Vector2i vec, int shift, Vector2i dest) {
		dest.x = vec.x >> shift;
		dest.y = vec.y >> shift;
		return dest;
	}

	public static Vector2f rotate(Vector2f vec, double angle) {
		float cos = (float) Math.cos(angle);
		float sin = (float) Math.sin(angle);
		float x = vec.x;
		float y = vec.y;
		vec.x = x * cos - y * sin;
		vec.y = x * sin + y * cos;
		return vec;
	}

	public static Vector3f rotate(Vector3f vec, double angle) {
		float cos = (float) Math.cos(angle);
		float sin = (float) Math.sin(angle);
		float x = vec.x;
		float y = vec.y;
		vec.x = x * cos - y * sin;
		vec.y = x * sin + y * cos;
		return vec;
	}

	public static Vector2f toVector2f(Vector3f vec) {
		return new Vector2f(vec.x, vec.y);
	}


	public static float fastDot(float x1, float y1, float x2, float y2) {
		return x1 * x2 + y1 * y2;
	}
}
