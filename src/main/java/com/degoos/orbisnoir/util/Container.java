package com.degoos.orbisnoir.util;

public class Container<T> {

	T value;

	public Container(T value) {
		this.value = value;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public boolean isEmpty() {
		return value == null;
	}
}
