package com.degoos.orbisnoir.util;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.object.Text;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.editor.Editor;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector3f;

public class GraphicUtils {

	public static void addFPSDebugger(Room room) {
		room.addGObject(new Text(true, 0, Engine.getFontManager().getFontUnsafe("Arial"),
				new Vector3f(Engine.getRenderArea().getMin().x + 0.8f, 0.95f, 0.999999f),
				new Area(0, 0, 0.3f, 0.05f), "", GColor.WHITE, 1) {
			@Override
			public void onTick(long dif, Room room) {
				double d = dif / 1000000f;
				if (d > 20) setColor(GColor.RED);
				else setColor(GColor.GREEN);
				setText(String.format("%.2f", d));
			}
		});

		room.addGObject(new Text(true, 0, Engine.getFontManager().getFontUnsafe("Arial"),
				new Vector3f(Engine.getRenderArea().getMin().x + 0.8f, 0.9f, 0.999999f),
				new Area(0, 0, 0.3f, 0.05f), "", GColor.WHITE, 1) {
			@Override
			public void onTick(long dif, Room room) {
				double d = Engine.getRenderer().getLoopDelay() / 1000000f;
				if (d > 5) setColor(GColor.RED);
				else setColor(GColor.GREEN);
				setText(String.format("%.2f", d));
			}
		});

		room.addGObject(new Text(true, 0, Engine.getFontManager().getFontUnsafe("Arial"),
				new Vector3f(Engine.getRenderArea().getMin().x + 0.8f, 0.85f, 0.999999f),
				new Area(0, 0, 0.3f, 0.05f), "", GColor.WHITE, 1) {
			@Override
			public void onTick(long dif, Room room) {
				double d = Engine.getRenderer().getBufferSwapDelay() / 1000000f;
				if (d > 20) setColor(GColor.RED);
				else setColor(GColor.GREEN);
				setText(String.format("%.2f", d));
			}
		});

		if (!(room instanceof Editor)) return;

		room.addGObject(new Text(true, 0, Engine.getFontManager().getFontUnsafe("Arial"),
				new Vector3f(Engine.getRenderArea().getMin().x + 0.8f, 0.8f, 0.999999f),
				new Area(0, 0, 0.3f, 0.05f), "", GColor.WHITE, 1) {
			@Override
			public void onTick(long dif, Room room) {
				double d = ((Editor) room).getDrawDelay() / 1000000f;
				if (d > 1) setColor(GColor.RED);
				else setColor(GColor.GREEN);
				setText(String.format("%.2f", d));
			}
		});

		room.addGObject(new Text(true, 0, Engine.getFontManager().getFontUnsafe("Arial"),
				new Vector3f(Engine.getRenderArea().getMin().x + 0.8f, 0.75f, 0.999999f),
				new Area(0, 0, 0.3f, 0.05f), "", GColor.WHITE, 1) {
			@Override
			public void onTick(long dif, Room room) {
				double d = ((Editor) room).getTickDelay() / 1000000f;
				if (d > 2) setColor(GColor.RED);
				else setColor(GColor.GREEN);
				setText(String.format("%.2f", d));
			}
		});


	}
}
