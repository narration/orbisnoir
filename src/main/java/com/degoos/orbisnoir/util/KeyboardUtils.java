package com.degoos.orbisnoir.util;

import com.degoos.graphicengine2.enums.EnumKeyboardKey;

public class KeyboardUtils {

	public static int getHotbarIndex(EnumKeyboardKey keyboardKey) {
		switch (keyboardKey) {
			case NUMBER_1:
				return 0;
			case NUMBER_2:
				return 1;
			case NUMBER_3:
				return 2;
			case NUMBER_4:
				return 3;
			case NUMBER_5:
				return 4;
			case NUMBER_6:
				return 5;
			case NUMBER_7:
				return 6;
			default:
				return -1;
		}
	}

}
