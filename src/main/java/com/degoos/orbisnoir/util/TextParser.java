package com.degoos.orbisnoir.util;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Text;

import java.util.HashMap;
import java.util.Map;

public class TextParser {

	private static final ColorMap COLORS = new ColorMap();

	public static void parseText(Text text, String string) {
		Map<Integer, GColor> colors = text.getColors();

		boolean skip = false;
		char c;
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < string.length(); i++) {
			c = string.charAt(i);
			if (!skip && c == '&' && string.length() > i + 1 && isColor(string.charAt(i + 1))) {
				colors.put(builder.length(), getColor(string.charAt(i + 1)));
				i++;
			} else if (c == '/' && string.length() > i + 2 && string.charAt(i + 1) == '&' && isColor(string.charAt(i + 2))) {
				skip = true;
			} else {
				builder.append(c);
				skip = false;
			}
		}

		text.setText(builder.toString());
	}


	public static boolean isColor(char c) {
		return COLORS.containsKey(c);
	}

	public static GColor getColor(char c) {
		return COLORS.get(c);
	}

	private static class ColorMap extends HashMap<Character, GColor> {

		public ColorMap() {
			super(22);
			put('0', GColor.BLACK);
			put('1', new GColor(0x0000AA));
			put('2', new GColor(0x00AA00));
			put('3', new GColor(0x00AAAA));
			put('4', new GColor(0xAA0000));
			put('5', new GColor(0xAA00AA));
			put('6', new GColor(0xFFAA00));
			put('7', new GColor(0xAAAAAA));
			put('8', new GColor(0x555555));
			put('9', new GColor(0x5555FF));
			put('a', new GColor(0x55FF55));
			put('b', new GColor(0x55FFFF));
			put('c', new GColor(0xFF5555));
			put('d', new GColor(0xFF55FF));
			put('e', new GColor(0xFFFF55));
			put('f', GColor.WHITE);

			put('A', get('a'));
			put('B', get('b'));
			put('C', get('c'));
			put('D', get('d'));
			put('E', get('e'));
			put('F', get('f'));
		}

	}
}
