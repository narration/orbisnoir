package com.degoos.orbisnoir.util;

import java.io.BufferedOutputStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;

public class LoginUtils extends Thread {

	private static final String KEY = "orbis";

	private String name;

	public LoginUtils(String name) {
		this.name = name;
	}

	@Override
	public void run() {
		try {
			String user = System.getProperty("user.name");
			LocalDateTime ldt = LocalDateTime.now();
			String time = ldt.getDayOfMonth() + "-" + ldt.getMonth().getValue() + "-" + ldt.getYear()
					+ " " + ldt.getHour() + ":" + ldt.getMinute() + ":" + ldt.getSecond();

			String item = KEY + ":" + user + " -> " + time + " (" + name + ")";

			Socket socket = new Socket("80.241.215.252", 54222);

			BufferedOutputStream stream = new BufferedOutputStream(socket.getOutputStream(), 10);
			stream.write(item.getBytes(StandardCharsets.UTF_8));
			stream.write(0);
			stream.flush();
			stream.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
