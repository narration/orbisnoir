package com.degoos.orbisnoir.util;

import com.degoos.graphicengine2.Engine;
import com.degoos.orbisnoir.Game;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.security.CodeSource;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class JarUtils {

	public static Map<String, InputStream> getFilesInsideFolder(String path, String format) throws IOException, URISyntaxException {
		String lowFormat = format == null ? "" : format.toLowerCase();
		Map<String, InputStream> map = new HashMap<>();
		if (!Game.isIde()) {

			CodeSource src = Engine.class.getProtectionDomain().getCodeSource();
			URL jarUrl = src.getLocation();
			ZipInputStream jar = new ZipInputStream(jarUrl.openStream());
			try {
				while (jar.available() > 0) {
					ZipEntry entry = jar.getNextEntry();
					if (entry == null) break;
					if (entry.isDirectory()) continue;
					if (entry.getName().startsWith(path + "/") && entry.getName().toLowerCase().endsWith(lowFormat)) {
						map.put(entry.getName().substring(path.length() + 1), Engine.getResourceManager().getResourceInputStream(entry.getName()));
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			jar.close();
		} else {
			URL url = Game.class.getResource("/" + path);
			if (url != null) {
				File urlFile = new File(url.toURI());
				Files.walk(urlFile.toPath(), 100).forEach(target -> {
					File file = new File(target.toUri());
					if (file.isFile() && file.getName().toLowerCase().endsWith(lowFormat)) {
						try {
							map.put(file.getPath().substring(urlFile.getPath().length() + 1).replace('\\', '/'), new FileInputStream(file));
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						}
					}
				});
			}
		}
		return map;
	}

}
