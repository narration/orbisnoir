package com.degoos.orbisnoir.util;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class FileUtils {

	public static Map<String, File> getFilesInsideFolder(File folder, String format) {
		return getFilesInsideFolder(folder, new HashMap<>(), format, "");
	}

	public static Map<String, File> getFilesInsideFolder(File folder, Map<String, File> map, String format, String parents) {
		File[] files = folder.listFiles();
		if (files == null) return map;
		for (File file : files) {
			if (file.isDirectory()) getFilesInsideFolder(file, map, format, parents + file.getName() + "/");
			else if (file.getName().toLowerCase().endsWith(format)) map.put(parents + file.getName(), file);
		}
		return map;
	}

}
