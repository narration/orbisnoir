package com.degoos.orbisnoir.util;

import com.degoos.graphicengine2.font.TrueTypeFont;

import java.util.ArrayList;
import java.util.List;

public class TextCutter {

	public static List<String> cut(String string, TrueTypeFont width, int pixels) {
		List<String> list = new ArrayList<>();
		String[] sl = string.split(" ");
		StringBuilder currentString = new StringBuilder();

		for (String s : sl) {
			if (currentString.length() == 0) {
				currentString.append(s);
				continue;
			}
			String preview = currentString.toString() + ' ' + s;
			if (width.getWidth(preview) >= pixels) {
				list.add(currentString.toString());
				currentString = new StringBuilder(s);
			} else currentString = new StringBuilder(preview);
		}
		list.add(currentString.toString());
		return list;
	}

}
