package com.degoos.orbisnoir.handler;

import com.degoos.graphicengine2.event.drop.DropFilesEvent;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.window.WindowResizeEvent;
import com.degoos.graphicengine2.object.GObject;
import com.degoos.graphicengine2.object.Room;
import com.degoos.orbisnoir.entity.Entity;
import org.joml.Vector2f;

public class ComplexMoveHandler extends GObject {

	private Entity entity;
	private Vector2f to, toVelocity;
	private long toNanos, countNanos;
	private Runnable then;

	public ComplexMoveHandler(Entity entity, Vector2f to, long toMillis, Runnable then) {
		this.entity = entity;
		this.to = new Vector2f(to);
		this.toNanos = toMillis * 1000000;
		this.toVelocity = new Vector2f(to).sub(entity.getPosition()).mul(1 / (float) toNanos);
		this.countNanos = 0;
		this.then = then;
	}

	@Override
	public void onTick(long l, Room room) {
		countNanos += l;
		if (countNanos >= toNanos) {
			entity.setPosition(to);
			delete();
			if (then != null) then.run();
		} else entity.setPosition(entity.getPosition().add(new Vector2f(toVelocity).mul(l)));
	}

	@Override
	public void onTick2(long l, Room room) {

	}

	@Override
	public void onAsyncTick(long l, Room room) {

	}

	@Override
	public void onMouseEvent(MouseEvent mouseEvent) {

	}

	@Override
	public void onKeyboardEvent(KeyEvent keyEvent) {

	}

	@Override
	public void onResize(WindowResizeEvent windowResizeEvent) {

	}

	@Override
	public void onDropFilesEvent(DropFilesEvent dropFilesEvent) {

	}

}
