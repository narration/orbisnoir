package com.degoos.orbisnoir.handler;

import com.degoos.graphicengine2.event.drop.DropFilesEvent;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.window.WindowResizeEvent;
import com.degoos.graphicengine2.object.GObject;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.object.Text;
import com.degoos.orbisnoir.script.ScriptExecuter;

public class OpacityHandler extends GObject {

	private ScriptExecuter executer;

	public GObject handled;

	public long delayNanos;
	public float finalOpacity;

	public float opacityPerNano;
	public float currentOpacity;

	public long passedNanos;

	public Runnable then;


	public OpacityHandler(GObject handled, long delayNanos, float finalOpacity, Runnable then) {
		this(null, handled, delayNanos, finalOpacity, then);
	}

	public OpacityHandler(ScriptExecuter executer, GObject handled, long delayNanos, float finalOpacity, Runnable then) {
		this.executer = executer;
		this.handled = handled;
		this.delayNanos = delayNanos;
		this.finalOpacity = finalOpacity;
		this.currentOpacity = getOpacity();
		this.then = then;
		this.passedNanos = 0;

		this.opacityPerNano = (finalOpacity - getOpacity()) / (float) delayNanos;
	}

	@Override
	public void onTick(long l, Room room) {
		if (executer != null && !executer.isAlive()) return;
		passedNanos += l;
		if (passedNanos >= delayNanos) {
			setOpacity(finalOpacity);
			delete();
			if (then != null) then.run();
		} else {

			currentOpacity += opacityPerNano * l;
			setOpacity(currentOpacity);
		}
	}


	private float getOpacity() {
		if (handled instanceof Rectangle)
			return ((Rectangle) handled).getOpacity();
		if (handled instanceof Text)
			return ((Text) handled).getOpacity();
		return 0;
	}

	private void setOpacity(float opacity) {
		if (handled instanceof Rectangle) {
			((Rectangle) handled).setOpacity(opacity);
		}
		if (handled instanceof Text) {
			((Text) handled).setOpacity(opacity);
		}
	}

	@Override
	public void onTick2(long l, Room room) {

	}

	@Override
	public void onAsyncTick(long l, Room room) {

	}

	@Override
	public void onMouseEvent(MouseEvent mouseEvent) {

	}

	@Override
	public void onKeyboardEvent(KeyEvent keyEvent) {

	}

	@Override
	public void onResize(WindowResizeEvent windowResizeEvent) {

	}

	@Override
	public void onDropFilesEvent(DropFilesEvent dropFilesEvent) {

	}
}
