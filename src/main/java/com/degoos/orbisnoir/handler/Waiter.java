package com.degoos.orbisnoir.handler;

import com.degoos.graphicengine2.event.drop.DropFilesEvent;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.window.WindowResizeEvent;
import com.degoos.graphicengine2.object.GObject;
import com.degoos.graphicengine2.object.Room;

import java.util.function.Consumer;

public class Waiter extends GObject {

	private long waited, nanos;
	private Consumer<Waiter> then;

	public Waiter(long millis, Consumer<Waiter> then) {
		this.nanos = millis * 1000000;
		this.then = then;
		this.waited = 0;
	}

	@Override
	public void onTick(long l, Room room) {
		waited += l;
		if (waited >= nanos) {
			then.accept(this);
			delete();
		}
	}

	@Override
	public void onTick2(long l, Room room) {

	}

	@Override
	public void onAsyncTick(long l, Room room) {

	}

	@Override
	public void onMouseEvent(MouseEvent mouseEvent) {

	}

	@Override
	public void onKeyboardEvent(KeyEvent keyEvent) {

	}

	@Override
	public void onResize(WindowResizeEvent windowResizeEvent) {

	}

	@Override
	public void onDropFilesEvent(DropFilesEvent dropFilesEvent) {

	}

}
