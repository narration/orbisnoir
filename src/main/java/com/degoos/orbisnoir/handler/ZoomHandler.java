package com.degoos.orbisnoir.handler;

import com.degoos.graphicengine2.event.drop.DropFilesEvent;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.window.WindowResizeEvent;
import com.degoos.graphicengine2.object.GObject;
import com.degoos.graphicengine2.object.Room;
import com.degoos.orbisnoir.world.Camera;

public class ZoomHandler extends GObject {

	private Camera camera;
	private float to, velocity;
	private Runnable then;

	public ZoomHandler(float to, long delay, Runnable then, Camera camera) {
		this.to = to;
		this.velocity = (to - camera.getBoxSize()) / (delay * 1000000);
		this.then = then;
		this.camera = camera;
	}


	@Override
	public void onTick(long l, Room room) {
		camera.setBoxSize(camera.getBoxSize() + (velocity * l));
		if (velocity < 0) {
			if (camera.getBoxSize() <= to) {
				finish();
			}
		} else if (camera.getBoxSize() >= to) finish();
	}

	private void finish() {
		camera.setBoxSize(to);
		delete();
		if (then != null) then.run();
	}

	@Override
	public void onTick2(long l, Room room) {

	}

	@Override
	public void onAsyncTick(long l, Room room) {

	}

	@Override
	public void onMouseEvent(MouseEvent mouseEvent) {

	}

	@Override
	public void onKeyboardEvent(KeyEvent keyEvent) {

	}

	@Override
	public void onResize(WindowResizeEvent windowResizeEvent) {

	}

	@Override
	public void onDropFilesEvent(DropFilesEvent dropFilesEvent) {

	}
}