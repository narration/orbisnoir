package com.degoos.orbisnoir.handler;

import com.degoos.graphicengine2.event.drop.DropFilesEvent;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.window.WindowResizeEvent;
import com.degoos.graphicengine2.object.GObject;
import com.degoos.graphicengine2.object.Room;
import com.degoos.orbisnoir.entity.MovableEntity;
import com.degoos.orbisnoir.enums.EnumDirection;
import org.joml.Vector2f;

public class MoveHandler extends GObject {

	private MovableEntity entity;
	private EnumDirection direction;
	private Vector2f start;
	private Vector2f to, distance;
	private boolean run, checkCollisions;
	private Runnable then;


	public MoveHandler(MovableEntity entity, EnumDirection direction, boolean run, boolean checkCollisions, Runnable then) {
		this.entity = entity;
		this.direction = direction;
		this.start = new Vector2f(entity.getPosition());
		this.to = new Vector2f(entity.getPosition()).add(direction.getRelative().x, direction.getRelative().y);
		this.distance = new Vector2f(to).sub(entity.getPosition());
		this.then = then;
		this.run = run;
		this.checkCollisions = checkCollisions;
	}

	private boolean checkFinished() {
		if (finished()) {
			if (then != null) then.run();
			delete();
			return true;
		}
		return false;
	}

	private boolean finished() {
		return entity.getPosition().distanceSquared(start) >= distance.lengthSquared();
	}

	@Override
	public void onTick(long l, Room room) {
		if (checkFinished()) return;
		entity.setRunning(run);
		if (entity.move(direction, l, checkCollisions)) {
			if (then != null) then.run();
			delete();
		}
		checkFinished();
	}

	@Override
	public void onTick2(long l, Room room) {

	}

	@Override
	public void onAsyncTick(long l, Room room) {

	}

	@Override
	public void onMouseEvent(MouseEvent mouseEvent) {

	}

	@Override
	public void onKeyboardEvent(KeyEvent keyEvent) {

	}

	@Override
	public void onResize(WindowResizeEvent windowResizeEvent) {

	}

	@Override
	public void onDropFilesEvent(DropFilesEvent dropFilesEvent) {

	}

}
