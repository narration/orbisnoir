package com.degoos.orbisnoir.handler;

import com.degoos.graphicengine2.event.drop.DropFilesEvent;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.window.WindowResizeEvent;
import com.degoos.graphicengine2.object.GObject;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.sound.SoundSource;
import com.degoos.orbisnoir.script.ScriptExecuter;

public class VolumeHandler extends GObject {

	private ScriptExecuter executer;

	public SoundSource handled;
	public long delayNanos;
	public float volumePerTick;
	public long passedNanos;
	public boolean delete;
	public Runnable then;


	public VolumeHandler(SoundSource handled, float volume, long delayNanos, boolean delete, Runnable then) {
		this(null, handled, volume, delayNanos, delete, then);
	}


	public VolumeHandler(ScriptExecuter executer, SoundSource handled, float volume, long delayNanos, boolean delete, Runnable then) {
		this.executer = executer;
		this.handled = handled;
		this.delayNanos = delayNanos;
		this.then = then;
		this.passedNanos = 0;
		this.delete = delete;
		this.volumePerTick = (volume - handled.getGain()) / delayNanos;
	}


	@Override
	public void onTick(long l, Room room) {
		if (executer != null && !executer.isAlive()) return;
		passedNanos += l;
		if (passedNanos >= delayNanos) {
			if (delete) handled.cleanup();
			delete();
			if (then != null) then.run();
		} else {
			handled.setGain(handled.getGain() + volumePerTick * l);
		}
	}

	@Override
	public void onTick2(long l, Room room) {

	}

	@Override
	public void onAsyncTick(long l, Room room) {

	}

	@Override
	public void onMouseEvent(MouseEvent mouseEvent) {

	}

	@Override
	public void onKeyboardEvent(KeyEvent keyEvent) {

	}

	@Override
	public void onResize(WindowResizeEvent windowResizeEvent) {

	}

	@Override
	public void onDropFilesEvent(DropFilesEvent dropFilesEvent) {

	}
}