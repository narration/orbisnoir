package com.degoos.orbisnoir.editor.hotbar;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.enums.EnumWheelDirection;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.mouse.MouseWheelEvent;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.orbisnoir.editor.Editor;
import com.degoos.orbisnoir.editor.bar.Bar;
import com.degoos.orbisnoir.texture.TextureManager;
import com.degoos.orbisnoir.util.KeyboardUtils;
import org.joml.Vector3f;

public class Hotbar<E> extends Rectangle {

	protected Editor editor;
	protected Bar bar;


	protected Rectangle selection;
	protected HotbarIndex<E>[] indices;
	protected int selected;

	public Hotbar(Editor editor, Bar bar) {
		super(new Vector3f(0, -1, 0.885f), new Vector3f(-0.7f, 0, 0), new Vector3f(0.7f, 0.2f, 0));
		this.editor = editor;
		this.bar = bar;

		selected = 0;
		selection = new Rectangle(new Vector3f(-0.7f, -1, 0.901f), new Vector3f(0), new Vector3f(0.2f, 0.2f, 0));


		setVisible(true);
		selection.setVisible(true);
		setTexture(TextureManager.loadImageOrNull("gui/editor/block_hotbar.png"));
		selection.setTexture(TextureManager.loadImageOrNull("gui/editor/block_selected_hotbar_index.png"));

		editor.addGObject(selection);
		loadIndices();
	}

	public Editor getEditor() {
		return editor;
	}

	public Bar getBar() {
		return bar;
	}

	public int getSelected() {
		return selected;
	}

	public HotbarIndex<E> getSelectedIndex() {
		return indices[selected];
	}

	public HotbarIndex[] getIndices() {
		return indices;
	}

	protected void loadIndices() {
		indices = (HotbarIndex<E>[]) new HotbarIndex[7];
		for (int i = 0; i < indices.length; i++) {
			indices[i] = new HotbarIndex<E>(this, i);
			editor.addGObject(indices[i]);
		}
	}

	@Override
	public void onMouseEvent(MouseEvent event) {
		if (event instanceof MouseWheelEvent) {
			if (!bar.isExpanded() || !bar.isInArea(event.getPosition())) {
				if (((MouseWheelEvent) event).getWheelDirection() == EnumWheelDirection.UP)
					selected--;
				else selected++;
				if (selected == 7) selected = 0;
				if (selected == -1) selected = 6;
				selection.setPosition(selection.getPosition().set(-0.7f + (0.2f * selected), -1, 0.901f));
			}
		}
	}

	@Override
	public void onKeyboardEvent(KeyEvent event) {
		if (editor.getChat().isExtended()) return;
		if (event instanceof KeyPressEvent) {
			if (event.getKeyboardKey() == EnumKeyboardKey.Q) {
				getSelectedIndex().setElement(null, null);
				return;
			}
			if (bar.isInArea(Engine.getMousePosition())) return;
			int sel = KeyboardUtils.getHotbarIndex(event.getKeyboardKey());
			if (sel == -1) return;
			selected = sel;
			selection.setPosition(selection.getPosition().set(-0.7f + (0.2f * selected), -1, 0.901f));
		}
	}

	public void refreshTextures() {
		for (HotbarIndex<E> index : indices) {
			index.refreshTexture();
		}
	}
}
