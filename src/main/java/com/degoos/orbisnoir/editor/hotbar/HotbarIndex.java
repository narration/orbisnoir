package com.degoos.orbisnoir.editor.hotbar;

import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.texture.ITexture;
import org.joml.Vector3f;

public class HotbarIndex<E> extends Rectangle {

	protected Hotbar hotbar;
	protected E element;

	public HotbarIndex(Hotbar hotbar, int index) {
		super(new Vector3f(-0.7f + (0.2f * index) + 0.001f, -1, 0.903f),
				new Vector3f(0.02f, 0.02f, 0), new Vector3f(0.18f, 0.18f, 0));
		setVisible(false);
		this.hotbar = hotbar;
		element = null;
	}

	public Hotbar getHotbar() {
		return hotbar;
	}

	public E getElement() {
		return element;
	}

	public void setElement(E element, ITexture texture) {
		this.element = element;
		setTexture(texture);
		setVisible(texture != null);
		refreshTexture();
	}

	public void refreshTexture () {
	}
}
