package com.degoos.orbisnoir.editor.script;

import com.degoos.graphicengine2.enums.EnumMouseButton;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.mouse.MousePressEvent;
import com.degoos.graphicengine2.event.mouse.MouseReleaseEvent;
import com.degoos.orbisnoir.editor.hotbar.Hotbar;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.world.Box;

import java.util.NoSuchElementException;

public class ScriptHotbar extends Hotbar<Script> {

	public ScriptHotbar(ScriptEditor editor, ScriptBar bar) {
		super(editor, bar);
	}

	@Override
	public ScriptEditor getEditor() {
		return (ScriptEditor) super.getEditor();
	}

	@Override
	protected void loadIndices() {
		indices = new ScriptHotbarIndex[7];
		for (int i = 0; i < indices.length; i++) {
			indices[i] = new ScriptHotbarIndex(this, editor, i);
			editor.addGObject(indices[i]);
		}
	}

	@Override
	public ScriptHotbarIndex getSelectedIndex() {
		return (ScriptHotbarIndex) super.getSelectedIndex();
	}

	@Override
	public void onMouseEvent(MouseEvent event) {
		if (event instanceof MouseReleaseEvent) {
			DraggedScript draggedScript = getEditor().draggedScript;
			if (draggedScript == null) return;
			getEditor().setDraggedScript(null);

			for (int i = 0; i < indices.length; i++)
				if (((ScriptHotbarIndex) indices[i]).checkDraggedCollision(draggedScript)) break;
		} else if (event instanceof MousePressEvent) {
			if (((MousePressEvent) event).getMouseButton() == EnumMouseButton.WHEEL) {
				try {
					Box box = editor.getWorld().getCamera().getPointedBox(editor.getWorld());
					getSelectedIndex().setElement(box.getScript());
				} catch (NoSuchElementException ex) {
					getSelectedIndex().setElement(null, null);
				}
			}
		} else super.onMouseEvent(event);
	}
}
