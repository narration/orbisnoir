package com.degoos.orbisnoir.editor.script;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.object.Text;
import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.orbisnoir.chat.Chat;
import com.degoos.orbisnoir.editor.Editor;
import com.degoos.orbisnoir.editor.hotbar.HotbarIndex;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.util.VectorUtils;
import org.joml.Vector2f;
import org.joml.Vector3f;

public class ScriptHotbarIndex extends HotbarIndex<Script> {

	private Text text;

	public ScriptHotbarIndex(ScriptHotbar hotbar, Editor editor, int index) {
		super(hotbar, index);
		text = new Text(Chat.FONT, new Vector3f(getPosition()), VectorUtils.toVector2f(getMin()), VectorUtils.toVector2f(getMax()),
				"", GColor.WHITE, 1);
		text.getPosition().z += 0.00001f;
		editor.addGObject(text);
		setColor(GColor.GREEN.darker());
	}

	public boolean checkDraggedCollision(DraggedScript draggedScript) {
		if (isInArea(new Vector2f(draggedScript.getPosition().x, draggedScript.getPosition().y))) {
			setElement(draggedScript.getElement());
			return true;
		}
		return false;
	}

	@Override
	public void setElement(Script element, ITexture texture) {
		this.element = element;
		setTexture(texture);
		setVisible(texture != null);
		text.setVisible(isVisible());
	}

	@Override
	public void onTick(long dif, Room room) {
		super.onTick(dif, room);
	}

	public void setElement(Script element) {
		this.element = element;
		setVisible(element != null);
		text.setVisible(element != null);
		text.setText(element == null ? "" : element.getName().replace("/", "\n"));
	}
}
