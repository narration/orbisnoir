package com.degoos.orbisnoir.editor.script;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Text;
import com.degoos.orbisnoir.chat.Chat;
import com.degoos.orbisnoir.editor.DraggedItem;
import com.degoos.orbisnoir.editor.Editor;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.util.VectorUtils;
import org.joml.Vector3f;

public class DraggedScript extends DraggedItem<Script> {

	private Text text;

	public DraggedScript(Editor editor, Vector3f position, Script element) {
		super(position, element, null);


		setColor(GColor.GREEN.darker());
		text = new Text(Chat.FONT, new Vector3f(getPosition()), VectorUtils.toVector2f(getMin()),
				VectorUtils.toVector2f(getMax()), element.getName().replace("/", "\n"), GColor.WHITE, 1);
		text.getPosition().z += 0.0001;
		text.setVisible(true);
		editor.addGObject(text);
	}


	@Override
	public DraggedScript setPosition(Vector3f position) {
		super.setPosition(position);
		text.getPosition().set(position);
		text.getPosition().z += 0.0001;
		text.setRequiresRecalculation(true);
		return this;
	}

	@Override
	public void setRotation(float rotation) {
		super.setRotation(rotation);
		text.setRotation(rotation);
	}

	@Override
	public void delete() {
		super.delete();
		text.delete();
	}
}
