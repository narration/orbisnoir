package com.degoos.orbisnoir.editor.script;

import com.degoos.graphicengine2.object.GObject;
import com.degoos.orbisnoir.editor.bar.Bar;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptManager;
import org.joml.Vector2i;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class ScriptBar extends Bar<Script> {

	public ScriptBar(ScriptEditor scriptEditor) {
		super(scriptEditor);

		loadScripts();
		scroll.setMaximumOffset(maximumY - 18);
	}

	@Override
	public ScriptEditor getEditor() {
		return (ScriptEditor) super.getEditor();
	}

	public void loadScripts() {
		if (elements != null) {
			elements.values().forEach(GObject::delete);
			elements.clear();
		} else
			elements = new HashMap<>();

		int barY = 0;

		List<Script> scripts = new ArrayList<>(ScriptManager.getScripts().values());
		scripts.sort(Comparator.comparing(Script::getName));

		ScriptBarElement element;
		for (Script script : scripts) {
			element = new ScriptBarElement(this, script, barY);
			editor.addGObject(element);
			elements.put(new Vector2i(0, barY), element);

			barY++;
		}
		maximumY = barY;
	}
}
