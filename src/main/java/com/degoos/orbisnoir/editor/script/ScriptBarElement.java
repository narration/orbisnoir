package com.degoos.orbisnoir.editor.script;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumMouseButton;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.mouse.MousePressEvent;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.object.Text;
import com.degoos.orbisnoir.chat.Chat;
import com.degoos.orbisnoir.editor.bar.BarElement;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.util.KeyboardUtils;
import com.degoos.orbisnoir.util.VectorUtils;
import org.joml.Vector2f;
import org.joml.Vector3f;

public class ScriptBarElement extends BarElement<Script> {

	private Text text;

	public ScriptBarElement(ScriptBar bar, Script element, int y) {
		super(bar, element, y, null);
		setOpacity(0);

		text = new Text(Chat.FONT, new Vector3f(getPosition()), VectorUtils.toVector2f(getMin()),
				VectorUtils.toVector2f(getMax()), element.getName(), GColor.WHITE, 1);
		text.getPosition().z += 0.0001f;
		text.setVisible(true);
		bar.getEditor().addGObject(text);
	}

	@Override
	public ScriptBar getBar() {
		return (ScriptBar) super.getBar();
	}

	@Override
	public void onTick2(long dif, Room room) {
		super.onTick2(dif, room);
		position.y = 0.85f - ((barPosition.y + offset) * 0.07f);
		if (!text.getPosition().equals(getPosition())) {
			text.getPosition().set(getPosition());
			text.getPosition().z += 0.0001f;
			text.setRequiresRecalculation(true);
		}
		setVisible(Engine.willRender(getMin().x + position.x, getMin().y + position.y,
				getMax().x + position.x, getMax().y + position.y));
		text.setVisible(isVisible());
	}

	@Override
	public void onMouseEvent(MouseEvent event) {
		if (bar.isExpanded() && event instanceof MousePressEvent && ((MousePressEvent) event).getMouseButton() == EnumMouseButton.PRIMARY) {
			Vector2f mouse = event.getPosition();
			if (isInArea(mouse)) {
				getBar().getEditor().setDraggedScript(new DraggedScript(getBar().getEditor(),
						new Vector3f(mouse, 0.999f), element));
			}
		}
	}

	@Override
	public void onKeyboardEvent(KeyEvent event) {
		if (!bar.getEditor().getChat().isExtended() && bar.getEditor().getChat().canUseKeyboard() && bar.isExpanded()
				&& isInArea(Engine.getMousePosition()) && event instanceof KeyPressEvent) {
			int selected = KeyboardUtils.getHotbarIndex(event.getKeyboardKey());
			if (selected == -1) return;
			((ScriptHotbarIndex) getBar().getEditor().getHotbar().getIndices()[selected]).setElement(element);
		}
	}

	@Override
	public void setOffset(int offset) {
		setVisible((barPosition.y + offset) < 19 && (barPosition.y + offset) >= 0);
		this.offset = offset;
		position.y = 0.85f - ((barPosition.y + offset) * 0.07f);
		requiresRecalculation = true;
	}

	@Override
	public void delete() {
		super.delete();
		text.delete();
	}
}
