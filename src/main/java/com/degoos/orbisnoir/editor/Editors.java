package com.degoos.orbisnoir.editor;

import com.degoos.orbisnoir.editor.collision.CollisionEditor;
import com.degoos.orbisnoir.editor.entity.EntityEditor;
import com.degoos.orbisnoir.editor.script.ScriptEditor;
import com.degoos.orbisnoir.editor.texture.TextureEditor;

public class Editors {

	public static TextureEditor textureEditor;
	public static CollisionEditor collisionEditor;
	public static EntityEditor entityEditor;
	public static ScriptEditor scriptEditor;

	public static void reset() {
		textureEditor = null;
		collisionEditor = null;
		entityEditor = null;
		scriptEditor = null;
	}
}
