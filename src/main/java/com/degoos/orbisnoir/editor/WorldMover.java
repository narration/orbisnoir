package com.degoos.orbisnoir.editor;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.event.drop.DropFilesEvent;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.window.WindowResizeEvent;
import com.degoos.graphicengine2.io.Keyboard;
import com.degoos.graphicengine2.object.GObject;
import com.degoos.graphicengine2.object.Room;
import com.degoos.orbisnoir.editor.Editor;
import com.degoos.orbisnoir.world.Camera;

public class WorldMover extends GObject {

	private Editor editor;

	public WorldMover(Editor editor) {
		this.editor = editor;
	}

	@Override
	public void onTick(long l, Room room) {
		if (editor.getChat().isExtended() || !editor.getChat().canUseKeyboard()) return;
		Keyboard keyboard = Engine.getKeyboard();
		Camera camera = editor.getWorld().getCamera();
		float velocity = keyboard.isKeyPressed(EnumKeyboardKey.LEFT_CONTROL) || keyboard.isKeyPressed(EnumKeyboardKey.RIGHT_CONTROL)
				|| keyboard.isKeyPressed(EnumKeyboardKey.Z) ? 0.3f : 0.1f;
		if (keyboard.isKeyPressed(EnumKeyboardKey.ARROW_UP) || keyboard.isKeyPressed(EnumKeyboardKey.W))
			camera.getPosition().add(0, velocity, 0);
		if (keyboard.isKeyPressed(EnumKeyboardKey.ARROW_DOWN) || keyboard.isKeyPressed(EnumKeyboardKey.S))
			camera.getPosition().add(0, -velocity, 0);
		if (keyboard.isKeyPressed(EnumKeyboardKey.ARROW_RIGHT) || keyboard.isKeyPressed(EnumKeyboardKey.D))
			camera.getPosition().add(velocity, 0, 0);
		if (keyboard.isKeyPressed(EnumKeyboardKey.ARROW_LEFT) || keyboard.isKeyPressed(EnumKeyboardKey.A))
			camera.getPosition().add(-velocity, 0, 0);
	}

	@Override
	public void onTick2(long l, Room room) {

	}

	@Override
	public void onAsyncTick(long l, Room room) {

	}

	@Override
	public void onMouseEvent(MouseEvent mouseEvent) {

	}

	@Override
	public void onKeyboardEvent(KeyEvent keyEvent) {

	}

	@Override
	public void onResize(WindowResizeEvent windowResizeEvent) {

	}

	@Override
	public void onDropFilesEvent(DropFilesEvent dropFilesEvent) {

	}
}
