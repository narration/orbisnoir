package com.degoos.orbisnoir.editor.texture;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.enums.EnumMouseButton;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.event.mouse.MouseDragEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.mouse.MousePressEvent;
import com.degoos.graphicengine2.event.mouse.MouseReleaseEvent;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Room;
import com.degoos.orbisnoir.world.Box;
import com.degoos.orbisnoir.world.Camera;
import com.degoos.orbisnoir.world.Chunk;
import org.joml.Vector2i;
import org.joml.Vector3f;

public class EditorSelection extends Rectangle {

	private TextureEditor editor;

	private Vector3f min, max;
	private Rectangle blockArea;


	public EditorSelection(TextureEditor editor) {
		super(new Vector3f(0, 0, 0.1f), new Vector3f(0), new Vector3f(0));
		this.editor = editor;
		setColor(new GColor(0, 0, 1));
		setOpacity(0.4f);
		min = new Vector3f(0);
		max = new Vector3f(0);

		blockArea = new Rectangle(new Vector3f(0, 0, 0.0999f), new Vector3f(0), new Vector3f(0));
		blockArea.setColor(new GColor(0, 0, 1));
		blockArea.setOpacity(0.4f);
		editor.addGObject(blockArea);
	}

	@Override
	public Vector3f getMin() {
		return min;
	}

	@Override
	public Vector3f getMax() {
		return max;
	}

	public boolean hasSelection() {
		return isVisible();
	}

	public boolean intersects(Chunk chunk) {
		return intersects(chunk.getPosition().x << 5, chunk.getPosition().y << 5, 32, 32,
				min.x, min.y, max.x - min.x, max.y - min.y);
	}

	public boolean intersects(Box box) {
		return intersects(box.getWorldPosition().x, box.getWorldPosition().y, 1, 1,
				min.x, min.y, max.x - min.x, max.y - min.y);
	}

	public boolean intersectsBox(Vector2i position) {
		return intersects(position.x, position.y, 1, 1,
				min.x, min.y, max.x - min.x, max.y - min.y);
	}

	public boolean intersectsBox(int x, int y) {
		return intersects(x, y, 1, 1, min.x, min.y, max.x - min.x, max.y - min.y);
	}

	private void fixArea() {
		float minX = Math.min(min.x, max.x);
		float maxX = Math.max(min.x, max.x);
		float minY = Math.min(min.y, max.y);
		float maxY = Math.max(min.y, max.y);
		float minZ = Math.min(min.z, max.z);
		float maxZ = Math.max(min.z, max.z);
		min.set(minX, minY, minZ);
		max.set(maxX, maxY, maxZ);
	}


	@Override
	public void onMouseEvent(MouseEvent event) {
		Camera camera = editor.getWorld().getCamera();
		if (event instanceof MousePressEvent) {
			if (((MousePressEvent) event).getMouseButton() == EnumMouseButton.SECONDARY) {
				camera.toGamePosition(event.getPosition(), 0f, min);
				max.set(min);
				setVisible(true);

				recalculateBlockArea(camera);
				blockArea.setVisible(true);

				setMin(camera.toEnginePosition(min));
				setMax(camera.toEnginePosition(max));
			}
		} else if (event instanceof MouseDragEvent) {
			if (((MouseDragEvent) event).getMouseButton() == EnumMouseButton.SECONDARY) {
				camera.toGamePosition(event.getPosition(), 0f, max);
				setMax(camera.toEnginePosition(max));
				recalculateBlockArea(camera);
			}
		} else if (event instanceof MouseReleaseEvent) {
			if (((MouseReleaseEvent) event).getMouseButton() == EnumMouseButton.SECONDARY) {
				camera.toGamePosition(event.getPosition(), 0f, max);
				setMax(camera.toEnginePosition(max));
				recalculateBlockArea(camera);
				fixArea();
			}
		}
	}

	private void recalculateBlockArea(Camera camera) {
		Vector3f min = new Vector3f(this.min).min(max).floor();
		Vector3f max = new Vector3f(this.max).max(this.min).ceil();
		blockArea.setMin(camera.toEnginePosition(min));
		blockArea.setMax(camera.toEnginePosition(max));
	}

	@Override
	public void onKeyboardEvent(KeyEvent event) {
		if (editor.getChat().isExtended()) return;
		if (event instanceof KeyPressEvent) {
			if (event.getKeyboardKey() == EnumKeyboardKey.E) {
				min.set(0);
				max.set(0);
				setVisible(false);
				blockArea.setVisible(false);
			} else if (event.getKeyboardKey() == EnumKeyboardKey.C) {
				if (Engine.getKeyboard().isKeyPressed(EnumKeyboardKey.LEFT_CONTROL)) {
					if (hasSelection()) {
						editor.getClipboard().copy(editor.getSelection());
						editor.getChat().addText("&bSelection copied to clipboard.");
					}
				}
			}
		}
	}

	@Override
	public void onTick2(long dif, Room room) {
		Camera camera = editor.getWorld().getCamera();
		setMin(camera.toEnginePosition(min));
		setMax(camera.toEnginePosition(max));
		recalculateBlockArea(camera);
	}

	private static boolean intersects(float ax, float ay, float aw, float ah, float bx, float by, float bw, float bh) {
		return bx + bw > ax && by + bh > ay && ax + aw > bx && ay + ah > by;
	}
}
