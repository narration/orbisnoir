package com.degoos.orbisnoir.editor.texture;

import com.degoos.graphicengine2.enums.EnumMouseButton;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.mouse.MousePressEvent;
import com.degoos.graphicengine2.event.mouse.MouseReleaseEvent;
import com.degoos.orbisnoir.editor.hotbar.Hotbar;
import com.degoos.orbisnoir.world.Box;
import org.joml.Vector2i;

import java.util.NoSuchElementException;

public class TextureHotbar extends Hotbar<Vector2i> {

	public TextureHotbar(TextureEditor editor, TextureBar bar) {
		super(editor, bar);
	}

	@Override
	public TextureEditor getEditor() {
		return (TextureEditor) super.getEditor();
	}

	@Override
	protected void loadIndices() {
		indices = new TextureHotbarIndex[7];
		for (int i = 0; i < indices.length; i++) {
			indices[i] = new TextureHotbarIndex(this, i);
			editor.addGObject(indices[i]);
		}
	}

	@Override
	public void onMouseEvent(MouseEvent event) {
		if (event instanceof MouseReleaseEvent) {
			DraggedTexture draggedTexture = getEditor().draggedTexture;
			if (draggedTexture == null) return;
			getEditor().setDraggedTexture(null);

			for (int i = 0; i < indices.length; i++)
				if (((TextureHotbarIndex) indices[i]).checkDraggedTexture(draggedTexture)) break;
		} else if (event instanceof MousePressEvent) {
			if (((MousePressEvent) event).getMouseButton() == EnumMouseButton.WHEEL) {
				try {
					Box box = editor.getWorld().getCamera().getPointedBox(editor.getWorld());
					getSelectedIndex().setElement(box.getPaletteTexture(), null);
					getSelectedIndex().refreshTexture();
				} catch (NoSuchElementException ex) {
					getSelectedIndex().setElement(null, null);
				}
			}
		} else super.onMouseEvent(event);
	}
}
