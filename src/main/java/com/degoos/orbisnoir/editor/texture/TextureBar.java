package com.degoos.orbisnoir.editor.texture;

import com.degoos.graphicengine2.object.GObject;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.object.texture.Palette;
import com.degoos.orbisnoir.editor.bar.Bar;
import com.degoos.orbisnoir.world.Chunk;
import org.joml.Vector2i;
import org.joml.Vector3f;

import java.util.HashMap;
import java.util.NoSuchElementException;

public class TextureBar extends Bar<Vector2i> {

	private Palette palette;
	private String paletteName;
	private Chunk chunk;

	public TextureBar(TextureEditor textureEditor, Palette palette, String paletteName) {
		super(textureEditor);
		this.palette = palette;
		this.paletteName = paletteName;
		this.elements = null;
		loadBlocks();
		scroll.setMaximumOffset(maximumY - 18);
		chunk = null;
	}

	public TextureEditor getEditor() {
		return (TextureEditor) super.getEditor();
	}

	public Palette getPalette() {
		return palette;
	}

	public String getPaletteName() {
		return paletteName;
	}

	public Chunk getChunk() {
		return chunk;
	}

	public boolean isExpanded() {
		return expanded;
	}

	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}
	private void loadBlocks() {
		if (elements != null) {
			elements.values().forEach(GObject::delete);
			elements.clear();
		} else elements = new HashMap<>();

		int barX = 0;
		int barY = 0;

		Vector2i size = palette.getPaletteSize();
		TextureBarElement block;
		for (int y = 0; y < size.y; y++) {
			for (int x = 0; x < size.x; x++) {
				block = new TextureBarElement(this, new Vector2i(x, y), barX, barY, palette.getTexture(x, y));
				editor.addGObject(block);
				elements.put(new Vector2i(barX, barY), block);

				barX++;
				if (barX == 6) {
					barX = 0;
					barY++;
				}
			}
		}
		maximumY = barX == 0 ? barY - 1 : barY;
	}

	@Override
	public void onTick(long dif, Room room) {
		super.onTick(dif, room);
		Vector3f pos = editor.getWorld().getCamera().toGamePosition(new Vector3f(0));
		try {
			Chunk chunk = editor.getWorld().getChunkByBlockPosition(new Vector2i((int) Math.floor(pos.x), (int) Math.floor(pos.y)));
			if (!chunk.getPalette().equals(this.palette)) {
				palette = chunk.getPalette();
				paletteName = chunk.getPaletteName();
				loadBlocks();
				this.chunk = chunk;
				((TextureEditor)editor).hotbar.refreshTextures();
			}
			else this.chunk = chunk;
		} catch (NoSuchElementException ignore) {
		}
	}
}
