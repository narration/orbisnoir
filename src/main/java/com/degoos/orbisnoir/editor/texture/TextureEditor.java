package com.degoos.orbisnoir.editor.texture;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.enums.EnumMouseButton;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.event.mouse.MouseDragEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.mouse.MousePressEvent;
import com.degoos.graphicengine2.util.object.Button;
import com.degoos.orbisnoir.chat.editorcommands.EditorCommands;
import com.degoos.orbisnoir.editor.Editor;
import com.degoos.orbisnoir.editor.Editors;
import com.degoos.orbisnoir.editor.WorldMover;
import com.degoos.orbisnoir.enums.EnumBlockCollision;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.util.VectorUtils;
import com.degoos.orbisnoir.world.Box;
import com.degoos.orbisnoir.world.Chunk;
import com.degoos.orbisnoir.world.World;
import com.degoos.orbisnoir.world.WorldManager;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import java.util.NoSuchElementException;

public class TextureEditor extends Editor {

	protected DraggedTexture draggedTexture;

	protected EditorSelection selection;
	protected TextureHotbar hotbar;
	protected TextureBar bar;
	protected EditorClipboard clipboard;
	protected Button toCollisionButton;

	public TextureEditor(World world) {
		super("texture editor", world);
		EditorCommands.addTextureCommands(chat.getCommands());

		draggedTexture = null;

		addGObject(new WorldMover(this));
		addGObject(clipboard = new EditorClipboard(this));
		addGObject(selection = new EditorSelection(this));

		try {
			addGObject(bar = new TextureBar(this,
					PaletteManager.loadPalette("outworld"), "outworld"));
			addGObject(hotbar = new TextureHotbar(this, bar));
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		addGObject(toCollisionButton = new TextureToCollisionButton(this));
	}

	public EditorClipboard getClipboard() {
		return clipboard;
	}

	public EditorSelection getSelection() {
		return selection;
	}

	public TextureHotbar getHotbar() {
		return hotbar;
	}

	public TextureBar getBar() {
		return bar;
	}

	public Button getToCollisionButton() {
		return toCollisionButton;
	}

	public void setDraggedTexture(DraggedTexture draggedTexture) {
		if (this.draggedTexture != null)
			this.draggedTexture.delete();
		this.draggedTexture = draggedTexture;
		if (draggedTexture != null)
			addGObject(draggedTexture);
	}

	@Override
	public void attach() {
		super.attach();
		world.getEntities().values().forEach(target -> {
			target.setSlept(true);
		});
	}

	@Override
	public void onMouseEvent(MouseEvent event) {
		super.onMouseEvent(event);
		if (!(event instanceof MouseDragEvent || event instanceof MousePressEvent) || draggedTexture != null || bar.isExpanded() &&
				bar.isInArea(event.getPosition()) || toCollisionButton.isInArea(event.getPosition()) || hotbar.isInArea(event.getPosition()))
			return;

		EnumMouseButton button;
		if (event instanceof MouseDragEvent) button = ((MouseDragEvent) event).getMouseButton();
		else button = ((MousePressEvent) event).getMouseButton();
		if (button != EnumMouseButton.PRIMARY) return;

		Vector3f floatPosition = world.getCamera().toGamePosition(new Vector3f(event.getPosition(), 0));
		Vector2i position = new Vector2i((int) Math.floor(floatPosition.x), (int) Math.floor(floatPosition.y));
		Vector2i chunkPosition = VectorUtils.shiftRight(position, 5, new Vector2i());
		Vector2i boxPosition = position.sub(VectorUtils.shiftLeft(chunkPosition, 5, new Vector2i()));

		Chunk chunk;

		try {
			chunk = world.getChunk(chunkPosition);
		} catch (NoSuchElementException ex) {
			chunk = new Chunk(world, chunkPosition, bar.getPalette(), bar.getPaletteName());
			world.registerChunk(chunk);
		}

		Box box;

		Vector2i paletteIndex = hotbar.getSelectedIndex().getElement();
		try {
			box = chunk.getBox(boxPosition);
			if (paletteIndex == null)
				chunk.unregisterBox(boxPosition);
			else
				box.setPaletteTexture(paletteIndex);
		} catch (NoSuchElementException ex) {
			if (paletteIndex == null) return;
			box = new Box(boxPosition, chunk, hotbar.getSelectedIndex().getElement(), EnumBlockCollision.LEVEL_0, null);
			chunk.registerBox(box);
		}

	}

	@Override
	public void onKeyEvent(KeyEvent event) {
		super.onKeyEvent(event);
		if (event instanceof KeyPressEvent && event.getKeyboardKey() == EnumKeyboardKey.P && !chat.isExtended()) {
			try {
				Editors.reset();
				Vector3f cameraPos = getWorld().getCamera().getPosition();
				Vector2f position = new Vector2f(cameraPos.x, cameraPos.y);
				World world = WorldManager.loadWorld(getWorld().getName(), position);
				Engine.setRoom(world);
				world.runInitScript();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
}
