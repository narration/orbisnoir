package com.degoos.orbisnoir.editor.texture;

import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.orbisnoir.editor.DraggedItem;
import org.joml.Vector2i;
import org.joml.Vector3f;

public class DraggedTexture extends DraggedItem<Vector2i> {

	public DraggedTexture(Vector3f position, Vector2i paletteIndex, ITexture texture) {
		super(position, paletteIndex, texture);
	}
}
