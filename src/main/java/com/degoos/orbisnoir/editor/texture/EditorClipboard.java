package com.degoos.orbisnoir.editor.texture;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.event.drop.DropFilesEvent;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.window.WindowResizeEvent;
import com.degoos.graphicengine2.object.GObject;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.object.texture.Palette;
import com.degoos.orbisnoir.chat.editorcommands.texture.ECTFill;
import com.degoos.orbisnoir.enums.EnumBlockCollision;
import com.degoos.orbisnoir.util.VectorUtils;
import com.degoos.orbisnoir.world.Box;
import com.degoos.orbisnoir.world.Chunk;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2i;
import org.joml.Vector3f;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

public class EditorClipboard extends GObject {

	private TextureEditor editor;

	private Map<Vector2i, Vector2i> indices;
	private Vector2i max;

	private Palette palette;
	private String paletteName;

	public EditorClipboard(TextureEditor editor) {
		this.editor = editor;
		indices = new HashMap<>();
	}


	public void copy(EditorSelection selection) {
		indices.clear();
		palette = null;

		Vector2i min = new Vector2i((int) Math.floor(selection.getMin().x), (int) Math.floor(selection.getMin().y));
		max = new Vector2i((int) Math.floor(selection.getMax().x), (int) Math.floor(selection.getMax().y));

		for (int x = min.x; x <= max.x; x++) {
			for (int y = min.y; y <= max.y; y++) {
				indices.put(new Vector2i(x - min.x, y - min.y), getTexture(editor.getWorld(), x, y));
			}
		}
		max = max.sub(min);
	}

	public void paste(Vector2i position) {
		Vector2i start = new Vector2i();
		VectorUtils.shiftLeft(VectorUtils.shiftRight(position, 5, start), 5, start);

		for (int x = start.x; x <= max.x + position.x; x += 32) {
			for (int y = start.y; y <= max.y + position.y; y += 32) {
				fillChunk(ECTFill
						.getOrCreateChunk(editor.getWorld(), new Vector2i(x >> 5, y >> 5), palette, paletteName), position);
			}
		}
	}


	private void fillChunk(Chunk chunk, Vector2i position) {
		Vector2i shift = VectorUtils.shiftLeft(chunk.getPosition(), 5, new Vector2i());
		Vector2i min = new Vector2i(position).sub(shift).max(new Vector2i(0));
		Vector2i max = new Vector2i(position).add(this.max).sub(shift).min(new Vector2i(31));

		Box box;
		Vector2i paletteIndex;
		for (int x = min.x; x <= max.x; x++) {
			for (int y = min.y; y <= max.y; y++) {
				box = chunk.getBox(x, y);

				paletteIndex = indices.get(new Vector2i(x, y).add(shift).sub(position));

				if (paletteIndex == null) {
					if (box != null)
						chunk.unregisterBox(box.getChunkPosition());
				} else {
					if (box != null)
						box.setPaletteTexture(paletteIndex);
					else {
						box = new Box(new Vector2i(x, y), chunk, paletteIndex, EnumBlockCollision.LEVEL_0, null);
						chunk.registerBox(box);
					}
				}
			}
		}
	}

	private Vector2i getTexture(World world, int x, int y) {
		try {
			Box box = world.getBox(x, y);
			if (palette != null) return box.getPaletteTexture();
			palette = box.getChunk().getPalette();
			paletteName = box.getChunk().getPaletteName();
			return box.getPaletteTexture();
		} catch (NoSuchElementException ex) {
			return null;
		}
	}

	@Override
	public void onTick(long l, Room room) {

	}

	@Override
	public void onTick2(long l, Room room) {

	}

	@Override
	public void onAsyncTick(long l, Room room) {

	}

	@Override
	public void onMouseEvent(MouseEvent mouseEvent) {

	}

	@Override
	public void onKeyboardEvent(KeyEvent keyEvent) {
		if (editor.getChat().isExtended()) return;
		if (keyEvent.getKeyboardKey() == EnumKeyboardKey.V && Engine.getKeyboard().isKeyPressed(EnumKeyboardKey.LEFT_CONTROL)) {
			if (!Engine.getRenderArea().isInside(Engine.getMousePosition())) return;
			Vector3f position = editor.getWorld().getCamera().toGamePosition(Engine.getMousePosition(), 0);
			paste(new Vector2i((int) Math.floor(position.x), (int) Math.floor(position.y)));
		}
	}

	@Override
	public void onResize(WindowResizeEvent windowResizeEvent) {

	}

	@Override
	public void onDropFilesEvent(DropFilesEvent dropFilesEvent) {

	}
}
