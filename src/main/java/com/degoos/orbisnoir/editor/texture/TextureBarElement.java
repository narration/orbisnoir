package com.degoos.orbisnoir.editor.texture;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumMouseButton;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.mouse.MousePressEvent;
import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.orbisnoir.editor.bar.BarElement;
import com.degoos.orbisnoir.util.KeyboardUtils;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

public class TextureBarElement extends BarElement<Vector2i> {

	public TextureBarElement(TextureBar textureBar, Vector2i paletteIndex, int x, int y, ITexture texture) {
		super(textureBar, paletteIndex, x, y, texture);
	}

	@Override
	public TextureBar getBar() {
		return (TextureBar) super.getBar();
	}

	@Override
	public void onMouseEvent(MouseEvent event) {
		if (bar.isExpanded() && event instanceof MousePressEvent && ((MousePressEvent) event).getMouseButton() == EnumMouseButton.PRIMARY) {
			Vector2f mouse = event.getPosition();
			if (isInArea(mouse)) {
				getBar().getEditor().setDraggedTexture(new DraggedTexture(new Vector3f(mouse, 0.999f), element, getTexture()));
			}
		}
	}

	@Override
	public void onKeyboardEvent(KeyEvent event) {
		if (!bar.getEditor().getChat().isExtended() && bar.getEditor().getChat().canUseKeyboard() && bar.isExpanded()
				&& isInArea(Engine.getMousePosition()) && event instanceof KeyPressEvent) {
			int selected = KeyboardUtils.getHotbarIndex(event.getKeyboardKey());
			if (selected == -1) return;
			getBar().getEditor().getHotbar().getIndices()[selected].setElement(element, null);
		}
	}
}
