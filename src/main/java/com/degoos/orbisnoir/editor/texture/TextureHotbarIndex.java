package com.degoos.orbisnoir.editor.texture;

import com.degoos.orbisnoir.editor.hotbar.HotbarIndex;
import com.degoos.orbisnoir.world.Chunk;
import org.joml.Vector2f;
import org.joml.Vector2i;

public class TextureHotbarIndex extends HotbarIndex<Vector2i> {

	public TextureHotbarIndex(TextureHotbar hotbar, int index) {
		super(hotbar, index);
	}

	public boolean checkDraggedTexture(DraggedTexture draggedTexture) {
		if (isInArea(new Vector2f(draggedTexture.getPosition().x, draggedTexture.getPosition().y))) {
			setElement(draggedTexture.getElement(), null);
			refreshTexture();
			return true;
		}
		return false;
	}

	@Override
	public void refreshTexture() {
		if(element == null) return;
		Chunk chunk = ((TextureHotbar) hotbar).getEditor().bar.getChunk();
		if(chunk == null) return;
		setTexture(chunk.getPalette().getTexture(element));
		setVisible(true);
	}
}
