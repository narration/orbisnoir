package com.degoos.orbisnoir.editor;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyHoldEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.window.WindowResizeEvent;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.object.Text;
import com.degoos.orbisnoir.chat.Chat;
import com.degoos.orbisnoir.handler.ZoomHandler;
import com.degoos.orbisnoir.util.GraphicUtils;
import com.degoos.orbisnoir.world.Box;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

public class Editor extends Room {

	protected World world;
	protected Chat chat;
	protected Text pointer;

	protected long tickDelay, drawDelay;
	protected float zoom;
	protected ZoomHandler zoomHandler;

	public Editor(String name, World world) {
		super(name);
		this.world = world;
		this.chat = new Chat(this);
		addGObject(chat);
		world.getPlayer().delete();
		world.getCamera().setTarget(null);

		GraphicUtils.addFPSDebugger(this);
		zoomHandler = null;

		pointer = new Text(Chat.FONT, new Vector3f(0, 0.95f, 0.95f), new Vector2f(-0.5f, 0.05f),
				new Vector2f(0.5f, 0), "()", GColor.WHITE, 1);
		pointer.setXCentered(true);
		pointer.setVisible(true);
		addGObject(pointer);
	}

	public World getWorld() {
		return world;
	}

	public Chat getChat() {
		return chat;
	}

	public long getDrawDelay() {
		return drawDelay;
	}

	public long getTickDelay() {
		return tickDelay;
	}

	public void attach() {
		zoom = world.getCamera().getBoxSize();
	}


	public void detach() {

	}

	//region gObject methods.

	@Override
	public void draw() {
		long n = System.nanoTime();
		world.draw();
		super.draw();
		drawDelay = System.nanoTime() - n;
	}

	@Override
	public void tick(long dif) {
		long n = System.nanoTime();
		world.tick(dif);
		super.tick(dif);
		tickDelay = System.nanoTime() - n;
		Vector3f position = world.getCamera().toGamePosition(Engine.getMousePosition(), 0);
		pointer.setText(String.format("(%.2f, %.2f)", position.x, position.y));
	}

	@Override
	public void onMouseEvent(MouseEvent event) {
		world.onMouseEvent(event);
		super.onMouseEvent(event);
	}

	@Override
	public void onKeyEvent(KeyEvent event) {
		world.onKeyEvent(event);
		super.onKeyEvent(event);
		if (!chat.isExtended() && chat.canUseKeyboard() && (event instanceof KeyPressEvent || event instanceof KeyHoldEvent)) {
			if (zoomHandler != null) {
				removeObject(zoomHandler);
				zoomHandler = null;
			}
			if (event.getKeyboardKey() == EnumKeyboardKey.O)
				zoom = zoom * 1.1f;
			else if (event.getKeyboardKey() == EnumKeyboardKey.L)
				zoom = zoom / 1.1f;
			zoomHandler = new ZoomHandler(zoom, 100, null, world.getCamera());
			addGObject(zoomHandler);
		}
	}

	@Override
	public void onResizeEvent(WindowResizeEvent event) {
		world.onResizeEvent(event);
		super.onResizeEvent(event);
	}

	//endregion
}
