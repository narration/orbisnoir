package com.degoos.orbisnoir.editor.entity;

import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.mouse.MousePressEvent;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Text;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.chat.Chat;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.texture.TextureManager;
import org.joml.Vector2f;
import org.joml.Vector3f;

public class SelectedEntityBarOptionDelete extends SelectedEntitySimpleBarOption {

	private Text text;
	private Entity entity;
	private SelectedEntityBar bar;

	public SelectedEntityBarOptionDelete(Entity entity, SelectedEntityBar bar, int index) {
		super(bar, "", index);
		setVisible(true);
		setTexture(TextureManager.loadImageOrNull("gui/editor/selected_entity_bar_input.png"));
		setParent(bar);
		this.bar = bar;
		this.entity = entity;
		this.text = new Text(Chat.FONT, new Vector3f(0.401f, 0, 0.01f), new Area(0, 0.03f, 0.36f, 0.07f),
				"DELETE", new GColor(1f, 0.3f, 0.3f), 1);
		this.text.setXCentered(true);
		this.text.setVisible(true);
		this.text.setParent(this);
		bar.editor.addGObject(this.text);
		setColor(new GColor(1, 0.3f, 0.3f));
	}

	@Override
	public void onMouseEvent(MouseEvent event) {
		if (!(event instanceof MousePressEvent)) return;
		Vector2f vector = event.getPosition();
		Vector3f position = getAbsolutePosition();
		if (position.x + min.x <= vector.x && position.x + max.x >= vector.x
				&& position.y + min.y <= vector.y && position.y + max.y >= vector.y) {
			entity.deleteEditorHints();
			entity.delete();
			entity.getWorld().getEntitySketchContainer().getSketches().remove(entity.getSketch());
			bar.editor.selectEntity(null);
		}
	}

	@Override
	public void delete() {
		super.delete();
		text.delete();
	}
}
