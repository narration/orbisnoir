package com.degoos.orbisnoir.editor.entity;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Text;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.chat.Chat;
import org.joml.Vector3f;

public class SelectedEntitySimpleBarOption extends Rectangle {

	protected Text name;

	public SelectedEntitySimpleBarOption(SelectedEntityBar bar, String name, int index) {
		super(new Vector3f(0, 1.9f - (index * 0.1f), 0.01f), new Vector3f(0.4f, 0.02f, 0), new Vector3f(0.77f, 0.08f, 0));
		setParent(bar);
		this.name = new Text(Chat.FONT, new Vector3f(0.05f, 0, 0.01f), new Area(0.03f, 0.03f, 0.27f, 0.07f),
				name, new GColor(0.7f, 0.7f, 0.7f), 1);
		this.name.setVisible(true);
		this.name.setParent(this);
		bar.editor.addGObject(this.name);
	}

	@Override
	public void delete() {
		super.delete();
		name.delete();
	}
}
