package com.degoos.orbisnoir.editor.entity;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.event.drop.DropFilesEvent;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.window.WindowResizeEvent;
import com.degoos.graphicengine2.object.GObject;
import com.degoos.graphicengine2.object.Room;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.entity.sketch.EntitySketch;
import com.degoos.orbisnoir.entity.sketch.EntitySketchManager;
import org.joml.Vector2f;
import org.joml.Vector3f;

public class EntityClipboard extends GObject {

	EntityEditor editor;
	EntitySketch<?> entitySketch;

	public EntityClipboard(EntityEditor editor) {
		this.editor = editor;
		entitySketch = null;
	}

	@Override
	public void onTick(long l, Room room) {

	}

	@Override
	public void onTick2(long l, Room room) {

	}

	@Override
	public void onAsyncTick(long l, Room room) {

	}

	@Override
	public void onMouseEvent(MouseEvent mouseEvent) {

	}

	@Override
	public void onKeyboardEvent(KeyEvent keyEvent) {
		if (!Engine.getKeyboard().isKeyPressed(EnumKeyboardKey.LEFT_CONTROL) &&
				!Engine.getKeyboard().isKeyPressed(EnumKeyboardKey.RIGHT_CONTROL))
			return;
		if (keyEvent.getKeyboardKey() == EnumKeyboardKey.C) {
			SelectedEntityBar bar = editor.selectedEntityBar;
			if (bar == null) return;
			entitySketch = bar.entity.getSketch();
		} else if (keyEvent.getKeyboardKey() == EnumKeyboardKey.V) {
			EntitySketch sketch = copy();
			if (sketch == null) return;
			Entity entity = copy().toEntity(editor.getWorld());
			editor.getWorld().registerEntity(entity);
			editor.getWorld().getEntitySketchContainer().getSketches().add(sketch);
			entity.displayEditorHints();
			entity.setSlept(true);
		}
	}

	@Override
	public void onResize(WindowResizeEvent windowResizeEvent) {

	}

	@Override
	public void onDropFilesEvent(DropFilesEvent dropFilesEvent) {

	}

	public EntitySketch<?> copy() {
		if (entitySketch == null) return null;
		EntitySketch<?> sketch = EntitySketchManager.createSketch(entitySketch.copyMap());
		if (sketch == null) return null;
		Vector3f position = editor.getWorld().getCamera().toGamePosition(Engine.getMousePosition(), 0);
		sketch.setPosition(new Vector2f(position.x, position.y));
		return sketch;
	}

}
