package com.degoos.orbisnoir.editor.entity;

import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.chat.Chat;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.gui.text.KeyboardInput;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptManager;
import com.degoos.orbisnoir.util.NumericUtils;
import org.joml.Vector3f;

import java.util.Map;

public class SelectedEntityBarOptionInput extends KeyboardInput {

	private SelectedEntityBarOption option;
	private Entity entity;

	private Map map;
	private String mapKey;
	private Class<?> type;
	private boolean position;
	private boolean selected;

	public SelectedEntityBarOptionInput(Entity entity, SelectedEntityBarOption option, Map map, String mapKey, String value, Class<?> type, boolean position) {
		super(Chat.FONT, new Vector3f(0.401f, 0, 0.01f), new Area(0.03f, 0.03f, 0.36f, 0.07f),
				value, new GColor(0.7f, 0.7f, 0.7f), 1);
		this.entity = entity;
		this.option = option;
		this.map = map;
		this.mapKey = mapKey;
		this.type = type;
		this.position = position;
		selected = false;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		if (selected == this.selected) return;
		this.selected = selected;
		if (!selected) {
			setText(map.get(mapKey).toString());
			setColor(new GColor(0.7f, 0.7f, 0.7f));
		}
	}

	@Override
	public void onKeyboardEvent(KeyEvent event) {
		if (!selected) return;
		super.onKeyboardEvent(event);
		if (!(event instanceof KeyPressEvent)) {
			if (event.getKeyboardKey() == EnumKeyboardKey.ENTER) {
				if (getColor().equals(GColor.RED)) return;
				selected = false;
				option.setColor(new GColor(0.7f, 0.7f, 0.7f));
				setColor(new GColor(0.7f, 0.7f, 0.7f));
				Object o = null;
				if (type.equals(int.class) || type.equals(long.class)) {
					o = Long.valueOf(getText());
				} else if (type.equals(float.class) || type.equals(double.class)) {
					o = Double.valueOf(getText());
				} else if (type.equals(String.class) || type.isEnum() || type.equals(Script.class)) {
					o = getText();
				} else if (type.equals(boolean.class)) {
					o = Boolean.parseBoolean(getText());
				}
				map.put(mapKey, o);
				entity.getSketch().refreshFromMap();
				entity.restoreToSketch();
				return;
			}
			if (type.equals(int.class)) {
				setColor(NumericUtils.isInteger(getText()) ? GColor.GREEN : GColor.RED);
			} else if (type.equals(long.class)) {
				setColor(NumericUtils.isLong(getText()) ? GColor.GREEN : GColor.RED);
			} else if (type.equals(float.class)) {
				setColor(NumericUtils.isFloat(getText()) ? GColor.GREEN : GColor.RED);
			} else if (type.equals(double.class)) {
				setColor(NumericUtils.isDouble(getText()) ? GColor.GREEN : GColor.RED);
			} else if (type.equals(boolean.class)) {
				setColor(getText().equalsIgnoreCase("true") || getText().equalsIgnoreCase("false") ? GColor.GREEN : GColor.RED);
			} else if (type.equals(String.class)) {
				setColor(GColor.GREEN);
			} else if (type.equals(Script.class)) {
				setColor(ScriptManager.getScriptUnsafe(getText()) != null ? GColor.GREEN : GColor.RED);
			} else if (type.isEnum()) {
				try {
					Enum.valueOf((Class<? extends Enum>) type, getText());
					setColor(GColor.GREEN);
				} catch (Exception ex) {
					setColor(GColor.RED);
				}
			}
		}
	}

	@Override
	public void onTick(long dif, Room room) {
		if (position) {
			setText(map.get(mapKey).toString());
		}
	}
}
