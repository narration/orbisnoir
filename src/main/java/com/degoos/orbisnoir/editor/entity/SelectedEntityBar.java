package com.degoos.orbisnoir.editor.entity;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumWheelDirection;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.mouse.MouseWheelEvent;
import com.degoos.graphicengine2.object.GObject;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Room;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.texture.TextureManager;
import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SelectedEntityBar extends Rectangle {

	protected EntityEditor editor;
	protected Entity entity;
	protected List<SelectedEntitySimpleBarOption> options;

	public SelectedEntityBar(EntityEditor editor, Entity entity) {
		super(new Vector3f(Engine.getRenderArea().getMin(), 0.9f), new Vector3f(), new Vector3f(0.8f, 2f, 0));
		this.editor = editor;
		this.entity = entity;
		this.options = new ArrayList<>();

		setVisible(true);
		setTexture(TextureManager.loadImageOrNull("gui/editor/selected_entity_bar.png"));
		int index = loadMap(entity, 0, entity.getSketch().getJsonMap(), 0, entity.getSketch().getTypesMap(), "");
		SelectedEntityBarOptionDelete delete = new SelectedEntityBarOptionDelete(entity, this, index);
		editor.addGObject(delete);
		options.add(delete);
	}

	private int loadMap(Entity entity, int index, Map map, int sub, Map<String, Class<?>> types, String subname) {
		SelectedEntitySimpleBarOption option;
		Object value;
		StringBuilder builder;
		for (Object key : map.keySet()) {
			if (key.equals("type")) continue;
			value = map.get(key);

			builder = new StringBuilder();
			for (int i = 0; i < sub; i++)
				builder.append("    ");

			if (value instanceof Map) {
				option = new SelectedEntitySimpleBarOption(this, builder.append(key).toString(), index);
				editor.addGObject(option);
				options.add(option);
				index = loadMap(entity, index + 1, (Map) value, sub + 1, types, subname.isEmpty() ? key.toString() : subname + "_" + key);
				continue;
			}
			option = new SelectedEntityBarOption(entity, this, map,
					key.toString(), builder.append(key).toString(), value,
					types.get(subname.isEmpty() ? key : subname + "_" + key), index, subname.equals("position"));
			editor.addGObject(option);
			options.add(option);
			index++;
		}
		return index;
	}

	@Override
	public void onTick(long dif, Room room) {
		setPosition(new Vector3f(Engine.getRenderArea().getMin(), 0.9f));
	}


	@Override
	public void onMouseEvent(MouseEvent event) {
		if (event instanceof MouseWheelEvent && isInArea(event.getPosition())) {
			float add;
			if (((MouseWheelEvent) event).getWheelDirection() == EnumWheelDirection.UP) add = -0.1f;
			else add = 0.1f;

			options.forEach(target -> target.setPosition(target.getPosition().add(0, add, 0)));
		}
	}

	@Override
	public void delete() {
		super.delete();
		options.forEach(GObject::delete);
		options.clear();
	}
}
