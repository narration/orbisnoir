package com.degoos.orbisnoir.editor.entity;

import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.mouse.MousePressEvent;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.texture.TextureManager;
import org.joml.Vector2f;
import org.joml.Vector3f;

import java.util.Map;

public class SelectedEntityBarOption extends SelectedEntitySimpleBarOption {

	protected SelectedEntityBarOptionInput input;

	public SelectedEntityBarOption(Entity entity, SelectedEntityBar bar, Map map, String mapKey, String key, Object value, Class<?> type, int index, boolean position) {
		super(bar, key, index);
		setVisible(true);
		setTexture(TextureManager.loadImageOrNull("gui/editor/selected_entity_bar_input.png"));
		setParent(bar);

		this.input = new SelectedEntityBarOptionInput(entity, this, map, mapKey, value == null ? "null" : value.toString(), type, position);
		this.input.setVisible(true);
		this.input.setParent(this);
		bar.editor.addGObject(this.input);
		setColor(new GColor(0.7f, 0.7f, 0.7f));
	}

	@Override
	public void onMouseEvent(MouseEvent event) {
		if (!(event instanceof MousePressEvent)) return;
		Vector2f vector = event.getPosition();
		Vector3f position = getAbsolutePosition();
		input.setSelected(position.x + min.x <= vector.x && position.x + max.x >= vector.x
				&& position.y + min.y <= vector.y && position.y + max.y >= vector.y);
		setColor(input.isSelected() ? GColor.WHITE : new GColor(0.7f, 0.7f, 0.7f));
	}

	@Override
	public void delete() {
		super.delete();
		input.delete();
	}
}
