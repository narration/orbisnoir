package com.degoos.orbisnoir.editor.entity;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumMouseButton;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.mouse.MousePressEvent;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.object.Text;
import com.degoos.orbisnoir.chat.Chat;
import com.degoos.orbisnoir.editor.bar.BarElement;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.entity.sketch.EntitySketch;
import com.degoos.orbisnoir.entity.sketch.EntitySketchPackage;
import com.degoos.orbisnoir.util.KeyboardUtils;
import com.degoos.orbisnoir.util.VectorUtils;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.joml.Vector3f;

public class EntityBarElement extends BarElement<EntitySketchPackage> {

	private Text text;

	public EntityBarElement(EntityBar bar, EntitySketchPackage element, int x, int y) {
		super(bar, element, x, y, null);
		setColor(element.getColor());

		text = new Text(Chat.FONT, new Vector3f(getPosition()), VectorUtils.toVector2f(getMin()),
				VectorUtils.toVector2f(getMax()), element.getAbbreviation(), GColor.WHITE, 1);
		text.getPosition().z += 0.0001f;
		text.setVisible(true);
		bar.getEditor().addGObject(text);
	}

	@Override
	public EntityBar getBar() {
		return (EntityBar) super.getBar();
	}

	@Override
	public void onTick2(long dif, Room room) {
		super.onTick2(dif, room);
		if (!text.getPosition().equals(getPosition())) {
			text.getPosition().set(getPosition());
			text.getPosition().z += 0.0001f;
			text.setRequiresRecalculation(true);
		}
	}

	@Override
	public void onMouseEvent(MouseEvent event) {
		if (bar.isExpanded() && event instanceof MousePressEvent && ((MousePressEvent) event).getMouseButton() == EnumMouseButton.PRIMARY) {
			Vector2f mouse = event.getPosition();
			if (isInArea(mouse)) {
				try {
					World world = bar.getEditor().getWorld();
					EntitySketch sketch = element.getEntityClass().newInstance();
					Vector3f vector =world.getCamera().toGamePosition(Engine.getMousePosition(), 0);
					sketch.setPosition(new Vector2f(vector.x, vector.y));
					Entity entity = sketch.toEntity(bar.getEditor().getWorld());
					world.registerEntity(entity);
					world.getEntitySketchContainer().getSketches().add(sketch);
					getBar().getEditor().setDraggedEntity(entity);
					entity.setSlept(true);

					bar.getEditor().getWorld().getEntities().values().forEach(Entity::displayEditorHints);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}
}
