package com.degoos.orbisnoir.editor.entity;

import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.orbisnoir.editor.bar.Bar;
import com.degoos.orbisnoir.entity.sketch.EntitySketchManager;
import com.degoos.orbisnoir.entity.sketch.EntitySketchPackage;
import org.joml.Vector2i;

import java.util.HashMap;

public class EntityBar extends Bar<EntitySketchPackage> {

	public EntityBar(EntityEditor entityEditor) {
		super(entityEditor);

		loadEntities();
		scroll.setMaximumOffset(maximumY - 18);
	}

	@Override
	public EntityEditor getEditor() {
		return (EntityEditor) super.getEditor();
	}

	private void loadEntities() {
		elements = new HashMap<>();

		int barX = 0;
		int barY = 0;

		EntityBarElement element;
		for (EntitySketchPackage entitySketch : EntitySketchManager.getSketches()) {
			element = new EntityBarElement(this, entitySketch, barX, barY);
			editor.addGObject(element);
			elements.put(new Vector2i(barX, barY), element);

			barX++;
			if (barX == 6) {
				barX = 0;
				barY++;
			}
		}
		maximumY = barX == 0 ? barY - 1 : barY;
	}

	@Override
	public void onKeyboardEvent(KeyEvent event) {
		if (!editor.getChat().isExtended() && event instanceof KeyPressEvent && event.getKeyboardKey() == EnumKeyboardKey.M && getEditor().noneSelected) {
			expanded = !expanded;
		}
	}
}
