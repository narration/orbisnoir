package com.degoos.orbisnoir.editor.entity;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.event.mouse.MouseClickEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.mouse.MouseMoveEvent;
import com.degoos.graphicengine2.event.mouse.MouseReleaseEvent;
import com.degoos.orbisnoir.chat.editorcommands.EditorCommands;
import com.degoos.orbisnoir.editor.Editor;
import com.degoos.orbisnoir.editor.Editors;
import com.degoos.orbisnoir.editor.WorldMover;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.world.World;
import com.degoos.orbisnoir.world.WorldManager;
import org.joml.Vector2f;
import org.joml.Vector3f;

import java.util.Objects;

public class EntityEditor extends Editor {

	EntityBar bar;
	EntityClipboard clipboard;
	SelectedEntityBar selectedEntityBar;
	Entity draggedEntity;
	EntityToScriptButton entityToScriptButton;
	boolean noneSelected;

	public EntityEditor(World world) {
		super("entity editor", world);
		EditorCommands.addEntityCommands(chat.getCommands());
		bar = new EntityBar(this);
		clipboard = new EntityClipboard(this);
		selectedEntityBar = null;
		draggedEntity = null;

		addGObject(bar);
		addGObject(clipboard);
		addGObject(new WorldMover(this));
		addGObject(entityToScriptButton = new EntityToScriptButton(this));
	}

	public EntityToScriptButton getEntityToScriptButton() {
		return entityToScriptButton;
	}

	public Entity getDraggedEntity() {
		return draggedEntity;
	}

	public void setDraggedEntity(Entity draggedEntity) {
		this.draggedEntity = draggedEntity;
	}

	public void selectEntity(Entity selectedEntity) {
		if (selectedEntityBar != null) {
			selectedEntityBar.delete();
			selectedEntityBar = null;
		}
		if (selectedEntity != null) {
			selectedEntityBar = new SelectedEntityBar(this, selectedEntity);
			addGObject(selectedEntityBar);
		}
	}

	@Override
	public void attach() {
		super.attach();
		world.getEntities().values().forEach(target -> {
			target.displayEditorHints();
			target.setSlept(true);
		});
	}

	@Override
	public void detach() {
		world.getEntities().values().forEach(Entity::deleteEditorHints);
	}

	@Override
	public void onMouseEvent(MouseEvent event) {
		super.onMouseEvent(event);
		if (bar.isInArea(event.getPosition()) || entityToScriptButton.isInArea(event.getPosition()) ||
				selectedEntityBar != null && selectedEntityBar.isInArea(event.getPosition()))
			return;

		if (event instanceof MouseMoveEvent && draggedEntity != null) {
			Vector3f vector = world.getCamera().toGamePosition(event.getPosition(), 0);
			draggedEntity.getPosition().set(vector.x, vector.y);
			draggedEntity.getSketch().setPosition(new Vector2f(vector.x, vector.y));
		} else if (event instanceof MouseReleaseEvent) {
			draggedEntity = null;
		} else if (event instanceof MouseClickEvent) {
			Vector3f vector3 = world.getCamera().toGamePosition(event.getPosition(), 0);
			Vector2f vector = new Vector2f(vector3.x, vector3.y);
			Entity entity = world.getEntities().values().stream().filter(target -> target.getEntitySize()
					.isInside(new Vector2f(vector).sub(target.getPosition()))).findAny().orElse(null);
			if (!Objects.equals(entity, this.selectedEntityBar == null ? null : this.selectedEntityBar.entity)) {
				selectEntity(entity);
			}
			setDraggedEntity(entity);
		}
	}

	@Override
	public void onKeyEvent(KeyEvent event) {
		noneSelected = selectedEntityBar == null || selectedEntityBar.options.stream().noneMatch(target ->
				target instanceof SelectedEntityBarOption && ((SelectedEntityBarOption) target).input.isSelected());
		chat.setCanUseKeyboard(noneSelected);
		super.onKeyEvent(event);
		if (event instanceof KeyPressEvent && event.getKeyboardKey() == EnumKeyboardKey.P && !chat.isExtended() && noneSelected) {
			try {
				Editors.reset();
				Vector3f cameraPos = getWorld().getCamera().getPosition();
				Vector2f position = new Vector2f(cameraPos.x, cameraPos.y);
				World world = WorldManager.loadWorld(getWorld().getName(), position);
				Engine.setRoom(world);
				world.runInitScript();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
}
