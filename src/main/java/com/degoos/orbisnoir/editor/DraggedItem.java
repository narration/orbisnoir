package com.degoos.orbisnoir.editor;

import com.degoos.graphicengine2.event.mouse.MouseDragEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.texture.ITexture;
import org.joml.Vector3f;

public class DraggedItem<E> extends Rectangle {

	private E element;

	public DraggedItem(Vector3f position, E element, ITexture texture) {
		super(position, new Vector3f(-0.15f, -0.3f, 0), new Vector3f(0.15f, 0, 0));
		this.element = element;
		setTexture(texture);
		setVisible(true);
	}

	public E getElement() {
		return element;
	}

	@Override
	public void onMouseEvent(MouseEvent event) {
		if (event instanceof MouseDragEvent) {
			float d = event.getPosition().x - position.x;
			setRotation((-200 * d) * 0.3f + getRotation() * 0.7f);
			setPosition(new Vector3f(event.getPosition(), 0.9032f));
		}
	}
}
