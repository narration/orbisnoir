package com.degoos.orbisnoir.editor.bar;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.object.texture.ITexture;
import org.joml.Vector2i;
import org.joml.Vector3f;

public class BarElement<E> extends Rectangle {

	protected Bar bar;
	protected E element;

	protected float previousM;
	protected Vector2i barPosition;
	protected int offset;

	private float expandOffset;

	public BarElement(Bar bar, E element, int x, int y, ITexture texture) {
		super(new Vector3f(0, 0, 0.901f), new Vector3f(), new Vector3f(0.09f, 0.09f, 0));
		setVisible((y + offset) < 19 && (y + offset) >= 0);
		setTexture(texture);
		this.bar = bar;
		this.element = element;

		this.barPosition = new Vector2i(x, y);
		this.offset = 0;

		this.expandOffset = bar.expandOffset;

		float m = previousM = Engine.getRenderArea().getMax().x;
		float minX = m - 0.85f + 0.05f;
		position.x = minX + (0.8f * x / 6) + bar.expandOffset;
		position.y = 0.85f - ((y + offset) * 0.1f);
	}

	public BarElement(Bar bar, E element, int y, ITexture texture) {
		super(new Vector3f(0, 0, 0.901f), new Vector3f(), new Vector3f(0.7f, 0.07f, 0));
		setVisible((y + offset) < 19 && (y + offset) >= 0);
		setTexture(texture);
		this.bar = bar;
		this.element = element;

		this.barPosition = new Vector2i(0, y);
		this.offset = 0;

		this.expandOffset = bar.expandOffset;

		float m = previousM = Engine.getRenderArea().getMax().x;
		float minX = m - 0.85f + 0.05f;
		position.x = minX + bar.expandOffset;
		position.y = 0.85f - ((y + offset) * 0.07f);
	}

	public Bar getBar() {
		return bar;
	}

	public E getElement() {
		return element;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		setVisible((barPosition.y + offset) < 19 && (barPosition.y + offset) >= 0);
		this.offset = offset;
		position.y = 0.85f - ((barPosition.y + offset) * 0.1f);
		requiresRecalculation = true;
	}

	@Override
	public void onTick2(long dif, Room room) {
		//Cannot use onresize for some reason.
		float m = Engine.getRenderArea().getMax().x;

		if (m == previousM && expandOffset == bar.expandOffset) return;
		float minX = m - 0.85f + 0.05f;

		position.x = minX + (0.8f * barPosition.x / 6) + bar.expandOffset;
		position.y = 0.85f - ((barPosition.y + offset) * 0.1f);
		requiresRecalculation = true;

		expandOffset = bar.expandOffset;
		previousM = m;
	}
}
