package com.degoos.orbisnoir.editor.bar;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.event.window.WindowResizeEvent;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Room;
import org.joml.Vector3f;

public class BarScroll extends Rectangle {

	private Bar bar;
	private float maximumOffset;
	private float size;

	private float expandOffset;

	public BarScroll(Bar bar, int maximumOffset) {
		super(new Vector3f(0, 0, 0.901f), new Vector3f(-0.02f, 0, 0), new Vector3f(0f, 0.3f, 0));
		this.bar = bar;
		this.maximumOffset = maximumOffset;
		this.expandOffset = bar.expandOffset;

		position.x = Engine.getRenderArea().getMax().x + expandOffset;
		position.y = 0.66f;

		size = 2 - 0.34f - 0.04f;

		setVisible(true);
		setColor(new GColor(0.75f, 0.75f, 0.75f));
	}

	public void setOffset(int offset) {
		position.y = 0.66f - size * (offset / maximumOffset);
		requiresRecalculation = true;
	}

	public void setMaximumOffset(float maximumOffset) {
		this.maximumOffset = maximumOffset;
	}

	@Override
	public void onTick2(long dif, Room room) {
		if (expandOffset != bar.expandOffset) {
			expandOffset = bar.expandOffset;
			position.x = Engine.getRenderArea().getMax().x + expandOffset;
			requiresRecalculation = true;
		}
	}

	@Override
	public void onResize(WindowResizeEvent event) {
		position.x = event.getNewRenderArea().getMax().x + expandOffset;
		requiresRecalculation = true;
	}
}
