package com.degoos.orbisnoir.editor.bar;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.enums.EnumWheelDirection;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.mouse.MouseWheelEvent;
import com.degoos.graphicengine2.event.window.WindowResizeEvent;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Room;
import com.degoos.orbisnoir.editor.Editor;
import com.degoos.orbisnoir.texture.TextureManager;
import org.joml.Vector2i;
import org.joml.Vector3f;

import java.util.Map;

public class Bar<E> extends Rectangle {

	protected Editor editor;
	protected int offset;
	protected int maximumY;

	protected Map<Vector2i, BarElement<E>> elements;

	protected BarScroll scroll;

	protected boolean expanded;
	protected float expandOffset;

	public Bar(Editor editor) {
		super(new Vector3f(0, 0, 0.9f), new Vector3f(0), new Vector3f(0));
		this.editor = editor;
		this.offset = 0;
		this.maximumY = 0;

		this.elements = null;

		this.expanded = true;
		this.expandOffset = 0;

		float m = Engine.getRenderArea().getMax().x;
		min.x = m - 0.9f;
		min.y = -1;
		max.x = m;
		max.y = 1;

		setVisible(true);
		setTexture(TextureManager.loadImageOrNull("gui/editor/block_bar.png"));

		scroll = new BarScroll(this, maximumY - 18);
		editor.addGObject(scroll);
	}

	public Editor getEditor() {
		return editor;
	}

	public boolean isExpanded() {
		return expanded;
	}

	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}

	@Override
	public void onTick(long dif, Room room) {
		if (expanded) {
			if (expandOffset > 0) {
				expandOffset -= 4 * dif / 1000000000f;
				if (expandOffset < 0) expandOffset = 0;
				position.x = expandOffset;
				requiresRecalculation = true;
			}
		} else {
			if (expandOffset < 1) {
				expandOffset += 4 * dif / 1000000000f;
				if (expandOffset > 1) expandOffset = 1;
				position.x = expandOffset;
				requiresRecalculation = true;
			}
		}
	}

	@Override
	public void onMouseEvent(MouseEvent event) {
		if (maximumY > 18 && expanded && event instanceof MouseWheelEvent && isInArea(event.getPosition())) {
			boolean control = Engine.getKeyboard().isKeyPressed(EnumKeyboardKey.RIGHT_CONTROL) || Engine.getKeyboard().isKeyPressed(EnumKeyboardKey.LEFT_CONTROL);
			if (((MouseWheelEvent) event).getWheelDirection() == EnumWheelDirection.UP) {
				if (control) offset -= 2;
				else offset--;
			} else {
				if (control) offset += 2;
				else offset++;
			}
			if (offset < 0) offset = 0;
			if (offset > maximumY - 18) offset = maximumY - 18;
			scroll.setOffset(offset);
			elements.values().forEach(target -> target.setOffset(-offset));
		}
	}

	@Override
	public void onKeyboardEvent(KeyEvent event) {
		if (!editor.getChat().isExtended() && event instanceof KeyPressEvent && event.getKeyboardKey() == EnumKeyboardKey.M) {
			expanded = !expanded;
		}
	}

	@Override
	public void onResize(WindowResizeEvent event) {
		float m = event.getNewRenderArea().getMax().x;
		min.x = m - 0.9f;
		min.y = -1;
		max.x = m;
		max.y = 1;
		requiresRecalculation = true;
	}
}
