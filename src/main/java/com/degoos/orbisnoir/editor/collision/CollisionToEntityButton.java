package com.degoos.orbisnoir.editor.collision;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.object.Text;
import com.degoos.orbisnoir.chat.Chat;
import com.degoos.orbisnoir.editor.EditorButton;
import com.degoos.orbisnoir.editor.Editors;
import com.degoos.orbisnoir.editor.entity.EntityEditor;
import com.degoos.orbisnoir.util.VectorUtils;
import org.joml.Vector3f;

public class CollisionToEntityButton extends EditorButton {

	private CollisionEditor editor;
	private float lastSize;

	private Text text;

	public CollisionToEntityButton(CollisionEditor editor) {
		super(new Vector3f(Engine.getRenderArea().getMin().x + 0.1f, -0.9f, 0.5f), new Vector3f(0),
				new Vector3f(0.15f, 0.15f, 0), editor);
		this.editor = editor;
		lastSize = Engine.getRenderArea().getMin().x;
		setVisible(true);

		setColor(new GColor(0.5f, 0.5f, 0.5f));

		text = new Text(Chat.FONT, new Vector3f(getPosition()), VectorUtils.toVector2f(getMin()),
				VectorUtils.toVector2f(getMax()), "Ent", GColor.WHITE, 1);
		text.getPosition().z += 0.001f;
		text.setVisible(true);
		editor.addGObject(text);
	}

	@Override
	public void press() {
		if (Editors.entityEditor == null) Editors.entityEditor = new EntityEditor(editor.getWorld());
		Editors.collisionEditor = editor;
		editor.detach();
		Editors.entityEditor.attach();
		Engine.setRoom(Editors.entityEditor);
	}

	@Override
	public void onTick2(long dif, Room room) {
		float f = Engine.getRenderArea().getMin().x;
		if (lastSize != f) {
			lastSize = f;
			getPosition().x = f + 0.1f;
			requiresRecalculation = true;

			text.getPosition().x = getPosition().x;
			text.setRequiresRecalculation(true);
		}
	}
}
