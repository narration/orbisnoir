package com.degoos.orbisnoir.editor.collision;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Text;
import com.degoos.orbisnoir.chat.Chat;
import com.degoos.orbisnoir.editor.DraggedItem;
import com.degoos.orbisnoir.editor.Editor;
import com.degoos.orbisnoir.enums.EnumBlockCollision;
import com.degoos.orbisnoir.util.VectorUtils;
import org.joml.Vector3f;

import java.awt.*;

public class DraggedCollision extends DraggedItem<EnumBlockCollision> {

	private Text text;

	public DraggedCollision(Editor editor, Vector3f position, EnumBlockCollision element) {
		super(position, element, null);


		setColor(element.getColor());
		text = new Text(Chat.FONT, new Vector3f(getPosition()), VectorUtils.toVector2f(getMin()),
				VectorUtils.toVector2f(getMax()), element.getAbbreviation(), GColor.WHITE, 1);
		text.getPosition().z += 0.0001;
		text.setVisible(true);
		editor.addGObject(text);
	}


	@Override
	public DraggedCollision setPosition(Vector3f position) {
		super.setPosition(position);
		text.getPosition().set(position);
		text.getPosition().z += 0.0001;
		text.setRequiresRecalculation(true);
		return this;
	}

	@Override
	public void setRotation(float rotation) {
		super.setRotation(rotation);
		text.setRotation(rotation);
	}

	@Override
	public void delete() {
		super.delete();
		text.delete();
	}
}
