package com.degoos.orbisnoir.editor.collision;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.enums.EnumMouseButton;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.event.mouse.MouseDragEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.mouse.MousePressEvent;
import com.degoos.graphicengine2.util.object.Button;
import com.degoos.orbisnoir.chat.editorcommands.EditorCommands;
import com.degoos.orbisnoir.editor.Editor;
import com.degoos.orbisnoir.editor.Editors;
import com.degoos.orbisnoir.editor.WorldMover;
import com.degoos.orbisnoir.enums.EnumBlockCollision;
import com.degoos.orbisnoir.util.VectorUtils;
import com.degoos.orbisnoir.world.Box;
import com.degoos.orbisnoir.world.Chunk;
import com.degoos.orbisnoir.world.World;
import com.degoos.orbisnoir.world.WorldManager;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import java.util.NoSuchElementException;

public class CollisionEditor extends Editor {

	protected DraggedCollision draggedCollision;

	private CollisionBar bar;
	private CollisionHotbar hotbar;
	private Button toTextureButton;

	public CollisionEditor(World world) {
		super("collision editor", world);
		EditorCommands.addCollisionCommands(chat.getCommands());
		draggedCollision = null;

		bar = new CollisionBar(this);
		hotbar = new CollisionHotbar(this, bar);
		toTextureButton = new CollisionToEntityButton(this);

		addGObject(bar);
		addGObject(hotbar);
		addGObject(toTextureButton);

		addGObject(new WorldMover(this));
	}

	public CollisionBar getBar() {
		return bar;
	}

	public CollisionHotbar getHotbar() {
		return hotbar;
	}

	public Button getToTextureButton() {
		return toTextureButton;
	}

	public void setDraggedCollision(DraggedCollision draggedCollision) {
		if (this.draggedCollision != null)
			this.draggedCollision.delete();
		this.draggedCollision = draggedCollision;
		if (draggedCollision != null)
			addGObject(draggedCollision);
	}

	@Override
	public void attach() {
		super.attach();
		world.getEntities().values().forEach(target -> {
			target.setSlept(true);
		});
	}

	@Override
	public void onMouseEvent(MouseEvent event) {
		super.onMouseEvent(event);
		if (!(event instanceof MouseDragEvent || event instanceof MousePressEvent) || draggedCollision != null || bar.isExpanded() &&
				bar.isInArea(event.getPosition()) || toTextureButton.isInArea(event.getPosition()) || hotbar.isInArea(event.getPosition()))
			return;

		EnumMouseButton button;
		if (event instanceof MouseDragEvent) button = ((MouseDragEvent) event).getMouseButton();
		else button = ((MousePressEvent) event).getMouseButton();
		if (button != EnumMouseButton.PRIMARY) return;

		Vector3f floatPosition = world.getCamera().toGamePosition(new Vector3f(event.getPosition(), 0));
		Vector2i position = new Vector2i((int) Math.floor(floatPosition.x), (int) Math.floor(floatPosition.y));
		Vector2i chunkPosition = VectorUtils.shiftRight(position, 5, new Vector2i());
		Vector2i boxPosition = position.sub(VectorUtils.shiftLeft(chunkPosition, 5, new Vector2i()));

		Chunk chunk;

		try {
			chunk = world.getChunk(chunkPosition);
		} catch (NoSuchElementException ex) {
			return;
		}

		Box box;

		EnumBlockCollision collision = hotbar.getSelectedIndex().getElement();
		if (collision == null) return;
		try {
			box = chunk.getBox(boxPosition);
			box.setCollision(collision);
		} catch (NoSuchElementException ignore) {
		}
	}

	@Override
	public void onKeyEvent(KeyEvent event) {
		super.onKeyEvent(event);
		if (event instanceof KeyPressEvent && event.getKeyboardKey() == EnumKeyboardKey.P && !chat.isExtended()) {
			try {
				Editors.reset();
				Vector3f cameraPos = getWorld().getCamera().getPosition();
				Vector2f position = new Vector2f(cameraPos.x, cameraPos.y);
				World world = WorldManager.loadWorld(getWorld().getName(), position);
				Engine.setRoom(world);
				world.runInitScript();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
}
