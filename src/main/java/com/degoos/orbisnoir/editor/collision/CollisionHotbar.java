package com.degoos.orbisnoir.editor.collision;

import com.degoos.graphicengine2.enums.EnumMouseButton;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.mouse.MousePressEvent;
import com.degoos.graphicengine2.event.mouse.MouseReleaseEvent;
import com.degoos.orbisnoir.editor.hotbar.Hotbar;
import com.degoos.orbisnoir.enums.EnumBlockCollision;
import com.degoos.orbisnoir.world.Box;

import java.util.NoSuchElementException;

public class CollisionHotbar extends Hotbar<EnumBlockCollision> {

	public CollisionHotbar(CollisionEditor editor, CollisionBar bar) {
		super(editor, bar);
	}

	@Override
	public CollisionEditor getEditor() {
		return (CollisionEditor) super.getEditor();
	}

	@Override
	protected void loadIndices() {
		indices = new CollisionHotbarIndex[7];
		for (int i = 0; i < indices.length; i++) {
			indices[i] = new CollisionHotbarIndex(this, editor, i);
			editor.addGObject(indices[i]);
		}
	}

	@Override
	public CollisionHotbarIndex getSelectedIndex() {
		return (CollisionHotbarIndex) super.getSelectedIndex();
	}

	@Override
	public void onMouseEvent(MouseEvent event) {
		if (event instanceof MouseReleaseEvent) {
			DraggedCollision draggedCollision = getEditor().draggedCollision;
			if (draggedCollision == null) return;
			getEditor().setDraggedCollision(null);

			for (int i = 0; i < indices.length; i++)
				if (((CollisionHotbarIndex) indices[i]).checkDraggedCollision(draggedCollision)) break;
		} else if (event instanceof MousePressEvent) {
			if (((MousePressEvent) event).getMouseButton() == EnumMouseButton.WHEEL) {
				try {
					Box box = editor.getWorld().getCamera().getPointedBox(editor.getWorld());
					getSelectedIndex().setElement(box.getCollision());
				} catch (NoSuchElementException ex) {
					getSelectedIndex().setElement(null, null);
				}
			}
		} else super.onMouseEvent(event);
	}
}
