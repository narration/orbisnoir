package com.degoos.orbisnoir.editor.collision;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Text;
import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.orbisnoir.chat.Chat;
import com.degoos.orbisnoir.editor.Editor;
import com.degoos.orbisnoir.editor.hotbar.HotbarIndex;
import com.degoos.orbisnoir.enums.EnumBlockCollision;
import com.degoos.orbisnoir.util.VectorUtils;
import org.joml.Vector2f;
import org.joml.Vector3f;

public class CollisionHotbarIndex extends HotbarIndex<EnumBlockCollision> {

	private Text text;

	public CollisionHotbarIndex(CollisionHotbar hotbar, Editor editor, int index) {
		super(hotbar, index);
		text = new Text(Chat.FONT, new Vector3f(getPosition()), VectorUtils.toVector2f(getMin()), VectorUtils.toVector2f(getMax()),
				"", GColor.WHITE, 1);
		text.getPosition().z += 0.00001f;
		editor.addGObject(text);
	}

	public boolean checkDraggedCollision(DraggedCollision draggedCollision) {
		if (isInArea(new Vector2f(draggedCollision.getPosition().x, draggedCollision.getPosition().y))) {
			setElement(draggedCollision.getElement());
			return true;
		}
		return false;
	}

	@Override
	public void setElement(EnumBlockCollision element, ITexture texture) {
		this.element = element;
		setTexture(texture);
		setVisible(texture != null);
		text.setVisible(isVisible());
	}

	public void setElement(EnumBlockCollision element) {
		this.element = element;
		setTexture(null);
		setVisible(true);
		text.setVisible(true);

		setColor(element.getColor());
		text.setText(element.getAbbreviation());
	}
}
