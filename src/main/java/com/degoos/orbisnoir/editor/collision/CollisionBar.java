package com.degoos.orbisnoir.editor.collision;

import com.degoos.orbisnoir.editor.bar.Bar;
import com.degoos.orbisnoir.enums.EnumBlockCollision;
import org.joml.Vector2i;

import java.util.HashMap;

public class CollisionBar extends Bar<EnumBlockCollision> {

	public CollisionBar(CollisionEditor collisionEditor) {
		super(collisionEditor);

		loadCollisions();
		scroll.setMaximumOffset(maximumY - 18);
	}

	@Override
	public CollisionEditor getEditor() {
		return (CollisionEditor) super.getEditor();
	}

	private void loadCollisions() {
		elements = new HashMap<>();

		int barX = 0;
		int barY = 0;

		CollisionBarElement element;
		for (EnumBlockCollision collision : EnumBlockCollision.values()) {
			element = new CollisionBarElement(this, collision, barX, barY);
			editor.addGObject(element);
			elements.put(new Vector2i(barX, barY), element);

			barX++;
			if (barX == 6) {
				barX = 0;
				barY++;
			}
		}
		maximumY = barX == 0 ? barY - 1 : barY;
	}
}
