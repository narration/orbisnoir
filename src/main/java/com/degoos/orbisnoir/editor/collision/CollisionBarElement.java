package com.degoos.orbisnoir.editor.collision;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumMouseButton;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.mouse.MousePressEvent;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.object.Text;
import com.degoos.orbisnoir.chat.Chat;
import com.degoos.orbisnoir.editor.bar.BarElement;
import com.degoos.orbisnoir.enums.EnumBlockCollision;
import com.degoos.orbisnoir.util.KeyboardUtils;
import com.degoos.orbisnoir.util.VectorUtils;
import org.joml.Vector2f;
import org.joml.Vector3f;

public class CollisionBarElement extends BarElement<EnumBlockCollision> {

	private Text text;

	public CollisionBarElement(CollisionBar bar, EnumBlockCollision element, int x, int y) {
		super(bar, element, x, y, null);
		setColor(element.getColor());

		text = new Text(Chat.FONT, new Vector3f(getPosition()), VectorUtils.toVector2f(getMin()),
				VectorUtils.toVector2f(getMax()), element.getAbbreviation(), GColor.WHITE, 1);
		text.getPosition().z += 0.0001f;
		text.setVisible(true);
		bar.getEditor().addGObject(text);
	}

	@Override
	public CollisionBar getBar() {
		return (CollisionBar) super.getBar();
	}

	@Override
	public void onTick2(long dif, Room room) {
		super.onTick2(dif, room);
		if (!text.getPosition().equals(getPosition())) {
			text.getPosition().set(getPosition());
			text.getPosition().z += 0.0001f;
			text.setRequiresRecalculation(true);
		}
	}

	@Override
	public void onMouseEvent(MouseEvent event) {
		if (bar.isExpanded() && event instanceof MousePressEvent && ((MousePressEvent) event).getMouseButton() == EnumMouseButton.PRIMARY) {
			Vector2f mouse = event.getPosition();
			if (isInArea(mouse)) {
				getBar().getEditor().setDraggedCollision(new DraggedCollision(getBar().getEditor(),
						new Vector3f(mouse, 0.999f), element));
			}
		}
	}

	@Override
	public void onKeyboardEvent(KeyEvent event) {
		if (!bar.getEditor().getChat().isExtended() && bar.getEditor().getChat().canUseKeyboard() && bar.isExpanded()
				&& isInArea(Engine.getMousePosition()) && event instanceof KeyPressEvent) {
			int selected = KeyboardUtils.getHotbarIndex(event.getKeyboardKey());
			if (selected == -1) return;
			((CollisionHotbarIndex) getBar().getEditor().getHotbar().getIndices()[selected]).setElement(element);
		}
	}
}
