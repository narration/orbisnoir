package com.degoos.orbisnoir.editor;

import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.util.object.Button;
import org.joml.Vector3f;

public abstract class EditorButton extends Button {

	private Editor editor;

	public EditorButton(Vector3f position, Vector3f min, Vector3f max, Editor editor) {
		super(position, min, max);
		this.editor = editor;
	}


	@Override
	public void onKeyboardEvent(KeyEvent event) {
		if (event instanceof KeyPressEvent && event.getKeyboardKey() == EnumKeyboardKey.K && editor.chat.canUseKeyboard() && !editor.chat.isExtended()) {
			press();
		}
	}
}
