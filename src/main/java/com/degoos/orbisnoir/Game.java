package com.degoos.orbisnoir;

import com.degoos.graphicengine2.Engine;
import com.degoos.orbisnoir.cache.Cache;
import com.degoos.orbisnoir.entity.sketch.EntitySketchManager;
import com.degoos.orbisnoir.gui.diary.DiaryManager;
import com.degoos.orbisnoir.gui.main.MainMenu;
import com.degoos.orbisnoir.save.Save;
import com.degoos.orbisnoir.script.ScriptManager;
import com.degoos.orbisnoir.sound.SoundLoader;
import com.degoos.orbisnoir.world.WorldManager;
import org.joml.Vector2i;

import java.awt.*;
import java.io.File;
import java.util.Arrays;

public class Game {

	private static boolean ide;
	private static Cache cache;
	private static Save save;

	public static void main(String[] args) {
		System.out.println("Cargando juego...");
		ide = Arrays.stream(args).anyMatch(target -> target.equalsIgnoreCase("-ide"));
		Engine.init(new Vector2i(1920 / 2, 1080 / 2), false, "Orbis Noir", () -> {
			if (!loadFont()) return;
			save = new Save(new File("save.dat"), true);
			cache = new Cache();
			ScriptManager.load();
			SoundLoader.load();
			EntitySketchManager.load();
			WorldManager.load();
			DiaryManager.load();
			Engine.setRoom(new MainMenu());
		});
	}

	public static boolean isIde() {
		return ide;
	}

	public static Cache getCache() {
		return cache;
	}

	public static Save getSave() {
		return save;
	}

	public static void setSave(Save save) {
		Game.save = save;
	}

	private static boolean loadFont() {
		try {
			Engine.getFontManager().addFont("Game", Font.createFont(Font.TRUETYPE_FONT, Engine.getResourceManager()
					.getResourceInputStream("text/Game.ttf")).deriveFont(100f));
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			Engine.getRenderer().kill();
			return false;
		}
	}
}
