package com.degoos.orbisnoir.entity.player;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.event.keyboard.KeyReleaseEvent;
import com.degoos.graphicengine2.io.Keyboard;
import com.degoos.graphicengine2.sound.SoundSource;
import com.degoos.orbisnoir.Game;
import com.degoos.orbisnoir.cache.Cache;
import com.degoos.orbisnoir.chat.editorcommands.script.ECSReloadScripts;
import com.degoos.orbisnoir.collision.Collision;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.editor.entity.EntityEditor;
import com.degoos.orbisnoir.entity.Controller;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.entity.MovableEntity;
import com.degoos.orbisnoir.enums.EnumDirection;
import com.degoos.orbisnoir.game.entity.level3.FrenchWarrior;
import com.degoos.orbisnoir.game.entity.level3.SpanishWarrior;
import com.degoos.orbisnoir.game.entity.level3.boss.sub.RunningWarrior;
import com.degoos.orbisnoir.gui.diary.DiaryRoom;
import com.degoos.orbisnoir.gui.pause.PauseMenu;
import com.degoos.orbisnoir.world.World;
import com.degoos.orbisnoir.world.WorldManager;
import org.joml.Vector2f;
import org.joml.Vector2i;

import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


public class PlayerController implements Controller {

	private static final EnumKeyboardKey[] KEYS = {EnumKeyboardKey.ARROW_UP, EnumKeyboardKey.ARROW_DOWN,
			EnumKeyboardKey.ARROW_LEFT, EnumKeyboardKey.ARROW_RIGHT};

	private Keyboard keyboard;
	private List<EnumKeyboardKey> keys;
	private boolean canBeControlled;

	private CollisionBox interactBox;
	private Entity looking;
	private MovableEntity controlable;


	public PlayerController() {
		keys = new ArrayList<>();
		keyboard = Engine.getKeyboard();
		canBeControlled = true;
		interactBox = new CollisionBox(new Vector2f(0), new Vector2f(-0.7f, -0.7f), new Vector2f(0.7f, 0.7f));
		looking = null;
	}

	public boolean canBeControlled() {
		return canBeControlled;
	}

	public void setCanBeControlled(boolean canBeControlled) {
		this.canBeControlled = canBeControlled;
		System.out.println("Can be controller is now " + canBeControlled);
	}

	@Override
	public void onTick(Entity entity, long dif) {
		if (!canBeControlled) {
			if (entity instanceof Player && ((Player) entity).isSurfing()) {
				surf((Player) entity, null, dif);
				return;
			}
			return;
		}

		keys.removeIf(key -> !keyboard.isKeyPressed(key));

		for (EnumKeyboardKey key : KEYS) {
			if (keyboard.isKeyPressed(key) && !keys.contains(key))
				keys.add(key);
		}

		EnumKeyboardKey x = null, y = null;

		EnumKeyboardKey key;
		for (int i = keys.size() - 1; i >= 0; i--) {
			key = keys.get(i);
			if (key == EnumKeyboardKey.ARROW_UP || key == EnumKeyboardKey.ARROW_DOWN) {
				if (y != null) continue;
				y = key;
			} else if (key == EnumKeyboardKey.ARROW_RIGHT || key == EnumKeyboardKey.ARROW_LEFT) {
				if (x != null) continue;
				x = key;
			}
		}

		if (x == null) {
			x = y;
			y = null;
		}

		if (entity instanceof Player && ((Player) entity).getShield().isActive()) x = null;

		if (entity instanceof Player && ((Player) entity).isSurfing()) {
			surf((Player) entity, x, dif);
			return;
		}

		if (x == null) {
			if (entity instanceof Player && ((Player) entity).getControlling() != null)
				((Player) entity).getControlling().setMoving(false);
			else if (entity instanceof MovableEntity) ((MovableEntity) entity).setMoving(false);
			return;
		}

		boolean running = keyboard.isKeyPressed(EnumKeyboardKey.LEFT_CONTROL)
				|| keyboard.isKeyPressed(EnumKeyboardKey.RIGHT_CONTROL)
				|| keyboard.isKeyPressed(EnumKeyboardKey.Z);

		if (entity.getWorld().getGUI().getMoveButton().isActive())
			entity.getWorld().getGUI().getMoveButton().disappear();
		if (entity.getWorld().getGUI().getRunButton().isActive() && running)
			entity.getWorld().getGUI().getRunButton().disappear();
		if (entity instanceof Player && ((Player) entity).getControlling() != null) {
			((Player) entity).getControlling().setRunning(running);
			((Player) entity).getControlling().move(EnumDirection.getByKeys(x, y)
					.orElse(EnumDirection.NONE), dif, true);
			checkMovement(((Player) entity).getControlling());
		} else if (entity instanceof MovableEntity) {
			((MovableEntity) entity).setRunning(running);
			((MovableEntity) entity).move(EnumDirection.getByKeys(x, y).orElse(EnumDirection.NONE), dif, true);
			checkMovement((MovableEntity) entity);
		} else
			entity.move(new Vector2f(EnumDirection.getByKeys(x, y).orElse(EnumDirection.NONE).getRelative())
					.mul(dif * 4f / 1000000000f), true);
	}

	private void surf(Player player, EnumKeyboardKey x, long dif) {
		player.setDirection(EnumDirection.UP);
		if (x == EnumKeyboardKey.ARROW_RIGHT)
			player.surf(1, dif);
		else if (x == EnumKeyboardKey.ARROW_LEFT)
			player.surf(-1, dif);
		else player.surf(0, dif);
	}

	@Override
	public void keyEvent(Entity entity, KeyEvent event) {
		if (!canBeControlled) return;

		if (event instanceof KeyReleaseEvent && entity instanceof Player && event.getKeyboardKey() == EnumKeyboardKey.C) {
			((Player) entity).getShield().setActive(false);
		}

		if (!(event instanceof KeyPressEvent)) return;
		EnumKeyboardKey key = event.getKeyboardKey();
		if (key == EnumKeyboardKey.X) {
			if (looking == null) return;
			if (entity instanceof MovableEntity)
				((MovableEntity) entity).setMoving(false);
			Cache cache = new Cache();
			if (looking.getName() != null)
				cache.put("trigger", looking.getName());
			else cache.put("trigger", "null");
			cache.put("trigger_type", "entity");
			cache.put("position_x", entity.getPosition().x);
			cache.put("position_y", entity.getPosition().y);
			entity.getWorld().getGUI().getInteractButton().disappear();
			entity.getWorld().getScriptContainer().runScript(looking.getScript(), scriptExecuter -> {
				if (entity instanceof MovableEntity) checkMovement((MovableEntity) entity);
			}, cache);
		} else if (event.getKeyboardKey() == EnumKeyboardKey.D) {
			if (Game.getSave().diary_unlocked && Engine.getRoom() instanceof World) {
				entity.getWorld().getGUI().getDiaryButton().disappear();
				if (entity instanceof MovableEntity) {
					((MovableEntity) entity).setMoving(false);
					((MovableEntity) entity).setMoving(false);
				}
				Engine.setRoom(new DiaryRoom(entity.getWorld()));
			}
		} else if (event.getKeyboardKey() == EnumKeyboardKey.C) {
			if (entity instanceof Player && ((Player) entity).getControlling() == null
					&& Game.getSave().shield_unlocked) {
				((Player) entity).getShield().setActive(true);
				entity.getWorld().getGUI().getShieldButton().disappear();
			}
		} else if (event.getKeyboardKey() == EnumKeyboardKey.S) {
			if (entity instanceof Player) {
				if (controlable != null) {
					if (controlable instanceof RunningWarrior) {
						((RunningWarrior) controlable).invert();
						return;
					}
					if (((Player) entity).getControlling() != null) {
						((Player) entity).getControlling().setMoving(false);
					}
					((Player) entity).setControlling(controlable);
					((Player) entity).setMoving(false);
					((Player) entity).setCasting(true);
					entity.getWorld().getCamera().setTarget(controlable);
					entity.getWorld().getGUI().getControlButton().disappear();
					SoundSource source = Engine.getSoundManager()
							.createSoundSource("sound/ring_theme_2.ogg", "control", false, false);
					source.setPitch(0.7f);
					source.play();
				} else {
					if (!((Player) entity).canUncontrol()) return;
					if (((Player) entity).getControlling() != null) {
						((Player) entity).getControlling().setMoving(false);
						((Player) entity).setControlling(null);
						((Player) entity).setCasting(false);
						entity.getWorld().getCamera().setTarget(entity);
					}
				}
			}
		} else if (event.getKeyboardKey() == EnumKeyboardKey.ESCAPE) {
			Engine.setRoom(new PauseMenu(entity.getWorld()));
		} else if (Game.isIde() && event.getKeyboardKey() == EnumKeyboardKey.P) {
			try {
				EntityEditor editor = new EntityEditor(WorldManager.loadWorld(entity.getWorld().getName(),
						new Vector2f(entity.getPosition())));
				editor.attach();
				Engine.setRoom(editor);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		//DEBUG
		else if (event.getKeyboardKey() == EnumKeyboardKey.A && Game.isIde()) {
			((Player) entity).setSurfing(!((Player) entity).isSurfing());
		} else if (event.getKeyboardKey() == EnumKeyboardKey.R && Game.isIde()) {
			ECSReloadScripts.reloadScripts(entity.getWorld());
		}

	}

	private void checkMovement(MovableEntity entity) {
		if (entity instanceof Player && ((Player) entity).getControlling() != null)
			interactBox.setPosition(new Vector2f(((Player) entity).getControlling().getDirection().getRelative()).mul(0.7f)
					.add(((Player) entity).getControlling().getPosition()));
		else
			interactBox.setPosition(new Vector2f(entity.getDirection().getRelative()).add(entity.getPosition()));
		Collision collision;
		looking = null;
		for (Entity target : entity.getWorld().getEntities().values()) {
			if (target.getScript() == null) continue;
			collision = target.getCollisionBox().collision(interactBox);
			if (collision != null) {
				looking = target;
				break;
			}
		}
		if (looking == null) entity.getWorld().getGUI().getInteractButton().disappear();
		else entity.getWorld().getGUI().getInteractButton().appear();
		if (Game.getSave().french_controller_unlocked || Game.getSave().spanish_controller_unlocked)
			checkControl(entity, entity.getDirection().getRelative());
	}

	private void checkControl(Entity ent, Vector2i relative) {
		Entity entity;
		if (ent instanceof Player && ((Player) ent).getControlling() != null)
			entity = ((Player) ent).getControlling();
		else entity = ent;
		controlable = null;
		Vector2f pos = new Vector2f(entity.getPosition()).add(entity.getEntitySize().getMid().x, 0.1f);
		Line2D.Float line = new Line2D.Float(pos.x, pos.y, pos.x + relative.x * 5, pos.y + relative.y * 5);
		if (Game.getSave().french_controller_unlocked) {
			Set<FrenchWarrior> warriors = entity.getWorld().getEntities().values().stream()
					.filter(target -> target instanceof FrenchWarrior)
					.map(target -> (FrenchWarrior) target).collect(Collectors.toSet());
			Optional<FrenchWarrior> optional = warriors.stream().filter(target ->
					target != entity && target.getCollisionBox().collidesWithLine(line)).findAny();
			if (optional.isPresent()) {
				controlable = optional.get();
				entity.getWorld().getGUI().getControlButton().appear();
				return;
			}
		}
		if (Game.getSave().spanish_controller_unlocked) {
			Set<SpanishWarrior> warriors = entity.getWorld().getEntities().values().stream()
					.filter(target -> target instanceof SpanishWarrior)
					.map(target -> (SpanishWarrior) target).collect(Collectors.toSet());
			warriors.stream().filter(target -> target != entity && target.getCollisionBox().collidesWithLine(line))
					.findAny().ifPresent(spanishWarrior -> controlable = spanishWarrior);
		}
		if (controlable == null)
			entity.getWorld().getGUI().getControlButton().disappear();
		else entity.getWorld().getGUI().getControlButton().appear();
	}
}
