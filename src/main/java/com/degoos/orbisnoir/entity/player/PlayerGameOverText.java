package com.degoos.orbisnoir.entity.player;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.object.Text;
import com.degoos.graphicengine2.util.Area;
import org.joml.Vector3f;

public class PlayerGameOverText extends Text {

	private long nanos;
	private Vector3f position;

	public PlayerGameOverText(Vector3f position) {
		super(Engine.getFontManager().getFontUnsafe("Game"), position, new Area(-1, -0.5f, 1, 0.5f),
				"GAME OVER", GColor.WHITE, 0);
		position.z = 0.8f;
		setKey("game_over");
		setVisible(true);
		this.position = position;
	}

	@Override
	public void onTick(long dif, Room room) {
		super.onTick(dif, room);
		nanos += dif;

		float cos = (float) Math.cos(nanos / 550000000d);
		setPosition(new Vector3f(position).add(0, cos * 0.1f, 0));
		setRotation((float) Math.cos(nanos / 650000000d) * 3);
	}
}
