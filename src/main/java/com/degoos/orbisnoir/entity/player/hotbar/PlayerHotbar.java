package com.degoos.orbisnoir.entity.player.hotbar;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.event.window.WindowResizeEvent;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.orbisnoir.Game;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.texture.TextureManager;
import com.degoos.orbisnoir.util.NumericUtils;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector3f;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class PlayerHotbar extends Rectangle {

	private World world;
	private List<PlayerHotbarElement> elements;

	public PlayerHotbar(World world) {
		super(new Vector3f(Engine.getRenderArea().getMax(), 0.8805f), new Vector3f(0), new Vector3f(0));
		this.world = world;
		elements = new LinkedList<>();
		loadElements();
	}

	private void loadElements() {
		if (Game.getSave().hotbar.isEmpty()) return;
		String[] sl = Game.getSave().hotbar.split(";");
		String[] sll;
		PlayerHotbarElement element;
		for (String s : sl) {
			if (s.isEmpty()) continue;
			sll = s.split(":");
			if (sll[0].isEmpty() || sll[1].isEmpty() || sll[2].isEmpty() || !NumericUtils.isInteger(sll[2])
					|| sll[3].isEmpty()) continue;
			element = new PlayerHotbarElement(this, elements.size(),
					getImage(sll[1], sll[0], Integer.valueOf(sll[2])), sll[3], s);
			elements.add(element);
			world.addGObject(element);
		}
	}

	public ITexture getImage(String option, String value, int paletteIndex) {
		try {
			switch (option) {
				case "null":
					return null;
				case "palette":
					return PaletteManager.loadPalette(value).getTexture(paletteIndex);
				case "palette-animation":
					return PaletteManager.loadPalette(value).getAnimation(paletteIndex);
				case "image":
					return TextureManager.loadImage(value);
				case "animation":
					return TextureManager.loadAnimation(value);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new NullPointerException();
		}
		return null;
	}

	public World getWorld() {
		return world;
	}

	public int getSize() {
		return elements.size();
	}

	public void addElement(ITexture texture, String hotbarKey, String fullValue) {
		if (elements.stream().anyMatch(target -> target.getHotbarKey().equals(hotbarKey))) return;
		PlayerHotbarElement element = new PlayerHotbarElement(this, elements.size(), texture, hotbarKey, fullValue);
		elements.add(element);
		world.addGObject(element);
		if (Game.getSave().hotbar.isEmpty()) Game.getSave().hotbar = fullValue;
		else Game.getSave().hotbar += ";" + fullValue;
	}

	public void removeElement(String hotbarKey) {
		Iterator<PlayerHotbarElement> iterator = elements.iterator();
		PlayerHotbarElement element;
		while (iterator.hasNext()) {
			element = iterator.next();
			if (element.getHotbarKey().equals(hotbarKey)) {
				element.delete();
				iterator.remove();
				elements.forEach(PlayerHotbarElement::moveToCorrectPosition);
				Game.getSave().hotbar = Game.getSave().hotbar.replaceFirst(element.getFullValue(), "");
				break;
			}
		}
	}

	public boolean contains(String hotbarKey) {
		return elements.stream().anyMatch(target -> target.getHotbarKey().equals(hotbarKey));
	}

	@Override
	public void onResize(WindowResizeEvent event) {
		position.x = event.getNewRenderArea().getMax().x;
		requiresRecalculation = true;
	}
}
