package com.degoos.orbisnoir.entity.player;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.GObject;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.object.texture.Animation;
import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.orbisnoir.editor.Editor;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.texture.TextureManager;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector3f;

public class PlayerHealth extends Rectangle {

	private float relative;

	private double health;
	private Player player;

	public PlayerHealth(Player player) {
		super(new Vector3f(0, 0, 0.88f), new Vector3f(0, -1, 0), new Vector3f(0, 1, 0));
		this.player = player;
		this.health = 1;
		setVisible(true);
		relative = 0;


		setTexture(TextureManager.loadImageOrNull("gui/blood.png"));

		setOpacity(0);
	}

	public double getHealth() {
		return health;
	}

	public void setHealth(double health) {
		if (Engine.getRoom() instanceof Editor) return;
		this.health = Math.max(0, Math.min(health, 1));
		if (health == 0) gameOver();
	}

	public void damage(double damage) {
		if (Engine.getRoom() instanceof Editor) return;

		if (player.getShield().isActive()) {
			player.getShield().damage(damage / 5);
			return;
		}
		this.health -= damage;
		health = Math.max(0, Math.min(health, 1));
		if (health == 0) gameOver();
	}

	public void gameOver() {
		World world = player.getWorld();
		world.setBackground(GColor.BLACK);
		if (player.getController() instanceof PlayerController)
			((PlayerController) player.getController()).setCanBeControlled(false);
		player.setSurfing(false);
		world.getScriptContainer().stopAllScripts();
		try {
			Engine.getSoundManager().cleanUpAllSoundSources();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		world.getEntities().values().stream().filter(target -> !target.equals(player)).forEach(Entity::delete);
		world.getObjects().stream().filter(target -> !target.equals(player.getRectangle()) && !target.equals(world.getBlackScreen())).forEach(GObject::delete);
		ITexture texture = player.getRectangle().getTexture();
		if (texture instanceof Animation)
			player.getRectangle().setTexture(((Animation) texture).getTextureList()
					.get(((Animation) texture).getImageSequence().get(((Animation) texture).getCurrentFrame())));
		world.addGObject(new PlayerGameOver(world));
	}

	@Override
	public void onTick(long dif, Room room) {
		damage(-0.0001 * dif / 1000000);
		setOpacity(1 - (float) health);
	}

	@Override
	public void onTick2(long dif, Room room) {
		if (relative != Engine.getRenderArea().getMin().x) {
			relative = Engine.getRenderArea().getMin().x;
			min.x = -relative;
			max.x = relative;
			requiresRecalculation = true;
		}
	}
}
