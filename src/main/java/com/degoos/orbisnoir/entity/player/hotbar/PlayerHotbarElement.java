package com.degoos.orbisnoir.entity.player.hotbar;

import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.object.texture.TextureAbsImpl;
import com.degoos.orbisnoir.texture.TextureManager;
import org.joml.Vector3f;

public class PlayerHotbarElement extends Rectangle {

	private static final float SIZE = 0.15f;

	private PlayerHotbar hotbar;
	private int index;
	private String hotbarKey;
	private String fullValue;

	private Rectangle image;

	public PlayerHotbarElement(PlayerHotbar hotbar, int index, ITexture texture, String hotbarKey, String fullValue) {
		super(new Vector3f(0), new Vector3f(0, -SIZE, 0), new Vector3f(SIZE, 0, 0));
		setVisible(true);
		setParent(hotbar);

		this.hotbar = hotbar;
		this.index = index;
		this.hotbarKey = hotbarKey;
		this.fullValue = fullValue;

		image = new Rectangle(new Vector3f(0, 0, 0.00001f), new Vector3f(0.02f, -SIZE + 0.02f, 0),
				new Vector3f(SIZE - 0.02f, -0.02f, 0));
		image.setVisible(true);
		image.setParent(this);
		image.setTexture(texture);
		hotbar.getWorld().addGObject(image);
		hotbar.getWorld().addGObject((TextureAbsImpl) texture);
		setTexture(TextureManager.loadImageOrNull("gui/" + (index == 0 ? "hotbar_top.png" : "hotbar.png")));
	}

	public PlayerHotbar getHotbar() {
		return hotbar;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getHotbarKey() {
		return hotbarKey;
	}

	public Rectangle getImage() {
		return image;
	}

	public String getFullValue() {
		return fullValue;
	}

	public void moveToCorrectPosition() {
		position.x = (hotbar.getSize() - index) * SIZE;
		requiresRecalculation = true;
	}

	@Override
	public void delete() {
		super.delete();
		image.delete();
	}

	//ANIMATION

	@Override
	public void onTick(long dif, Room room) {
		int size = hotbar.getSize();
		float toX = (size - index) * -SIZE;
		if (position.x > toX) {
			position.x -= dif / 1000000000f;
			if (position.x <= toX) position.x = toX;
			requiresRecalculation = true;
		} else if (position.x < toX) {
			position.x += dif / 1000000000f;
			if (position.x >= toX) position.x = toX;
			requiresRecalculation = true;
		}
	}
}
