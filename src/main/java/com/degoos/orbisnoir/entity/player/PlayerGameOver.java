package com.degoos.orbisnoir.entity.player;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.event.drop.DropFilesEvent;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.window.WindowResizeEvent;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.GObject;
import com.degoos.graphicengine2.object.Room;
import com.degoos.orbisnoir.Game;
import com.degoos.orbisnoir.handler.VolumeHandler;
import com.degoos.orbisnoir.handler.Waiter;
import com.degoos.orbisnoir.save.Save;
import com.degoos.orbisnoir.script.ScriptManager;
import com.degoos.orbisnoir.world.World;
import com.degoos.orbisnoir.world.WorldManager;
import org.joml.Vector3f;

import java.io.File;

public class PlayerGameOver extends GObject {

	private boolean animationDone = false;
	private World world;

	public PlayerGameOver(World world) {
		this.world = world;
		world.addGObject(new PlayerGameOverText(new Vector3f(0, 0.5f, 0)));
		world.getScriptContainer().runScript(ScriptManager.getScriptUnsafe("global/game_over"))
				.setOnFinish(executer -> animationDone = true);
	}

	@Override
	public void onTick(long l, Room room) {

	}

	@Override
	public void onTick2(long l, Room room) {

	}

	@Override
	public void onAsyncTick(long l, Room room) {

	}

	@Override
	public void onMouseEvent(MouseEvent mouseEvent) {

	}

	@Override
	public void onKeyboardEvent(KeyEvent keyEvent) {
		if (animationDone) {
			delete();
			String worldName = Game.getSave().player_room;

			try {
				world.addGObject(new VolumeHandler(Engine.getSoundManager().getSourceUnsafe("game_over"), 0,
						2000000000L, true, null));
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			world.getBlackScreen().setOpacity(1, 2000000000, () ->
					world.addGObject(new Waiter(1000, waiter -> {
						try {
							Game.setSave(new Save(new File("save.dat"), true));
							World world = WorldManager.loadWorld(worldName, Game.getSave().player_position);
							world.setBackground(GColor.BLACK);
							Engine.setRoom(world);
							world.runInitScript();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}))
			);
		}
	}

	@Override
	public void onResize(WindowResizeEvent windowResizeEvent) {

	}

	@Override
	public void onDropFilesEvent(DropFilesEvent dropFilesEvent) {

	}
}
