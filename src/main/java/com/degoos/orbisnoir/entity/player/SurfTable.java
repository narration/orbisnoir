package com.degoos.orbisnoir.entity.player;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.texture.TextureManager;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

public class SurfTable extends Entity {

	private Player player;

	public SurfTable(World world, Player player) {
		super(world, new Vector2f(player.getPosition()), GColor.WHITE, 1,
				new Area(new Vector2f(-0.5f, -1), new Vector2f(0.5f, 1)), EnumEntityDrawPriority.ALWAYS_BOTTOM,
				new CollisionBox(new Vector2f(0), new Vector2f(0),
						new Vector2f(0)), EnumEntityCollision.PASS, null, null);
		this.player = player;
		rectangle.setTexture(TextureManager.loadImageOrNull("gui/player/surf_table.png"));
		tickPriority = 1;
	}

	void surf(int x) {
		rectangle.setRotation(x * -10f + rectangle.getRotation() * 0.7f);
	}

	@Override
	public void tick(long dif) {
		super.tick(dif);
		position.set(player.getPosition()).add(player.getEntitySize().getMid().x, 0.5f);
	}
}
