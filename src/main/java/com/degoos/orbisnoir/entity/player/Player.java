package com.degoos.orbisnoir.entity.player;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.texture.Palette;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.Controller;
import com.degoos.orbisnoir.entity.MovableEntity;
import com.degoos.orbisnoir.entity.palette.MovablePalette;
import com.degoos.orbisnoir.entity.player.hotbar.PlayerHotbar;
import com.degoos.orbisnoir.enums.EnumDirection;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.handler.ZoomHandler;
import com.degoos.orbisnoir.world.Box;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.joml.Vector2i;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class Player extends MovableEntity {

	private PlayerHealth health;
	private PlayerHotbar hotbar;
	private PlayerShield shield;

	private MovableEntity controlling;
	private boolean canUncontrol;

	private boolean surfing;
	private SurfTable surfTable;
	private long surfBoost;

	public Player(World world, Vector2f position, Controller controller, EnumDirection direction, Palette palette) {
		super(world, position, GColor.WHITE, 1, new Area(-0.18f, 0, 1.18f, 2.2f),
				EnumEntityDrawPriority.RELATIVE,
				new CollisionBox(position, Arrays.asList(
						new Vector2f(0.1f, 0),
						new Vector2f(0.9f, 0),
						new Vector2f(0.9f, 0.8f),
						new Vector2f(0.1f, 0.8f))), EnumEntityCollision.COLLIDE,
				controller, null, direction, MovablePalette.fromNPCPalette(palette), false, false, false);
		setName("player");
		canUncontrol = true;
		surfing = false;
		health = new PlayerHealth(this);
		hotbar = new PlayerHotbar(world);
		shield = new PlayerShield(this);
		world.addGObject(health);
		world.addGObject(hotbar);
		world.addGObject(shield);
	}

	public PlayerHealth getHealth() {
		return health;
	}

	public PlayerHotbar getHotbar() {
		return hotbar;
	}

	public PlayerShield getShield() {
		return shield;
	}

	public MovableEntity getControlling() {
		return controlling;
	}

	public void setControlling(MovableEntity controlling) {
		this.controlling = controlling;
		if (controlling == null) setCasting(false);
	}

	public boolean canUncontrol() {
		return canUncontrol;
	}

	public void setCanUncontrol(boolean canUncontrol) {
		this.canUncontrol = canUncontrol;
	}

	public boolean isSurfing() {
		return surfing;
	}

	public void setSurfing(boolean surfing) {
		if (this.surfing == surfing) return;
		this.surfing = surfing;
		if (surfing) {
			world.registerEntity(surfTable = new SurfTable(world, this));
			setMoving(false);
			world.getCamera().setTargetBoxSize(0);
		} else {
			surfTable.delete();
			surfTable = null;
			world.getCamera().setTargetBoxSize(1.7f);
		}
	}

	public long getSurfBoost() {
		return surfBoost;
	}

	public void setSurfBoost(long surfBoost) {
		this.surfBoost = surfBoost;
		if (surfBoost > 0 && surfing) world.addGObject(new ZoomHandler(0.13f, surfBoost > 300000000 ? 300 :
				(surfBoost / 1000000), null, world.getCamera()));
	}

	@Override
	public boolean move(Vector2f displacement, boolean checkCollisions) {
		Vector2i oldBox = new Vector2i((int) Math.floor(position.x + 0.5f), (int) Math.floor(position.y + 0.5f));
		boolean collide = super.move(displacement, checkCollisions);
		Vector2i newBox = new Vector2i((int) Math.floor(position.x + 0.5f), (int) Math.floor(position.y + 0.5f));

		if (!oldBox.equals(newBox)) {
			{
				try {
					Box box = world.getBox(newBox);
					box.onScript(this);
				} catch (NoSuchElementException ignore) {
				}
			}
		}
		return collide;
	}

	void surf(int x, long dif) {
		move(new Vector2f(x * (surfBoost > 0 ? 8f : 6) * dif / 1000000000f,
				dif * (surfBoost > 0 ? 15 : 10) / 1000000000f), true);
		if (surfTable != null)
			surfTable.surf(x);
	}

	@Override
	public void tick(long dif) {
		super.tick(dif);
		if (surfBoost != 0) {
			surfBoost -= dif;
			if (surfBoost < 0) {
				surfBoost = 0;
				world.addGObject(new ZoomHandler(0.15f, 200, null, world.getCamera()));
			}
		}
	}

	@Override
	public void delete() {
		super.delete();
		health.delete();
		hotbar.delete();
	}
}
