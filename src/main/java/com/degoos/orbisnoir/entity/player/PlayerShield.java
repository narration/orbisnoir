package com.degoos.orbisnoir.entity.player;

import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.object.texture.Texture;
import com.degoos.graphicengine2.object.texture.TextureRegion;
import com.degoos.orbisnoir.texture.TextureManager;
import org.joml.Vector3f;

import java.util.Objects;

public class PlayerShield extends Rectangle {

	private static final float REMOVE_PER_NANO = 0.7f / 1000000000f;
	private static final float RESTORE_PER_NANO = 0.3f / 1000000000f;

	private Player player;
	private float health;

	private Rectangle bar, loadedBar;

	public PlayerShield(Player player) {
		super(new Vector3f(0), new Vector3f(0), new Vector3f(0));
		this.player = player;
		this.health = 1;

		setParent(player.getRectangle());
		setTexture(TextureManager.loadImageOrNull("gui/player/shield.png"));
		setOpacity(0.5f);

		bar = new Rectangle(new Vector3f(0), new Vector3f(0, -0.01f, 0), new Vector3f(0.1f, 0.01f, 0));
		bar.setParent(this);
		bar.setTexture(TextureManager.loadImageOrNull("gui/player/shield_bar_unloaded.png"));
		loadedBar = new Rectangle(new Vector3f(0, 0, 0.0001f), new Vector3f(bar.getMin()), new Vector3f(bar.getMax()));
		loadedBar.setParent(bar);
		loadedBar.setTexture(new TextureRegion((Texture)
				Objects.requireNonNull(TextureManager.loadImageOrNull("gui/player/shield_bar.png")), 0f, 0f, 1f, 1f));

		bar.setVisible(true);
		loadedBar.setVisible(true);
		bar.setOpacity(0);
		loadedBar.setOpacity(0);
		player.getWorld().addGObject(bar);
		player.getWorld().addGObject(loadedBar);
	}

	public boolean isActive() {
		return isVisible();
	}

	public void setActive(boolean active) {
		setVisible(active);
		player.setCasting(active);
	}

	public void damage(double damage) {
		this.health -= damage;
		if (health <= 0) {
			health = 0;
			setActive(false);
		}
	}

	@Override
	public void onTick(long dif, Room room) {
		if (isVisible()) {
			health -= REMOVE_PER_NANO * dif;
			if (health <= 0) {
				health = 0;
				setActive(false);
			} else recalculateSize();
		} else if (health != 1) {
			health += RESTORE_PER_NANO * dif;
			if (health >= 1) health = 1;
		}

		if (isVisible() || health == 1) {
			loadedBar.setOpacity(Math.max(0, loadedBar.getOpacity() - 0.1f));
			bar.setOpacity(Math.max(0, bar.getOpacity() - 0.1f));
		} else {
			loadedBar.setOpacity(Math.min(1, loadedBar.getOpacity() + 0.1f));
			bar.setOpacity(Math.min(1, bar.getOpacity() + 0.1f));
		}

		if(loadedBar.getOpacity() > 0) {
			loadedBar.setMax(new Vector3f(0.1f * health, 0.01f, 0));
			loadedBar.getTexture().setU2(health);
			bar.setPosition(new Vector3f(-0.05f, player.getWorld().getCamera().getBoxSize() * 1.2f, 0));
		}
	}

	private void recalculateSize() {
		float boxSize = player.getWorld().getCamera().getBoxSize();
		float maxX = 0.5f * boxSize;
		float maxY = 0.7f * boxSize;
		float delta = 1.5f * boxSize * health;
		setPosition(new Vector3f(maxX, maxY, 0.001f));
		setMin(new Vector3f(-delta, -delta, 0));
		setMax(new Vector3f(delta, delta, 0));
	}

	@Override
	public void delete() {
		super.delete();
		bar.delete();
		loadedBar.delete();
	}
}
