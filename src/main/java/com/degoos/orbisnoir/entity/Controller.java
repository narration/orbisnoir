package com.degoos.orbisnoir.entity;

import com.degoos.graphicengine2.event.keyboard.KeyEvent;

public interface Controller {

	void onTick(Entity entity, long dif);

	void keyEvent(Entity entity, KeyEvent keyEvent);

}
