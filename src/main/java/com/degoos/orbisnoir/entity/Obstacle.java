package com.degoos.orbisnoir.entity;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.Collision;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

public class Obstacle extends Entity {

	public Obstacle(World world, Vector2f position, GColor color, float opacity, Area entitySize,
	                EnumEntityDrawPriority drawPriority, CollisionBox collisionBox, EnumEntityCollision collision,
	                Controller controller, Script script) {
		super(world, position, color, opacity, entitySize, drawPriority, collisionBox, collision, controller, script);
	}

	@Override
	public boolean move(Vector2f displacement, boolean checkCollisions) {
		boolean collide = false;
		displacement = new Vector2f(displacement);
		if (displacement.x == 0 && displacement.y == 0) return false;

		int loops = 1;
		while (Math.abs(displacement.x) > 0.2f || Math.abs(displacement.y) > 0.2f) {
			loops = loops << 1;
			displacement.mul(0.5f);
		}

		for (int i = 0; i < loops; i++) {
			if(deleted) return collide;
			collisionBox.setPosition(new Vector2f(position).add(displacement));

			Collision collision;
			//CHECKING ENTITIES
			Player player = world.getPlayer();
			if (player.collision != EnumEntityCollision.PASS) {
				collision = collisionBox.collision(player.collisionBox);
				if (collision != null) {
					player.collide(this, collision);
					collideSelfEntity(player, collision);
					if (checkCollisions) {
						collisionBox.getPosition().add(collision.getMtv());
						collide = true;
					}
				}
			}
			position.set(collisionBox.getPosition());
		}
		return collide;
	}
}
