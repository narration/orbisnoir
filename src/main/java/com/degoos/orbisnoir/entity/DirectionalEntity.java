package com.degoos.orbisnoir.entity;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.palette.DirectionablePalette;
import com.degoos.orbisnoir.enums.EnumDirection;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

import java.util.NoSuchElementException;

public class DirectionalEntity extends Entity {

	protected EnumDirection direction;
	protected DirectionablePalette directionablePalette;

	public DirectionalEntity(World world, Vector2f position, GColor color, float opacity,
	                         Area entitySize, EnumEntityDrawPriority drawPriority, CollisionBox collisionBox,
	                         EnumEntityCollision collision, Controller controller, Script script,
	                         EnumDirection direction, DirectionablePalette directionablePalette) {
		super(world, position, color, opacity, entitySize, drawPriority, collisionBox, collision, controller, script);
		this.direction = direction;
		this.directionablePalette = directionablePalette;

		directionablePalette.addToWorld(world);
		try {
			rectangle.setTexture(directionablePalette.getTexture(direction));
		} catch (NoSuchElementException ignore) {
		}
	}

	public EnumDirection getDirection() {
		return direction;
	}

	public void setDirection(EnumDirection direction) {
		if (direction == EnumDirection.REFRESH) direction = this.direction;
		this.direction = direction;
		try {
			rectangle.setTexture(directionablePalette.getTexture(direction));
		} catch (NoSuchElementException ignore) {
		}
	}

	public DirectionablePalette getDirectionablePalette() {
		return directionablePalette;
	}

	public void setDirectionablePalette(DirectionablePalette directionablePalette) {
		this.directionablePalette = directionablePalette;
		directionablePalette.addToWorld(world);
	}
}
