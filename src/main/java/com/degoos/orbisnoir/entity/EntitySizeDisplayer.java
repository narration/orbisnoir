package com.degoos.orbisnoir.entity;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.texture.TextureManager;
import com.degoos.orbisnoir.world.Camera;
import org.joml.Vector2f;
import org.joml.Vector3f;

public class EntitySizeDisplayer extends Rectangle {

	private Entity entity;

	public EntitySizeDisplayer(Entity entity) {
		super(new Vector3f(0), new Vector3f(0), new Vector3f(0));
		setVisible(true);
		this.entity = entity;
		setTexture(TextureManager.loadImageOrNull("gui/editor/entity_size.png"));
	}

	@Override
	public void onTick2(long dif, Room room) {
		Camera camera = entity.world.getCamera();

		setMin(camera.toEnginePosition(new Vector2f(entity.position).add(entity.entitySize.getMin()), 0));
		setMax(camera.toEnginePosition(new Vector2f(entity.position).add(entity.entitySize.getMax()), 0));

		Area area = Engine.getRenderArea();

		setVisible(area.isInsideX(min.x) && area.isInsideY(min.y)
				|| area.isInsideX(max.x) && area.isInsideY(min.y)
				|| area.isInsideX(max.x) && area.isInsideY(max.y)
				|| area.isInsideX(min.x) && area.isInsideY(max.y));
	}
}
