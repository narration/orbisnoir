package com.degoos.orbisnoir.entity.sketch;

import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.world.World;
import org.jooq.tools.json.ParseException;

import java.util.Map;

public class EntityEntitySketch extends EntitySketch<Entity> {

	public EntityEntitySketch() {
		super();
		jsonMap.put("type", "entity");
	}

	public EntityEntitySketch(String json) throws ParseException {
		super(json);
		jsonMap.put("type", "entity");
	}


	public EntityEntitySketch(Map json) throws ParseException {
		super(json);
		jsonMap.put("type", "entity");
	}

	@Override
	public Entity toEntity(World world) {
		return new Entity(world, this);
	}
}
