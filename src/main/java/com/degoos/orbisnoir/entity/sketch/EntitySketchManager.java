package com.degoos.orbisnoir.entity.sketch;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.orbisnoir.game.entity.global.ClaraEntitySketch;
import com.degoos.orbisnoir.game.entity.global.RaquelEntitySketch;
import com.degoos.orbisnoir.game.entity.level1.DamageBallEntitySketch;
import com.degoos.orbisnoir.game.entity.level1.RotatingBallsEntitySketch;
import com.degoos.orbisnoir.game.entity.level2.LineBallsEntitySketch;
import com.degoos.orbisnoir.game.entity.level2.PlatformEntitySketch;
import com.degoos.orbisnoir.game.entity.level2.VoidEntitySketch;
import com.degoos.orbisnoir.game.entity.level3.FrenchWarriorEntitySketch;
import com.degoos.orbisnoir.game.entity.level3.GiantWarriorEntitySketch;
import com.degoos.orbisnoir.game.entity.level3.SpanishWarriorEntitySketch;
import com.degoos.orbisnoir.game.entity.level4.StaticDamageEntitySketch;
import com.degoos.orbisnoir.game.entity.level4.StaticTurretEntitySketch;
import com.degoos.orbisnoir.game.entity.level5.DeathEntitySketch;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class EntitySketchManager {

	private static Set<EntitySketchPackage> sketches;

	public static void load() {
		sketches = new HashSet<>();
		loadDefault();
	}

	private static void loadDefault() {
		sketches.add(new EntitySketchPackage("entity", EntityEntitySketch.class, GColor.GREEN, "ENTITY"));
		sketches.add(new EntitySketchPackage("villager", VillagerEntitySketch.class, GColor.BLUE, "VILLAGE"));
		sketches.add(new EntitySketchPackage("image", EntityImageEntitySketch.class, GColor.BLUE, "IMG"));
		sketches.add(new EntitySketchPackage("damage_ball", DamageBallEntitySketch.class, GColor.BLUE, "DMG_BALL"));
		sketches.add(new EntitySketchPackage("rotating_balls", RotatingBallsEntitySketch.class, GColor.BLUE, "ROT_BALL"));
		sketches.add(new EntitySketchPackage("platform", PlatformEntitySketch.class, GColor.BLUE, "PLATFORM"));
		sketches.add(new EntitySketchPackage("void", VoidEntitySketch.class, GColor.BLUE, "VOID"));
		sketches.add(new EntitySketchPackage("line_balls", LineBallsEntitySketch.class, GColor.BLUE, "LINE_BALLS"));
		sketches.add(new EntitySketchPackage("static_turret", StaticTurretEntitySketch.class, GColor.BLUE, "ST_TURRET"));
		sketches.add(new EntitySketchPackage("static_damage", StaticDamageEntitySketch.class, GColor.BLUE, "ST_DMG"));
		sketches.add(new EntitySketchPackage("spanish_warrior", SpanishWarriorEntitySketch.class, new GColor(1, 1, 0), "ESP"));
		sketches.add(new EntitySketchPackage("french_warrior", FrenchWarriorEntitySketch.class, GColor.RED, "FR"));
		sketches.add(new EntitySketchPackage("giant_warrior", GiantWarriorEntitySketch.class, GColor.BLUE, "GIANT"));
		sketches.add(new EntitySketchPackage("death", DeathEntitySketch.class, GColor.BLACK, "DEATH"));
		sketches.add(new EntitySketchPackage("clara", ClaraEntitySketch.class, GColor.BLACK, "CLARA"));
		sketches.add(new EntitySketchPackage("raquel", RaquelEntitySketch.class, GColor.BLACK, "RAQUEL"));
	}

	public static Set<EntitySketchPackage> getSketches() {
		return sketches;
	}

	public static EntitySketch<?> createSketch(Map jsonMap) {
		String name = (String) jsonMap.get("type");
		EntitySketchPackage pack = sketches.stream().filter(target -> target.getName().equals(name)).findAny().orElse(null);
		if (pack == null) return null;
		try {
			return pack.getEntityClass().getConstructor(Map.class).newInstance(jsonMap);
		} catch (Exception ex) {
			return null;
		}
	}
}
