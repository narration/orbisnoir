package com.degoos.orbisnoir.entity.sketch;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.util.Area;
import com.degoos.graphicengine2.util.Validate;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.interfaces.Savable;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptManager;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.jooq.tools.json.JSONValue;
import org.jooq.tools.json.ParseException;

import java.util.*;

public abstract class EntitySketch<T extends Entity> implements Savable {

	private static final Map<String, Class<?>> typesMap = new HashMap<>();

	static {
		typesMap.put("name", String.class);
		typesMap.put("script", Script.class);
		typesMap.put("opacity", double.class);
		typesMap.put("collision_type", EnumEntityCollision.class);
		typesMap.put("entity_draw_priority", EnumEntityDrawPriority.class);

		typesMap.put("position", Map.class);
		typesMap.put("position_x_pos", float.class);
		typesMap.put("position_y_pos", float.class);

		typesMap.put("color", Map.class);
		typesMap.put("color_red", long.class);
		typesMap.put("color_green", long.class);
		typesMap.put("color_blue", long.class);

		typesMap.put("collision_box", Map.class);
		typesMap.put("collision_box_min_x", float.class);
		typesMap.put("collision_box_min_y", float.class);
		typesMap.put("collision_box_max_x", float.class);
		typesMap.put("collision_box_max_y", float.class);

		typesMap.put("entity_size", Map.class);
		typesMap.put("entity_size_min_x", float.class);
		typesMap.put("entity_size_min_y", float.class);
		typesMap.put("entity_size_max_x", float.class);
		typesMap.put("entity_size_max_y", float.class);
	}

	protected Map jsonMap;

	protected String name;
	protected Vector2f position;
	protected Script script;
	protected float opacity;
	protected GColor color;
	protected EnumEntityCollision collisionType;
	protected EnumEntityDrawPriority entityDrawPriority;

	protected Area entitySize;
	protected CollisionBox collisionBox;

	public EntitySketch() {
		jsonMap = new TreeMap();
		jsonMap.put("collision_box", new TreeMap());
		jsonMap.put("entity_size", new TreeMap());
		jsonMap.put("color", new TreeMap());
		jsonMap.put("position", new TreeMap());
		setName(UUID.randomUUID().toString());
		setPosition(new Vector2f(0));
		setScript(null);
		setOpacity(1);
		setColor(GColor.WHITE);
		setCollisionType(EnumEntityCollision.COLLIDE);
		setEntityDrawPriority(EnumEntityDrawPriority.RELATIVE);
		setCollisionBox(new CollisionBox(new Vector2f(0), new Vector2f(-0.5f, -0.5f), new Vector2f(0.5f, 0.5f)));
		setEntitySize(new Area(new Vector2f(-0.5f, -0.5f), new Vector2f(0.5f, 0.5f)));
	}

	public EntitySketch(String json) throws ParseException {
		this(new TreeMap((Map) JSONValue.parseWithException(json)));
	}

	public EntitySketch(Map json) {
		jsonMap = new TreeMap(json);
		toTreeMap(jsonMap);
		updateMap();
		refreshFromMap();
	}

	private void toTreeMap(Map map) {
		for (Object key : map.keySet()) {
			if (map.get(key) instanceof Map && !(map.get(key) instanceof TreeMap)) {
				Map subMap = new TreeMap((Map) map.get(key));
				toTreeMap(subMap);
				map.put(key, subMap);
			}
		}
	}

	protected void updateMap() {
		if (!jsonMap.containsKey("entity_draw_priority"))
			jsonMap.put("entity_draw_priority", EnumEntityDrawPriority.RELATIVE.name());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		Validate.notNull(name, "Name cannot be null!");
		this.name = name;
		jsonMap.put("name", name);
	}

	public Vector2f getPosition() {
		return new Vector2f(position);
	}

	public void setPosition(Vector2f position) {
		Validate.notNull(position, "Position cannot be null!");
		this.position = position;
		Map positionMap = (Map) jsonMap.get("position");
		positionMap.put("x_pos", (double) position.x);
		positionMap.put("y_pos", (double) position.y);
	}

	public Script getScript() {
		return script;
	}

	public void setScript(Script script) {
		this.script = script;
		jsonMap.put("script", script == null ? "null" : script.getName());
	}

	public Area getEntitySize() {
		return entitySize;
	}

	public void setEntitySize(Area entitySize) {
		Validate.notNull(entitySize, "Entity size cannot be null!");
		this.entitySize = entitySize;
		Map entitySizeMap = (Map) jsonMap.get("entity_size");
		entitySizeMap.put("min_x", (double) entitySize.getMin().x);
		entitySizeMap.put("min_y", (double) entitySize.getMin().y);
		entitySizeMap.put("max_x", (double) entitySize.getMax().x);
		entitySizeMap.put("max_y", (double) entitySize.getMax().y);
	}

	public float getOpacity() {
		return opacity;
	}

	public void setOpacity(float opacity) {
		this.opacity = opacity;
		jsonMap.put("opacity", (double) opacity);
	}

	public GColor getColor() {
		return color;
	}

	public void setColor(GColor color) {
		Validate.notNull(color, "Color cannot be null!");
		Map colorMap = (Map) jsonMap.get("color");
		colorMap.put("red", (long) (color.getRed() * 255));
		colorMap.put("green", (long) (color.getGreen() * 255));
		colorMap.put("blue", (long) (color.getBlue() * 255));
		this.color = color;
	}

	public EnumEntityCollision getCollisionType() {
		return collisionType;
	}

	public void setCollisionType(EnumEntityCollision collisionType) {
		Validate.notNull(collisionType, "Collision type cannot be null!");
		this.collisionType = collisionType;
		jsonMap.put("collision_type", collisionType.name());
	}

	public EnumEntityDrawPriority getEntityDrawPriority() {
		return entityDrawPriority;
	}

	public void setEntityDrawPriority(EnumEntityDrawPriority entityDrawPriority) {
		Validate.notNull(entityDrawPriority, "Entity draw priority cannot be null!");
		this.entityDrawPriority = entityDrawPriority;
		jsonMap.put("entity_draw_priority", entityDrawPriority.name());
	}

	public CollisionBox getCollisionBox() {
		return collisionBox;
	}

	public void setCollisionBox(CollisionBox collisionBox) {
		Validate.notNull(collisionBox, "Collision box cannot be null!");
		this.collisionBox = collisionBox;
		Map collisionBoxMap = (Map) jsonMap.get("collision_box");
		collisionBoxMap.put("min_x", (double) collisionBox.getMin().x);
		collisionBoxMap.put("min_y", (double) collisionBox.getMin().y);
		collisionBoxMap.put("max_x", (double) collisionBox.getMax().x);
		collisionBoxMap.put("max_y", (double) collisionBox.getMax().y);
	}

	public Map getJsonMap() {
		return jsonMap;
	}

	@Override
	public String toJSON() {
		return JSONValue.toJSONString(jsonMap);
	}

	public abstract T toEntity(World world);

	public Map<String, Class<?>> getTypesMap() {
		return typesMap;
	}

	public void refreshFromMap() {
		name = (String) jsonMap.get("name");
		script = ScriptManager.getScriptUnsafe((String) jsonMap.getOrDefault("script", null));
		opacity = (float) (double) jsonMap.get("opacity");
		collisionType = EnumEntityCollision.getByName((String) jsonMap.get("collision_type")).orElse(EnumEntityCollision.PASS);
		entityDrawPriority = EnumEntityDrawPriority.getByName((String) jsonMap.get("entity_draw_priority")).orElse(EnumEntityDrawPriority.RELATIVE);

		Map positionMap = (Map) jsonMap.get("position");
		position = new Vector2f((float) (double) positionMap.get("x_pos"), (float) (double) positionMap.get("y_pos"));

		Map colorMap = (Map) jsonMap.get("color");
		color = new GColor((long) colorMap.get("red") / 255f, (long) colorMap.get("green") / 255f, (long) colorMap.get("blue") / 255f);

		Map collisionBoxMap = (Map) jsonMap.get("collision_box");
		collisionBox = new CollisionBox(new Vector2f(0), new Vector2f((float) (double) collisionBoxMap.get("min_x"), (float) (double) collisionBoxMap.get("min_y")),
				new Vector2f((float) (double) collisionBoxMap.get("max_x"), (float) (double) collisionBoxMap.get("max_y")));

		Map entitySizeMap = (Map) jsonMap.get("entity_size");
		entitySize = new Area(new Vector2f((float) (double) entitySizeMap.get("min_x"), (float) (double) entitySizeMap.get("min_y")),
				new Vector2f((float) (double) entitySizeMap.get("max_x"), (float) (double) entitySizeMap.get("max_y")));
	}

	public Map copyMap() {
		return copyMap0(jsonMap);
	}

	private Map copyMap0(Map old) {
		Map map = new HashMap();
		old.forEach((o1, o2) -> {
			if (o2 instanceof Map) map.put(o1, copyMap0((Map) o2));
			else map.put(o1, o2);
		});
		return map;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		EntitySketch<?> sketch = (EntitySketch<?>) o;
		return Objects.equals(name, sketch.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}
}
