package com.degoos.orbisnoir.entity.sketch;

import com.degoos.graphicengine2.object.GColor;

import java.util.Objects;

public class EntitySketchPackage {

	private String name;
	private Class<? extends EntitySketch> entityClass;
	private GColor color;
	private String abbreviation;

	public EntitySketchPackage(String name, Class<? extends EntitySketch> entityClass, GColor color, String abbreviation) {
		this.name = name;
		this.entityClass = entityClass;
		this.color = color;
		this.abbreviation = abbreviation;
	}

	public String getName() {
		return name;
	}

	public Class<? extends EntitySketch> getEntityClass() {
		return entityClass;
	}

	public GColor getColor() {
		return color;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		EntitySketchPackage that = (EntitySketchPackage) o;
		return name.equals(that.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}
}
