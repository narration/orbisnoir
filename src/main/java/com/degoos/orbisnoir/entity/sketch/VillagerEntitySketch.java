package com.degoos.orbisnoir.entity.sketch;

import com.degoos.graphicengine2.util.Area;
import com.degoos.graphicengine2.util.Validate;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.villager.Villager;
import com.degoos.orbisnoir.enums.EnumDirection;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.jooq.tools.json.ParseException;

import java.util.HashMap;
import java.util.Map;

public class VillagerEntitySketch extends EntitySketch<Villager> {

	private EnumDirection direction;

	public VillagerEntitySketch() {
		super();
		setEntitySize(new Area(new Vector2f(0), new Vector2f(1f, 1.4f)));
		setCollisionBox(new CollisionBox(new Vector2f(0), new Vector2f(0.1f, 0), new Vector2f(0.8f, 0.9f)));
		setDirection(EnumDirection.DOWN);
		jsonMap.put("type", "villager");
	}

	public VillagerEntitySketch(String json) throws ParseException {
		super(json);
		jsonMap.put("type", "villager");
	}

	public VillagerEntitySketch(Map json) throws ParseException {
		super(json);
		jsonMap.put("type", "villager");
	}

	public EnumDirection getDirection() {
		return direction;
	}

	public void setDirection(EnumDirection direction) {
		Validate.notNull(direction, "Direction cannot be null!");
		this.direction = direction;
		jsonMap.put("direction", direction.name());
	}

	@Override
	public Villager toEntity(World world) {
		try {
			return new Villager(world, this);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Map<String, Class<?>> getTypesMap() {
		Map<String, Class<?>> map = new HashMap<>(super.getTypesMap());
		map.put("direction", EnumDirection.class);
		return map;
	}

	@Override
	public void refreshFromMap() {
		super.refreshFromMap();
		direction = EnumDirection.getByName((String) jsonMap.get("direction")).orElse(EnumDirection.DOWN);
	}
}
