package com.degoos.orbisnoir.entity.sketch;

import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.util.Validate;
import com.degoos.orbisnoir.entity.image.EnumImageType;
import com.degoos.orbisnoir.entity.image.ImageEntity;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.texture.TextureManager;
import com.degoos.orbisnoir.world.World;
import org.jooq.tools.json.ParseException;

import java.util.HashMap;
import java.util.Map;

public class EntityImageEntitySketch extends EntitySketch<ImageEntity> {

	private EnumImageType type;
	private String image;
	private float rotation, angularVelocity;

	public EntityImageEntitySketch() {
		super();
		setType(EnumImageType.IMAGE);
		setImage("null");
		setRotation(0);
		setAngularVelocity(0);
		jsonMap.put("type", "image");
	}

	public EntityImageEntitySketch(String json) throws ParseException {
		super(json);
		jsonMap.put("type", "image");
	}

	public EntityImageEntitySketch(Map json) {
		super(json);
		jsonMap.put("type", "image");
	}

	protected void updateMap() {
		super.updateMap();
		if (!jsonMap.containsKey("rotation"))
			jsonMap.put("rotation", 0D);
		if (!jsonMap.containsKey("angular_velocity"))
			jsonMap.put("angular_velocity", 0D);
	}

	public EnumImageType getType() {
		return type;
	}

	public void setType(EnumImageType type) {
		Validate.notNull(type, "Image type cannot be null!");
		this.type = type;
		jsonMap.put("image_type", type.name());
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image == null ? "null" : image;
		jsonMap.put("image", this.image);
	}

	public float getRotation() {
		return rotation;
	}

	public void setRotation(float rotation) {
		this.rotation = rotation;
		jsonMap.put("rotation", (double) this.rotation);
	}

	public float getAngularVelocity() {
		return angularVelocity;
	}

	public void setAngularVelocity(float angularVelocity) {
		this.angularVelocity = angularVelocity;
		jsonMap.put("angular_velocity", (double) angularVelocity);
	}

	public ITexture toImage() {
		try {
			String[] sl;
			switch (type) {
				case ANIMATION:
					return TextureManager.loadAnimation(image);
				case PALETTE_IMAGE:
					sl = image.split(";");
					return PaletteManager.loadPalette(sl[0]).getTexture(Integer.valueOf(sl[1]));
				case PALETTE_ANIMATION:
					sl = image.split(";");
					return PaletteManager.loadPalette(sl[0]).getAnimation(Integer.valueOf(sl[1]));
				case IMAGE:
				default:
					return TextureManager.loadImage(image);
			}
		} catch (Exception ignore) {
			return null;
		}
	}

	@Override
	public ImageEntity toEntity(World world) {
		return new ImageEntity(world, this);
	}

	@Override
	public Map<String, Class<?>> getTypesMap() {
		Map<String, Class<?>> map = new HashMap<>(super.getTypesMap());
		map.put("image_type", EnumImageType.class);
		map.put("image", String.class);
		map.put("rotation", float.class);
		map.put("angular_velocity", float.class);
		return map;
	}

	@Override
	public void refreshFromMap() {
		super.refreshFromMap();
		type = EnumImageType.getByName((String) jsonMap.get("image_type")).orElse(EnumImageType.IMAGE);
		image = (String) jsonMap.get("image");
		rotation = (float) (double) jsonMap.get("rotation");
		angularVelocity = (float) (double) jsonMap.get("angular_velocity");
	}
}
