package com.degoos.orbisnoir.entity.image;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.object.texture.TextureAbsImpl;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.Controller;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.entity.sketch.EntityImageEntitySketch;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;


public class ImageEntity extends Entity {

	private float angularVelocity;

	public ImageEntity(World world, EntityImageEntitySketch sketch) {
		this(world, sketch.getPosition(), sketch.getColor(), sketch.getOpacity(), sketch.getEntitySize(),
				sketch.getEntityDrawPriority(), sketch.getCollisionBox(),
				sketch.getCollisionType(), null, sketch.getScript(),
				sketch.toImage(), sketch.getRotation(), sketch.getAngularVelocity());
		setName(sketch.getName());
		this.sketch = sketch;
	}


	public ImageEntity(World world, Vector2f position, GColor color, float opacity, Area entitySize,
	                   EnumEntityDrawPriority drawPriority, CollisionBox collisionBox, EnumEntityCollision collision,
	                   Controller controller, Script script, ITexture image, float rotation, float angularVelocity) {
		super(world, position, color, opacity, entitySize, drawPriority, collisionBox, collision, controller, script);
		this.angularVelocity = angularVelocity;

		this.angularVelocity = angularVelocity / 1000000000;
		rectangle.setTexture(image);
		rectangle.setRotation(rotation);
		if (image != null) {
			world.addGObject((TextureAbsImpl) image);
		}
	}

	public double getAngularVelocity() {
		return angularVelocity * 1000000000;
	}

	public void setAngularVelocity(float angularVelocity) {
		this.angularVelocity = angularVelocity / 1000000000;
	}

	@Override
	public void restoreToSketch() {
		super.restoreToSketch();
		if (sketch == null) return;

		this.angularVelocity = ((EntityImageEntitySketch) sketch).getAngularVelocity() / 1000000000;
		rectangle.setTexture(((EntityImageEntitySketch) sketch).toImage());
		rectangle.setRotation(((EntityImageEntitySketch) sketch).getRotation());
		if (rectangle.getTexture() != null)
			world.addGObject((TextureAbsImpl) rectangle.getTexture());
	}

	@Override
	public void tick(long dif) {
		if (slept || angularVelocity == 0) return;
		rectangle.setRotation(rectangle.getRotation() + angularVelocity * dif);
		if (rectangle.getRotation() < 0) rectangle.setRotation(rectangle.getRotation() + 360);
		else rectangle.setRotation(rectangle.getRotation() % 360);
	}
}
