package com.degoos.orbisnoir.entity.image;

import java.util.Arrays;
import java.util.Optional;

public enum EnumImageType {

	IMAGE, ANIMATION, PALETTE_IMAGE, PALETTE_ANIMATION;

	public static Optional<EnumImageType> getByName (String name) {
		return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
	}

}
