package com.degoos.orbisnoir.entity;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.util.Area;
import com.degoos.graphicengine2.util.Validate;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.palette.DirectionablePalette;
import com.degoos.orbisnoir.entity.palette.MovablePalette;
import com.degoos.orbisnoir.enums.EnumDirection;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

import java.util.NoSuchElementException;

public class MovableEntity extends DirectionalEntity {

	protected boolean moving, running, casting;

	public MovableEntity(World world, Vector2f position, GColor color, float opacity, Area entitySize,
						 EnumEntityDrawPriority drawPriority, CollisionBox collisionBox, EnumEntityCollision collision,
						 Controller controller, Script script, EnumDirection direction,
						 MovablePalette movablePalette, boolean moving, boolean running, boolean casting) {
		super(world, position, color, opacity, entitySize, drawPriority, collisionBox, collision, controller, script, direction, movablePalette);
		this.moving = moving;
		this.running = running;
		this.casting = casting;

		try {
			rectangle.setTexture(movablePalette.getTexture(direction, moving, running, casting));
		} catch (NoSuchElementException ignore) {
		}
	}

	public boolean isMoving() {
		return moving;
	}

	public boolean isRunning() {
		return running;
	}

	public boolean isCasting() {
		return casting;
	}

	public void setMoving(boolean moving) {
		this.moving = moving;
		try {
			rectangle.setTexture(getDirectionablePalette().getTexture(direction, moving, running, casting));
		} catch (NoSuchElementException ignore) {
		}
	}

	public void setRunning(boolean running) {
		this.running = running;
		try {
			rectangle.setTexture(getDirectionablePalette().getTexture(direction, moving, running, casting));
		} catch (NoSuchElementException ignore) {
		}
	}

	public void setCasting(boolean casting) {
		this.casting = casting;
		try {
			rectangle.setTexture(getDirectionablePalette().getTexture(direction, moving, running, casting));
		} catch (NoSuchElementException ignore) {
		}
	}

	@Override
	public void setDirection(EnumDirection direction) {
		if (direction == EnumDirection.REFRESH) {
			direction = this.direction;
			setRunning(false);
			setMoving(false);
		}
		this.direction = direction;
		try {
			rectangle.setTexture(getDirectionablePalette().getTexture(direction, moving, running, casting));
		} catch (NoSuchElementException ignore) {
		}
	}

	@Override
	public MovablePalette getDirectionablePalette() {
		return (MovablePalette) super.getDirectionablePalette();
	}

	@Override
	public void setDirectionablePalette(DirectionablePalette directionablePalette) {
		Validate.isTrue(directionablePalette instanceof MovablePalette, "Palette must be a movable palette.");
		super.setDirectionablePalette(directionablePalette);
	}

	public boolean move(EnumDirection direction, long dif, boolean checkCollision) {
		if (direction == EnumDirection.NONE) {
			setMoving(false);
			return false;
		}
		float vel = (running ? 6 : 4) * dif / 1000000000f;
		Vector2f to = new Vector2f(direction.getRelative()).mul(vel);
		moving = true;
		setDirection(direction);
		return move(to, checkCollision);
	}
}
