package com.degoos.orbisnoir.entity.palette;

import com.degoos.graphicengine2.object.GObject;
import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.object.texture.Palette;
import com.degoos.orbisnoir.enums.EnumDirection;
import com.degoos.orbisnoir.world.World;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

public class DirectionablePalette {

	protected Palette palette;
	protected Map<EnumDirection, ITexture> textures;

	public DirectionablePalette(Palette palette) {
		this.palette = palette;
		this.textures = new HashMap<>();
	}

	public Palette getPalette() {
		return palette;
	}

	public ITexture getTexture(EnumDirection direction) {
		ITexture texture = textures.get(direction);
		if (texture == null) throw new NoSuchElementException();
		return texture;
	}

	public void addToWorld(World world) {
		textures.values().stream().filter(target -> target instanceof GObject)
				.forEach(target -> world.addGObject((GObject) target));
	}
}
