package com.degoos.orbisnoir.entity.palette;

import com.degoos.graphicengine2.object.GObject;
import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.object.texture.Palette;
import com.degoos.orbisnoir.enums.EnumDirection;
import com.degoos.orbisnoir.world.World;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

public class MovablePalette extends DirectionablePalette {

	protected Map<EnumDirection, ITexture> moveTextures, runTextures, castTextures;

	public static MovablePalette fromNPCPalette(Palette palette) {
		MovablePalette movablePalette = new MovablePalette(palette);
		Map<EnumDirection, ITexture> runAnimations = movablePalette.runTextures;
		Map<EnumDirection, ITexture> moveAnimations = movablePalette.moveTextures;
		Map<EnumDirection, ITexture> stayAnimations = movablePalette.textures;
		Map<EnumDirection, ITexture> castAnimations = movablePalette.castTextures;

		runAnimations.put(EnumDirection.DOWN, palette.getAnimation(6));
		runAnimations.put(EnumDirection.UP, palette.getAnimation(4));
		runAnimations.put(EnumDirection.LEFT, palette.getAnimation(5));
		runAnimations.put(EnumDirection.RIGHT, palette.getAnimation(7));

		runAnimations.put(EnumDirection.DOWN_LEFT, runAnimations.get(EnumDirection.DOWN));
		runAnimations.put(EnumDirection.DOWN_RIGHT, runAnimations.get(EnumDirection.DOWN));
		runAnimations.put(EnumDirection.UP_LEFT, runAnimations.get(EnumDirection.UP));
		runAnimations.put(EnumDirection.UP_RIGHT, runAnimations.get(EnumDirection.UP));

		moveAnimations.put(EnumDirection.DOWN, palette.getAnimation(2));
		moveAnimations.put(EnumDirection.UP, palette.getAnimation(0));
		moveAnimations.put(EnumDirection.LEFT, palette.getAnimation(1));
		moveAnimations.put(EnumDirection.RIGHT, palette.getAnimation(3));

		moveAnimations.put(EnumDirection.DOWN_LEFT, moveAnimations.get(EnumDirection.DOWN));
		moveAnimations.put(EnumDirection.DOWN_RIGHT, moveAnimations.get(EnumDirection.DOWN));
		moveAnimations.put(EnumDirection.UP_LEFT, moveAnimations.get(EnumDirection.UP));
		moveAnimations.put(EnumDirection.UP_RIGHT, moveAnimations.get(EnumDirection.UP));

		stayAnimations.put(EnumDirection.DOWN, palette.getTexture(130));
		stayAnimations.put(EnumDirection.UP, palette.getTexture(104));
		stayAnimations.put(EnumDirection.LEFT, palette.getTexture(117));
		stayAnimations.put(EnumDirection.RIGHT, palette.getTexture(143));

		stayAnimations.put(EnumDirection.DOWN_LEFT, stayAnimations.get(EnumDirection.DOWN));
		stayAnimations.put(EnumDirection.DOWN_RIGHT, stayAnimations.get(EnumDirection.DOWN));
		stayAnimations.put(EnumDirection.UP_LEFT, stayAnimations.get(EnumDirection.UP));
		stayAnimations.put(EnumDirection.UP_RIGHT, stayAnimations.get(EnumDirection.UP));

		castAnimations.put(EnumDirection.DOWN, palette.getTexture(3, 2));
		castAnimations.put(EnumDirection.UP, palette.getTexture(3, 0));
		castAnimations.put(EnumDirection.LEFT, palette.getTexture(3, 1));
		castAnimations.put(EnumDirection.RIGHT, palette.getTexture(3, 3));

		castAnimations.put(EnumDirection.DOWN_LEFT, castAnimations.get(EnumDirection.DOWN));
		castAnimations.put(EnumDirection.DOWN_RIGHT, castAnimations.get(EnumDirection.DOWN));
		castAnimations.put(EnumDirection.UP_LEFT, castAnimations.get(EnumDirection.UP));
		castAnimations.put(EnumDirection.UP_RIGHT, castAnimations.get(EnumDirection.UP));

		return movablePalette;
	}

	public MovablePalette(Palette palette) {
		super(palette);
		moveTextures = new HashMap<>();
		runTextures = new HashMap<>();
		castTextures = new HashMap<>();
	}

	public ITexture getTexture(EnumDirection direction, boolean moving, boolean running,  boolean casting) {
		if (!moving && !casting) return getTexture(direction);
		ITexture texture = (casting ? castTextures : (running ? runTextures : moveTextures)).get(direction);
		if (texture == null) throw new NoSuchElementException();
		return texture;
	}

	@Override
	public void addToWorld(World world) {
		super.addToWorld(world);
		moveTextures.values().stream().filter(target -> target instanceof GObject)
				.forEach(target -> world.addGObject((GObject) target));
		runTextures.values().stream().filter(target -> target instanceof GObject)
				.forEach(target -> world.addGObject((GObject) target));
		castTextures.values().stream().filter(target -> target instanceof GObject)
				.forEach(target -> world.addGObject((GObject) target));
	}
}
