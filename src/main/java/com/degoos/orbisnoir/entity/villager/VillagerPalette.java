package com.degoos.orbisnoir.entity.villager;

import com.degoos.graphicengine2.object.texture.Palette;
import com.degoos.graphicengine2.object.texture.TextureAbsImpl;
import com.degoos.orbisnoir.entity.palette.MovablePalette;
import com.degoos.orbisnoir.enums.EnumDirection;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.world.World;
import org.jooq.tools.json.ParseException;

import java.io.IOException;
import java.util.Map;

public class VillagerPalette extends MovablePalette {

	public VillagerPalette(Palette palette, Map<EnumDirection, TextureAbsImpl> stayAnimations,
	                       Map<EnumDirection, TextureAbsImpl> moveAnimations, Map<EnumDirection, TextureAbsImpl> runAnimations) {
		super(palette);
	}

	public VillagerPalette(World world) throws IOException, ParseException {
		super(PaletteManager.loadPalette("villager"));
		runTextures = moveTextures;

		moveTextures.put(EnumDirection.DOWN, palette.getAnimation(0));
		moveTextures.put(EnumDirection.UP, palette.getAnimation(1));
		moveTextures.put(EnumDirection.LEFT, palette.getAnimation(2));
		moveTextures.put(EnumDirection.RIGHT, palette.getAnimation(3));

		moveTextures.put(EnumDirection.DOWN_LEFT, moveTextures.get(EnumDirection.DOWN));
		moveTextures.put(EnumDirection.DOWN_RIGHT, moveTextures.get(EnumDirection.DOWN));
		moveTextures.put(EnumDirection.UP_LEFT, moveTextures.get(EnumDirection.UP));
		moveTextures.put(EnumDirection.UP_RIGHT, moveTextures.get(EnumDirection.UP));

		textures.put(EnumDirection.DOWN, (TextureAbsImpl) palette.getTexture(0));
		textures.put(EnumDirection.UP, (TextureAbsImpl) palette.getTexture(1));
		textures.put(EnumDirection.LEFT, (TextureAbsImpl) palette.getTexture(2));
		textures.put(EnumDirection.RIGHT, (TextureAbsImpl) palette.getTexture(3));

		textures.put(EnumDirection.DOWN_LEFT, textures.get(EnumDirection.DOWN));
		textures.put(EnumDirection.DOWN_RIGHT, textures.get(EnumDirection.DOWN));
		textures.put(EnumDirection.UP_LEFT, textures.get(EnumDirection.UP));
		textures.put(EnumDirection.UP_RIGHT, textures.get(EnumDirection.UP));

		moveTextures.values().forEach(target -> world.addGObject((TextureAbsImpl) target));
		textures.values().forEach(target -> world.addGObject((TextureAbsImpl) target));
	}
}
