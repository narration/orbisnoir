package com.degoos.orbisnoir.entity;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.Collision;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.sketch.EntityEntitySketch;
import com.degoos.orbisnoir.entity.sketch.EntitySketch;
import com.degoos.orbisnoir.enums.EnumBlockCollision;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.interfaces.Positionable;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.world.Box;
import com.degoos.orbisnoir.world.Camera;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import java.util.*;

public class Entity implements Positionable {

	protected Rectangle rectangle;

	protected World world;
	protected Vector2f position;
	protected EntitySketch sketch;

	protected Area entitySize;
	protected EnumEntityDrawPriority drawPriority;
	protected EnumEntityCollision collision;
	protected CollisionBox collisionBox;

	protected Script script;
	protected Controller controller;
	protected boolean onPlatform;
	protected boolean deleted;

	protected boolean slept;
	protected EntitySizeDisplayer sizeDisplayer;
	protected CollisionBoxDisplayer collisionBoxDisplayer;

	protected int tickPriority;

	public Entity(World world, EntityEntitySketch sketch) {
		this(world, sketch.getPosition(), sketch.getColor(), sketch.getOpacity(), sketch.getEntitySize(),
				sketch.getEntityDrawPriority(), sketch.getCollisionBox(),
				sketch.getCollisionType(), null, sketch.getScript());
		setName(sketch.getName());
		this.sketch = sketch;
	}

	public Entity(World world, Vector2f position, GColor color, float opacity, Area entitySize, EnumEntityDrawPriority drawPriority,
				  CollisionBox collisionBox, EnumEntityCollision collision, Controller controller, Script script) {
		this.world = world;

		this.position = position;
		this.entitySize = entitySize;
		this.drawPriority = drawPriority;
		this.collisionBox = collisionBox;
		this.collision = collision;
		this.sketch = null;
		this.controller = controller;
		this.script = script;
		this.onPlatform = false;
		this.deleted = false;

		this.slept = false;
		this.sizeDisplayer = null;
		this.collisionBoxDisplayer = null;
		this.tickPriority = 0;

		collisionBox.setPosition(position);

		Camera camera = world.getCamera();
		this.rectangle = new Rectangle(camera.toGamePosition(new Vector3f(position, 0)),
				new Vector3f(entitySize.getMin(), 0).mul(camera.getBoxSize()),
				new Vector3f(entitySize.getMax(), 0).mul(camera.getBoxSize()));
		rectangle.setVisible(true);
		rectangle.setKey(rectangle.getUuid().toString());
		rectangle.setColor(color);
		rectangle.setOpacity(opacity);
	}

	public World getWorld() {
		return world;
	}

	public UUID getUuid() {
		return rectangle.getUuid();
	}

	public String getName() {
		return rectangle.getKey();
	}

	public void setName(String name) {
		rectangle.setKey(name);
	}

	public Rectangle getRectangle() {
		return rectangle;
	}

	@Override
	public Vector2f getPosition() {
		return position;
	}

	public Vector2i getBoxPosition() {
		return new Vector2i((int) Math.floor(position.x), (int) Math.floor(position.y));
	}

	@Override
	public void setPosition(Vector2f position) {
		this.position = position;
		collisionBox.setPosition(position);
		recalculateRectangle();
	}

	public Area getEntitySize() {
		return entitySize;
	}

	public void setEntitySize(Area entitySize) {
		this.entitySize = entitySize;
		Camera camera = world.getCamera();
		rectangle.setMin(new Vector3f(entitySize.getMin(), 0).mul(camera.getBoxSize()));
		rectangle.setMax(new Vector3f(entitySize.getMax(), 0).mul(camera.getBoxSize()));
	}

	public EnumEntityDrawPriority getDrawPriority() {
		return drawPriority;
	}

	public void setDrawPriority(EnumEntityDrawPriority drawPriority) {
		this.drawPriority = drawPriority;
	}

	public CollisionBox getCollisionBox() {
		return collisionBox;
	}

	public void setCollisionBox(CollisionBox collisionBox) {
		this.collisionBox = collisionBox;
	}

	public EnumEntityCollision getCollision() {
		return collision;
	}

	public void setCollision(EnumEntityCollision collision) {
		this.collision = collision;
	}

	public Controller getController() {
		return controller;
	}

	public void setController(Controller controller) {
		this.controller = controller;
	}

	public Script getScript() {
		return script;
	}

	public void setScript(Script script) {
		this.script = script;
	}

	public EntitySketch getSketch() {
		return sketch;
	}

	public void setSketch(EntitySketch sketch) {
		this.sketch = sketch;
	}

	public boolean isSlept() {
		return slept;
	}

	public void setSlept(boolean slept) {
		this.slept = slept;
	}

	public boolean isOnPlatform() {
		return onPlatform;
	}

	public void setOnPlatform(boolean onPlatform) {
		this.onPlatform = onPlatform;
	}

	public int getTickPriority() {
		return tickPriority;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void delete() {
		deleted = true;
		world.unregisterEntity(rectangle.getUuid());
		deleteEditorHints();
	}

	public EnumBlockCollision getCurrentBoxCollision() {
		try {
			Vector2f mid = collisionBox.getCenter();
			Box currentBox = world.getBox((int) Math.floor(mid.x), (int) Math.floor(mid.y));
			return currentBox.getCollision();
		} catch (NoSuchElementException ex) {
			return EnumBlockCollision.LEVEL_0;
		}
	}

	public boolean move(Vector2f displacement, boolean checkCollisions) {
		boolean collide = false;
		displacement = new Vector2f(displacement);
		if (displacement.x == 0 && displacement.y == 0) return false;

		int loops = 1;
		while (Math.abs(displacement.x) > 0.2f || Math.abs(displacement.y) > 0.2f) {
			loops = loops << 1;
			displacement.mul(0.5f);
		}
		EnumBlockCollision currentBlockCollision;
		for (int i = 0; i < loops; i++) {
			currentBlockCollision = getCurrentBoxCollision();
			collisionBox.setPosition(new Vector2f(position).add(displacement));

			Area area = new Area(position, collisionBox.getPosition());
			area.getMin().add(collisionBox.getMin());
			area.getMax().add(collisionBox.getMax());

			List<Box> boxes = getBoxesToCheck(area);
			boxes.sort(Comparator.comparingDouble(o -> o.getCollisionBox().getCenter()
					.distanceSquared(collisionBox.getCenter())));

			Collision collision;
			for (Box box : boxes) {
				if (deleted) return collide;
				collision = collisionBox.collision(box.getCollisionBox());
				if (collision == null) continue;
				box.collide(this, collision);
				collideSelfBox(box, collision);
				if (!checkCollisions || currentBlockCollision.canPassThrough(box.getCollision())) continue;
				collisionBox.getPosition().add(collision.getMtv());
				collide = true;
			}

			//CHECKING ENTITIES
			for (Entity entity : world.getEntities().values()) {
				if (deleted) return collide;
				if (entity == this) continue;
				collision = collisionBox.collision(entity.collisionBox);
				if (collision == null) continue;
				entity.collide(this, collision);
				collideSelfEntity(entity, collision);
				if (checkCollisions && entity.getCollision() != EnumEntityCollision.PASS) {
					if (Float.isNaN(collision.getMtv().x) || Float.isNaN(collision.getMtv().y)) continue;
					collisionBox.getPosition().add(collision.getMtv());
					collide = true;
				}
			}

			position.set(collisionBox.getPosition());
		}
		return collide;
	}

	private List<Box> getBoxesToCheck(Area area) {
		List<Box> boxes = new ArrayList<>();
		Box box;

		for (int x = (int) Math.floor(area.getMin().x); x <= area.getMax().x; x++) {
			for (int y = (int) Math.floor(area.getMin().y); y <= area.getMax().y; y++) {
				try {
					box = world.getBox(x, y);
				} catch (NoSuchElementException ex) {
					continue;
				}
				boxes.add(box);
			}
		}
		return boxes;
	}

	public void displayEditorHints() {
		if (sizeDisplayer != null || collisionBoxDisplayer != null) return;
		sizeDisplayer = new EntitySizeDisplayer(this);
		collisionBoxDisplayer = new CollisionBoxDisplayer(this);
		world.addGObject(sizeDisplayer);
		world.addGObject(collisionBoxDisplayer);
	}

	public void deleteEditorHints() {
		if (sizeDisplayer != null) {
			sizeDisplayer.delete();
			sizeDisplayer = null;
		}
		if (collisionBoxDisplayer != null) {
			collisionBoxDisplayer.delete();
			collisionBoxDisplayer = null;
		}
	}

	public void tick(long dif) {
		deleted = false;
		if (!slept && controller != null) controller.onTick(this, dif);
	}

	public void tick2(long dif) {
		recalculateRectangle();
		onPlatform = false;
	}

	private void recalculateRectangle() {
		Camera camera = world.getCamera();
		Vector3f pos = camera.toEnginePosition(position, 0);
		refreshDrawPriority(pos);
		rectangle.setPosition(pos);

		rectangle.setVisible(Engine.getRenderArea().collide(
				new Area(rectangle.getMin().x, rectangle.getMin().y,
						rectangle.getMax().x, rectangle.getMax().y)));

		if (camera.requiresResize()) {
			rectangle.setMin(new Vector3f(entitySize.getMin(), 0).mul(camera.getBoxSize()));
			rectangle.setMax(new Vector3f(entitySize.getMax(), 0).mul(camera.getBoxSize()));
		}
		onPlatform = false;
	}

	protected void refreshDrawPriority(Vector3f pos) {
		switch (drawPriority) {
			case RELATIVE:
				pos.z = -position.y / 1000000f;
				break;
			case ALWAYS_TOP:
				pos.z = 0.1f;
				break;
			case ALWAYS_TOP_2:
				pos.z = 0.10001f;
				break;
			case ALWAYS_TOP_3:
				pos.z = 0.10002f;
				break;
			case ALWAYS_BOTTOM:
				pos.z = -0.1f;
				break;
			case ALWAYS_BOTTOM_2:
				pos.z = -0.10001f;
				break;
			case ALWAYS_BOTTOM_3:
				pos.z = -0.10002f;
				break;
		}
	}

	public void keyEvent(KeyEvent event) {
		if (!slept && controller != null) controller.keyEvent(this, event);
	}

	public void collide(Entity entity, Collision collision) {
	}

	public void collideSelfEntity(Entity entity, Collision collision) {
	}

	public void collideSelfBox(Box box, Collision collision) {
	}

	public void restoreToSketch() {
		if (sketch == null) return;
		rectangle.setKey(sketch.getName());
		this.script = sketch.getScript();
		this.entitySize = sketch.getEntitySize();
		this.collisionBox = sketch.getCollisionBox();
		this.collision = sketch.getCollisionType();
		this.drawPriority = sketch.getEntityDrawPriority();

		setPosition(sketch.getPosition());

		rectangle.setColor(sketch.getColor());
		rectangle.setOpacity(sketch.getOpacity());
		setEntitySize(entitySize);
		rectangle.setVisible(true);
	}
}
