package com.degoos.orbisnoir;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.io.Keyboard;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Room;
import com.degoos.orbisnoir.collision.Collision;
import com.degoos.orbisnoir.collision.CollisionBox;
import org.joml.Vector2f;
import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.List;

public class CollisionTest extends Room {

	public CollisionTest() {
		super("test");

		List<Vector2f> vertices1 = new ArrayList<>();
		vertices1.add(new Vector2f(-0.25f, -0.25f));
		vertices1.add(new Vector2f(0.25f, -0.25f));
		vertices1.add(new Vector2f(0.25f, 0.25f));
		vertices1.add(new Vector2f(-0.25f, 0.25f));

		CollisionBox box1 = new CollisionBox(new Vector2f(0), vertices1);

		List<Vector2f> vertices2 = new ArrayList<>();
		vertices2.add(new Vector2f(-0.25f, -0.25f));
		vertices2.add(new Vector2f(0.25f, -0.25f));
		vertices2.add(new Vector2f(0.25f, 0.25f));
		vertices2.add(new Vector2f(-0.25f, 0.25f));

		CollisionBox box2 = new CollisionBox(new Vector2f(0f, 0f), vertices2);

		Rectangle rectangle = new Rectangle(new Vector3f(0), new Vector3f(-0.25f, -0.25f, 0), new Vector3f(0.25f, 0.25f, 0)) {
			@Override
			public void onTick(long dif, Room room) {

				Keyboard keyboard = Engine.getKeyboard();
				if (keyboard.isKeyPressed(EnumKeyboardKey.ARROW_UP))
					box1.getPosition().y += 0.01f;
				if (keyboard.isKeyPressed(EnumKeyboardKey.ARROW_DOWN))
					box1.getPosition().y -= 0.01f;
				if (keyboard.isKeyPressed(EnumKeyboardKey.ARROW_LEFT))
					box1.getPosition().x -= 0.01f;
				if (keyboard.isKeyPressed(EnumKeyboardKey.ARROW_RIGHT))
					box1.getPosition().x += 0.01f;

				Collision collision = box1.collision(box2);

				if (collision != null) {
					box1.setPosition(box1.getPosition().add(collision.getMtv()));
				}

				setPosition(new Vector3f(box1.getPosition(), 0));
			}
		};

		Rectangle rectangle2 = new Rectangle(new Vector3f(0), new Vector3f(-0.25f, -0.25f, 0), new Vector3f(0.25f, 0.25f, 0)) {
			@Override
			public void onTick(long dif, Room room) {
				setPosition(new Vector3f(box2.getPosition(), 0));
			}
		};


		rectangle.setColor(GColor.GREEN);
		rectangle.setVisible(true);
		rectangle2.setVisible(true);
		addGObject(rectangle);
		addGObject(rectangle2);
	}

}
