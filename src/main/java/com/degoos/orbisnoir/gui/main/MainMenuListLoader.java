package com.degoos.orbisnoir.gui.main;

import com.degoos.orbisnoir.Game;
import com.degoos.orbisnoir.gui.main.option.*;
import com.degoos.orbisnoir.gui.main.option.dev.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainMenuListLoader {

	protected static Map<String, MainMenuOptionList> lists;

	public static MainMenuOptionList getList(String name) {
		return lists.get(name);
	}

	public static void loadLists(MainMenu mainMenu) {
		lists = new HashMap<>();
		loadMainList(mainMenu);
		loadNewSaveList(mainMenu);

		List<String> worlds = getWorldList();

		loadDevList(mainMenu, worlds);
		loadWorldList(mainMenu, worlds);
	}

	public static void loadMainList(MainMenu mainMenu) {
		List<MainMenuOption> options = new ArrayList<>();
		options.add(new MainMenuOptionExit(0));
		options.add(new MainMenuOptionNewSave(1));
		options.add(new MainMenuOptionLoadSave(2));
		if (Game.isIde())
			options.add(new MainMenuOptionDevMenu(3));
		MainMenuOptionList list = new MainMenuOptionList(mainMenu, options, 0, true);
		mainMenu.addGObject(list);
		lists.put("main", list);
	}

	public static void loadNewSaveList(MainMenu mainMenu) {
		List<MainMenuOption> options = new ArrayList<>();
		options.add(new MainMenuOptionNewSaveNo(0));
		options.add(new MainMenuOptionNewSaveYes(1));
		MainMenuOptionList list = new MainMenuOptionList(mainMenu, options, 1, false);
		mainMenu.addGObject(list);
		lists.put("new_save", list);
	}

	public static void loadDevList(MainMenu mainMenu, List<String> worlds) {
		List<MainMenuOption> options = new ArrayList<>();

		options.add(new MainMenuOptionMoveToList(0, "Volver", "main", 0));
		options.add(new MainMenuOptionDevResetSave(1));
		options.add(new MainMenuOptionDevCreateWorld(2, worlds));
		options.add(new MainMenuOptionDevUnlockAllAbilities(3));
		options.add(new MainMenuOptionDevWorldList(4));
		MainMenuOptionList list = new MainMenuOptionList(mainMenu, options, 1, false);
		mainMenu.addGObject(list);
		lists.put("dev", list);
	}

	public static void loadWorldList(MainMenu mainMenu, List<String> worlds) {
		List<MainMenuOption> options = new ArrayList<>();
		options.add(new MainMenuOptionMoveToList(0, "Volver", "dev", 1));
		int i = 1;
		for (String name : worlds) {
			options.add(new MainMenuOptionDevWorld(i, name));
			i++;
		}

		MainMenuOptionList list = new MainMenuOptionList(mainMenu, options, 2, false);
		mainMenu.addGObject(list);
		lists.put("world_list", list);
	}

	private static List<String> getWorldList() {
		List<String> list = new ArrayList<>();
		File folder = new File("worlds");
		if (!folder.isDirectory()) return list;
		for (File file : folder.listFiles()) {
			if (file.isDirectory()) list.add(file.getName());
		}
		return list;
	}

}
