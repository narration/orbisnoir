package com.degoos.orbisnoir.gui.main.option;

import com.degoos.orbisnoir.gui.main.MainMenuListLoader;
import com.degoos.orbisnoir.gui.main.MainMenuOption;

public class MainMenuOptionMoveToList extends MainMenuOption {
	public MainMenuOptionMoveToList(int index, String text, String list, int submenu) {
		super(index, text, (mainMenuOption, mainMenuOptionList) ->
				mainMenuOptionList.setSelected(false, submenu, () ->
						MainMenuListLoader.getList(list).setSelected(true, submenu, null)));
	}
}
