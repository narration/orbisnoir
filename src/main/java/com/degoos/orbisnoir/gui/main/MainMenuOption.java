package com.degoos.orbisnoir.gui.main;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.object.Text;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.chat.Chat;
import org.joml.Vector3f;

import java.awt.*;
import java.util.function.BiConsumer;

public class MainMenuOption extends Rectangle {

	private static final float SPACE = 0.1f;

	private Text text;

	private int index;
	private boolean selected, onAnimation;

	private BiConsumer<MainMenuOption, MainMenuOptionList> on;

	public MainMenuOption(int index, String text, BiConsumer<MainMenuOption, MainMenuOptionList> on) {
		super(new Vector3f(0.1f, index * SPACE, 0.01f), new Vector3f(0), new Vector3f(0.3f, 0.1f, 0));
		setVisible(true);
		this.index = index;
		this.on = on;
		this.text = new Text(Chat.FONT, new Vector3f(0), new Area(0, 0, getMax().x, getMax().y), text, GColor.WHITE, 1);
		this.text.setVisible(true);
		this.text.setParent(this);
		this.text.setXCentered(false);
		this.text.setColor(selected ? new GColor(1, 1, 0).darker() : GColor.WHITE);
		this.text.setFixedScale(0.002f);

		//setTexture(TextureManager.loadImageOrNull("text/textbox.png"));
		setOpacity(0);
	}

	public Text getText() {
		return text;
	}

	public boolean isSelected() {
		return selected;
	}

	public BiConsumer<MainMenuOption, MainMenuOptionList> getOn() {
		return on;
	}

	public void setOn(BiConsumer<MainMenuOption, MainMenuOptionList> on) {
		this.on = on;
	}

	public void on(MainMenuOptionList list) {
		if (on != null) on.accept(this, list);
	}

	public void setSelected(boolean selected) {
		if (this.selected == selected) return;
		this.selected = selected;
		onAnimation = true;
		text.setColor(selected ? new GColor(1, 1, 0).darker() : GColor.WHITE);
	}

	public void setIndexOffset(int offset, int size) {
		position.y = (index - offset) * SPACE;
		requiresRecalculation = true;
	}

	@Override
	public void onTick(long dif, Room room) {
		if (onAnimation) {
			if (selected) {
				position.x += dif / 1000000000f;
				if (position.x >= 0.2f) {
					position.x = 0.2f;
					onAnimation = false;
				}
			} else {
				position.x -= dif / 1000000000f;
				if (position.x <= 0.1f) {
					position.x = 0.1f;
					onAnimation = false;
				}
			}
			requiresRecalculation = true;
		}
	}
}
