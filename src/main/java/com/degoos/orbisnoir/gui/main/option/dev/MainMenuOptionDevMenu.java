package com.degoos.orbisnoir.gui.main.option.dev;

import com.degoos.orbisnoir.gui.main.option.MainMenuOptionMoveToList;

public class MainMenuOptionDevMenu extends MainMenuOptionMoveToList {

	public MainMenuOptionDevMenu(int index) {
		super(index, "Dev", "dev", 1);
	}
}
