package com.degoos.orbisnoir.gui.main.option.dev;

import com.degoos.orbisnoir.Game;
import com.degoos.orbisnoir.gui.main.MainMenuOption;
import com.degoos.orbisnoir.save.Save;

import java.io.File;

public class MainMenuOptionDevResetSave extends MainMenuOption {

	public MainMenuOptionDevResetSave(int index) {
		super(index, "Reset save file", (mainMenuOption, list) -> {
			Game.setSave(new Save(new File("save.dat"), false));
			Game.getSave().save();
		});
	}
}
