package com.degoos.orbisnoir.gui.main.option;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.orbisnoir.Game;
import com.degoos.orbisnoir.gui.BlackScreen;
import com.degoos.orbisnoir.gui.main.MainMenuOption;
import com.degoos.orbisnoir.world.World;
import com.degoos.orbisnoir.world.WorldManager;

public class MainMenuOptionLoadSave extends MainMenuOption {

	public MainMenuOptionLoadSave(int index) {
		super(index, "Cargar partida", (mainMenuOption, mainMenuOptionList) -> {

			mainMenuOptionList.getMainMenu().stopMusic();

			mainMenuOptionList.getMainMenu().addGObject(new BlackScreen().setOpacity(1, 500000000, () -> {
				try {
					World world = WorldManager.loadWorld(Game.getSave().player_room, Game.getSave().player_position);
					mainMenuOptionList.getMainMenu().setBackground(GColor.BLACK);
					Engine.setRoom(world);
					world.runInitScript();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}));
		});
	}
}
