package com.degoos.orbisnoir.gui.main.option.dev;

import com.degoos.orbisnoir.gui.main.option.MainMenuOptionMoveToList;

public class MainMenuOptionDevWorldList extends MainMenuOptionMoveToList {

	public MainMenuOptionDevWorldList(int index) {
		super(index, "World list", "world_list", 2);
	}
}
