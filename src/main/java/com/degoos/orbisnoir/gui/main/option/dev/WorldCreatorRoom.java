package com.degoos.orbisnoir.gui.main.option.dev;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.object.Text;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.chat.Chat;
import com.degoos.orbisnoir.enums.EnumBlockCollision;
import com.degoos.orbisnoir.gui.main.MainMenu;
import com.degoos.orbisnoir.gui.text.KeyboardInput;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.world.*;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import java.io.File;
import java.util.List;

public class WorldCreatorRoom extends Room {

	private MainMenu mainMenu;

	public WorldCreatorRoom(List<String> worldList, MainMenu mainMenu) {
		super("world creator");
		this.mainMenu = mainMenu;
		addGObject(new Text(true, 0, Chat.FONT, new Vector3f(0, 0.4f, 0),
				new Area(-1, -0.1f, 1, 0.2f), "Insert a name", GColor.WHITE, 1));

		KeyboardInput input = new KeyboardInput("", new Vector3f(0, 0, 0), new Area(-1, -0.2f, 1, 0.2f)) {
			@Override
			public void onKeyboardEvent(KeyEvent event) {
				if (event instanceof KeyPressEvent) {
					if (event.getKeyboardKey() == EnumKeyboardKey.ENTER) {
						if (getColor().equals(GColor.GREEN)) {
							createWorld(getText());
							return;
						} else return;
					} else if (event.getKeyboardKey() == EnumKeyboardKey.ESCAPE) {
						Engine.setRoom(mainMenu);
					}
				}
				super.onKeyboardEvent(event);
				setColor(getText().isEmpty() || worldList.stream().anyMatch(target ->
						target.equalsIgnoreCase(getText())) ? GColor.RED : GColor.GREEN);
			}
		};
		addGObject(input);
	}

	public void createWorld(String name) {
		try {
			File worldFolder = new File(WorldManager.worldsFolder, name);
			worldFolder.mkdirs();
			File chunkFolder = new File(worldFolder, "chunks");
			chunkFolder.mkdirs();
			World world = new World(name, new EntitySketchContainer(new File(worldFolder, "entities.dat")), new Vector2f(16, 16), false);
			Chunk chunk = new Chunk(world, new Vector2i(0, 0), PaletteManager.loadPalette("outworld"), "outworld");
			for (int x = 10; x < 20; x++) {
				for (int y = 10; y < 20; y++) {
					chunk.registerBox(new Box(new Vector2i(x, y), chunk, new Vector2i(1, 6), EnumBlockCollision.LEVEL_0, null));
				}
			}
			world.registerChunk(chunk);
			WorldManager.saveChunk(chunk);
			world.getEntitySketchContainer().saveSketches();
			mainMenu.getTheme().cleanup();
			Engine.setRoom(world);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
