package com.degoos.orbisnoir.gui.main.option;

import com.degoos.graphicengine2.Engine;
import com.degoos.orbisnoir.gui.main.MainMenuOption;

public class MainMenuOptionExit extends MainMenuOption {

	public MainMenuOptionExit(int index) {
		super(index, "Salir", (mainMenuOption, mainMenuOptionList) -> Engine.getRenderer().kill());
	}
}
