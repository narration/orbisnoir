package com.degoos.orbisnoir.gui.main;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.event.window.WindowResizeEvent;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.sound.SoundSource;
import com.degoos.orbisnoir.handler.VolumeHandler;
import com.degoos.orbisnoir.texture.TextureManager;
import org.joml.Vector3f;


public class MainMenu extends Room {

	private SoundSource theme;

	public MainMenu() {
		super("main menu");
		MainMenuListLoader.loadLists(this);

		addGObject(new MainMenuFog());
		addGObject(new MainMenuBackground());

		theme = Engine.getSoundManager().createSoundSource("music/main_theme.ogg", "background", true, true);
		theme.setGain(0.5f);
		theme.setPitch(0.7f);
		theme.play();

		Rectangle rectangle = new Rectangle(new Vector3f(0), new Vector3f(Engine.getRenderArea().getMin().x, -1, 0),
				new Vector3f(Engine.getRenderArea().getMax().x, 1, 0)) {

			@Override
			public void onResize(WindowResizeEvent event) {
				setMin(new Vector3f(Engine.getRenderArea().getMin().x, -1, 0));
				setMax(new Vector3f(Engine.getRenderArea().getMax().x, 1, 0));
			}
		};
		rectangle.setTexture(TextureManager.loadImageOrNull("gui/main_menu_name.png"));
		addGObject(rectangle.setVisible(true));
	}

	public SoundSource getTheme() {
		return theme;
	}

	public void stopMusic() {
		addGObject(new VolumeHandler(theme, 0, 400000000, true, null));
	}
}
