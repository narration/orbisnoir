package com.degoos.orbisnoir.gui.main.option.dev;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.orbisnoir.gui.BlackScreen;
import com.degoos.orbisnoir.gui.main.MainMenuOption;
import com.degoos.orbisnoir.world.World;
import com.degoos.orbisnoir.world.WorldManager;
import org.joml.Vector2f;

public class MainMenuOptionDevWorld extends MainMenuOption {

	public MainMenuOptionDevWorld(int index, String name) {
		super(index, name, (mainMenuOption, mainMenuOptionList) -> {

			mainMenuOptionList.getMainMenu().stopMusic();

			mainMenuOptionList.getMainMenu().addGObject(new BlackScreen().setOpacity(1, 500000000, () -> {
				try {
					World world = WorldManager.loadWorld(name, new Vector2f(0));
					mainMenuOptionList.getMainMenu().setBackground(GColor.BLACK);
					Engine.setRoom(world);
					world.runInitScript();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}));
		});
	}
}
