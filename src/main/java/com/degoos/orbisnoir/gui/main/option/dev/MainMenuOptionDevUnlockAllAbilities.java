package com.degoos.orbisnoir.gui.main.option.dev;

import com.degoos.orbisnoir.Game;
import com.degoos.orbisnoir.gui.main.MainMenuOption;
import com.degoos.orbisnoir.save.Save;

public class MainMenuOptionDevUnlockAllAbilities extends MainMenuOption {

	public MainMenuOptionDevUnlockAllAbilities(int index) {
		super(index, "Unlock all abilities", (mainMenuOption, list) -> {
			Save save = Game.getSave();
			save.diary_unlocked = true;
			save.shield_unlocked = true;
			save.spanish_controller_unlocked = true;
			save.french_controller_unlocked = true;
			save.diary_page_0 = true;
			save.diary_page_1 = true;
			save.diary_page_2 = true;
			save.diary_page_3 = true;
			save.diary_page_4 = true;
			save.diary_page_5 = true;
			save.diary_page_6 = true;
			save.diary_page_7 = true;
			save.diary_page_8 = true;
			save.diary_page_9 = true;
			save.diary_page_10 = true;
			save.diary_page_11 = true;
			save.diary_page_12 = true;
			save.save();
		});
	}
}
