package com.degoos.orbisnoir.gui.main;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyHoldEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.object.GObject;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Room;
import com.degoos.orbisnoir.handler.Waiter;
import org.joml.Vector3f;

import java.util.List;

public class MainMenuOptionList extends Rectangle {

	private MainMenu mainMenu;
	private List<MainMenuOption> options;
	private int submenu;
	private float to;

	private MainMenuOption selectedOption;

	private boolean selected, onAnimation;
	private Runnable then;

	private int offset;

	public MainMenuOptionList(MainMenu mainMenu, List<MainMenuOption> options, int submenu, boolean selected) {
		super(new Vector3f(Engine.getRenderArea().getMin().x, -0.5f, 0), new Vector3f(), new Vector3f(1, 2, 0));
		setVisible(false);
		this.mainMenu = mainMenu;
		this.options = options;
		this.submenu = submenu;
		options.forEach(target -> {
			target.setParent(this);
			mainMenu.addGObject(target);
			mainMenu.addGObject(target.getText());
			target.setIndexOffset(0, options.size());
		});

		this.selectedOption = options.isEmpty() ? null : options.get(0);
		if (selectedOption != null) selectedOption.setSelected(true);

		this.selected = selected;
		this.onAnimation = false;
		offset = 0;

		to = 0.05f + submenu * 0.7f;
		if (submenu == 0 || selected) position.x += to;
		else position.x += -1;

	}

	public MainMenu getMainMenu() {
		return mainMenu;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected, int toSubmenu, Runnable then) {
		if (this.selected == selected) return;
		this.selected = selected;
		if (submenu < toSubmenu && !selected) {
			if (then != null) mainMenu.addGObject(new Waiter(1, waiter -> then.run()));
			return;
		}
		onAnimation = true;
		this.then = then;
	}

	@Override
	public void delete() {
		super.delete();
		options.forEach(GObject::delete);
		options.forEach(target -> target.getText().delete());
	}

	@Override
	public void onTick(long dif, Room room) {
		if (onAnimation) {
			if (selected) {
				position.x += dif * (3 + submenu) / 1000000000f;
				if (position.x >= Engine.getRenderArea().getMin().x + to) {
					position.x = Engine.getRenderArea().getMin().x + to;
					onAnimation = false;
					if (then != null) then.run();
				}
			} else {
				position.x -= dif * (3 + submenu) / 1000000000f;
				if (position.x <= Engine.getRenderArea().getMin().x - 1) {
					position.x = Engine.getRenderArea().getMin().x - 1;
					onAnimation = false;
					if (then != null) then.run();
				}
			}
			requiresRecalculation = true;
		}
	}

	@Override
	public void onKeyboardEvent(KeyEvent event) {
		if (!selected || options.isEmpty() || !(event instanceof KeyPressEvent || event instanceof KeyHoldEvent))
			return;
		EnumKeyboardKey key = event.getKeyboardKey();

		if (key == EnumKeyboardKey.ENTER) {
			if (selectedOption == null) return;
			selectedOption.on(this);
			return;
		} else if (key == EnumKeyboardKey.ARROW_UP) {
			offset++;
		} else if (key == EnumKeyboardKey.ARROW_DOWN) {
			offset--;
		} else return;

		if (offset < 0) offset = options.size() - 1;
		if (offset == options.size()) offset = 0;

		selectedOption.setSelected(false);
		selectedOption = options.get(offset);
		selectedOption.setSelected(true);

		options.forEach(target -> target.setIndexOffset(offset, options.size()));
	}
}
