package com.degoos.orbisnoir.gui.main.option.dev;

import com.degoos.graphicengine2.Engine;
import com.degoos.orbisnoir.gui.main.MainMenuOption;

import java.util.List;

public class MainMenuOptionDevCreateWorld extends MainMenuOption {

	public MainMenuOptionDevCreateWorld(int index, List<String> worlds) {
		super(index, "Create world", (mainMenuOption, list) -> {
			Engine.setRoom(new WorldCreatorRoom(worlds, list.getMainMenu()));
		});
	}
}
