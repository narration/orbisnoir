package com.degoos.orbisnoir.gui.main.option;

public class MainMenuOptionNewSave extends MainMenuOptionMoveToList {

	public MainMenuOptionNewSave(int index) {
		super(index, "Nueva partida", "new_save", 1);
	}
}
