package com.degoos.orbisnoir.gui.main;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Room;
import com.degoos.orbisnoir.texture.TextureManager;
import org.joml.Vector2f;
import org.joml.Vector3f;

import java.util.Random;

public class MainMenuBackground extends Rectangle {

	private Random random;
	private Vector2f velocity;

	public MainMenuBackground() {
		super(new Vector3f(0, 0, -0.999f), new Vector3f(Engine.getRenderArea().getMin(), 0),
				new Vector3f(Engine.getRenderArea().getMax(), 0));
		setVisible(true);
		velocity = new Vector2f(0.0001f, 0.0015f);
		random = new Random();


		setTexture(TextureManager.loadImageOrNull("gui/main_menu_background.png"));

		setOpacity(0);
	}


	@Override
	public void onTick(long l, Room room) {
		float dif = Math.min(1, l / 1000000f);
		Vector2f force = new Vector2f(position.x, position.y).mul(-0.0001f).add(random.nextFloat() * 0.00001f, random.nextFloat() * 0.000001f);
		velocity.add(force.mul(dif));
		position.add(new Vector3f(velocity, 0).mul(dif));
		position.x = Math.min(1, Math.max(-1, position.x));
		position.y = Math.min(1, Math.max(-1, position.y));
		requiresRecalculation = true;

		setMin(new Vector3f(Engine.getRenderArea().getMin().sub(1, 1), 0));
		setMax(new Vector3f(Engine.getRenderArea().getMax().add(1, 1), 0));

		setOpacity((float) Math.min((Math.abs(velocity.y) * 300) - 0.26, 0.5));
	}

}
