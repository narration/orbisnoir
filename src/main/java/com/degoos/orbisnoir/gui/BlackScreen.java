package com.degoos.orbisnoir.gui;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Room;
import org.joml.Vector3f;

public class BlackScreen extends Rectangle {

	private float relative;
	private float opacityPerNano;
	private float finalOpacity;

	private Runnable then;

	public BlackScreen() {
		super(new Vector3f(0), new Vector3f(0, -1, 0), new Vector3f(0, 1, 0));
		setKey("blackscreen");
		setVisible(true);
		position.z = 0.89f;
		relative = 0;
		opacityPerNano = 0;
		finalOpacity = 0;
		then = null;
		setColor(new GColor(0, 0, 0));
		setOpacity(0);
	}

	public BlackScreen setOpacity(float opacity, long nanos, Runnable then) {
		if (nanos == 0) {
			setOpacity(opacity);
			if (then != null) then.run();
			return this;
		}
		finalOpacity = opacity;
		opacityPerNano = (finalOpacity - getOpacity()) / (float) nanos;
		if (this.then != null) {
			this.then.run();
		}
		this.then = then;
		return this;
	}

	@Override
	public void onTick2(long dif, Room room) {
		if (relative != Engine.getRenderArea().getMax().x) {
			relative = Engine.getRenderArea().getMax().x;
			min.x = -relative;
			max.x = relative;
			requiresRecalculation = true;
		}
		if (opacityPerNano != 0) {

			setOpacity(getOpacity() + dif * opacityPerNano);
			if (opacityPerNano < 0 && finalOpacity >= getOpacity() || opacityPerNano > 0 && finalOpacity <= getOpacity()) {
				opacityPerNano = 0;
				if (then != null) {
					Runnable then = this.then;
					this.then = null;
					then.run();
				}
			}
		}
	}
}
