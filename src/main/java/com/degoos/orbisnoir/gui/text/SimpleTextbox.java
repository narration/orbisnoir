package com.degoos.orbisnoir.gui.text;

import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.event.drop.DropFilesEvent;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.window.WindowResizeEvent;
import com.degoos.graphicengine2.font.TrueTypeFont;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.GObject;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.object.Text;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.util.TextCutter;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.List;

public class SimpleTextbox extends GObject {


	protected Text text;
	protected List<String> lines;

	protected long textVelocity;

	protected int status;
	protected List<String> renderedText;
	protected int listIndex, stringIndex;
	protected long nanos;

	protected Runnable then;

	public SimpleTextbox(World world, TrueTypeFont font, String text, long textVelocity, boolean centered, GColor color, Vector2f position, Area area) {
		this(world, font, TextCutter.cut(text, font, (int) (area.getXSize() * 1920 / 5)), textVelocity, centered, color, position, area);
	}

	public SimpleTextbox(World world, TrueTypeFont font, List<String> lines, long textVelocity, boolean centered, GColor color, Vector2f position, Area area) {
		setVisible(true);
		this.lines = lines;

		text = new Text(font, new Vector3f(position, 0.9f), area, "", color, 1);
		text.setXCentered(centered);
		text.setVisible(true);


		world.addGObject(text);

		renderedText = new ArrayList<>();
		this.textVelocity = textVelocity;
		status = 0;
		then = null;
	}

	public Text getText() {
		return text;
	}

	public void setThen(Runnable then) {
		this.then = then;
	}

	@Override
	public void delete() {
		super.delete();
		text.delete();
	}

	@Override
	public void onTick(long l, Room room) {
		if (status == 0) textTick(l);

	}

	public void textTick(long l) {
		if (lines.isEmpty()) status = 1;
		nanos += l;
		String string = lines.get(listIndex);
		while (nanos > textVelocity) {
			nanos -= textVelocity;

			while (string.length() <= stringIndex) {
				listIndex++;
				if (lines.size() <= listIndex) {
					status = 1;
					return;
				}
				string = nextLine();
			}


			if (stringIndex == 0)
				renderedText.add(listIndex, string.substring(0, stringIndex + 1));
			else renderedText.set(listIndex, string.substring(0, stringIndex + 1));
			stringIndex++;
		}
		StringBuilder builder = new StringBuilder();
		for (String s : renderedText)
			builder.append(s).append('\n');

		text.setText(builder.toString());
	}

	private String nextLine() {
		stringIndex = 0;
		return lines.get(listIndex);
	}

	@Override
	public void onTick2(long l, Room room) {
	}

	@Override
	public void onAsyncTick(long l, Room room) {

	}

	@Override
	public void onMouseEvent(MouseEvent mouseEvent) {

	}

	@Override
	public void onKeyboardEvent(KeyEvent keyEvent) {
		if (!(keyEvent instanceof KeyPressEvent)) return;
		if (status == 0) {
			if (keyEvent.getKeyboardKey().equals(EnumKeyboardKey.Z)) {
				renderedText = lines;
				StringBuilder builder = new StringBuilder();
				for (String s : renderedText)
					builder.append(s).append('\n');
				text.setText(builder.toString());
				status = 1;
			}
		} else {
			if (keyEvent.getKeyboardKey().equals(EnumKeyboardKey.X) && then != null) {
				delete();
				if (then != null)
					then.run();
			}
		}
	}

	@Override
	public void onResize(WindowResizeEvent windowResizeEvent) {

	}

	@Override
	public void onDropFilesEvent(DropFilesEvent dropFilesEvent) {

	}
}
