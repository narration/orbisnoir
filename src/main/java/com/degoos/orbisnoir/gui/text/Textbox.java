package com.degoos.orbisnoir.gui.text;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.event.drop.DropFilesEvent;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.window.WindowResizeEvent;
import com.degoos.graphicengine2.object.*;
import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.sound.SoundSource;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.chat.Chat;
import com.degoos.orbisnoir.texture.TextureManager;
import com.degoos.orbisnoir.util.TextCutter;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector3f;

import java.util.List;

public class Textbox extends GObject {

	private Rectangle textRectangle;
	private Rectangle portraitRectangle;
	private Rectangle portraitBase;

	private ITexture portrait;

	private Text text;
	private List<String> lines;

	private boolean canBeClosed;
	private long textVelocity;

	private int status;
	private String[] renderedText;
	private int listIndex, stringIndex;
	private long nanos;

	private Runnable then;

	public Textbox(World world, String text, boolean executeMoveAnimation, ITexture texture) {
		this(world, TextCutter.cut(text, Chat.FONT, (int) (1920 * 0.62)), executeMoveAnimation, texture);
	}

	public Textbox(World world, List<String> lines, boolean executeMoveAnimation, ITexture portrait) {
		this.lines = lines;
		this.portrait = portrait;

		boolean hasPortrait = portrait != null;

		textRectangle = new Rectangle(new Vector3f(hasPortrait ? 0.4f : 0, executeMoveAnimation ? -1.8f : -1, 0.905f),
				new Vector3f(-1, 0, 0), new Vector3f(1, 0.8f, 0));
		textRectangle.setOpacity(0.95f);
		textRectangle.setTexture(TextureManager.loadImageOrNull("text/textbox.png"));
		textRectangle.setVisible(true);

		if (hasPortrait) {
			portraitBase = new Rectangle(new Vector3f(0), new Vector3f(-1.8f, 0, 0), new Vector3f(-1, 0.8f, 0));
			portraitBase.setTexture(textRectangle.getTexture());
			portraitBase.setVisible(true);
			portraitBase.setParent(textRectangle);
			portraitRectangle = new Rectangle(new Vector3f(0, 0, 0.01f),
					new Vector3f(-1.78f, 0.02f, 0), new Vector3f(-1.02f, 0.78f, 0));
			portraitRectangle.setTexture(portrait);
			portraitRectangle.setVisible(true);
			portraitRectangle.setParent(portraitBase);
		} else portraitRectangle = portraitBase = null;

		text = new Text(Chat.FONT, new Vector3f(0, 0, 0.02f), new Area(textRectangle.getMin().x + 0.1f,
				textRectangle.getMin().y + 0.1f, textRectangle.getMax().x - 0.1f, textRectangle.getMax().y - 0.1f),
				"", GColor.BLACK, 1);
		text.setFixedScale(0.003f);
		text.setYCentered(false);
		text.setVisible(true);
		text.setParent(textRectangle);

		world.addGObject(textRectangle);
		world.addGObject(text);
		if (hasPortrait) {
			world.addGObject(portraitRectangle);
			world.addGObject(portraitBase);
		}

		renderedText = new String[]{"", "", ""};
		textVelocity = 58000000;
		status = executeMoveAnimation ? 0 : 1;

		canBeClosed = true;
		then = null;
	}

	public boolean isCanBeClosed() {
		return canBeClosed;
	}

	public void setCanBeClosed(boolean canBeClosed) {
		this.canBeClosed = canBeClosed;
	}

	public void setThen(Runnable then) {
		this.then = then;
	}

	@Override
	public void delete() {
		super.delete();
		textRectangle.delete();
		text.delete();
		if (portrait != null) {
			portraitRectangle.delete();
			portraitBase.delete();
		}
	}

	@Override
	public void onTick(long l, Room room) {
		if (status == 0) animationTick(l);
		else if (status == 1) textTick(l);

	}

	private void animationTick(long l) {
		float add = l / 350000000f;
		textRectangle.getPosition().y += add;
		if (textRectangle.getPosition().y >= -1) {
			textRectangle.getPosition().y = -1;
			status = 1;
		}
		textRectangle.setRequiresRecalculation(true);
	}

	public void textTick(long l) {
		if (lines.isEmpty()) status = 2;
		nanos += l;
		String string = lines.get(listIndex);
		while (nanos > textVelocity) {
			nanos -= textVelocity;

			while (string.length() <= stringIndex) {
				listIndex++;
				if (lines.size() <= listIndex) {
					status = 2;
					return;
				}
				string = nextLine();
			}

			renderedText[Math.min(2, listIndex)] += string.charAt(stringIndex);
			stringIndex++;

			if (stringIndex % 2 == 0) {
				SoundSource source = Engine.getSoundManager().createSoundSource("sound/click.ogg", "click", false, true);
				source.setGain(1);
				source.setPitch(3);
				source.play();
			}

		}
		text.setText(renderedText[0] + '\n' + renderedText[1] + '\n' + renderedText[2]);
	}

	private String nextLine() {
		stringIndex = 0;

		if (listIndex > 2) {
			renderedText[0] = renderedText[1];
			renderedText[1] = renderedText[2];
			renderedText[2] = "";
		}

		return lines.get(listIndex);
	}

	@Override
	public void onTick2(long l, Room room) {
	}

	@Override
	public void onAsyncTick(long l, Room room) {

	}

	@Override
	public void onMouseEvent(MouseEvent mouseEvent) {

	}

	@Override
	public void onKeyboardEvent(KeyEvent keyEvent) {
		if (!canBeClosed || !(keyEvent instanceof KeyPressEvent)) return;
		if (status == 1) {
			if (keyEvent.getKeyboardKey() != null && keyEvent.getKeyboardKey().equals(EnumKeyboardKey.Z) && !lines.isEmpty()) {
				if (lines.size() == 1) {
					renderedText[0] = lines.get(0);
				} else if (lines.size() == 2) {
					renderedText[0] = lines.get(0);
					renderedText[1] = lines.get(1);
				} else {
					renderedText[0] = lines.get(lines.size() - 3);
					renderedText[1] = lines.get(lines.size() - 2);
					renderedText[2] = lines.get(lines.size() - 1);
				}
				text.setText(renderedText[0] + '\n' + renderedText[1] + '\n' + renderedText[2]);
				status = 2;
			}
		} else if (status == 2) {
			if (keyEvent.getKeyboardKey().equals(EnumKeyboardKey.X)) {
				delete();
				if (then != null) then.run();
			}
		}
	}

	@Override
	public void onResize(WindowResizeEvent windowResizeEvent) {

	}

	@Override
	public void onDropFilesEvent(DropFilesEvent dropFilesEvent) {

	}
}
