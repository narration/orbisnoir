package com.degoos.orbisnoir.gui.text;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.event.drop.DropFilesEvent;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.window.WindowResizeEvent;
import com.degoos.graphicengine2.object.*;
import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.sound.SoundSource;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.chat.Chat;
import com.degoos.orbisnoir.texture.TextureManager;
import com.degoos.orbisnoir.util.TextCutter;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector3f;

import java.util.List;

public class SmallTextbox extends GObject {

	private Rectangle textRectangle;
	private Rectangle portraitRectangle;
	private Rectangle portraitBase;

	private ITexture portrait;

	private Text text;
	private List<String> lines;

	private boolean canBeClosed;
	private long textVelocity;

	private int status;
	private String[] renderedText;
	private int listIndex, stringIndex;
	private long nanos;

	private Runnable then;

	public SmallTextbox(World world, String text, boolean executeMoveAnimation, ITexture texture) {
		this(world, TextCutter.cut(text, Chat.FONT, (int) (1920 * 0.45)), executeMoveAnimation, texture);
	}

	public SmallTextbox(World world, List<String> lines, boolean executeMoveAnimation, ITexture portrait) {
		this.lines = lines;
		this.portrait = portrait;

		boolean hasPortrait = portrait != null;

		textRectangle = new Rectangle(new Vector3f(Engine.getRenderArea().getMax().x
				+ (executeMoveAnimation ? (portrait == null ? 0.7f : 1.1f) : 0), 1, 0.9f),
				new Vector3f(-0.9f, -0.4f, 0), new Vector3f(0));
		textRectangle.setOpacity(0.95f);
		textRectangle.setTexture(TextureManager.loadImageOrNull("text/textbox.png"));

		if (hasPortrait) {
			portraitBase = new Rectangle(new Vector3f(-0.9f, 0, 0.0001f),
					new Vector3f(-0.4f, -0.4f, 0),
					new Vector3f(0));
			portraitBase.setTexture(textRectangle.getTexture());
			portraitBase.setParent(textRectangle);
			portraitRectangle = new Rectangle(new Vector3f(0), new Vector3f(-0.39f, -0.39f, 0),
					new Vector3f(-0.01f, 0.01f, 0));
			portraitRectangle.setTexture(portrait);
			portraitRectangle.setParent(portraitBase);
		} else portraitRectangle = portraitBase = null;

		text = new Text(Chat.FONT, new Vector3f(0, 0, 0.02f), new Area(textRectangle.getMin().x + 0.03f,
				textRectangle.getMin().y + 0.03f, textRectangle.getMax().x - 0.03f, textRectangle.getMax().y - 0.03f),
				"", GColor.BLACK, 1);
		text.setFixedScale(0.002f);
		text.setYCentered(false);
		text.setParent(textRectangle);

		world.addGObject(textRectangle);
		world.addGObject(text);
		textRectangle.setVisible(true);
		text.setVisible(true);
		if (hasPortrait) {
			world.addGObject(portraitRectangle);
			world.addGObject(portraitBase);
			portraitRectangle.setVisible(true);
			portraitBase.setVisible(true);
		}

		renderedText = new String[]{"", "", ""};
		textVelocity = 58000000;
		status = executeMoveAnimation ? 0 : 1;

		canBeClosed = true;
		then = null;
	}

	public boolean isCanBeClosed() {
		return canBeClosed;
	}

	public void setCanBeClosed(boolean canBeClosed) {
		this.canBeClosed = canBeClosed;
	}

	public void setThen(Runnable then) {
		this.then = then;
	}

	@Override
	public void delete() {
		super.delete();
		textRectangle.delete();
		text.delete();
		if (portrait != null) {
			portraitRectangle.delete();
			portraitBase.delete();
		}
	}

	@Override
	public void onTick(long l, Room room) {
		if (status == 0) animationTick(l);
		else if (status == 1) textTick(l);
		else {
			nanos += l;
			if (nanos >= 2000000000) {
				delete();
				if (then != null) then.run();
			}
		}
	}

	private void animationTick(long l) {
		float add = l / 350000000f;
		float max = Engine.getRenderArea().getMax().x;
		textRectangle.getPosition().x -= add;
		if (textRectangle.getPosition().x <= max) {
			textRectangle.getPosition().x = max;
			status = 1;
		}
		textRectangle.setRequiresRecalculation(true);
		text.setRequiresRecalculation(true);
		if (portrait != null) {
			portraitBase.setRequiresRecalculation(true);
			portraitRectangle.setRequiresRecalculation(true);
		}
	}

	public void textTick(long l) {
		if (lines.isEmpty()) status = 2;
		nanos += l;
		String string = lines.get(listIndex);
		while (nanos > textVelocity) {
			nanos -= textVelocity;

			while (string.length() <= stringIndex) {
				listIndex++;
				if (lines.size() <= listIndex) {
					status = 2;
					return;
				}
				string = nextLine();
			}

			renderedText[Math.min(2, listIndex)] += string.charAt(stringIndex);
			stringIndex++;

			if (stringIndex % 2 == 0) {
				SoundSource source = Engine.getSoundManager().createSoundSource("sound/click.ogg", "click", false, true);
				source.setGain(1);
				source.setPitch(3);
				source.play();
			}

		}
		text.setText(renderedText[0] + '\n' + renderedText[1] + '\n' + renderedText[2]);
	}

	private String nextLine() {
		stringIndex = 0;

		if (listIndex > 2) {
			renderedText[0] = renderedText[1];
			renderedText[1] = renderedText[2];
			renderedText[2] = "";
		}

		return lines.get(listIndex);
	}

	@Override
	public void onTick2(long l, Room room) {
	}

	@Override
	public void onAsyncTick(long l, Room room) {

	}

	@Override
	public void onMouseEvent(MouseEvent mouseEvent) {

	}

	@Override
	public void onKeyboardEvent(KeyEvent keyEvent) {

	}

	@Override
	public void onResize(WindowResizeEvent windowResizeEvent) {

	}

	@Override
	public void onDropFilesEvent(DropFilesEvent dropFilesEvent) {

	}
}
