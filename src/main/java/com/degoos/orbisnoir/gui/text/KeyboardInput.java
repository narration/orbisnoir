package com.degoos.orbisnoir.gui.text;


import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyHoldEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.font.TrueTypeFont;
import com.degoos.graphicengine2.io.KeyboardCharParser;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Text;
import com.degoos.graphicengine2.util.Area;
import org.joml.Vector3f;

public class KeyboardInput extends Text {


	public KeyboardInput(String text, Vector3f position, Area area) {
		super(true, 1, Engine.getFontManager().getFontUnsafe("Game"),
				position, area, text, GColor.WHITE, 1, 0, new Vector3f(0, 0, 1), true);
	}

	public KeyboardInput(TrueTypeFont font, Vector3f position, Area area, String text, GColor color, float opacity) {
		super(font, position, area, text, color, opacity);
	}

	@Override
	public void onKeyboardEvent(KeyEvent event) {
		if (!(event instanceof KeyPressEvent || event instanceof KeyHoldEvent)) return;
		EnumKeyboardKey keyboardKey = event.getKeyboardKey();
		if (keyboardKey == EnumKeyboardKey.BACKSPACE) {
			if (text.length() > 0) setText(text.substring(0, text.length() - 1));
		} else {
			char c = KeyboardCharParser.toFormattedChar((char) keyboardKey.getId());
			if (font.canDisplay(c))
				setText(text += c);
		}
	}
}
