package com.degoos.orbisnoir.gui.diary;

import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Room;
import com.degoos.orbisnoir.chat.Chat;
import com.degoos.orbisnoir.texture.TextureManager;
import org.joml.Vector3f;

import java.util.ArrayList;

public class DiaryDisplay extends Rectangle {

	private DiaryTextbox textbox;
	private DiaryList list;
	private int lastSelected;

	public DiaryDisplay(DiaryList list) {
		super(new Vector3f(0.8f, 0, 0), new Vector3f(0), new Vector3f(1.6f, 1.6f, 0));
		setVisible(true);
		this.list = list;
		lastSelected = list.selected.getIndex();
		textbox = new DiaryTextbox(this, list.selected.isUnlocked() ? DiaryManager.getText(lastSelected) : new ArrayList<>());
		setTexture(TextureManager.loadImageOrNull("gui/diary/paper.png"));
		list.room.addGObject(textbox);
		setParent(list);
	}

	@Override
	public void onTick(long dif, Room room) {
		super.onTick(dif, room);

		if (lastSelected != list.selected.getIndex()) {
			lastSelected = list.selected.getIndex();
			textbox.setText(DiaryTextbox.splitLines(Chat.FONT, list.selected.isUnlocked() ?
					DiaryManager.getText(lastSelected) : new ArrayList<>()));
		}

		textbox.onTick(dif, room);
	}
}
