package com.degoos.orbisnoir.gui.diary;

import com.degoos.graphicengine2.Engine;

import java.util.*;

public class DiaryManager {

	public static final int PAGES = 13;

	private static Map<Integer, List<String>> texts;

	public static void load() {
		loadTexts();
	}

	private static void loadTexts() {
		texts = new HashMap<>();
		for (int i = 0; i < PAGES; i++) {
			try {
				Scanner scanner = new Scanner(Engine.getResourceManager()
						.getResourceInputStream("diary/" + i + ".diary"), "UTF-8");
				List<String> list = new ArrayList<>();
				while (scanner.hasNextLine()) {
					list.add(scanner.nextLine());
				}
				texts.put(i, list);
			} catch (Exception ignore) {
			}
		}
	}

	public static List<String> getText(int index) {
		return texts.getOrDefault(index, null);
	}
}
