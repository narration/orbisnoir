package com.degoos.orbisnoir.gui.diary;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.object.Room;
import com.degoos.orbisnoir.Game;
import com.degoos.orbisnoir.entity.player.PlayerController;
import com.degoos.orbisnoir.script.ScriptManager;
import com.degoos.orbisnoir.world.World;

public class DiaryRoom extends Room {

	private World world;
	DiaryList diaryList;

	public DiaryRoom(World world) {
		super("diary");
		this.world = world;
		addGObject(diaryList = new DiaryList(this));
		addGObject(new DiaryDisplay(diaryList));
		if (world.getPlayer().getController() instanceof PlayerController)
			((PlayerController) world.getPlayer().getController()).setCanBeControlled(false);
	}

	@Override
	public void onKeyEvent(KeyEvent event) {
		if (event instanceof KeyPressEvent) {
			if (event.getKeyboardKey() == EnumKeyboardKey.ESCAPE || event.getKeyboardKey() == EnumKeyboardKey.D) {
				exit();
			}
		}
		super.onKeyEvent(event);
	}

	@Override
	public void draw() {
		world.draw();
		super.draw();
	}

	@Override
	public void tick(long dif) {
		world.tick(dif);
		super.tick(dif);
	}


	public void exit() {
		Engine.setRoom(world);

		if (!Game.getSave().level_1_room_2_diary_close &&
				(world.getName().equals("level1room2") || world.getName().equals("level1room3"))) {
			world.getScriptContainer().runScript(ScriptManager.getScriptUnsafe("level1/room2/diary_close"));
		}

		if (Game.getSave().diary_page_1_read && !Game.getSave().level_2_diary_close) {
			Game.getSave().level_2_diary_close = true;
			world.getScriptContainer().runScript(ScriptManager.getScriptUnsafe("level2/diary_close"));
		}

		if (world.getPlayer().getController() instanceof PlayerController)
			((PlayerController) world.getPlayer().getController()).setCanBeControlled(true);
	}

}
