package com.degoos.orbisnoir.gui.diary;

import com.degoos.graphicengine2.font.TrueTypeFont;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Text;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.chat.Chat;
import com.degoos.orbisnoir.script.value.PlaceholderParser;
import com.degoos.orbisnoir.util.TextCutter;
import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.List;

public class DiaryTextbox extends Text {

	public DiaryTextbox(DiaryDisplay textbox, List<String> text) {
		super(Chat.FONT, new Vector3f(0, 0, 0.01f),
				new Area(0.2f, 0.2f, 1.2f, 1.2f),
				splitLines(Chat.FONT, text), GColor.BLACK, 1);
		setVisible(true);
		setYCentered(false);
		setParent(textbox);
	}

	static String splitLines(TrueTypeFont font, List<String> text) {
		if (text == null) return null;
		for (int i = 0; i < text.size(); i++) {
			text.set(i, PlaceholderParser.parse(null, text.get(i)));
		}
		List<String> newList = new ArrayList<>();
		for (String line : text) {
			newList.addAll(TextCutter.cut(line, font, (int) (1920 * 0.70)));
		}

		StringBuilder builder = new StringBuilder();
		for (String line : newList)
			builder.append(line).append('\n');

		return builder.toString().isEmpty() ? "" : builder.substring(0, builder.length() - 1);
	}
}
