package com.degoos.orbisnoir.gui.diary;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.sound.SoundSource;
import com.degoos.orbisnoir.texture.TextureManager;
import org.joml.Vector3f;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class DiaryList extends Rectangle {

	private Map<Integer, DiaryOption> options;
	DiaryOption selected;
	Room room;

	public DiaryList(Room room) {
		super(new Vector3f(-1.2f, -0.6f, 0.95f), new Vector3f(0), new Vector3f(0.8f, 1.53f, 0));
		this.room = room;
		setVisible(true);
		setTexture(TextureManager.loadImageOrNull("text/textbox.png"));
		options = new HashMap<>();
		DiaryOption option;
		for (int i = 0; i < DiaryManager.PAGES; i++) {
			option = new DiaryOption(room, i);
			options.put(i, option);
			room.addGObject(option);
		}
		selected = options.get(0);
		selected.select();
	}

	@Override
	public void onKeyboardEvent(KeyEvent event) {
		if (!(event instanceof KeyPressEvent)) return;
		if (event.getKeyboardKey() == EnumKeyboardKey.ARROW_UP) {
			selected.deselect();
			selected = options.get((selected.getIndex() + options.size() - 1) % options.size());
			selected.select();
			playFlipSound();
		} else if (event.getKeyboardKey() == EnumKeyboardKey.ARROW_DOWN) {
			selected.deselect();
			selected = options.get((selected.getIndex() + 1) % options.size());
			selected.select();
			playFlipSound();
		}
	}

	private void playFlipSound() {
		if (new Random().nextBoolean()) {
			SoundSource source = Engine.getSoundManager().createSoundSource("sound/paper_flip_1.ogg",
					"paper_flip", false, true);
			source.setGain(1);
			source.setPitch((float) (0.9 + (Math.random() * 0.4)));
			source.play();
		} else {
			SoundSource source = Engine.getSoundManager().createSoundSource("sound/paper_flip_1.ogg",
					"paper_flip", false, true);
			source.setGain(1);
			source.setPitch((float) (0.9 + (Math.random() * 0.6)));
			source.play();
		}
	}
}
