package com.degoos.orbisnoir.gui.diary;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.object.Text;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.Game;
import com.degoos.orbisnoir.chat.Chat;
import com.degoos.orbisnoir.texture.TextureManager;
import org.joml.Vector3f;

public class DiaryOption extends Rectangle {

	private Text text;
	private int index;
	private boolean unlocked, read;

	public DiaryOption(Room room, int page) {
		super(new Vector3f(-1.15f, 0.78f - (page * 0.075f * 3 / 2), 0.951f), new Vector3f(0), new Vector3f(0.7f, 0.105f, 0));
		setVisible(true);
		this.index = page;
		unlocked = (boolean) Game.getSave().getValue("diary_page_" + page);
		read = (boolean) Game.getSave().getValue("diary_page_" + page + "_read");
		setTexture(TextureManager.loadImageOrNull("text/textbox.png"));


		text = new Text(Chat.FONT, new Vector3f(0, 0, 0.01f), new Area(getMin().x + 0.02f, getMin().y + 0.02f,
				getMax().x - 0.02f, getMax().y - 0.02f),
				unlocked ? "P\u00e1gina " + (page + 1) : "???",
				unlocked ? !read ? GColor.GREEN : GColor.BLACK : GColor.RED.darker(), 1);
		text.setVisible(true);
		text.setXCentered(true);
		text.setParent(this);
		room.addGObject(text);
	}

	public int getIndex() {
		return index;
	}

	public boolean isUnlocked() {
		return unlocked;
	}

	public void select() {
		text.setColor(unlocked ? new GColor(1, 1, 0) : GColor.RED.brighter());
		if (unlocked && !read) {
			try {
				Game.getSave().injectDataToField("diary_page_" + index + "_read", true);
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			}
		}
	}

	public void deselect() {
		text.setColor(unlocked ? GColor.BLACK : GColor.RED.darker());
	}
}
