package com.degoos.orbisnoir.gui.button;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.object.Room;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector3f;

public class ControlButton extends ButtonHint {

	private World world;

	public ControlButton(World world) {
		super(new Vector3f(Engine.getRenderArea().getMin().x + 0.3f, -1.4f, 0.91f),
				new Vector3f(-0.25f, 0, 0), new Vector3f(0.4f, 0.3f, 0), "gui/control.png", -0.8f);
		this.world = world;
	}

	@Override
	public void onTick(long dif, Room room) {
		super.onTick(dif, room);
		if (world.getGUI().getInteractButton().isActive()) {
			if (maxHeight != -0.8) {
				maxHeight = -0.8f;
				animationDone = false;
			}
		} else if (maxHeight != -1) {
			maxHeight = -1;
			animationDone = false;
		}
	}
}
