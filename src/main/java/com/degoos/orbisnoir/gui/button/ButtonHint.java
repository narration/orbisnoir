package com.degoos.orbisnoir.gui.button;

import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Room;
import com.degoos.orbisnoir.texture.TextureManager;
import org.joml.Vector3f;

public class ButtonHint extends Rectangle {

	protected float maxHeight;

	protected boolean active, animationDone;
	private long nanos;

	public ButtonHint(Vector3f position, Vector3f min, Vector3f max, String texture, float maxHeight) {
		super(position, min, max);
		this.maxHeight = maxHeight;
		setVisible(true);
		setTexture(TextureManager.loadImageOrNull(texture));
		active = false;
		animationDone = true;
	}

	public boolean isActive() {
		return active;
	}

	public void appear() {
		if (active) return;
		active = true;
		animationDone = false;
	}

	public void disappear() {
		if (!active) return;
		active = false;
		animationDone = false;
	}

	@Override
	public void onTick(long dif, Room room) {
		if (!animationDone) {
			if (active) {
				position.y += dif * 1.2f / 1000000000f;
				if (position.y >= maxHeight) {
					position.y = maxHeight;
					animationDone = true;
				}
			} else {
				position.y += -dif * 1.2f / 1000000000f;
				if (position.y <= -1.4) {
					position.y = -1.4f;
					animationDone = true;
					nanos = 0;
				}
			}
			requiresRecalculation = true;
		}
		if (active) {
			nanos += dif;
			setRotation((float) Math.cos(nanos * 2 / 1000000000d));
		}
	}

}
