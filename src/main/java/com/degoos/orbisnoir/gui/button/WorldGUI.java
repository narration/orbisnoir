package com.degoos.orbisnoir.gui.button;

import com.degoos.orbisnoir.world.World;

public class WorldGUI {

	private ContinueButton continueButton;
	private DiaryButton diaryButton;
	private InteractButton interactButton;
	private MoveButton moveButton;
	private RunButton runButton;
	private TurboButton turboButton;
	private ControlButton controlButton;
	private ShieldButton shieldButton;

	public WorldGUI(World world) {
		continueButton = new ContinueButton();
		diaryButton = new DiaryButton();
		interactButton = new InteractButton();
		moveButton = new MoveButton();
		runButton = new RunButton();
		turboButton = new TurboButton();
		controlButton = new ControlButton(world);
		shieldButton = new ShieldButton();

		world.addGObject(continueButton);
		world.addGObject(diaryButton);
		world.addGObject(interactButton);
		world.addGObject(moveButton);
		world.addGObject(runButton);
		world.addGObject(turboButton);
		world.addGObject(controlButton);
		world.addGObject(shieldButton);
	}

	public ContinueButton getContinueButton() {
		return continueButton;
	}

	public DiaryButton getDiaryButton() {
		return diaryButton;
	}

	public InteractButton getInteractButton() {
		return interactButton;
	}

	public MoveButton getMoveButton() {
		return moveButton;
	}

	public RunButton getRunButton() {
		return runButton;
	}

	public TurboButton getTurboButton() {
		return turboButton;
	}

	public ControlButton getControlButton() {
		return controlButton;
	}

	public ShieldButton getShieldButton() {
		return shieldButton;
	}
}
