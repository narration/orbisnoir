package com.degoos.orbisnoir.gui.button;

import com.degoos.graphicengine2.Engine;
import org.joml.Vector3f;

public class MoveButton extends ButtonHint {

	public MoveButton() {
		super(new Vector3f(Engine.getRenderArea().getMax().x - 0.7f, -1.4f, 0.91f),
				new Vector3f(-0.25f, 0, 0), new Vector3f(0.5f, 0.3f, 0), "gui/move.png", -0.95f);
	}
}
