package com.degoos.orbisnoir.gui.button;

import com.degoos.graphicengine2.Engine;
import org.joml.Vector3f;

public class TurboButton extends ButtonHint {

	public TurboButton() {
		super(new Vector3f(Engine.getRenderArea().getMin().x + 0.3f, -1.4f, 0.91f),
				new Vector3f(-0.25f, 0, 0), new Vector3f(0.4f, 0.3f, 0), "gui/turbo.png", -0.8f);
	}
}
