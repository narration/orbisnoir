package com.degoos.orbisnoir.gui.pause;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Text;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.chat.Chat;
import org.joml.Vector3f;

public abstract class PauseOption extends Text {

	private int optionIndex;
	private long nanos;
	private int animationPhase;

	public PauseOption(String name, int index) {
		super(true, 0, Chat.FONT, new Vector3f(Engine.getRenderArea().getMin().x - 1.5f, index * -0.2f + 0.7f, 0.97f),
				new Area(0, -0.2f, 1, 0), name, GColor.WHITE, 1);
		this.optionIndex = index;
		setMaximumScale(0.003f);
		nanos = animationPhase = 0;
	}

	public void select() {
		setColor(new GColor(1, 1, 0));
	}

	public void deselect() {
		setColor(GColor.WHITE);
	}

	public abstract void execute();

	public void tick(long dif) {
		if (animationPhase == 2) return;
		nanos += dif;
		switch (animationPhase) {
			case 0:
				if (nanos >= 100000000L * optionIndex) {
					animationPhase = 1;
					nanos = 0;
				}
				break;
			case 1:
				position.x += 0.0000000005 * nanos;
				if (position.x >= Engine.getRenderArea().getMin().x + 0.2f) {
					position.x = Engine.getRenderArea().getMin().x + 0.2f;
					animationPhase = 2;
				}
				requiresRecalculation = true;
		}
	}
}
