package com.degoos.orbisnoir.gui.pause;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.object.Room;
import com.degoos.orbisnoir.Game;
import com.degoos.orbisnoir.entity.player.PlayerController;
import com.degoos.orbisnoir.gui.BlackScreen;
import com.degoos.orbisnoir.gui.diary.DiaryRoom;
import com.degoos.orbisnoir.gui.main.MainMenu;
import com.degoos.orbisnoir.world.World;

import java.util.ArrayList;
import java.util.List;


public class PauseMenu extends Room {

	private World world;
	private List<PauseOption> options;
	private int selected;

	public PauseMenu(World world) {
		super("Pause");
		this.world = world;
		loadOptions();
		if (world.getPlayer().getController() instanceof PlayerController)
			((PlayerController) world.getPlayer().getController()).setCanBeControlled(false);
		BlackScreen blackScreen = new BlackScreen().setOpacity(0.5f, 200000000, null);
		blackScreen.getPosition().z = 0.96f;
		addGObject(blackScreen);
		addGObject(new PauseRectangle());
	}

	private void loadOptions() {
		boolean diary = Game.getSave().diary_unlocked;
		options = new ArrayList<>();
		options.add(new PauseOption("Volver a la partida", 0) {
			@Override
			public void execute() {
				resume();
			}
		});
		if (diary)
			options.add(new PauseOption("Diario", 1) {
				@Override
				public void execute() {
					Engine.setRoom(new DiaryRoom(world));
				}
			});
		options.add(new PauseOption("Volver al men\u00fa principal", diary ? 2 : 1) {
			@Override
			public void execute() {
				Engine.getSoundManager().cleanUpAllSoundSources();
				Engine.setRoom(new MainMenu());
			}
		});
		options.add(new PauseOption("Salir del juego", diary ? 3 : 2) {
			@Override
			public void execute() {
				Engine.getRenderer().kill();
			}
		});
		options.forEach(this::addGObject);
		select(0);
	}

	public World getWorld() {
		return world;
	}

	public void select(int selection) {
		options.get(selected).deselect();
		selected = (selection + options.size()) % options.size();
		options.get(selected).select();
	}

	public void executeSelected() {
		options.get(selected).execute();
	}

	public void resume() {
		Engine.setRoom(world);
		if (world.getPlayer().getController() instanceof PlayerController)
			((PlayerController) world.getPlayer().getController()).setCanBeControlled(true);
	}

	@Override
	public void tick(long dif) {
		super.tick(dif);
		options.forEach(target -> target.tick(dif));
	}

	@Override
	public void draw() {
		world.draw();
		super.draw();
	}

	@Override
	public void onKeyEvent(KeyEvent event) {
		if (!(event instanceof KeyPressEvent)) return;

		switch (event.getKeyboardKey()) {
			case ESCAPE:
				resume();
				break;
			case ARROW_UP:
				select(selected - 1);
				break;
			case ARROW_DOWN:
				select(selected + 1);
				break;
			case ENTER:
				executeSelected();
				break;
		}
	}

}
