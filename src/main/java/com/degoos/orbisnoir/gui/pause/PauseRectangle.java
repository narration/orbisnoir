package com.degoos.orbisnoir.gui.pause;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.Room;
import org.joml.Vector3f;

public class PauseRectangle extends Rectangle {

	public PauseRectangle() {
		super(new Vector3f(-1, 0, 0.965f), new Vector3f(-100, -1, 0), new Vector3f(-0.2f, 1, 0));
		setVisible(true);
		setColor(GColor.BLUE);
		setOpacity(0.4f);
	}


	@Override
	public void onTick(long dif, Room room) {
		if (position.x < 0) {
			position.x += 0.000000005 * dif;
			if (position.x >= 0) {
				position.x = 0;
			}
			requiresRecalculation = true;
		}
	}
}
