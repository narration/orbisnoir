package com.degoos.orbisnoir.game.script.level1;

import com.degoos.graphicengine2.event.drop.DropFilesEvent;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.window.WindowResizeEvent;
import com.degoos.graphicengine2.object.GObject;
import com.degoos.graphicengine2.object.Room;
import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepGoto;
import com.degoos.orbisnoir.script.step.type.ScriptStepRunScript;
import com.degoos.orbisnoir.script.step.type.ScriptStepWait;
import com.degoos.orbisnoir.script.value.parser.IntValueParser;
import com.degoos.orbisnoir.script.value.parser.LongValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.world.Box;
import com.degoos.orbisnoir.world.Chunk;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2i;
import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.List;

public class Level1ScriptBoxFall extends Script {

	private int initY;
	private boolean reset = true;

	public Level1ScriptBoxFall() {
		super("level1/room7/special_box_fall", new ArrayList<>());

		this.initY = 23;

		addStep(new ScriptStep(null, null) {
			@Override
			public void run(ScriptExecuter executer, Runnable then) {
				reset = true;
				then.run();
			}
		});
		addStep(new ScriptStepRunScript(null, null, new StringValueParser("level1/room7/earthquake")));
		addStep(new ScriptStepWait(null, null, new LongValueParser(200)));
		addStep(new ScripStepBoxFall());
		addStep(new ScriptStepGoto(null, null, new IntValueParser(2)));
	}

	private class ScripStepBoxFall extends ScriptStep {

		private int currentY, currentChunk;
		private BoxHandler boxHandler;

		private Player player;

		public ScripStepBoxFall() {
			super(null, null);
			reset();
		}

		private void reset() {
			currentChunk = initY >> 5;
			currentY = initY - (currentChunk * 32);
			boxHandler = null;
		}

		@Override
		public void run(ScriptExecuter executer, Runnable then) {
			if (reset) {
				reset();
				reset = false;
			}
			World world = executer.getWorld();

			if (boxHandler == null) {
				boxHandler = new BoxHandler();
				world.addGObject(boxHandler);
				player = world.getPlayer();
			}

			forChunk(world.getChunkOrNull(new Vector2i(0, currentChunk)));
			forChunk(world.getChunkOrNull(new Vector2i(1, currentChunk)));

			if (player.getPosition().y >= currentY + currentChunk * 32) {
				player.getHealth().damage(1);
				return;
			}

			currentY--;
			if (currentY < 0) {
				currentY += 32;
				currentChunk--;
			}

			if (currentChunk < -2 && currentY < 23) {
				executer.stop();
				boxHandler.delete();
				boxHandler.boxes.forEach(target -> target.setOpacity(0));
				return;
			}

			if (then != null) then.run();
		}

		private void forChunk(Chunk chunk) {
			if (chunk == null) return;
			Box[][] boxes = chunk.getBoxes();
			Box box;
			for (int x = 0; x < 32; x++) {
				box = boxes[x][currentY];
				if (box != null) boxHandler.boxes.add(box);
			}
		}
	}


	private class BoxHandler extends GObject {

		private static final float BOX_SIZE_TICK = 0.1f / 1000000000f;
		private static final float BOX_ROTATE_TICK = 180 / 1000000000F;
		List<Box> boxes;


		public BoxHandler() {
			boxes = new ArrayList<>();
		}


		@Override
		public void onTick(long l, Room room) {
			float sizeTick = BOX_SIZE_TICK * l;
			float rotationTick = BOX_ROTATE_TICK * l;
			Box box;
			for (int i = boxes.size() - 1; i >= 0; i--) {
				box = boxes.get(i);
				box.setMin(new Vector3f(box.getMin()).add(sizeTick, sizeTick, 0));
				box.setMax(new Vector3f(box.getMax()).sub(sizeTick, sizeTick, 0));
				box.setRotation(box.getRotation() + ((box.getChunkPosition().x
						+ box.getChunkPosition().y) % 3 == 0 ? -rotationTick : rotationTick));
				if (box.getMax().x < box.getMin().x) {
					boxes.remove(i);
					box.setOpacity(0);
				}
			}
		}

		@Override
		public void onTick2(long l, Room room) {

		}

		@Override
		public void onAsyncTick(long l, Room room) {

		}

		@Override
		public void onMouseEvent(MouseEvent mouseEvent) {

		}

		@Override
		public void onKeyboardEvent(KeyEvent keyEvent) {

		}

		@Override
		public void onResize(WindowResizeEvent windowResizeEvent) {

		}

		@Override
		public void onDropFilesEvent(DropFilesEvent dropFilesEvent) {

		}
	}
}
