package com.degoos.orbisnoir.game.entity.level3;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.Controller;
import com.degoos.orbisnoir.entity.palette.MovablePalette;
import com.degoos.orbisnoir.entity.villager.Villager;
import com.degoos.orbisnoir.enums.EnumDirection;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.game.entity.level3.boss.sub.RunningWarrior;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.jooq.tools.json.ParseException;

import java.io.IOException;

public class SpanishWarrior extends Villager {


	public SpanishWarrior(World world, SpanishWarriorEntitySketch sketch) throws IOException, ParseException {
		this(world, sketch.getName(), sketch.getPosition(), sketch.getColor(), sketch.getOpacity(), sketch.getEntitySize(), sketch.getEntityDrawPriority(),
				sketch.getCollisionBox(), sketch.getCollisionType(), null, sketch.getScript(), sketch.getDirection(),
				MovablePalette.fromNPCPalette(PaletteManager.loadPalette("level3/spanish")), false, false, false);
		this.sketch = sketch;
	}

	public SpanishWarrior(World world, String name, Vector2f position, GColor color, float opacity, Area entitySize,
						  EnumEntityDrawPriority drawPriority, CollisionBox collisionBox, EnumEntityCollision collision,
						  Controller controller, Script script, EnumDirection direction,
						  MovablePalette movablePalette, boolean moving, boolean running, boolean casting) {
		super(world, name, position, color, opacity, entitySize, drawPriority, collisionBox,
				collision, controller, script, direction, movablePalette, moving, running, casting);
	}


	@Override
	public void tick(long dif) {
		super.tick(dif);
		if (slept) return;
		if (!(this instanceof RunningWarrior) && world.getPlayer().getControlling() != this
				&& world.getPlayer().getCollisionBox().collision(collisionBox) != null)
			world.getPlayer().getHealth().damage(1);
	}
}
