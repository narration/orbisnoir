package com.degoos.orbisnoir.game.entity.purgatorio;

import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.event.window.WindowResizeEvent;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.Game;
import com.degoos.orbisnoir.gui.text.KeyboardInput;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector3f;

import java.util.function.Consumer;

public class PurgatorioKeyboardInput extends PurgatorioText {

	private KeyboardInput input;
	private Consumer<PurgatorioKeyboardInput> then;

	private boolean activatedInput, disappearing;

	public PurgatorioKeyboardInput(World world, String string) {
		super(world, string);
		setVisible(true);


		setOnFinish(target -> {
			input.setText("");
			activatedInput = true;
		});
		activatedInput = false;
		disappearing = false;
		then = null;


		input = new KeyboardInput("", new Vector3f(0, 0, 0), new Area(-0.5f, -0.2f, 0.5f, 0.2f)) {

			@Override
			public void onKeyboardEvent(KeyEvent event) {
				if (activatedInput && event.getKeyboardKey() == EnumKeyboardKey.ENTER && !getText().isEmpty()) {
					activatedInput = false;
					disappearing = true;
					Game.getSave().player_name = getText();
					PurgatorioKeyboardInput.this.setPhase(2);
				} else super.onKeyboardEvent(event);
			}
		};


		input.setXCentered(true);
		input.setFont(text.getFont());
		input.setColor(new GColor(1, 1, 0).darker());
		input.setOpacity(0);
		input.setFixedScale(0.0028f);

		world.addGObject(input);
	}

	public Consumer<PurgatorioKeyboardInput> getThen() {
		return then;
	}

	public void setThen(Consumer<PurgatorioKeyboardInput> then) {
		this.then = then;
	}


	@Override
	public void delete() {
		super.delete();
		input.delete();
	}

	@Override
	public void onTick(long dif, Room room) {
		super.onTick(dif, room);
		input.onTick(dif, room);

		float opacity = input.getOpacity();

		if (activatedInput && opacity < 1) {
			opacity += dif / 1500000000f;
			if (opacity >= 1)
				opacity = 1;
		}
		if (disappearing) {
			opacity -= dif / 1500000000f;
			if (opacity <= 0) {
				delete();
				if (then != null)
					then.accept(this);
			}
		}
		input.setOpacity(opacity);
	}

	@Override
	public void onKeyboardEvent(KeyEvent keyEvent) {
		if (!(keyEvent instanceof KeyPressEvent)) return;
		if (status == 0) {
			if (keyEvent.getKeyboardKey().equals(EnumKeyboardKey.Z))
				textVelocity = 75000000;
		}
	}

	@Override
	public void onResize(WindowResizeEvent windowResizeEvent) {

	}
}
