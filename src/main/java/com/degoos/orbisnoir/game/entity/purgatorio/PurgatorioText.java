package com.degoos.orbisnoir.game.entity.purgatorio;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Room;
import com.degoos.graphicengine2.util.Area;
import com.degoos.graphicengine2.util.Validate;
import com.degoos.orbisnoir.gui.text.SimpleTextbox;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

import java.util.function.Consumer;

public class PurgatorioText extends SimpleTextbox {


	private int phase;
	private Consumer<PurgatorioText> onFinish;

	public PurgatorioText(World world, String text) {
		super(world, Engine.getFontManager().getFontUnsafe("Game"), text, 150000000,
				true, GColor.WHITE, new Vector2f(0, 0.1f),
				new Area(new Vector2f(-1.6f, 0), new Vector2f(1.6f, 0.7f)));
		phase = 0;
		this.text.setYCentered(false);
		this.text.setOpacity(0);
		this.text.setFixedScale(0.0025f);
		onFinish = null;
	}

	public void setPhase(int phase) {
		this.phase = phase;
	}

	public void setOnFinish(Consumer<PurgatorioText> onFinish) {
		Validate.notNull(onFinish, "OnFinish cannot be null!");
		this.onFinish = onFinish;
	}

	@Override
	public void onTick(long dif, Room room) {
		boolean finished = status == 1;
		super.onTick(dif, room);
		float opacity = text.getOpacity();
		if (phase == 0) {
			opacity += dif / 1500000000f;
			if (opacity >= 1) {
				opacity = 1;
				phase = 1;
			}
			text.setOpacity(opacity);
		} else if (phase == 2) {
			opacity -= dif / 1500000000f;
			if (opacity <= 0) {
				delete();
				if (then != null)
					then.run();
			}
			text.setOpacity(opacity);
		}
		if (finished != (status == 1)) {
			if (onFinish != null)
				onFinish.accept(this);
		}
	}

	@Override
	public void onKeyboardEvent(KeyEvent event) {
		if (!(event instanceof KeyPressEvent)) return;
		if (status == 0) {
			if (event.getKeyboardKey().equals(EnumKeyboardKey.Z))
				textVelocity = 75000000;
		} else {
			if (event.getKeyboardKey().equals(EnumKeyboardKey.X) && then != null) {
				phase = 2;
			}
		}
	}
}
