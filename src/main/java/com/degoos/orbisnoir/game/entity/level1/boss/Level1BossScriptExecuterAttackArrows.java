package com.degoos.orbisnoir.game.entity.level1.boss;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.sound.SoundSource;
import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepWait;
import com.degoos.orbisnoir.util.VectorUtils;
import org.joml.Vector2f;

import java.util.ArrayList;

public class Level1BossScriptExecuterAttackArrows extends Script {

	public Level1BossScriptExecuterAttackArrows(Player player, Level1Boss boss) {
		super("level_1_boss_attack_arrows", new ArrayList<>());
		addStep(new ScriptStepWait(500));

		for (int i = 0; i < 4; i++) {
			launchArrow(player, boss);
		}
		addStep(new ScriptStep(null, null) {
			@Override
			public void run(ScriptExecuter room, Runnable then) {
				boss.getRectangle().setTexture(boss.getDirectionablePalette().getPalette().getTexture(1, 2));
				then.run();
			}
		});
		addStep(new ScriptStepWait(200));
	}

	private void launchArrow(Player player, Level1Boss boss) {

		getSteps().add(new ScriptStep(null, null) {
			@Override
			public void run(ScriptExecuter executer, Runnable then) {

				if (player.getHealth().getHealth() == 0) {
					executer.stop();
					return;
				}
				boss.getRectangle().setTexture(boss.getDirectionablePalette().getPalette().getTexture(4, 2));

				Vector2f velocity = new Vector2f(player.getPosition()).sub(boss.getPosition()).normalize().mul(8).mul(1 / 1000000000f);
				int angle = 8;
				for (int i = 0; i < 3; i++) {
					executer.getWorld().registerEntity(new Level1BossArrow(executer.getWorld(), boss.getCollisionBox().getCenter(),
							VectorUtils.rotate(new Vector2f(velocity), Math.toRadians(angle)), boss));
					angle -= 8;
				}
				SoundSource soundSource = Engine.getSoundManager().createSoundSource("sound/magic_attack.ogg",
						"magic_attack", false, true);
				soundSource.setPitch(0.4f);
				soundSource.play();
				then.run();
			}
		});

		addStep(new ScriptStepWait(300));

		getSteps().add(new ScriptStep(null, null) {
			@Override
			public void run(ScriptExecuter executer, Runnable then) {
				boss.getRectangle().setTexture(boss.getDirectionablePalette().getPalette().getTexture(0, 2));
				then.run();
			}
		});
		addStep(new ScriptStepWait(500));
	}
}

