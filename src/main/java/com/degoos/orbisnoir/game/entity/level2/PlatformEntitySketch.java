package com.degoos.orbisnoir.game.entity.level2;

import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.util.Area;
import com.degoos.graphicengine2.util.Validate;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.image.EnumImageType;
import com.degoos.orbisnoir.entity.sketch.EntitySketch;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.texture.TextureManager;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.jooq.tools.json.ParseException;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class PlatformEntitySketch extends EntitySketch<Platform> {

	private long moveDelay, waitDelay;
	private Vector2f to;

	private EnumImageType imageType;
	private String image;
	private double angularVelocity;

	public PlatformEntitySketch() {
		super();
		jsonMap.put("to", new TreeMap());
		setEntitySize(new Area(new Vector2f(-0.5f, -0.5f), new Vector2f(0.5f, 0.5f)));
		setCollisionBox(new CollisionBox(new Vector2f(0), new Vector2f(-0.5f, -0.5f), new Vector2f(0.5f, 0.5f)));
		setTo(new Vector2f(0));
		setWaitDelay(1000);
		setMoveDelay(3000);
		setImageType(EnumImageType.IMAGE);
		setImage("null");
		setAngularVelocity(0);
		jsonMap.put("type", "platform");
	}

	public PlatformEntitySketch(String json) throws ParseException {
		super(json);
		jsonMap.put("type", "platform");
	}

	public PlatformEntitySketch(Map json) throws ParseException {
		super(json);
		jsonMap.put("type", "platform");
	}

	@Override
	protected void updateMap() {
		super.updateMap();
		if (!jsonMap.containsKey("angular_velocity"))
			jsonMap.put("angular_velocity", 0D);
	}

	public long getWaitDelay() {
		return waitDelay;
	}

	public void setWaitDelay(long waitDelay) {
		this.waitDelay = waitDelay;
		jsonMap.put("wait_delay", waitDelay);
	}

	public long getMoveDelay() {
		return moveDelay;
	}

	public double getAngularVelocity() {
		return angularVelocity;
	}


	public void setAngularVelocity(double angularVelocity) {
		this.angularVelocity = angularVelocity;
		jsonMap.put("angular_velocity", angularVelocity);
	}

	public void setMoveDelay(long moveDelay) {
		this.moveDelay = moveDelay;
		jsonMap.put("mode_delay", moveDelay);
	}

	public Vector2f getTo() {
		return new Vector2f(to);
	}

	public void setTo(Vector2f to) {
		Validate.notNull(to, "To cannot be null!");
		this.to = to;
		Map toMap = (Map) jsonMap.get("to");
		toMap.put("x_pos", to.x);
		toMap.put("y_pos", to.x);
	}

	public EnumImageType getImageType() {
		return imageType;
	}

	public void setImageType(EnumImageType imageType) {
		Validate.notNull(imageType, "Image type cannot be null!");
		this.imageType = imageType;
		jsonMap.put("image_type", imageType.name());
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image == null ? "null" : image;
		jsonMap.put("image", this.image);
	}


	public ITexture toImage() {
		try {
			String[] sl;
			switch (imageType) {
				case ANIMATION:
					return TextureManager.loadAnimation(image);
				case PALETTE_IMAGE:
					sl = image.split(";");
					return PaletteManager.loadPalette(sl[0]).getTexture(Integer.valueOf(sl[1]));
				case PALETTE_ANIMATION:
					sl = image.split(";");
					return PaletteManager.loadPalette(sl[0]).getAnimation(Integer.valueOf(sl[1]));
				case IMAGE:
				default:
					return TextureManager.loadImage(image);
			}
		} catch (Exception ignore) {
			return null;
		}
	}

	@Override
	public Platform toEntity(World world) {
		try {
			return new Platform(world, this);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Map<String, Class<?>> getTypesMap() {
		Map<String, Class<?>> map = new HashMap<>(super.getTypesMap());
		map.put("wait_delay", long.class);
		map.put("mode_delay", long.class);
		map.put("angular_velocity", float.class);

		map.put("to", Map.class);
		map.put("to_x_pos", float.class);
		map.put("to_y_pos", float.class);

		map.put("image_type", EnumImageType.class);
		map.put("image", String.class);
		return map;
	}

	@Override
	public void refreshFromMap() {
		super.refreshFromMap();
		waitDelay = (long) jsonMap.get("wait_delay");
		moveDelay = (long) jsonMap.get("mode_delay");
		angularVelocity = (float) (double) jsonMap.get("angular_velocity");

		Map toMap = (Map) jsonMap.get("to");
		to = new Vector2f((float) (double) toMap.get("x_pos"), (float) (double) toMap.get("y_pos"));

		imageType = EnumImageType.getByName((String) jsonMap.get("image_type")).orElse(EnumImageType.IMAGE);
		image = (String) jsonMap.get("image");
	}
}
