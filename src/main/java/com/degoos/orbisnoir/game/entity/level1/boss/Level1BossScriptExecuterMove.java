package com.degoos.orbisnoir.game.entity.level1.boss;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepMoveComplex;
import org.joml.Vector2f;

import java.util.ArrayList;

public class Level1BossScriptExecuterMove extends Script {


	public Level1BossScriptExecuterMove(Level1Boss boss, Vector2f position) {
		super("level_1_boss_move", new ArrayList<>());

		Vector2f distance = new Vector2f(position).sub(boss.getPosition());
		boolean zero = distance.x == 0 && distance.y == 0;
		if (!zero) distance = distance.normalize();
		int deg = zero ? 0 : (int) Math.toDegrees(Math.atan2(distance.y, distance.x));
		if (deg < 0) deg += 360;
		int animation;
		if (deg < 45) animation = 3;
		else if (deg < 135) animation = 0;
		else if (deg < 225) animation = 1;
		else if (deg < 315) animation = 2;
		else animation = 3;
		addStep(new ScriptStep(null, null) {
			@Override
			public void run(ScriptExecuter room, Runnable then) {
				boss.getRectangle().setTexture(boss.getDirectionablePalette().getPalette().getAnimation(animation));
				then.run();
			}
		});
		addStep(new ScriptStepMoveComplex(null, null, "boss", false, position, 2000));
		addStep(new ScriptStep(null, null) {
			@Override
			public void run(ScriptExecuter room, Runnable then) {
				boss.getRectangle().setTexture(boss.getDirectionablePalette().getPalette().getTexture(0, animation));
				then.run();
			}
		});
	}
}
