package com.degoos.orbisnoir.game.entity.level1;

import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.util.Area;
import com.degoos.graphicengine2.util.Validate;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.image.EnumImageType;
import com.degoos.orbisnoir.entity.sketch.EntitySketch;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.texture.TextureManager;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.jooq.tools.json.ParseException;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class DamageBallEntitySketch extends EntitySketch<DamageBall> {

	private double amplification;
	private double velocity, angularVelocity;
	private boolean xMovement;

	private EnumImageType imagType;
	private String image;

	public DamageBallEntitySketch() {
		super();
		jsonMap.put("relative_position", new TreeMap());
		setEntitySize(new Area(new Vector2f(-0.5f, -0.5f), new Vector2f(0.5f, 0.5f)));
		setCollisionBox(new CollisionBox(new Vector2f(0), new Vector2f(-0.5f, -0.5f), new Vector2f(0.5f, 0.5f)));
		setAmplification(1);
		setVelocity(1);
		setAngularVelocity(0);
		setxMovement(false);
		setImagType(EnumImageType.IMAGE);
		setImage("null");
		jsonMap.put("type", "damage_ball");
	}

	@Override
	protected void updateMap() {
		super.updateMap();
		if (!jsonMap.containsKey("angular_velocity"))
			jsonMap.put("angular_velocity", 0D);
	}

	public DamageBallEntitySketch(String json) throws ParseException {
		super(json);
		jsonMap.put("type", "damage_ball");
	}

	public DamageBallEntitySketch(Map json) throws ParseException {
		super(json);
		jsonMap.put("type", "damage_ball");
	}

	public double getAmplification() {
		return amplification;
	}

	public void setAmplification(double amplification) {
		this.amplification = amplification;
		jsonMap.put("amplification", amplification);
	}

	public double getVelocity() {
		return velocity;
	}

	public void setVelocity(double velocity) {
		this.velocity = velocity;
		jsonMap.put("velocity", velocity);
	}

	public double getAngularVelocity() {
		return angularVelocity;
	}

	public void setAngularVelocity(double angularVelocity) {
		this.angularVelocity = angularVelocity;
		jsonMap.put("angular_velocity", angularVelocity);
	}

	public boolean isxMovement() {
		return xMovement;
	}

	public void setxMovement(boolean xMovement) {
		this.xMovement = xMovement;
		jsonMap.put("x_movement", xMovement);
	}

	public EnumImageType getImagType() {
		return imagType;
	}

	public void setImagType(EnumImageType imagType) {
		Validate.notNull(imagType, "Image type cannot be null!");
		this.imagType = imagType;
		jsonMap.put("image_type", imagType.name());
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image == null ? "null" : image;
		jsonMap.put("image", this.image);
	}

	public ITexture toImage() {
		try {
			String[] sl;
			switch (imagType) {
				case ANIMATION:
					return TextureManager.loadAnimation(image);
				case PALETTE_IMAGE:
					sl = image.split(";");
					return PaletteManager.loadPalette(sl[0]).getTexture(Integer.valueOf(sl[1]));
				case PALETTE_ANIMATION:
					sl = image.split(";");
					return PaletteManager.loadPalette(sl[0]).getAnimation(Integer.valueOf(sl[1]));
				case IMAGE:
				default:
					return TextureManager.loadImage(image);
			}
		} catch (Exception ignore) {
			return null;
		}
	}

	@Override
	public DamageBall toEntity(World world) {
		try {
			return new DamageBall(world, this);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Map<String, Class<?>> getTypesMap() {
		Map<String, Class<?>> map = new HashMap<>(super.getTypesMap());
		map.put("amplification", float.class);
		map.put("velocity", float.class);
		map.put("angular_velocity", float.class);
		map.put("x_movement", boolean.class);
		map.put("image_type", EnumImageType.class);
		map.put("image", String.class);
		return map;
	}

	@Override
	public void refreshFromMap() {
		super.refreshFromMap();
		amplification = (float) (double) jsonMap.get("amplification");
		velocity = (float) (double) jsonMap.get("velocity");
		angularVelocity = (float) (double) jsonMap.get("angular_velocity");
		xMovement = (boolean) jsonMap.get("x_movement");
		imagType = EnumImageType.getByName((String) jsonMap.get("image_type")).orElse(EnumImageType.IMAGE);
		image = (String) jsonMap.get("image");
	}
}
