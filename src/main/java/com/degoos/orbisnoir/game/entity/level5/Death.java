package com.degoos.orbisnoir.game.entity.level5;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.Controller;
import com.degoos.orbisnoir.entity.palette.MovablePalette;
import com.degoos.orbisnoir.entity.villager.Villager;
import com.degoos.orbisnoir.enums.EnumDirection;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.jooq.tools.json.ParseException;

import java.io.IOException;

public class Death extends Villager {


	public Death(World world, DeathEntitySketch sketch) throws IOException, ParseException {
		this(world, sketch.getName(), sketch.getPosition(), sketch.getColor(), sketch.getOpacity(), sketch.getEntitySize(), sketch.getEntityDrawPriority(),
				sketch.getCollisionBox(), sketch.getCollisionType(), null, sketch.getScript(), sketch.getDirection(),
				MovablePalette.fromNPCPalette(PaletteManager.loadPalette("level5/death")), false, false, false);
		this.sketch = sketch;
	}

	public Death(World world, String name, Vector2f position, GColor color, float opacity, Area entitySize,
				 EnumEntityDrawPriority drawPriority, CollisionBox collisionBox, EnumEntityCollision collision,
				 Controller controller, Script script, EnumDirection direction,
				 MovablePalette movablePalette, boolean moving, boolean running, boolean casting) {
		super(world, name, position, color, opacity, entitySize, drawPriority, collisionBox,
				collision, controller, script, direction, movablePalette, moving, running, casting);
	}
}
