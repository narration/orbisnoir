package com.degoos.orbisnoir.game.entity.level3;

import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.sketch.VillagerEntitySketch;
import com.degoos.orbisnoir.entity.villager.Villager;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.jooq.tools.json.ParseException;

import java.util.Arrays;
import java.util.Map;

public class SpanishWarriorEntitySketch extends VillagerEntitySketch {


	public SpanishWarriorEntitySketch() {
		super();
		setEntitySize(new Area(-0.18f, 0, 1.18f, 2.2f));
		setCollisionBox(new CollisionBox(position, Arrays.asList(
				new Vector2f(0.1f, 0),
				new Vector2f(0.9f, 0),
				new Vector2f(0.9f, 0.8f),
				new Vector2f(0.1f, 0.8f))));
		jsonMap.put("type", "spanish_warrior");
	}

	public SpanishWarriorEntitySketch(String json) throws ParseException {
		super(json);
		jsonMap.put("type", "spanish_warrior");
	}

	public SpanishWarriorEntitySketch(Map json) throws ParseException {
		super(json);
		jsonMap.put("type", "spanish_warrior");
	}

	@Override
	public SpanishWarrior toEntity(World world) {
		try {
			return new SpanishWarrior(world, this);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
