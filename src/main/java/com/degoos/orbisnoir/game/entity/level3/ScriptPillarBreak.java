package com.degoos.orbisnoir.game.entity.level3;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.object.texture.Palette;
import com.degoos.orbisnoir.entity.image.ImageEntity;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.step.type.ScriptStepRun;
import com.degoos.orbisnoir.script.step.type.ScriptStepWait;
import com.degoos.orbisnoir.texture.PaletteManager;
import org.jooq.tools.json.ParseException;

import java.io.IOException;
import java.util.ArrayList;

public class ScriptPillarBreak extends Script {

	private static Palette PALETTE;

	static {
		try {
			PALETTE = PaletteManager.loadPalette("level3/pillar");
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
	}

	public ScriptPillarBreak(ImageEntity entity, Runnable then) {
		super("pillar_break", new ArrayList<>());
		addStep(new ScriptStepRun(() -> entity.getRectangle().setTexture(PALETTE.getTexture(1))));
		playSound();
		addStep(new ScriptStepWait(50));
		addStep(new ScriptStepRun(() -> entity.getRectangle().setTexture(PALETTE.getTexture(2))));
		playSound();
		addStep(new ScriptStepWait(50));
		addStep(new ScriptStepRun(() -> entity.getRectangle().setTexture(PALETTE.getTexture(3))));
		playSound();
		addStep(new ScriptStepWait(50));
		addStep(new ScriptStepRun(() -> entity.getRectangle().setTexture(PALETTE.getTexture(4))));
		playSound();
		addStep(new ScriptStepWait(50));
		addStep(new ScriptStepRun(then));
	}

	private void playSound () {
		addStep(new ScriptStepRun(() -> Engine.getSoundManager()
				.createSoundSource("sound/flare.ogg", "pillar_break", false, false).play()));
	}
}
