package com.degoos.orbisnoir.game.entity.level2;

import com.degoos.graphicengine2.util.Area;
import com.degoos.graphicengine2.util.Validate;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.sketch.EntitySketch;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.jooq.tools.json.ParseException;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class VoidEntitySketch extends EntitySketch<Void> {

	private Vector2f respawn;

	public VoidEntitySketch() {
		super();
		jsonMap.put("respawn", new TreeMap());
		setEntitySize(new Area(new Vector2f(-0.5f, -0.5f), new Vector2f(0.5f, 0.5f)));
		setCollisionBox(new CollisionBox(new Vector2f(0), new Vector2f(-0.5f, -0.5f), new Vector2f(0.5f, 0.5f)));
		setRespawn(new Vector2f());
		jsonMap.put("type", "void");
	}

	public VoidEntitySketch(String json) throws ParseException {
		super(json);
		jsonMap.put("type", "void");
	}

	public VoidEntitySketch(Map json) throws ParseException {
		super(json);
		jsonMap.put("type", "void");
	}

	public Vector2f getRespawn() {
		return new Vector2f(respawn);
	}

	public void setRespawn(Vector2f respawn) {
		Validate.notNull(respawn, "To cannot be null!");
		this.respawn = respawn;
		Map respawnMap = (Map) jsonMap.get("respawn");
		respawnMap.put("x_pos", respawn.x);
		respawnMap.put("y_pos", respawn.y);
	}

	@Override
	public Void toEntity(World world) {
		try {
			return new Void(world, this);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Map<String, Class<?>> getTypesMap() {
		Map<String, Class<?>> map = new HashMap<>(super.getTypesMap());
		map.put("respawn", Map.class);
		map.put("respawn_x_pos", double.class);
		map.put("respawn_y_pos", double.class);
		return map;
	}

	@Override
	public void refreshFromMap() {
		super.refreshFromMap();
		Map respawnMap = (Map) jsonMap.get("respawn");
		respawn = new Vector2f((float) (double) respawnMap.get("x_pos"), (float) (double) respawnMap.get("y_pos"));
	}
}
