package com.degoos.orbisnoir.game.entity.level2;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.Collision;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptManager;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

import java.util.HashMap;
import java.util.Map;

public class Void extends Entity {

	private Vector2f respawn;

	public Void(World world, VoidEntitySketch sketch) {
		this(world, sketch.getName(), sketch.getPosition(), sketch.getEntitySize(), sketch.getScript(),
				sketch.getColor(), sketch.getOpacity(), sketch.getCollisionBox(), sketch.getCollisionType(),
				sketch.getEntityDrawPriority(), sketch.getRespawn());
		this.sketch = sketch;
	}

	public Void(World world, String name, Vector2f position, Area entitySize, Script script,
				GColor color, float opacity, CollisionBox collisionBox, EnumEntityCollision collisionType,
				EnumEntityDrawPriority entityDrawPriority, Vector2f respawn) {
		super(world, position, color, opacity, entitySize, entityDrawPriority, collisionBox, collisionType, null, script);
		setName(name);
		this.respawn = respawn;
	}


	@Override
	public void collide(Entity entity, Collision collision) {
		if (entity instanceof Player && !entity.isOnPlatform()) {
			Map<String, String> map = new HashMap<>();
			map.put("void_respawn_x", String.valueOf(respawn.x));
			map.put("void_respawn_y", String.valueOf(respawn.y));
			world.getScriptContainer().runScript(ScriptManager.getScriptUnsafe("global/void"), map);
		}
	}

	@Override
	public void restoreToSketch() {
		super.restoreToSketch();
		respawn = ((VoidEntitySketch) sketch).getRespawn();
	}
}
