package com.degoos.orbisnoir.game.entity.level3.boss;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.texture.TextureManager;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.joml.Vector3f;

public class Napolemobile extends Entity {

	private Level3Boss boss;

	public Napolemobile(World world, Level3Boss boss) {
		super(world, new Vector2f(boss.getPosition()), GColor.WHITE, 1,
				new Area(-0.924f + 0.5f, -0.8f, 0.924f + 0.5f, 0.8f),
				EnumEntityDrawPriority.ALWAYS_TOP, new CollisionBox(new Vector2f(boss.getPosition()),
						new Vector2f(-0.7f, -1f), new Vector2f(0.7f, 0.4f)),
				EnumEntityCollision.PASS, null, null);
		setName("napolemobile");
		this.boss = boss;
		rectangle.setTexture(TextureManager.loadImageOrNull("map/level3/napolemobile.png"));
	}

	@Override
	public void tick(long dif) {
		super.tick(dif);
		setPosition(new Vector2f(boss.getPosition()));
		rectangle.setRotation(boss.getRectangle().getRotation());
	}

	@Override
	protected void refreshDrawPriority(Vector3f pos) {
		pos.z = boss.getRectangle().getPosition().z;
	}
}

