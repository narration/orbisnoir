package com.degoos.orbisnoir.game.entity.level1;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.Collision;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.entity.Obstacle;
import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.entity.player.PlayerHealth;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;


public class Arrow extends Obstacle {

	protected Vector2f origin;
	protected Vector2f velocity;
	protected double damage, lowHealthDamage, distance;

	public Arrow(World world, Vector2f position, Vector2f velocity, ITexture texture,
				 double damage, double lowHealthDamage, double distance) {
		super(world, position, GColor.WHITE, 1,
				new Area(new Vector2f(-0.2f, -0.2f), new Vector2f(0.2f, 0.2f)), EnumEntityDrawPriority.RELATIVE,
				new CollisionBox(new Vector2f(0), new Vector2f(-0.2f, -0.2f), new Vector2f(0.2f, 0.2f)),
				EnumEntityCollision.PASS, null, null);
		this.velocity = velocity;
		this.damage = damage;
		this.lowHealthDamage = lowHealthDamage;
		this.origin = new Vector2f(position);
		this.distance = distance * distance;
		rectangle.setTexture(texture);
		setName("boss_arrow");

	}

	public Vector2f getVelocity() {
		return velocity;
	}

	public void setVelocity(Vector2f velocity) {
		this.velocity = velocity;
	}

	@Override
	public void tick(long dif) {
		if (slept) return;
		Vector2f to = new Vector2f(velocity).mul(dif);
		move(to, false);

		if (position.distanceSquared(origin) > distance) delete();
	}

	@Override
	public void collideSelfEntity(Entity entity, Collision collision) {
		if (entity instanceof Player) {
			PlayerHealth health = ((Player) entity).getHealth();
			health.damage(health.getHealth() > 0.5 ? damage : lowHealthDamage);
			delete();
		}
	}
}
