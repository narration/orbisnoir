package com.degoos.orbisnoir.game.entity.level3;

import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.sketch.VillagerEntitySketch;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.jooq.tools.json.ParseException;

import java.util.Arrays;
import java.util.Map;

public class GiantWarriorEntitySketch extends VillagerEntitySketch {


	public GiantWarriorEntitySketch() {
		super();
		setEntitySize(new Area(-1.8f/2, 0, 11.8f/2, 22f/2));
		setCollisionBox(new CollisionBox(position, Arrays.asList(
				new Vector2f(1f/2, 0),
				new Vector2f(9f/2, 0),
				new Vector2f(9f/2, 8f/2),
				new Vector2f(1f/2, 8f/2))));
		jsonMap.put("type", "giant_warrior");
	}

	public GiantWarriorEntitySketch(String json) throws ParseException {
		super(json);
		jsonMap.put("type", "giant_warrior");
	}

	public GiantWarriorEntitySketch(Map json) throws ParseException {
		super(json);
		jsonMap.put("type", "giant_warrior");
	}

	@Override
	public GiantWarrior toEntity(World world) {
		try {
			return new GiantWarrior(world, this);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
