package com.degoos.orbisnoir.game.entity.level2;

import com.degoos.orbisnoir.entity.sketch.EntityImageEntitySketch;
import com.degoos.orbisnoir.world.World;
import org.jooq.tools.json.ParseException;

import java.util.Map;

public class LineBallsEntitySketch extends EntityImageEntitySketch {

	private float radius;
	private double damage;

	public LineBallsEntitySketch() {
		super();
		setRadius(3);
		setDamage(0.07);
		jsonMap.put("type", "line_balls");
	}

	public LineBallsEntitySketch(String json) throws ParseException {
		super(json);
		jsonMap.put("type", "line_balls");
	}

	public LineBallsEntitySketch(Map json) {
		super(json);
		jsonMap.put("type", "line_balls");
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
		jsonMap.put("radius", (double) radius);
	}

	public double getDamage() {
		return damage;
	}

	public void setDamage(double damage) {
		this.damage = damage;
		jsonMap.put("damage", damage);
	}

	@Override
	public LineBalls toEntity(World world) {
		return new LineBalls(world, this);
	}

	@Override
	public Map<String, Class<?>> getTypesMap() {
		Map<String, Class<?>> map = super.getTypesMap();
		map.put("radius", float.class);
		map.put("damage", double.class);
		return map;
	}

	@Override
	public void refreshFromMap() {
		super.refreshFromMap();
		radius = (float) (double) jsonMap.get("radius");
		damage = (double) jsonMap.get("damage");
	}
}
