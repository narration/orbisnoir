package com.degoos.orbisnoir.game.entity.level1.boss;

import com.degoos.graphicengine2.Engine;
import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepWait;
import com.degoos.orbisnoir.util.VectorUtils;
import com.degoos.orbisnoir.world.Camera;
import org.joml.Vector2f;

import java.util.ArrayList;
import java.util.Random;

public class Level1BossScriptExecuterAttackFire extends Script {

    private int cameraPhase = 0;

    public Level1BossScriptExecuterAttackFire(Player player, Level1Boss boss) {
        super("level_1_boss_attack_fire", new ArrayList<>());

        addStep(new ScriptStepWait(1500));

        Random random = new Random();

        addStep(new ScriptStep(null, null) {
            @Override
            public void run(ScriptExecuter room, Runnable then) {
                boss.getRectangle().setTexture(boss.getDirectionablePalette().getPalette().getTexture(5, 2));
                then.run();
            }
        });

        for (int i = 0; i < 20; i++) {
            addStep(new ScriptStep(null, null) {
                @Override
                public void run(ScriptExecuter executer, Runnable then) {

                    if (player.getHealth().getHealth() == 0) {
                        executer.stop();
                        return;
                    }

                    Vector2f to = VectorUtils.rotate(new Vector2f(0, (random.nextFloat() * boss.getAreaRadius() * 0.8f) + 2),
                            random.nextDouble() * Math.PI * 2).add(boss.getAreaOrigin());


                    Engine.getSoundManager().createSoundSource("sound/flare.ogg", "flare", false, true).play();
                    executer.getWorld().registerEntity(new Level1BossFire(executer.getWorld(), boss.getCollisionBox().getCenter(), to));
                    cameraTick(executer.getWorld().getCamera());
                    then.run();
                }
            });
            getSteps().add(new ScriptStepWait(50));
        }

        getSteps().add(new ScriptStep(null, null) {
            @Override
            public void run(ScriptExecuter room, Runnable then) {
                boss.getRectangle().setTexture(boss.getDirectionablePalette().getPalette().getTexture(0, 2));
                then.run();
            }
        });

        getSteps().add(new ScriptStepWait(200));

    }

    private void cameraTick(Camera camera) {
        switch (cameraPhase) {
            case 0:
            case 3:
                camera.getPosition().add(0.05f, 0.05f, 0);
                break;
            case 1:
            case 2:
                camera.getPosition().sub(0.05f, 0.05f, 0);
                break;
        }
        cameraPhase = (cameraPhase + 1) % 4;
    }
}
