package com.degoos.orbisnoir.game.entity.level3.boss.sub;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.sound.SoundSource;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.palette.MovablePalette;
import com.degoos.orbisnoir.enums.EnumDirection;
import com.degoos.orbisnoir.game.entity.level3.boss.Level3Boss;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

import java.util.Arrays;

public class RunningGiant extends RunningWarrior {

	private Level3Boss boss;
	private boolean boosHit;

	public RunningGiant(World world, Vector2f position, EnumDirection direction, Level3Boss boss) {
		super(world, position, direction);
		setEntitySize(new Area(-1.8f / 2, 0, 11.8f / 2, 22f / 2));
		setCollisionBox(new CollisionBox(position, Arrays.asList(
				new Vector2f(1f / 2, 0),
				new Vector2f(9f / 2, 0),
				new Vector2f(9f / 2, 8f / 2),
				new Vector2f(1f / 2, 8f / 2))));
		this.boss = boss;
		boosHit = false;
	}

	@Override
	public void tick(long dif) {
		super.tick(dif);
		if (position.y > 10)
			position.add(new Vector2f(direction.getRelative()).mul(dif * 10 / 1000000000f));
		if (hit && boss.getCollisionBox().collision(collisionBox) != null) {
			if (!boosHit) {
				boss.damage(0.25f);
				boosHit = true;
				SoundSource source = Engine.getSoundManager().createSoundSource("sound/napoleon_hit.ogg", "hit", false, false);
				source.setGain(0.8f);
				source.setPitch(0.8f);
				source.play();
			}
		}
	}
}
