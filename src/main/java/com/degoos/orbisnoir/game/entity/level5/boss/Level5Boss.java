package com.degoos.orbisnoir.game.entity.level5.boss;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.sound.SoundSource;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.Collision;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.entity.palette.MovablePalette;
import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.entity.player.PlayerController;
import com.degoos.orbisnoir.enums.EnumDirection;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.game.entity.level5.Death;
import com.degoos.orbisnoir.game.entity.level5.boss.sub.L4BArrow;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.jooq.tools.json.ParseException;

import java.io.IOException;
import java.util.Arrays;

public class Level5Boss extends Death {

	private float angle;

	public Level5Boss(World world, Vector2f position) throws IOException, ParseException {
		super(world, "boss", position, GColor.WHITE, 1,
				new Area(-0.18f * 1.6f, 0, 1.18f * 1.6f, 2.2f * 1.6f), EnumEntityDrawPriority.ALWAYS_TOP,
				new CollisionBox(position, Arrays.asList(
						new Vector2f(0.1f, 0),
						new Vector2f(0.9f, 0),
						new Vector2f(0.9f, 0.8f),
						new Vector2f(0.1f, 0.8f))), EnumEntityCollision.PASS, null, null, EnumDirection.UP,
				MovablePalette.fromNPCPalette(PaletteManager.loadPalette("level5/death")), false, false, false);
		rectangle.setTexture(directionablePalette.getPalette().getTexture(4, 0));
		angle = 0;
	}

	@Override
	public void tick(long dif) {
		super.tick(dif);
		phase0(dif);
	}

	private void phase0(float dif) {
		Vector2f offset = new Vector2f(world.getPlayer().getPosition());
		offset.sub(position);
		float cos = (float) Math.cos(angle) / 8;

		setPosition(position.add(offset.mul(1.5f * dif / 1000000000f)).add(cos, 0));

		angle += dif * 2 / 1000000000f;

		entitySize.getMax().set(1.18f * 1.6f + cos, 2.2f * 1.6f + cos);
		entitySize.getMin().set(-0.18f * 1.6f - cos, -cos);
		setEntitySize(entitySize);
		if (Math.random() < 0.01) launchProjectile();
	}

	private void launchProjectile() {
		if (!((PlayerController) world.getPlayer().getController()).canBeControlled()) return;
		L4BArrow arrow = new L4BArrow(world, new Vector2f(position));
		world.registerEntity(arrow);
		SoundSource soundSource = Engine.getSoundManager().createSoundSource("sound/magic_attack.ogg",
				"magic_attack", false, true);
		soundSource.setPitch(0.4f);
		soundSource.play();
	}

	@Override
	public void collide(Entity entity, Collision collision) {
		if(entity instanceof Player && ((PlayerController)entity.getController()).canBeControlled()) {
			((Player) entity).getHealth().damage(1);
		}
	}
}
