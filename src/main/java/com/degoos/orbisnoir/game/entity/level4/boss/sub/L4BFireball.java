package com.degoos.orbisnoir.game.entity.level4.boss.sub;

import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.object.texture.Palette;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.Collision;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.entity.player.PlayerHealth;
import com.degoos.orbisnoir.game.entity.level1.Arrow;
import com.degoos.orbisnoir.game.entity.level4.boss.Level4Boss;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.jooq.tools.json.ParseException;

import java.io.IOException;

public class L4BFireball extends Arrow {

	private static Palette RED_PALETTE;

	static {
		try {
			RED_PALETTE = PaletteManager.loadPalette("level4/boss_red");
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
	}

	private Level4Boss boss;
	private boolean deflected, red;

	public L4BFireball(World world, Vector2f position, Vector2f velocity, ITexture texture, Level4Boss boss, boolean red) {
		super(world, position, velocity, red ? RED_PALETTE.getAnimation(0) : texture, 0.3f, 0.2f, 100);
		setEntitySize(new Area(-0.4f, -0.4f, 0.4f, 0.4f));
		this.red = red;
		this.boss = boss;
		rectangle.setOpacity(0.99f);
		deflected = false;
	}


	@Override
	public void collideSelfEntity(Entity entity, Collision collision) {
		if (entity instanceof Player) {
			Player player = (Player) entity;
			if (player.getShield().isActive() && !red) {
				player.getShield().damage(damage / 5);
				velocity = new Vector2f(boss.getPosition()).sub(position).normalize().mul(velocity.length());
				deflected = true;
			} else {
				if (red && player.getShield().isActive()) {
					player.getShield().damage(0.7);
					delete();
				} else {
					PlayerHealth health = player.getHealth();
					health.damage(health.getHealth() > 0.5 ? damage : lowHealthDamage);
					delete();
				}
			}
		}
	}

	@Override
	public void tick(long dif) {
		super.tick(dif);
		if (deflected && boss.getCollisionBox().collision(collisionBox) != null) {
			boss.damage(0.5);
			delete();
		}
	}
}

