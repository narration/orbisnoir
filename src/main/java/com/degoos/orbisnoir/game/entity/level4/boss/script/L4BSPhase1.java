package com.degoos.orbisnoir.game.entity.level4.boss.script;

import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.game.entity.level4.boss.Level4Boss;
import com.degoos.orbisnoir.game.entity.level4.boss.sub.L4BSnake;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.step.type.ScriptStepMoveComplex;
import com.degoos.orbisnoir.script.step.type.ScriptStepRun;
import com.degoos.orbisnoir.script.step.type.ScriptStepWait;
import org.joml.Vector2f;

import java.util.LinkedList;

public class L4BSPhase1 extends Script {

	public L4BSPhase1(Level4Boss boss) {
		super("l4b/phase1", new LinkedList<>());

		addStep(new ScriptStepWait(400));
		addStep(new ScriptStepMoveComplex(null, null, "boss", true, new Vector2f(0, 15), 1000));
		addStep(new ScriptStepRun(() -> spawnSnakes(boss)));
		addStep(new ScriptStepWait(4000));
		addStep(new ScriptStepRun(boss::startBattle));
	}

	private void spawnSnakes(Level4Boss boss) {
		Player player = boss.getWorld().getPlayer();
		Vector2f pos = new Vector2f(player.getPosition()).add(-15, 0);

		L4BSnake snake;
		for (int i = 0; i < 4; i++) {
			snake = new L4BSnake(boss.getWorld(), new Vector2f(pos).sub(i, 0),
					new Vector2f(10 / 1000000000f), boss.getRectangle().getTexture(), false);
			boss.getWorld().registerEntity(snake);
		}
		pos.set(player.getPosition().x + 15, player.getPosition().y);
		for (int i = 0; i < 4; i++) {
			snake = new L4BSnake(boss.getWorld(), new Vector2f(pos).add(i, 0),
					new Vector2f(-10 / 1000000000f), boss.getRectangle().getTexture(), true);
			boss.getWorld().registerEntity(snake);
		}
	}
}
