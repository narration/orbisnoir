package com.degoos.orbisnoir.game.entity.level4.boss.script;

import com.degoos.graphicengine2.Engine;
import com.degoos.orbisnoir.game.entity.level4.boss.Level4Boss;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.step.type.ScriptStepMoveComplex;
import com.degoos.orbisnoir.script.step.type.ScriptStepRun;
import com.degoos.orbisnoir.script.step.type.ScriptStepWait;
import org.joml.Vector2f;

import java.util.LinkedList;

public class L4BSPhase2 extends Script {

	public L4BSPhase2(Level4Boss boss) {
		super("l4b/phase2", new LinkedList<>());

		addStep(new ScriptStepMoveComplex(null, null, "boss", true,
				new Vector2f(boss.getWorld().getPlayer().getPosition()).sub(boss.getPosition()).normalize().mul(-1), 900));
		addStep(new ScriptStepWait(100));
		addStep(new ScriptStepRun(() -> Engine.getSoundManager()
				.createSoundSource("sound/level_4_boss_fireball.ogg", "boss_kill", false, false).play()));
		addStep(new ScriptStepMoveComplex(null, null, "boss", true,
				new Vector2f(boss.getWorld().getPlayer().getPosition()).sub(boss.getPosition()).normalize().mul(25), 1000));
		addStep(new ScriptStepRun(boss::startBattle));
	}
}
