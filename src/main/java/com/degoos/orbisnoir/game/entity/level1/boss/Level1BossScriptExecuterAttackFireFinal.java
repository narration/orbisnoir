package com.degoos.orbisnoir.game.entity.level1.boss;

import com.degoos.graphicengine2.Engine;
import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepGoto;
import com.degoos.orbisnoir.script.step.type.ScriptStepRunScript;
import com.degoos.orbisnoir.script.step.type.ScriptStepWait;
import com.degoos.orbisnoir.script.value.parser.IntValueParser;
import com.degoos.orbisnoir.script.value.parser.StringValueParser;
import com.degoos.orbisnoir.util.VectorUtils;
import com.degoos.orbisnoir.world.Camera;
import org.joml.Vector2f;

import java.util.ArrayList;
import java.util.Random;

public class Level1BossScriptExecuterAttackFireFinal extends Script {

	private int cameraPhase;

	public Level1BossScriptExecuterAttackFireFinal(Level1Boss boss, Player player) {
		super("level_1_boss_fire_final", new ArrayList<>());
		addStep(new ScriptStepWait(500));
		Random random = new Random();

		getSteps().add(new ScriptStep(null, null) {
			@Override
			public void run(ScriptExecuter room, Runnable then) {
				boss.getRectangle().setTexture(boss.getDirectionablePalette().getPalette().getTexture(5, 2));
				then.run();
			}
		});

		addStep(new ScriptStepRunScript(null, null, new StringValueParser("level1/room6/end_battle")));

		addStep(new ScriptStep(null, null) {
			@Override
			public void run(ScriptExecuter executer, Runnable then) {

				if (player.getHealth().getHealth() == 0) {
					executer.stop();
					return;
				}

				Vector2f to = VectorUtils.rotate(new Vector2f(0,
								(random.nextFloat() * boss.getAreaRadius() * 0.8f) + 2),
						random.nextDouble() * Math.PI * 2).add(boss.getAreaOrigin());
				Engine.getSoundManager().createSoundSource("sound/flare.ogg", "flare", false, true).play();
				executer.getWorld().registerEntity(new Level1BossFinalFire(executer.getWorld(),
						boss.getCollisionBox().getCenter(), to.sub(0, 15)));
				cameraTick(executer.getWorld().getCamera());
				then.run();
			}
		});

		addStep(new ScriptStepWait(50));
		addStep(new ScriptStepGoto(null, null, new IntValueParser(3)));


	}

	private void cameraTick(Camera camera) {
		switch (cameraPhase) {
			case 0:
			case 3:
				camera.getPosition().add(0.05f, 0.05f, 0);
				break;
			case 1:
			case 2:
				camera.getPosition().sub(0.05f, 0.05f, 0);
				break;
		}
		cameraPhase = (cameraPhase + 1) % 4;
	}
}
