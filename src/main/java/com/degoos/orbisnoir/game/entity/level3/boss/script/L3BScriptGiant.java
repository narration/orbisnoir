package com.degoos.orbisnoir.game.entity.level3.boss.script;

import com.degoos.orbisnoir.enums.EnumDirection;
import com.degoos.orbisnoir.game.entity.level3.boss.Level3Boss;
import com.degoos.orbisnoir.game.entity.level3.boss.sub.RunningGiant;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.step.type.ScriptStepRun;
import com.degoos.orbisnoir.script.step.type.ScriptStepWait;
import org.joml.Vector2f;

import java.util.LinkedList;

public class L3BScriptGiant extends Script {

	public L3BScriptGiant(Level3Boss boss, Runnable then) {
		super("giant", new LinkedList<>());
		addStep(new ScriptStepRun(() -> {
			RunningGiant giant = new RunningGiant(boss.getWorld(), new Vector2f(boss.getPosition().x - 2,
					boss.getPosition().y + 30), EnumDirection.DOWN, boss);
			boss.getWorld().registerEntity(giant);
		}));
		addStep(new ScriptStepWait(2000));
		addStep(new ScriptStepRun(then));
	}
}
