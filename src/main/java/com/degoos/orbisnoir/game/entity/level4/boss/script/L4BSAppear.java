package com.degoos.orbisnoir.game.entity.level4.boss.script;

import com.degoos.orbisnoir.game.entity.level4.boss.Level4Boss;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.step.type.ScriptStepFollowScript;
import com.degoos.orbisnoir.script.step.type.ScriptStepMoveComplex;
import com.degoos.orbisnoir.script.step.type.ScriptStepRun;
import com.degoos.orbisnoir.script.step.type.ScriptStepWait;
import org.joml.Vector2f;

import java.util.LinkedList;

public class L4BSAppear extends Script {

	public L4BSAppear(Level4Boss boss) {
		super("l4b/appear", new LinkedList<>());

		addStep(new ScriptStepWait(400));
		addStep(new ScriptStepMoveComplex(null, null, "boss", true, new Vector2f(0, -7.5f), 5000));
		addStep(new ScriptStepRun(boss::startWobbling));
		addStep(new ScriptStepFollowScript("level4/room4/boss_appear"));
		addStep(new ScriptStepRun(boss::startBattle));
	}
}
