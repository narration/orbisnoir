package com.degoos.orbisnoir.game.entity.level1.boss;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.sound.SoundSource;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.palette.MovablePalette;
import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.entity.villager.Villager;
import com.degoos.orbisnoir.enums.EnumDirection;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.ScriptManager;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.jooq.tools.json.ParseException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Level1Boss extends Villager {

	private Vector2f areaOrigin;
	private float areaRadius;
	private Player player;

	private ScriptExecuter currentExecuter;
	private int attacks;
	private int phase;

	private boolean launchingFire;

	public Level1Boss(World world, Player player, Vector2f areaOrigin, float areaRadius) throws IOException, ParseException {
		super(world, "boss", areaOrigin, GColor.WHITE, 1, new Area(new Vector2f(-0.15f, 0),
						new Vector2f(1.15f, 2.2f)), EnumEntityDrawPriority.RELATIVE,
				new CollisionBox(new Vector2f(0), new Vector2f(0.1f, 0), new Vector2f(0.9f, 0.8f)),
				EnumEntityCollision.COLLIDE, null, null, EnumDirection.DOWN,
				MovablePalette.fromNPCPalette(PaletteManager.loadPalette("level1/boss")),
				false, false, false);
		this.player = player;
		this.areaOrigin = areaOrigin;
		this.areaRadius = areaRadius;
		this.attacks = 0;
		this.phase = 0;
		this.launchingFire = false;
	}

	public Vector2f getAreaOrigin() {
		return areaOrigin;
	}

	public float getAreaRadius() {
		return areaRadius;
	}

	public boolean isLaunchingFire() {
		return launchingFire;
	}

	public void startBattle() {
		currentExecuter = new ScriptExecuter(new Level1BossScriptExecuterTeleport(this, player, new Vector2f(17, 15)), world);
		currentExecuter.setOnFinish(executer -> selectNextPhase());
		currentExecuter.next();
	}

	public void selectNextPhase() {
		if (player.getHealth().getHealth() == 0) return;
		switch (new Random().nextInt(5)) {
			case 0:
				currentExecuter = new ScriptExecuter(new Level1BossScriptExecuterMove(this, player.getPosition()), world);
				currentExecuter.setOnFinish(executer -> selectNextPhase());
				currentExecuter.next();
				break;
			case 1:
				currentExecuter = new ScriptExecuter(new Level1BossScriptExecuterTeleport(this, player, null), world);
				currentExecuter.setOnFinish(executer -> selectNextPhase());
				currentExecuter.next();
				break;
			case 2:
			case 3:
			case 4:
				attack();
				break;
		}
	}

	public void attack() {

		if (attacks >= 7) {
			phase++;
			if (phase == 3) {

				currentExecuter = new ScriptExecuter(new Level1BossScriptExecuterTeleport(this, player, new Vector2f(17, 15)), world);
				currentExecuter.setOnFinish(target -> {
					if (player.getHealth().getHealth() == 0) return;
					launchingFire = true;
					Engine.getSoundManager().getSource("background").ifPresent(SoundSource::cleanup);

					ScriptExecuter executer = new ScriptExecuter(new Level1BossScriptExecuterAttackFireFinal(this, player), world);
					world.getScriptContainer().getScripts().add(executer);
					executer.next();
				});


				currentExecuter.next();
				return;
			}

			currentExecuter = new ScriptExecuter(new Level1BossScriptExecuterAttackFire(player, this), world);
			currentExecuter.setOnFinish(target -> {
				launchingFire = false;
				selectNextPhase();
			});
			currentExecuter.next();
			launchingFire = true;
			attacks = 0;
			sendScript();
			return;
		}

		attacks++;
		if (new Random().nextBoolean())
			currentExecuter = new ScriptExecuter(new Level1BossScriptExecuterAttackSpiral(this, player), world);
		else
			currentExecuter = new ScriptExecuter(new Level1BossScriptExecuterAttackArrows(player, this), world);
		currentExecuter.setOnFinish(executer -> selectNextPhase());
		currentExecuter.next();
	}

	public void sendScript() {
		Map<String, String> map = new HashMap<>();

		Vector2f playerPosition = new Vector2f(player.getPosition()).add(player.getEntitySize().getMid());
		Vector2f bossPosition = new Vector2f(getPosition()).add(getEntitySize().getMid());

		map.put("boss_position_x", String.valueOf(bossPosition.x));
		map.put("boss_position_y", String.valueOf(bossPosition.y));
		map.put("player_position_x", String.valueOf(playerPosition.x));
		map.put("player_position_y", String.valueOf(playerPosition.y));
		world.getScriptContainer().runScript(ScriptManager.getScriptUnsafe("level1/room6/boss_phase_" + phase), map);
	}
}
