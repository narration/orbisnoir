package com.degoos.orbisnoir.game.entity.level1.boss;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.Collision;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.entity.Obstacle;
import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.entity.player.PlayerHealth;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.util.VectorUtils;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

import java.util.ArrayList;
import java.util.List;

public class Level1BossSpiral extends Entity {

	private long balls;
	private float angularVelocity, radius;
	private long livingNanos;

	private List<Ball> ballList;
	private Vector2f relativePosition;

	private Level1Boss boss;

	public Level1BossSpiral(World world, Vector2f position, int balls, Level1Boss boss) {
		super(world, position, GColor.BLACK, 0, new Area(0, 0, 0, 0),
				EnumEntityDrawPriority.ALWAYS_TOP,
				new CollisionBox(new Vector2f(0), new Vector2f(0), new Vector2f(0)),
				EnumEntityCollision.PASS, null, null);
		setName("boss_spiral");
		this.balls = balls;
		this.angularVelocity = 2;
		this.radius = 0.2f;
		this.relativePosition = new Vector2f();
		this.livingNanos = 0;
		this.boss = boss;
		initBalls();
	}

	private void initBalls() {
		ballList = new ArrayList<>();
		Vector2f init = new Vector2f(0, radius);
		double anglePerBall = Math.PI * 2 / balls;

		Ball ball;
		for (int i = 0; i < balls; i++) {
			ball = new Ball(VectorUtils.rotate(new Vector2f(init),
					anglePerBall * i).add(getPosition()).add(relativePosition));
			ballList.add(ball);
			world.registerEntity(ball);
		}
	}


	@Override
	public void setPosition(Vector2f position) {
		ballList.forEach(target -> target.setPosition(target.getPosition().sub(getPosition())));
		super.setPosition(position);
		ballList.forEach(target -> target.setPosition(target.getPosition().add(getPosition())));
	}

	public long getBalls() {
		return balls;
	}

	public double getRadius() {
		return radius;
	}

	public double getAngularVelocity() {
		return angularVelocity;
	}

	public void setAngularVelocity(float angularVelocity) {
		this.angularVelocity = angularVelocity;
	}

	@Override
	public void delete() {
		super.delete();
		ballList.forEach(Entity::delete);
	}

	@Override
	public void tick(long dif) {
		if (slept) return;
		livingNanos += dif;
		if (livingNanos > 10000000000L) delete();
	}


	public class Ball extends Obstacle {

		public Ball(Vector2f position) {
			super(Level1BossSpiral.this.world, position, GColor.WHITE, 1, new Area(new Vector2f(-0.2f, -0.2f),
							new Vector2f(0.2f, 0.2f)), EnumEntityDrawPriority.RELATIVE,
					new CollisionBox(new Vector2f(0), new Vector2f(-0.2f, -0.2f), new Vector2f(0.2f, 0.2f)),
					EnumEntityCollision.PASS, null, null);
			rectangle.setVisible(false);
			setName("ball");
		}

		@Override
		public void tick(long dif) {
			rectangle.setVisible(true);
			Vector2f to = VectorUtils.rotate(new Vector2f(getPosition()).sub(Level1BossSpiral.this.getPosition()),
					angularVelocity * (dif / 1000000000d)).add(Level1BossSpiral.this.getPosition());
			to = new Vector2f(to).add(new Vector2f(to).sub(Level1BossSpiral.this.getPosition())
					.normalize().mul(dif * 5 / 1000000000f));
			move(new Vector2f(to).sub(getPosition()), false);
		}

		@Override
		public void collideSelfEntity(Entity entity, Collision collision) {
			if (boss.isLaunchingFire()) return;
			if (entity instanceof Player) {
				PlayerHealth health = ((Player) entity).getHealth();
				health.damage(health.getHealth() > 0.5 ? 0.2 : 0.1);
				delete();
			}
		}
	}
}
