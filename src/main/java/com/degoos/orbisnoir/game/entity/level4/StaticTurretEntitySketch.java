package com.degoos.orbisnoir.game.entity.level4;

import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.util.Validate;
import com.degoos.orbisnoir.entity.image.EnumImageType;
import com.degoos.orbisnoir.entity.sketch.EntityImageEntitySketch;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.texture.TextureManager;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.jooq.tools.json.ParseException;

import java.util.Map;
import java.util.TreeMap;

public class StaticTurretEntitySketch extends EntityImageEntitySketch {

	private float bulletVelocity, distance;
	private Vector2f direction;
	private long millisPerBullet;

	private double damage, lowHealthDamage;

	private EnumImageType bulletImageType;
	private String bulletImage;

	public StaticTurretEntitySketch() {
		super();
		jsonMap.put("direction", new TreeMap());
		setBulletVelocity(2.5f);
		setDistance(100);
		setDirection(new Vector2f(1, 0));
		setMillisPerBullet(2000);

		setDamage(0.3f);
		setLowHealthDamage(0.2f);

		setBulletImageType(EnumImageType.IMAGE);
		setBulletImage("null");

		jsonMap.put("type", "static_turret");
	}

	public StaticTurretEntitySketch(String json) throws ParseException {
		super(json);
		jsonMap.put("type", "static_turret");
	}

	public StaticTurretEntitySketch(Map json) {
		super(json);
		jsonMap.put("type", "static_turret");
	}

	protected void updateMap() {
		super.updateMap();
		if (!jsonMap.containsKey("distance"))
			jsonMap.put("distance", 100D);
	}

	public float getBulletVelocity() {
		return bulletVelocity;
	}

	public void setBulletVelocity(float bulletVelocity) {
		this.bulletVelocity = bulletVelocity;
		jsonMap.put("bullet_velocity", (double) bulletVelocity);
	}

	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
		jsonMap.put("distance", (double) distance);
	}


	public Vector2f getDirection() {
		return new Vector2f(direction);
	}

	public void setDirection(Vector2f respawn) {
		Validate.notNull(respawn, "To cannot be null!");
		this.direction = respawn;
		Map directionMap = (Map) jsonMap.get("direction");
		directionMap.put("x_pos", (double) respawn.x);
		directionMap.put("y_pos", (double) respawn.y);
	}

	public long getMillisPerBullet() {
		return millisPerBullet;
	}

	public void setMillisPerBullet(long millisPerBullet) {
		this.millisPerBullet = millisPerBullet;
		jsonMap.put("millis_per_bullet", millisPerBullet);
	}

	public double getDamage() {
		return damage;
	}

	public void setDamage(double damage) {
		this.damage = damage;
		jsonMap.put("damage", damage);
	}

	public double getLowHealthDamage() {
		return lowHealthDamage;
	}


	public void setLowHealthDamage(double lowHealthDamage) {
		this.lowHealthDamage = lowHealthDamage;
		jsonMap.put("low_health_damage", lowHealthDamage);
	}

	public void setBulletImageType(EnumImageType bulletImageType) {
		Validate.notNull(bulletImageType, "Image type cannot be null!");
		this.bulletImageType = bulletImageType;
		jsonMap.put("bullet_image_type", bulletImageType.name());
	}

	public String getBulletImage() {
		return bulletImage;
	}

	public void setBulletImage(String bulletImage) {
		this.bulletImage = bulletImage == null ? "null" : bulletImage;
		jsonMap.put("bullet_image", this.bulletImage);
	}

	public ITexture toBulletImage() {
		try {
			String[] sl;
			switch (bulletImageType) {
				case ANIMATION:
					return TextureManager.loadAnimation(bulletImage);
				case PALETTE_IMAGE:
					sl = bulletImage.split(";");
					return PaletteManager.loadPalette(sl[0]).getTexture(Integer.valueOf(sl[1]));
				case PALETTE_ANIMATION:
					sl = bulletImage.split(";");
					return PaletteManager.loadPalette(sl[0]).getAnimation(Integer.valueOf(sl[1]));
				case IMAGE:
				default:
					return TextureManager.loadImage(bulletImage);
			}
		} catch (Exception ignore) {
			return null;
		}
	}

	@Override
	public StaticTurret toEntity(World world) {
		return new StaticTurret(world, this);
	}

	@Override
	public Map<String, Class<?>> getTypesMap() {
		Map<String, Class<?>> map = super.getTypesMap();
		map.put("bullet_velocity", float.class);
		map.put("distance", float.class);
		map.put("direction", Map.class);
		map.put("direction_x_pos", double.class);
		map.put("direction_y_pos", double.class);
		map.put("millis_per_bullet", long.class);
		map.put("damage", double.class);
		map.put("low_health_damage", double.class);
		map.put("bullet_image_type", EnumImageType.class);
		map.put("bullet_image", String.class);
		return map;
	}

	@Override
	public void refreshFromMap() {
		super.refreshFromMap();
		bulletVelocity = (float) (double) jsonMap.get("bullet_velocity");
		distance = (float) (double) jsonMap.get("distance");

		Map directionMap = (Map) jsonMap.get("direction");
		direction = new Vector2f((float) (double) directionMap.get("x_pos"), (float) (double) directionMap.get("y_pos"));

		millisPerBullet = (long) jsonMap.get("millis_per_bullet");

		damage = (double) jsonMap.get("damage");
		lowHealthDamage = (double) jsonMap.get("low_health_damage");

		bulletImageType = EnumImageType.getByName((String) jsonMap.get("bullet_image_type")).orElse(EnumImageType.IMAGE);
		bulletImage = (String) jsonMap.get("bullet_image");
	}
}
