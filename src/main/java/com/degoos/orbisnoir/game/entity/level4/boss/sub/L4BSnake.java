package com.degoos.orbisnoir.game.entity.level4.boss.sub;

import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.Collision;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.entity.player.PlayerHealth;
import com.degoos.orbisnoir.game.entity.level1.Arrow;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

public class L4BSnake extends Arrow {

	private boolean offset, hit;
	private float yOrigin;

	public L4BSnake(World world, Vector2f position, Vector2f velocity, ITexture texture, boolean offset) {
		super(world, position, velocity, texture, 0.5f, 0.3f, 100);
		setEntitySize(new Area(-0.4f, -0.4f, 0.4f, 0.4f));
		this.offset = offset;
		rectangle.setOpacity(0.99f);
		hit = false;
		yOrigin = position.y;
	}

	@Override
	public void tick(long dif) {
		super.tick(dif);
		float cos = (float) Math.cos(position.x) * 2;
		if (offset) cos = 1 - cos;
		position.y = yOrigin + cos;
	}

	@Override
	public void collideSelfEntity(Entity entity, Collision collision) {
		if (!hit && entity instanceof Player) {
			Player player = (Player) entity;
			PlayerHealth health = player.getHealth();
			health.damage(health.getHealth() > 0.5 ? damage : lowHealthDamage);
			hit = true;
		}
	}
}

