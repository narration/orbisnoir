package com.degoos.orbisnoir.game.entity.level1.boss;

import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepPlaySound;
import com.degoos.orbisnoir.script.step.type.ScriptStepWait;
import com.degoos.orbisnoir.util.VectorUtils;
import org.joml.Vector2f;

import java.util.ArrayList;
import java.util.Random;

public class Level1BossScriptExecuterTeleport extends Script {

	public Level1BossScriptExecuterTeleport(Level1Boss boss, Player player, Vector2f to) {
		super("level_1_boss_teleport", new ArrayList<>());

		setBossTexture(boss, 2, 2);
		addStep(new ScriptStepWait(300));
		addStep(new ScriptStepPlaySound(null, null, "teleport", "sound/teleport.ogg",
				1, 1, 0, false, true, 0));
		setBossTexture(boss, 4, 2);
		addStep(new ScriptStepWait(200));
		setBossTexture(boss, 7, 0);
		addStep(new ScriptStepWait(100));
		setBossTexture(boss, 8, 0);
		addStep(new ScriptStepWait(100));
		setBossTexture(boss, 9, 0);
		addStep(new ScriptStepWait(100));
		setBossTexture(boss, 9, 1);
		addStep(new ScriptStepWait(200));
		addStep(new ScriptStep(null, null) {
			@Override
			public void run(ScriptExecuter executer, Runnable then) {
				Random random = new Random();
				if (player.getHealth().getHealth() == 0) {
					executer.stop();
					return;
				}
				Vector2f teleport = to == null ? VectorUtils
						.rotate(new Vector2f(0, random.nextFloat() * boss.getAreaRadius()),
								random.nextDouble() * Math.PI * 2).add(boss.getAreaOrigin()) : to;
				boss.setPosition(teleport);
				then.run();
			}
		});
		addStep(new ScriptStepPlaySound(null, null, "teleport", "sound/teleport.ogg",
				1, 0.8f, 0, false, true, 0));
		addStep(new ScriptStepWait(100));
		setBossTexture(boss, 9, 0);
		addStep(new ScriptStepWait(100));
		setBossTexture(boss, 8, 0);
		addStep(new ScriptStepWait(100));
		setBossTexture(boss, 7, 0);
		addStep(new ScriptStepWait(100));
		setBossTexture(boss, 4, 2);
		addStep(new ScriptStepWait(200));
		setBossTexture(boss, 0, 2);
		addStep(new ScriptStepWait(300));
	}

	private void setBossTexture(Level1Boss boss, int x, int y) {
		addStep(new ScriptStep(null, null) {
			@Override
			public void run(ScriptExecuter executer, Runnable then) {
				boss.getRectangle().setTexture(boss.getDirectionablePalette().getPalette().getTexture(x, y));
				if (then != null) then.run();
			}
		});
	}
}
