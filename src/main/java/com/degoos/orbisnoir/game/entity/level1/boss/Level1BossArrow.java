package com.degoos.orbisnoir.game.entity.level1.boss;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.Collision;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.entity.Obstacle;
import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.entity.player.PlayerHealth;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;


public class Level1BossArrow extends Obstacle {

	private Vector2f velocity;
	private Level1Boss boss;

	public Level1BossArrow(World world, Vector2f position, Vector2f velocity, Level1Boss boss) {
		super(world, position, GColor.WHITE, 1,
				new Area(new Vector2f(-0.2f, -0.2f), new Vector2f(0.2f, 0.2f)), EnumEntityDrawPriority.RELATIVE,
				new CollisionBox(new Vector2f(0), new Vector2f(-0.2f, -0.2f), new Vector2f(0.2f, 0.2f)),
				EnumEntityCollision.PASS, null, null);
		rectangle.setVisible(false);
		setName("boss_arrow");
		this.velocity = velocity;
		this.boss = boss;
	}

	@Override
	public void tick(long dif) {
		rectangle.setVisible(true);
		if (slept) return;
		Vector2f to = new Vector2f(velocity).mul(dif);
		move(to, false);
	}

	@Override
	public void collideSelfEntity(Entity entity, Collision collision) {
		if (boss.isLaunchingFire()) return;
		if (entity instanceof Player) {
			PlayerHealth health = ((Player) entity).getHealth();
			health.damage(health.getHealth() > 0.5 ? 0.3 : 0.2);
			delete();
		}
	}
}
