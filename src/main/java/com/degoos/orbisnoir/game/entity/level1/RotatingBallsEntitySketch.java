package com.degoos.orbisnoir.game.entity.level1;

import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.util.Area;
import com.degoos.graphicengine2.util.Validate;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.image.EnumImageType;
import com.degoos.orbisnoir.entity.sketch.EntitySketch;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.texture.TextureManager;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.jooq.tools.json.ParseException;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class RotatingBallsEntitySketch extends EntitySketch<RotatingBalls> {

	private long balls;
	private float angularVelocity, radius;

	private EnumImageType centerImageType;
	private String centerImage;

	private EnumImageType ballImageType;
	private String ballImage;

	private Vector2f relativePosition;

	public RotatingBallsEntitySketch() {
		super();
		jsonMap.put("relative_position", new TreeMap());
		setEntitySize(new Area(new Vector2f(-0.5f, -0.5f), new Vector2f(0.5f, 0.5f)));
		setCollisionBox(new CollisionBox(new Vector2f(0), new Vector2f(-0.5f, -0.5f), new Vector2f(0.5f, 0.5f)));
		setBalls(3);
		setAngularVelocity((float) Math.PI);
		setRadius(3);
		setCenterImageType(EnumImageType.IMAGE);
		setCenterImage("null");
		setBallImageType(EnumImageType.IMAGE);
		setBallImage("null");
		setRelativePosition(new Vector2f(0));
		jsonMap.put("type", "rotating_balls");
	}

	public RotatingBallsEntitySketch(String json) throws ParseException {
		super(json);
		jsonMap.put("type", "rotating_balls");
	}

	public RotatingBallsEntitySketch(Map json) throws ParseException {
		super(json);
		jsonMap.put("type", "rotating_balls");
	}

	public long getBalls() {
		return balls;
	}

	public void setBalls(long balls) {
		this.balls = balls;
		jsonMap.put("balls", balls);
	}

	public float getAngularVelocity() {
		return angularVelocity;
	}

	public void setAngularVelocity(float angularVelocity) {
		this.angularVelocity = angularVelocity;
		jsonMap.put("angular_velocity", angularVelocity);
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
		jsonMap.put("radius", radius);
	}

	public EnumImageType getCenterImageType() {
		return centerImageType;
	}

	public void setCenterImageType(EnumImageType centerImageType) {
		Validate.notNull(centerImageType, "Image type cannot be null!");
		this.centerImageType = centerImageType;
		jsonMap.put("center_image_type", centerImageType.name());
	}

	public String getCenterImage() {
		return centerImage;
	}

	public void setCenterImage(String centerImage) {
		this.centerImage = centerImage == null ? "null" : centerImage;
		jsonMap.put("center_image", this.centerImage);
	}

	public EnumImageType getBallImageType() {
		return ballImageType;
	}

	public void setBallImageType(EnumImageType ballImageType) {
		Validate.notNull(ballImageType, "Image type cannot be null!");
		this.ballImageType = ballImageType;
		jsonMap.put("ball_image_type", ballImageType.name());
	}

	public String getBallImage() {
		return ballImage;
	}

	public void setBallImage(String ballImage) {
		this.ballImage = ballImage == null ? "null" : ballImage;
		jsonMap.put("ball_image", this.ballImage);
	}

	public Vector2f getRelativePosition() {
		return new Vector2f(relativePosition);
	}

	public void setRelativePosition(Vector2f relativePosition) {
		Validate.notNull(relativePosition, "Position cannot be null!");
		this.relativePosition = relativePosition;
		Map positionMap = (Map) jsonMap.get("relative_position");
		positionMap.put("x_pos", relativePosition.x);
		positionMap.put("y_pos", relativePosition.y);
	}


	public ITexture toCenterImage() {
		try {
			String[] sl;
			switch (centerImageType) {
				case ANIMATION:
					return TextureManager.loadAnimation(centerImage);
				case PALETTE_IMAGE:
					sl = centerImage.split(";");
					return PaletteManager.loadPalette(sl[0]).getTexture(Integer.valueOf(sl[1]));
				case PALETTE_ANIMATION:
					sl = centerImage.split(";");
					return PaletteManager.loadPalette(sl[0]).getAnimation(Integer.valueOf(sl[1]));
				case IMAGE:
				default:
					return TextureManager.loadImage(centerImage);
			}
		} catch (Exception ignore) {
			return null;
		}
	}

	public ITexture toBallImage() {
		try {
			String[] sl;
			switch (ballImageType) {
				case ANIMATION:
					return TextureManager.loadAnimation(ballImage);
				case PALETTE_IMAGE:
					sl = ballImage.split(";");
					return PaletteManager.loadPalette(sl[0]).getTexture(Integer.valueOf(sl[1]));
				case PALETTE_ANIMATION:
					sl = ballImage.split(";");
					return PaletteManager.loadPalette(sl[0]).getAnimation(Integer.valueOf(sl[1]));
				case IMAGE:
				default:
					return TextureManager.loadImage(ballImage);
			}
		} catch (Exception ignore) {
			return null;
		}
	}


	@Override
	public RotatingBalls toEntity(World world) {
		try {
			return new RotatingBalls(world, this);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Map<String, Class<?>> getTypesMap() {
		Map<String, Class<?>> map = new HashMap<>(super.getTypesMap());
		map.put("balls", long.class);
		map.put("angular_velocity", float.class);
		map.put("radius", float.class);
		map.put("center_image_type", EnumImageType.class);
		map.put("center_image", String.class);
		map.put("ball_image_type", EnumImageType.class);
		map.put("ball_image", String.class);

		map.put("relative_position", Map.class);
		map.put("relative_position_x_pos", float.class);
		map.put("relative_position_y_pos", float.class);
		return map;
	}

	@Override
	public void refreshFromMap() {
		super.refreshFromMap();
		balls = (long) jsonMap.get("balls");
		angularVelocity = (float) (double) jsonMap.get("angular_velocity");
		radius = (float) (double) jsonMap.get("radius");
		centerImageType = EnumImageType.getByName((String) jsonMap.get("center_image_type")).orElse(EnumImageType.IMAGE);
		centerImage = (String) jsonMap.get("center_image");
		ballImageType = EnumImageType.getByName((String) jsonMap.get("ball_image_type")).orElse(EnumImageType.IMAGE);
		ballImage = (String) jsonMap.get("ball_image");

		Map relativePositionMap = (Map) jsonMap.get("relative_position");
		relativePosition = new Vector2f((float) (double) relativePositionMap.get("x_pos"), (float) (double) relativePositionMap.get("y_pos"));

	}
}
