package com.degoos.orbisnoir.game.entity.level5;

import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.sketch.VillagerEntitySketch;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.jooq.tools.json.ParseException;

import java.util.Arrays;
import java.util.Map;

public class DeathEntitySketch extends VillagerEntitySketch {


	public DeathEntitySketch() {
		super();
		setEntitySize(new Area(-0.18f, 0, 1.18f, 2.2f));
		setCollisionBox(new CollisionBox(position, Arrays.asList(
				new Vector2f(0.1f, 0),
				new Vector2f(0.9f, 0),
				new Vector2f(0.9f, 0.8f),
				new Vector2f(0.1f, 0.8f))));
		jsonMap.put("type", "death");
	}

	public DeathEntitySketch(String json) throws ParseException {
		super(json);
		jsonMap.put("type", "death");
	}

	public DeathEntitySketch(Map json) throws ParseException {
		super(json);
		jsonMap.put("type", "death");
	}

	@Override
	public Death toEntity(World world) {
		try {
			return new Death(world, this);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
