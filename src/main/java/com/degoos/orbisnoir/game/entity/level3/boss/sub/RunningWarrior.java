package com.degoos.orbisnoir.game.entity.level3.boss.sub;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.sound.SoundSource;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.palette.MovablePalette;
import com.degoos.orbisnoir.entity.player.PlayerController;
import com.degoos.orbisnoir.enums.EnumDirection;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.game.entity.level3.SpanishWarrior;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

import java.util.Arrays;

public class RunningWarrior extends SpanishWarrior {

	private static MovablePalette SPANISH_PALETTE, FRENCH_PALETTE;

	static {
		try {
			SPANISH_PALETTE = MovablePalette.fromNPCPalette(PaletteManager.loadPalette("level3/spanish"));
			FRENCH_PALETTE = MovablePalette.fromNPCPalette(PaletteManager.loadPalette("level3/french"));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	protected long nanos;
	protected boolean hit;

	public RunningWarrior(World world, Vector2f position, EnumDirection direction) {
		super(world, "running_warrior", position, GColor.WHITE, 1, new Area(-0.18f, 0, 1.18f, 2.2f),
				EnumEntityDrawPriority.RELATIVE, new CollisionBox(position, Arrays.asList(
						new Vector2f(0.1f, 0),
						new Vector2f(0.9f, 0),
						new Vector2f(0.9f, 1f),
						new Vector2f(0.1f, 1f))), EnumEntityCollision.PASS, null, null, direction,
				Math.random() > 0.5f ? SPANISH_PALETTE : FRENCH_PALETTE, true, true, false);
		nanos = 0;
		hit = false;
	}


	public void invert() {
		if (!hit) {
			hit = true;
			setDirection(direction.getOpposite());
			if(this instanceof RunningGiant) {
				SoundSource source = Engine.getSoundManager()
						.createSoundSource("sound/ring_theme_2.ogg", "control", false, false);
				source.setPitch(0.7f);
				source.play();
			}
		}
	}

	@Override
	public void tick(long dif) {
		super.tick(dif);
		position.add(new Vector2f(direction.getRelative()).mul(dif * 10 / 1000000000f));
		nanos += dif;
		if (!hit && world.getPlayer().getCollisionBox().collision(collisionBox) != null) {
			if(!((PlayerController)world.getPlayer().getController()).canBeControlled()) return;
			hit = true;
			world.getPlayer().getHealth().damage(this instanceof RunningGiant ? 0.75 : 0.25);
		}
		if (nanos > 10000000000L || (position.y > 12.5 && !(this instanceof RunningGiant))) delete();
	}
}
