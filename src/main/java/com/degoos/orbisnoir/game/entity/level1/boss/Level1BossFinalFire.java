package com.degoos.orbisnoir.game.entity.level1.boss;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.texture.TextureAbsImpl;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;


public class Level1BossFinalFire extends Entity {

	private Vector2f velocity;
	private long nanos;
	private float lastSin;

	public Level1BossFinalFire(World world, Vector2f position, Vector2f to) {
		super(world, position, GColor.WHITE, 1, new Area(new Vector2f(-0.6f, -0.6f),
						new Vector2f(0.6f, 0.6f)), EnumEntityDrawPriority.RELATIVE,
				new CollisionBox(new Vector2f(0), new Vector2f(-0.6f, -0.6f), new Vector2f(0.6f, 0.6f)),
				EnumEntityCollision.PASS, null, null);
		setName("boss_fire");
		this.velocity = new Vector2f(to).sub(position).mul(1 / 2000000000f);
		this.nanos = 0;
		this.lastSin = 0;

		try {
			rectangle.setTexture(PaletteManager.loadPalette("level1/fire").getAnimation(0));
			world.addGObject((TextureAbsImpl) rectangle.getTexture());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void tick(long dif) {
		if (slept) return;
		this.nanos += dif;
		if (nanos >= 2000000000) {
			delete();
		}

		float sin = (float) Math.sin((nanos / 2000000000d) * Math.PI) * 6;
		Vector2f to = new Vector2f(getPosition()).add(velocity.x * dif,
				velocity.y * dif + (sin - lastSin));
		setPosition(to);
		this.lastSin = sin;
	}
}
