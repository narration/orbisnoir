package com.degoos.orbisnoir.game.entity.level3;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.Collision;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.Controller;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.entity.image.ImageEntity;
import com.degoos.orbisnoir.entity.palette.MovablePalette;
import com.degoos.orbisnoir.entity.villager.Villager;
import com.degoos.orbisnoir.enums.EnumDirection;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptManager;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.jooq.tools.json.ParseException;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class GiantWarrior extends Villager {

	private boolean active;
	private List<ImageEntity> pillars;

	public GiantWarrior(World world, GiantWarriorEntitySketch sketch) throws IOException, ParseException {
		this(world, sketch.getName(), sketch.getPosition(), sketch.getColor(), sketch.getOpacity(), sketch.getEntitySize(), sketch.getEntityDrawPriority(),
				sketch.getCollisionBox(), sketch.getCollisionType(), null, sketch.getScript(), sketch.getDirection(),
				MovablePalette.fromNPCPalette(PaletteManager.loadPalette("level3/spanish")), false, false, false);
		this.sketch = sketch;
	}

	public GiantWarrior(World world, String name, Vector2f position, GColor color, float opacity, Area entitySize,
						EnumEntityDrawPriority drawPriority, CollisionBox collisionBox, EnumEntityCollision collision,
						Controller controller, Script script, EnumDirection direction,
						MovablePalette movablePalette, boolean moving, boolean running, boolean casting) {
		super(world, name, position, color, opacity, entitySize, drawPriority, collisionBox,
				collision, controller, script, direction, movablePalette, moving, running, casting);
		active = false;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
		if (active) {
			pillars = world.getEntities().values().stream()
					.filter(target -> target instanceof ImageEntity && "pillar".equals(target.getName()))
					.map(target -> (ImageEntity) target)
					.collect(Collectors.toList());
		}
	}

	@Override
	public void tick(long dif) {
		super.tick(dif);
		if (slept) return;
		if (world.getPlayer().getControlling() != this && world.getPlayer().getCollisionBox().collision(collisionBox) != null)
			world.getPlayer().getHealth().damage(1);


		if (active) {
			setMoving(true);
			Vector2f direction = new Vector2f(world.getPlayer().getPosition()).sub(position).normalize().mul(3* dif / 1000000000f);
			move(direction, false);
			setDirection(getDirection(direction));
		}
	}

	private EnumDirection getDirection(Vector2f distance) {
		int deg = (int) Math.toDegrees(Math.atan2(distance.y, distance.x));
		if (deg < 0) deg += 360;
		EnumDirection direction;
		if (deg < 45) direction = EnumDirection.RIGHT;
		else if (deg < 135) direction = EnumDirection.UP;
		else if (deg < 225) direction = EnumDirection.LEFT;
		else if (deg < 315) direction = EnumDirection.DOWN;
		else direction = EnumDirection.DOWN;
		return direction;
	}

	@Override
	public void collideSelfEntity(Entity entity, Collision collision) {
		if (pillars.contains(entity)) {
			pillars.remove(entity);
			world.getScriptContainer().runScript(new ScriptPillarBreak((ImageEntity) entity, () -> {
				if (pillars.isEmpty()) finish();
			}));
		}
	}

	private void finish() {
		active = false;
		world.getScriptContainer().runScript(ScriptManager.getScriptUnsafe("level3/room2/finish_giant_battle"));
	}
}
