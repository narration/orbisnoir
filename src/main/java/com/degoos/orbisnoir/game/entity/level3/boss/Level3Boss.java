package com.degoos.orbisnoir.game.entity.level3.boss;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.texture.Texture;
import com.degoos.graphicengine2.object.texture.TextureRegion;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.palette.MovablePalette;
import com.degoos.orbisnoir.entity.villager.Villager;
import com.degoos.orbisnoir.enums.EnumDirection;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.game.entity.level3.boss.script.L3BScriptGiant;
import com.degoos.orbisnoir.game.entity.level3.boss.sub.RunningWarrior;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.ScriptManager;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.texture.TextureManager;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;
import org.jooq.tools.json.ParseException;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Level3Boss extends Villager {

	private static final float POS_1 = -3;
	private static final float POS_2 = -20;
	private static final float POS_3 = 50;

	private Napolemobile napolemobile;
	private Random random;

	private boolean active;

	private Rectangle bar, loadedBar;
	private float health;

	private long tempo;
	private long nanos;
	private float angle;

	private int phase;
	private int poses;

	public Level3Boss(World world, Vector2f position) throws IOException, ParseException {
		super(world, "boss", position, GColor.WHITE, 1, new Area(-0.18f, 0, 1.18f, 2.2f), EnumEntityDrawPriority.ALWAYS_TOP,
				new CollisionBox(position, Arrays.asList(
						new Vector2f(0.1f, 0),
						new Vector2f(0.9f, 0),
						new Vector2f(0.9f, 0.8f),
						new Vector2f(0.1f, 0.8f))), EnumEntityCollision.PASS, null, null, EnumDirection.UP,
				MovablePalette.fromNPCPalette(PaletteManager.loadPalette("level3/napoleon")), false, false, false);
		napolemobile = new Napolemobile(world, this);
		random = new Random();
		world.registerEntity(napolemobile);
		setDirection(EnumDirection.DOWN);
		angle = 0;
		tempo = 1000000000L * 60 / 135;
		phase = 0;
		poses = 0;
		health = 1f;
		active = false;
		world.addGObject(directionablePalette.getPalette().getAnimation(9));
		initBar();
	}

	private void initBar() {
		bar = new Rectangle(new Vector3f(0), new Vector3f(0, -0.01f, 0), new Vector3f(0.1f, 0.01f, 0));
		bar.setParent(this.rectangle);
		bar.setTexture(TextureManager.loadImageOrNull("gui/player/shield_bar_unloaded.png"));
		loadedBar = new Rectangle(new Vector3f(0, 0, 0.0001f), new Vector3f(bar.getMin()), new Vector3f(bar.getMax()));
		loadedBar.setParent(bar);
		loadedBar.setTexture(new TextureRegion((Texture)
				Objects.requireNonNull(TextureManager.loadImageOrNull("gui/player/shield_bar.png")), 0f, 0f, 1f, 1f));

		bar.setVisible(true);
		loadedBar.setVisible(true);
		bar.setOpacity(0);
		loadedBar.setOpacity(0);
		world.addGObject(bar);
		world.addGObject(loadedBar);
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public void damage(float damage) {
		health -= damage;
		if (health <= 0) {
			Engine.getSoundManager().cleanUpAllSoundSources();
			active = false;
			world.getScriptContainer()
					.runScript(ScriptManager.getScriptUnsafe("level3/room5/finish_battle"));
		}
	}

	@Override
	public void tick(long dif) {
		super.tick(dif);
		if(rectangle.getRotation() == 0) {
			float sin = (float) Math.sin(angle) * dif / 1000000000;
			position.y += sin;
			angle += 4 * dif / 1000000000f;
		}
		refreshBar();

		if (!active) return;

		nanos += dif;
		if (nanos > tempo) {
			nanos -= tempo;
			if (phase == 0) {
				pose();
				spawnRunningWarrior();
				if (poses > 20 && Math.random() > 0.94) phase1();
			}
		}
	}

	private void refreshBar() {
		if (health == 1 || health == 0) {
			loadedBar.setOpacity(Math.max(0, loadedBar.getOpacity() - 0.1f));
			bar.setOpacity(Math.max(0, bar.getOpacity() - 0.1f));
		} else {
			loadedBar.setOpacity(Math.min(1, loadedBar.getOpacity() + 0.1f));
			bar.setOpacity(Math.min(1, bar.getOpacity() + 0.1f));
		}

		if (loadedBar.getOpacity() > 0) {
			loadedBar.setMax(new Vector3f(0.1f * health, 0.01f, 0));
			loadedBar.getTexture().setU2(health);
			bar.setPosition(new Vector3f(0.022f, world.getCamera().getBoxSize() * 1.9f, 0));
		}
	}


	public void pose() {
		Vector2i vec = new Vector2i();
		switch (random.nextInt(10)) {
			case 0:
				vec.set(4, 0);
				break;
			case 1:
				vec.set(5, 2);
				break;
			case 2:
				vec.set(7, 6);
				break;
			case 3:
				vec.set(4, 12);
				break;
			case 4:
				vec.set(2, 12);
				break;
			case 5:
				vec.set(8, 17);
				break;
			case 6:
				vec.set(4, 20);
				break;
			case 7:
				vec.set(1, 18);
				break;
			case 8:
				vec.set(11, 18);
				break;
			case 9:
				vec.set(2, 14);
				break;
		}
		poses++;
		rectangle.setTexture(directionablePalette.getPalette().getTexture(vec.x, vec.y));
	}

	private void spawnRunningWarrior() {
		Vector2f pos;
		EnumDirection direction;
		switch (random.nextInt(3)) {
			case 0:
				pos = new Vector2f(POS_1, world.getPlayer().getPosition().y);
				direction = EnumDirection.RIGHT;
				break;
			case 1:
				pos = new Vector2f(world.getPlayer().getPosition().x, POS_2);
				direction = EnumDirection.UP;
				break;
			default:
				pos = new Vector2f(POS_3, world.getPlayer().getPosition().y);
				direction = EnumDirection.LEFT;
				break;
		}
		RunningWarrior warrior = new RunningWarrior(world, pos, direction);
		world.registerEntity(warrior);
	}

	private void phase1() {
		phase = 1;
		poses = 0;
		rectangle.setTexture(directionablePalette.getPalette().getAnimation(9));
		ScriptExecuter executer = new ScriptExecuter(new L3BScriptGiant(this, () -> {
			phase = 0;
		}), world);
		executer.next();
	}

	@Override
	protected void refreshDrawPriority(Vector3f pos) {
		if (active)
			super.refreshDrawPriority(pos);
		else pos.z = 0.8f;
	}

	@Override
	public void delete() {
		super.delete();
		napolemobile.delete();
		bar.delete();
		loadedBar.delete();
	}
}
