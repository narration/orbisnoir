package com.degoos.orbisnoir.game.entity.level2;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.object.texture.TextureAbsImpl;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.entity.MovableEntity;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

public class Platform extends Entity {

	private int status;

	private Vector2f pos1, pos2, movePerNano;

	private long moveDelay, waitDelay, currentNanos;
	private float angularVelocity;

	public Platform(World world, PlatformEntitySketch sketch) {
		this(world, sketch.getName(), sketch.getPosition(), sketch.getEntitySize(), sketch.getScript(),
				sketch.getColor(), sketch.getOpacity(), sketch.getCollisionBox(), sketch.getCollisionType(),
				sketch.getEntityDrawPriority(), sketch.getTo(), sketch.getMoveDelay(), sketch.getWaitDelay(),
				sketch.toImage(), sketch.getAngularVelocity());
		this.sketch = sketch;
	}

	public Platform(World world, String name, Vector2f position, Area entitySize, Script script,
	                GColor color, float opacity, CollisionBox collisionBox, EnumEntityCollision collisionType,
	                EnumEntityDrawPriority entityDrawPriority, Vector2f to, long moveDelay, long waitDelay,
	                ITexture texture, double angularVelocity) {
		super(world, position, color, opacity, entitySize, entityDrawPriority, collisionBox, collisionType, null, script);
		setName(name);
		tickPriority = -1;
		rectangle.setTickPriority(Integer.MIN_VALUE);
		pos1 = getPosition();
		pos2 = to;
		this.moveDelay = moveDelay * 1000000;
		this.waitDelay = waitDelay * 1000000;
		this.angularVelocity = (float) angularVelocity / 1000000000F;

		movePerNano = new Vector2f(pos2).sub(pos1).mul(1f / this.moveDelay);
		currentNanos = 0;
		status = 0;

		rectangle.setTexture(texture);
	}

	public Vector2f getPos1() {
		return pos1;
	}

	public Vector2f getPos2() {
		return pos2;
	}

	public long getMoveDelay() {
		return moveDelay;
	}

	public long getWaitDelay() {
		return waitDelay;
	}

	public long getCurrentNanos() {
		return currentNanos;
	}

	@Override
	public void restoreToSketch() {
		super.restoreToSketch();
		if (sketch == null) return;
		this.pos1 = new Vector2f(getPosition());
		this.pos2 = ((PlatformEntitySketch) sketch).getTo();
		this.angularVelocity = (float) ((PlatformEntitySketch) sketch).getAngularVelocity() / 1000000000F;

		this.moveDelay = ((PlatformEntitySketch) sketch).getMoveDelay() * 1000000;
		this.waitDelay = ((PlatformEntitySketch) sketch).getWaitDelay() * 1000000;

		rectangle.setTexture(((PlatformEntitySketch) sketch).toImage());
		if (rectangle.getTexture() != null)
			world.addGObject((TextureAbsImpl) rectangle.getTexture());


		movePerNano = new Vector2f(pos2).sub(pos1).mul(1f / this.moveDelay);
		currentNanos = 0;
		status = 0;

	}


	@Override
	public void tick(long dif) {
		if (slept) return;
		currentNanos += dif;

		if (status == 1 || status == 3) {
			moveEntities(new Vector2f(0));
			if (currentNanos >= waitDelay) {
				status++;
				status %= 4;
				currentNanos = 0;
			}
		} else {
			Vector2f add;
			if (status == 0)
				add = new Vector2f(movePerNano).mul(dif);
			else add = new Vector2f(movePerNano).mul(-dif);

			rectangle.setRotation(rectangle.getRotation() + (angularVelocity * dif * (status == 0 ? 1 : -1)));

			moveEntities(add);
			setPosition(getPosition().add(add));

			if (currentNanos >= moveDelay) {
				position.set(status == 0 ? pos2 : pos1);
				status++;
				currentNanos = 0;
			}
		}
	}

	private void moveEntities(Vector2f add) {
		world.getEntities().values().stream().filter(target -> target instanceof MovableEntity).filter(target ->
				target.getCollisionBox().collision(getCollisionBox()) != null)
				.forEach(target -> {
					target.setOnPlatform(true);
					if (add.x != 0 || add.y != 0)
						target.move(add, true);
				});
	}
}
