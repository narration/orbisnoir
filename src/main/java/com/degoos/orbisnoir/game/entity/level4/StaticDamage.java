package com.degoos.orbisnoir.game.entity.level4;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.image.ImageEntity;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

public class StaticDamage extends ImageEntity {

	private double damage;

	public StaticDamage(World world, StaticDamageEntitySketch sketch) {
		this(world, sketch.getName(), sketch.getPosition(), sketch.getEntitySize(), sketch.getScript(),
				sketch.getColor(), sketch.getOpacity(), sketch.getCollisionBox(), sketch.getCollisionType(),
				sketch.getEntityDrawPriority(), sketch.toImage(), sketch.getRotation(), sketch.getAngularVelocity(),
				sketch.getDamage());
		this.sketch = sketch;
	}

	public StaticDamage(World world, String name, Vector2f position, Area entitySize,
						Script script, GColor color, float opacity, CollisionBox collisionBox,
						EnumEntityCollision collisionType, EnumEntityDrawPriority entityDrawPriority,
						ITexture image, float rotation, float angularVelocity, double damage) {
		super(world, position, color, opacity, entitySize, entityDrawPriority, collisionBox, collisionType,
				null, script, image, rotation, angularVelocity);
		this.damage = damage / 1000000000f;
		setName(name);
	}

	public double getDamage() {
		return damage;
	}

	public void setDamage(double damage) {
		this.damage = damage;
	}

	@Override
	public void restoreToSketch() {
		super.restoreToSketch();
		damage = ((StaticDamageEntitySketch) sketch).getDamage() / 1000000000f;
	}

	@Override
	public void tick(long dif) {
		super.tick(dif);
		if (slept) return;
		if (world.getPlayer().getCollisionBox().collision(getCollisionBox()) != null) {
			world.getPlayer().getHealth().damage(damage * dif);
		}
	}
}
