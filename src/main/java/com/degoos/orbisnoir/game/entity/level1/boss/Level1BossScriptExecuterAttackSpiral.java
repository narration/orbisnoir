package com.degoos.orbisnoir.game.entity.level1.boss;

import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.step.ScriptStep;
import com.degoos.orbisnoir.script.step.type.ScriptStepPlaySound;
import com.degoos.orbisnoir.script.step.type.ScriptStepWait;
import org.joml.Vector2f;

import java.util.ArrayList;
import java.util.Random;

public class Level1BossScriptExecuterAttackSpiral extends Script {

	public Level1BossScriptExecuterAttackSpiral(Level1Boss boss, Player player) {
		super("level_1_boss_attack_spiral", new ArrayList<>());
		addStep(new ScriptStepWait(500));
		Random random = new Random();
		for (int i = 0; i < 4; i++) {
			int currentI = i;
			getSteps().add(new ScriptStep(null, null) {
				@Override
				public void run(ScriptExecuter executer, Runnable then) {
					boss.getRectangle().setTexture(boss.getDirectionablePalette().getPalette().getTexture(4, currentI));
					if (player.getHealth().getHealth() == 0) {
						executer.stop();
						return;
					}

					executer.getWorld().registerEntity(new Level1BossSpiral(executer.getWorld(),
							boss.getCollisionBox().getCenter(), random.nextInt(8) + 3, boss));
					then.run();
				}
			});
			addStep(new ScriptStepPlaySound(null, null, "magic_attack", "sound/magic_attack.ogg",
					1, 0.4f, 0, false, true, 0));
			addStep(new ScriptStepWait(500));
			getSteps().add(new ScriptStep(null, null) {
				@Override
				public void run(ScriptExecuter room, Runnable then) {
					boss.getRectangle().setTexture(boss.getDirectionablePalette().getPalette().getTexture(0, currentI));
					then.run();
				}
			});
			addStep(new ScriptStepWait(random.nextInt(700)));
		}
		addStep(new ScriptStepWait(10));
	}
}