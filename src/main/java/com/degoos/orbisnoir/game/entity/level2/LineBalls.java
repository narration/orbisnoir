package com.degoos.orbisnoir.game.entity.level2;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.Collision;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.entity.Obstacle;
import com.degoos.orbisnoir.entity.image.ImageEntity;
import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.util.VectorUtils;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

import java.util.ArrayList;
import java.util.List;


public class LineBalls extends ImageEntity {

	private float radius;
	private double damage;
	private List<Ball> ballList;


	public LineBalls(World world, LineBallsEntitySketch sketch) {
		this(world, sketch.getName(), sketch.getPosition(), sketch.getEntitySize(), sketch.getScript(),
				sketch.getColor(), sketch.getOpacity(), sketch.getCollisionBox(), sketch.getCollisionType(),
				sketch.getEntityDrawPriority(), sketch.toImage(), sketch.getRotation(), sketch.getAngularVelocity(),
				sketch.getRadius(), sketch.getDamage());
		this.sketch = sketch;
	}

	public LineBalls(World world, String name, Vector2f position, Area entitySize,
					 Script script, GColor color, float opacity, CollisionBox collisionBox,
					 EnumEntityCollision collisionType, EnumEntityDrawPriority entityDrawPriority,
					 ITexture image, float rotation, float angularVelocity, float radius, double damage) {
		super(world, position, color, opacity, entitySize, entityDrawPriority, collisionBox, collisionType, null, script, image, rotation, angularVelocity);
		setName(name);
		this.radius = radius;
		this.damage = damage;
		this.ballList = null;
		initBalls();
	}

	private void initBalls() {
		if (ballList != null) {
			ballList.forEach(Entity::delete);
			ballList.clear();
		} else ballList = new ArrayList<>();

		Ball ball;
		for (float d = 0; d <= radius; d += 0.4f) {
			ball = new Ball(new Vector2f(getPosition()).add(d, 0f), d);
			ballList.add(ball);
			world.registerEntity(ball);
		}
	}

	@Override
	public void setPosition(Vector2f position) {
		ballList.forEach(target -> target.setPosition(target.getPosition().sub(getPosition())));
		super.setPosition(position);
		ballList.forEach(target -> target.setPosition(target.getPosition().add(getPosition())));
	}

	public double getRadius() {
		return radius;
	}

	@Override
	public void restoreToSketch() {
		super.restoreToSketch();
		if (sketch == null) return;
		this.radius = ((LineBallsEntitySketch) sketch).getRadius();
		this.damage = ((LineBallsEntitySketch) sketch).getDamage();
		initBalls();
	}

	@Override
	public void delete() {
		super.delete();
		ballList.forEach(Entity::delete);
	}

	public class Ball extends Obstacle {

		float distance;

		public Ball(Vector2f position, float distance) {
			super(LineBalls.this.world, position, GColor.WHITE, 0, new Area(new Vector2f(-0.2f, -0.2f),
							new Vector2f(0.2f, 0.2f)), EnumEntityDrawPriority.RELATIVE,
					new CollisionBox(new Vector2f(0), new Vector2f(-0.2f, -0.2f), new Vector2f(0.2f, 0.2f)),
					EnumEntityCollision.PASS, null, null);
			setName("ball");
			this.distance = distance;
		}

		@Override
		public void tick(long dif) {
			if (LineBalls.this.isSlept()) return;
			Vector2f to = VectorUtils.rotate(new Vector2f(distance, 0),
					Math.toRadians(LineBalls.this.rectangle.getRotation()))
					.add(LineBalls.this.getPosition()).sub(getPosition());
			move(to, false);
		}

		@Override
		public void collideSelfEntity(Entity entity, Collision collision) {
			if (entity instanceof Player) {
				((Player) entity).getHealth().damage(damage);
			}
		}
	}
}
