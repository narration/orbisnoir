package com.degoos.orbisnoir.game.entity.level4;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.image.ImageEntity;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.game.entity.level1.Arrow;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

public class StaticTurret extends ImageEntity {

	private Vector2f direction;
	private float bulletVelocity, distance;
	private ITexture bulletImage;
	private long nanosPerBullet;
	private double damage, lowHealthDamage;

	private long nanos;

	public StaticTurret(World world, StaticTurretEntitySketch sketch) {
		this(world, sketch.getName(), sketch.getPosition(), sketch.getEntitySize(), sketch.getScript(),
				sketch.getColor(), sketch.getOpacity(), sketch.getCollisionBox(), sketch.getCollisionType(),
				sketch.getEntityDrawPriority(), sketch.toImage(), sketch.getRotation(), sketch.getAngularVelocity(),
				sketch.getDirection(), sketch.toBulletImage(), sketch.getBulletVelocity(), sketch.getDistance(), sketch.getMillisPerBullet(),
				sketch.getDamage(), sketch.getLowHealthDamage());
		this.sketch = sketch;
	}

	public StaticTurret(World world, String name, Vector2f position, Area entitySize,
						Script script, GColor color, float opacity, CollisionBox collisionBox,
						EnumEntityCollision collisionType, EnumEntityDrawPriority entityDrawPriority,
						ITexture image, float rotation, float angularVelocity, Vector2f direction,
						ITexture bulletImage, float bulletVelocity, float distance, long millisPerBullet, double damage, double lowHealthDamage) {
		super(world, position, color, opacity, entitySize, entityDrawPriority, collisionBox, collisionType, null, script, image, rotation, angularVelocity);
		setName(name);
		this.direction = direction;
		this.distance = distance;
		this.bulletVelocity = bulletVelocity / 1000000000;
		this.bulletImage = bulletImage;
		this.nanosPerBullet = millisPerBullet * 1000000;
		this.damage = damage;
		this.lowHealthDamage = lowHealthDamage;
	}


	public Vector2f getDirection() {
		return direction;
	}

	public void setDirection(Vector2f direction) {
		this.direction = direction;
	}

	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}

	public float getBulletVelocity() {
		return bulletVelocity;
	}

	public void setBulletVelocity(float bulletVelocity) {
		this.bulletVelocity = bulletVelocity;
	}

	public ITexture getBulletImage() {
		return bulletImage;
	}

	public void setBulletImage(ITexture bulletImage) {
		this.bulletImage = bulletImage;
	}

	public long getNanosPerBullet() {
		return nanosPerBullet;
	}

	public void setNanosPerBullet(long nanosPerBullet) {
		this.nanosPerBullet = nanosPerBullet;
	}

	public double getDamage() {
		return damage;
	}

	public void setDamage(double damage) {
		this.damage = damage;
	}

	public double getLowHealthDamage() {
		return lowHealthDamage;
	}

	public void setLowHealthDamage(double lowHealthDamage) {
		this.lowHealthDamage = lowHealthDamage;
	}

	public void shootBullet() {
		Arrow arrow = new Arrow(world, new Vector2f(getPosition()),
				new Vector2f(direction).mul(bulletVelocity), bulletImage, damage, lowHealthDamage, distance);
		world.registerEntity(arrow);
	}

	@Override
	public void restoreToSketch() {
		super.restoreToSketch();
		bulletVelocity = ((StaticTurretEntitySketch) sketch).getBulletVelocity() / 1000000000f;
		direction = ((StaticTurretEntitySketch) sketch).getDirection().normalize();
		bulletImage = ((StaticTurretEntitySketch) sketch).toBulletImage();
		nanosPerBullet = ((StaticTurretEntitySketch) sketch).getMillisPerBullet() * 1000000;
		damage = ((StaticTurretEntitySketch) sketch).getDamage();
		lowHealthDamage = ((StaticTurretEntitySketch) sketch).getLowHealthDamage();
	}

	@Override
	public void tick(long dif) {
		super.tick(dif);
		if (slept) return;
		nanos += dif;
		while (nanos >= nanosPerBullet) {
			shootBullet();
			nanos -= nanosPerBullet;
		}

	}
}
