package com.degoos.orbisnoir.game.entity.level4.boss;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.Rectangle;
import com.degoos.graphicengine2.object.texture.Texture;
import com.degoos.graphicengine2.object.texture.TextureAbsImpl;
import com.degoos.graphicengine2.object.texture.TextureRegion;
import com.degoos.graphicengine2.sound.SoundSource;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.game.entity.level4.boss.script.L4BSAppear;
import com.degoos.orbisnoir.game.entity.level4.boss.script.L4BSPhase1;
import com.degoos.orbisnoir.game.entity.level4.boss.script.L4BSPhase2;
import com.degoos.orbisnoir.game.entity.level4.boss.sub.L4BFireball;
import com.degoos.orbisnoir.script.ScriptExecuter;
import com.degoos.orbisnoir.script.ScriptManager;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.texture.TextureManager;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.jooq.tools.json.ParseException;

import java.io.IOException;
import java.util.Objects;

public class Level4Boss extends Entity {

	private ScriptExecuter currentScript;
	private Rectangle bar, loadedBar;
	private SoundSource music;

	private double health;

	private boolean dead;
	private double cosAngle;
	private float lastCos;
	private boolean wobbling;
	private int phase;

	public Level4Boss(World world, Vector2f position, SoundSource music) {
		super(world, position, GColor.WHITE, 0.99f, new Area(-1, -0.5f, 1, 1.5f),
				EnumEntityDrawPriority.RELATIVE, new CollisionBox(position, new Vector2f(-1, -0.5f),
						new Vector2f(1, 1.5f)), EnumEntityCollision.PASS, null, null);
		setName("boss");
		this.music = music;
		cosAngle = Math.PI;
		lastCos = 0;
		wobbling = false;
		dead = false;
		phase = -1;
		health = 5;
		initTexture();
		initBar();
	}

	private void initTexture() {
		try {
			rectangle.setTexture(PaletteManager.loadPalette("level4/boss").getAnimation(0));
			world.addGObject((TextureAbsImpl) rectangle.getTexture());
			world.addGObject(PaletteManager.loadPalette("level4/boss_red").getAnimation(0));
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
	}

	private void initBar() {
		bar = new Rectangle(new Vector3f(0), new Vector3f(0, -0.01f, 0), new Vector3f(0.1f, 0.01f, 0));
		bar.setParent(this.rectangle);
		bar.setTexture(TextureManager.loadImageOrNull("gui/player/shield_bar_unloaded.png"));
		loadedBar = new Rectangle(new Vector3f(0, 0, 0.0001f), new Vector3f(bar.getMin()), new Vector3f(bar.getMax()));
		loadedBar.setParent(bar);
		loadedBar.setTexture(new TextureRegion((Texture)
				Objects.requireNonNull(TextureManager.loadImageOrNull("gui/player/shield_bar.png")), 0f, 0f, 1f, 1f));

		bar.setVisible(true);
		loadedBar.setVisible(true);
		bar.setOpacity(0);
		loadedBar.setOpacity(0);
		world.addGObject(bar);
		world.addGObject(loadedBar);
	}

	public void appear() {
		currentScript = new ScriptExecuter(new L4BSAppear(this), world);
		currentScript.next();
	}

	public void startWobbling() {
		wobbling = true;
	}

	public void startBattle() {
		phase = 0;
	}

	public void damage(double damage) {
		if (dead) return;
		health -= damage;
		if (health <= 0) {
			health = 0;
			phase = -1;
			currentScript.stop();
			wobbling = false;
			dead = true;
			cosAngle = 0;
			music.setGain(0);
			music.cleanup();
			SoundSource effect = Engine.getSoundManager().createSoundSource("sound/level_4_boss_death.ogg", "boss_kill",
					false, false);
			effect.setGain(2);
			effect.play();
			world.getScriptContainer().runScript(ScriptManager.getScriptUnsafe("level4/room4/boss_finish"));
		}
	}

	@Override
	public void tick(long dif) {
		if (slept) return;
		super.tick(dif);
		refreshBar();
		if (wobbling) {
			lastCos = (float) Math.cos(cosAngle);
			position.add(0, lastCos * 1.5f * dif / 1000000000f);
			cosAngle += dif / 1000000000f;
		} else if (dead) {
			lastCos = (float) (cosAngle >= Math.PI ? -((cosAngle - Math.PI + 1) * 3) : (Math.cos(cosAngle) * 3));
			position.add(0, lastCos * 4 * dif / 1000000000f);
			cosAngle += dif * 10 / 1000000000f;
			if (cosAngle > 10) delete();
			return;
		}
		if (phase == 0)
			phase0(dif);

		if (world.getPlayer().getCollisionBox().collision(collisionBox) != null)
			world.getPlayer().getHealth().damage(2 * dif / 1000000000f);
	}

	private void refreshBar() {
		if (health == 5) {
			loadedBar.setOpacity(Math.max(0, loadedBar.getOpacity() - 0.1f));
			bar.setOpacity(Math.max(0, bar.getOpacity() - 0.1f));
		} else {
			loadedBar.setOpacity(Math.min(1, loadedBar.getOpacity() + 0.1f));
			bar.setOpacity(Math.min(1, bar.getOpacity() + 0.1f));
		}

		if (loadedBar.getOpacity() > 0) {
			loadedBar.setMax(new Vector3f(0.1f * (float) health / 5, 0.01f, 0));
			loadedBar.getTexture().setU2((float) health / 5);
			bar.setPosition(new Vector3f(-0.05f, world.getCamera().getBoxSize() * 1.6f, 0));
		}
	}

	private void phase0(float dif) {
		Vector2f offset = new Vector2f(world.getPlayer().getPosition());
		offset.sub(position);
		setPosition(position.add(offset.mul(dif / 1000000000f)));
		if (Math.random() < 0.025) changePhase();
	}

	private void changePhase() {
		double random = Math.random();
		if (random < 0.9)
			launchFireball();
		else if (random < 0.95)
			phase1();
		else phase2();
	}

	private void launchFireball() {
		Vector2f velocity = new Vector2f(world.getPlayer().getPosition()).add(0, 0.5f).sub(position).normalize().mul(10 / 1000000000f);
		L4BFireball fireball = new L4BFireball(world, new Vector2f(position), velocity, rectangle.getTexture(),
				this, Math.random() > 0.7);
		world.registerEntity(fireball);
		Engine.getSoundManager().createSoundSource("sound/level_4_boss_fireball.ogg", "boss_kill", false, false).play();
	}

	private void phase1() {
		phase = 1;
		currentScript = new ScriptExecuter(new L4BSPhase1(this), world);
		currentScript.next();
	}

	private void phase2() {
		phase = 2;
		currentScript = new ScriptExecuter(new L4BSPhase2(this), world);
		currentScript.next();
	}
}
