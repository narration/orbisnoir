package com.degoos.orbisnoir.game.entity.level1;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.texture.Palette;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

import java.util.Random;


public class FloatingBlock extends Entity {

	private Vector2f origin;
	private Vector2f velocity;
	private float angularVelocity;
	private long nanos;
	private Palette palette;

	public FloatingBlock(World world, Vector2f position, Vector2f velocity, float angularVelocity, Palette palette) {
		super(world, position, GColor.WHITE,
				1, new Area(-0.2f, -0.2f, 0.2f, 0.2f),
				EnumEntityDrawPriority.ALWAYS_BOTTOM,
				new CollisionBox(new Vector2f(0), new Vector2f(0), new Vector2f(0)),
				EnumEntityCollision.PASS, null, null);
		setName("floating_block");
		this.origin = new Vector2f(position);
		this.velocity = velocity;
		this.angularVelocity = angularVelocity;
		this.palette = palette;
		rectangle.setTexture(palette.getTexture(new Random().nextInt(30)));
	}

	public Vector2f getVelocity() {
		return velocity;
	}

	public void setVelocity(Vector2f velocity) {
		this.velocity = velocity;
	}

	@Override
	public void setPosition(Vector2f position) {
		super.setPosition(position);
	}

	@Override
	public void tick(long dif) {
		if (slept) return;
		nanos += dif;

		if (nanos > 50000000000L) {
			nanos = 0;
			setPosition(origin);
			rectangle.setTexture(palette.getTexture(new Random().nextInt(30)));
		}

		setPosition(getPosition().add(new Vector2f(velocity).mul(dif)));
		rectangle.setRotation(rectangle.getRotation() + angularVelocity * dif);
	}

	@Override
	public void tick2(long dif) {
		rectangle.getPosition().z = -0.8f;
	}
}
