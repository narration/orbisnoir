package com.degoos.orbisnoir.game.entity.level1.boss;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.texture.TextureAbsImpl;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.Collision;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;


public class Level1BossFire extends Entity {

	private boolean moving;
	private Vector2f to, velocity;
	private long nanos;
	private float lastSin;
	private double damage;

	public Level1BossFire(World world, Vector2f position, Vector2f to) {
		super(world, position, GColor.WHITE, 1, new Area(new Vector2f(-0.6f, -0.6f),
						new Vector2f(0.6f, 0.6f)), EnumEntityDrawPriority.RELATIVE,
				new CollisionBox(new Vector2f(0), new Vector2f(-0.6f, -0.6f), new Vector2f(0.6f, 0.6f)),
				EnumEntityCollision.PASS, null, null);
		setName("fire");
		this.moving = true;
		this.to = to == null ? position : to;
		this.velocity = new Vector2f(this.to).sub(position).mul(1 / 2000000000f);
		this.nanos = 0;
		this.lastSin = 0;
		this.damage = 0.005;

		if (to == null) moving = false;

		try {
			rectangle.setTexture(PaletteManager.loadPalette("level1/fire").getAnimation(0));
			world.addGObject((TextureAbsImpl) rectangle.getTexture());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Level1BossFire setDamage(double damage) {
		this.damage = damage;
		return this;
	}

	@Override
	public void tick(long dif) {
		if (slept) return;
		if (moving) {
			this.nanos += dif;
			if (nanos >= 2000000000) {
				moving = false;
				setPosition(to);
				return;
			}

			float sin = (float) Math.sin((nanos / 2000000000d) * Math.PI) * 6;
			Vector2f to = new Vector2f(getPosition()).add(new Vector2f(velocity).mul(dif)).add(0, sin - lastSin);
			setPosition(to);
			this.lastSin = sin;
		}
	}

	@Override
	public void collide(Entity entity, Collision collision) {
		if (moving) return;
		if (entity instanceof Player) {
			((Player) entity).getHealth().damage(damage);
		}
	}
}
