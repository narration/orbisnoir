package com.degoos.orbisnoir.game.entity.level1;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.object.texture.TextureAbsImpl;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.Collision;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.entity.Obstacle;
import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.util.VectorUtils;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

import java.util.ArrayList;
import java.util.List;


public class RotatingBalls extends Entity {

	private long balls;
	private float angularVelocity, radius;

	private List<Ball> ballList;
	private ITexture ballImage;
	private Vector2f relativePosition;

	public RotatingBalls(World world, RotatingBallsEntitySketch sketch) {
		this(world, sketch.getName(), sketch.getPosition(), sketch.getEntitySize(), sketch.getScript(),
				sketch.getColor(), sketch.getOpacity(), sketch.getCollisionBox(), sketch.getCollisionType(),
				sketch.getEntityDrawPriority(), sketch.getBalls(), sketch.getAngularVelocity(), sketch.getRadius(),
				sketch.getRelativePosition(), sketch.toCenterImage(), sketch.toBallImage());
		this.sketch = sketch;
	}

	public RotatingBalls(World world, String name, Vector2f position, Area entitySize, Script script,
	                     GColor color, float opacity, CollisionBox collisionBox, EnumEntityCollision collisionType,
	                     EnumEntityDrawPriority entityDrawPriority, long balls, float angularVelocity, float radius,
	                     Vector2f relativePosition, ITexture centerImage, ITexture ballImage) {
		super(world, position, color, opacity, entitySize, entityDrawPriority, collisionBox, collisionType, null, script);
		setName(name);
		this.balls = balls;
		this.angularVelocity = angularVelocity / 1000000000f;
		this.radius = radius;
		this.ballImage = ballImage;
		this.relativePosition = relativePosition;
		this.ballList = null;

		rectangle.setTexture(centerImage);
		initBalls();

		if (centerImage != null)
			world.addGObject((TextureAbsImpl) centerImage);
		if (ballImage != null)
			world.addGObject((TextureAbsImpl) ballImage);
	}

	private void initBalls() {
		if (ballList != null) ballList.forEach(Entity::delete);
		ballList = new ArrayList<>();
		Vector2f init = new Vector2f(0, radius);
		float anglePerBall = (float) Math.PI * 2 / balls;


		Ball ball;
		for (int i = 0; i < balls; i++) {
			ball = new Ball(new Vector2f(init), anglePerBall * i);
			world.registerEntity(ball);
			ballList.add(ball);
		}
	}


	@Override
	public void setPosition(Vector2f position) {
		super.setPosition(position);
	}

	public long getBalls() {
		return balls;
	}

	public double getRadius() {
		return radius;
	}

	public double getAngularVelocity() {
		return angularVelocity;
	}

	public void setAngularVelocity(float angularVelocity) {
		this.angularVelocity = angularVelocity;
	}

	@Override
	public void delete() {
		super.delete();
		ballList.forEach(Entity::delete);
	}

	@Override
	public void restoreToSketch() {
		super.restoreToSketch();
		if (sketch == null) return;
		this.balls = ((RotatingBallsEntitySketch) sketch).getBalls();
		this.angularVelocity = ((RotatingBallsEntitySketch) sketch).getAngularVelocity() / 1000000000f;
		this.radius = ((RotatingBallsEntitySketch) sketch).getRadius();
		this.ballImage = ((RotatingBallsEntitySketch) sketch).toBallImage();
		this.relativePosition = ((RotatingBallsEntitySketch) sketch).getRelativePosition();
		rectangle.setTexture(((RotatingBallsEntitySketch) sketch).toCenterImage());
		initBalls();

		if (rectangle.getTexture() != null)
			world.addGObject((TextureAbsImpl) rectangle.getTexture());
		if (ballImage != null)
			world.addGObject((TextureAbsImpl) ballImage);
	}

	public class Ball extends Obstacle {

		private long lastHit;
		private float rotation;
		private Vector2f origin;

		public Ball(Vector2f position, float rotation) {
			super(RotatingBalls.this.world, position,
					GColor.WHITE, 0.99f, new Area(new Vector2f(-0.5f, -0.5f),
							new Vector2f(0.5f, 0.5f)), EnumEntityDrawPriority.RELATIVE,
					new CollisionBox(new Vector2f(0), new Vector2f(-0.5f, -0.5f), new Vector2f(0.5f, 0.5f)),
					EnumEntityCollision.PASS, null, null);
			setName("ball");
			origin = new Vector2f(position);
			rectangle.setTexture(ballImage);
			lastHit = 0;
			this.rotation = rotation;
			rotate();
		}

		@Override
		public void tick(long dif) {
			if (RotatingBalls.this.isSlept()) return;
			rotation += angularVelocity * dif;
			if (rotation >= 20)
				rotation %= Math.PI * 2;
			rotate();
		}

		private void rotate () {
			Vector2f to = VectorUtils.rotate(new Vector2f(origin).add(relativePosition), rotation).add(RotatingBalls.this.getPosition());
			move(new Vector2f(to).sub(getPosition()), false);
			setPosition(to);
		}

		@Override
		public void collideSelfEntity(Entity entity, Collision collision) {
			if (System.nanoTime() - lastHit > 500000000) {
				if (entity instanceof Player) {
					lastHit = System.nanoTime();
					((Player) entity).getHealth().damage(0.30);
				}
			}
		}
	}
}
