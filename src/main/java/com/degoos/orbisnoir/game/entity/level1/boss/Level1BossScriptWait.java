package com.degoos.orbisnoir.game.entity.level1.boss;

import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.script.step.type.ScriptStepWait;

import java.util.ArrayList;

public class Level1BossScriptWait extends Script {

	public Level1BossScriptWait(long millis) {
		super("level_1_boss_wait", new ArrayList<>());
		addStep(new ScriptStepWait(millis));
	}
}
