package com.degoos.orbisnoir.game.entity.level5.boss.sub;

import com.degoos.orbisnoir.game.entity.level1.Arrow;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

public class L4BArrow extends Arrow {

	public L4BArrow(World world, Vector2f position) {
		super(world, position, new Vector2f(0, 20 / 1000000000f), null, 0.6f, 0.3f, 200);
	}
}
