package com.degoos.orbisnoir.game.entity.level4;

import com.degoos.orbisnoir.entity.sketch.EntityImageEntitySketch;
import com.degoos.orbisnoir.world.World;
import org.jooq.tools.json.ParseException;

import java.util.Map;

public class StaticDamageEntitySketch extends EntityImageEntitySketch {

	private double damage;


	public StaticDamageEntitySketch() {
		super();
		setDamage(0.3);
		jsonMap.put("type", "static_damage");
	}

	public StaticDamageEntitySketch(String json) throws ParseException {
		super(json);
		jsonMap.put("type", "static_damage");
	}

	public StaticDamageEntitySketch(Map json) {
		super(json);
		jsonMap.put("type", "static_damage");
	}


	public double getDamage() {
		return damage;
	}

	public void setDamage(double damage) {
		this.damage = damage;
		jsonMap.put("damage", damage);
	}

	@Override
	public StaticDamage toEntity(World world) {
		return new StaticDamage(world, this);
	}

	@Override
	public Map<String, Class<?>> getTypesMap() {
		Map<String, Class<?>> map = super.getTypesMap();
		map.put("damage", double.class);
		return map;
	}

	@Override
	public void refreshFromMap() {
		super.refreshFromMap();
		damage = (double) jsonMap.get("damage");
	}
}
