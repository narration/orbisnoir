package com.degoos.orbisnoir.game.entity.level1;

import com.degoos.graphicengine2.object.GColor;
import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.object.texture.TextureAbsImpl;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.collision.Collision;
import com.degoos.orbisnoir.collision.CollisionBox;
import com.degoos.orbisnoir.entity.Entity;
import com.degoos.orbisnoir.entity.Obstacle;
import com.degoos.orbisnoir.entity.player.Player;
import com.degoos.orbisnoir.enums.EnumEntityCollision;
import com.degoos.orbisnoir.enums.EnumEntityDrawPriority;
import com.degoos.orbisnoir.script.Script;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

public class DamageBall extends Obstacle {

	private double amplification, velocity;
	private double angularVelocity;
	private boolean xMovement;
	private Vector2f origin;
	private long nanos;

	public DamageBall(World world, DamageBallEntitySketch sketch) {
		this(world, sketch.getName(), sketch.getPosition(), sketch.getEntitySize(), sketch.getScript(),
				sketch.getColor(), sketch.getOpacity(), sketch.getCollisionBox(), sketch.getCollisionType(),
				sketch.getEntityDrawPriority(), sketch.getAmplification(), sketch.getVelocity(),
				sketch.isxMovement(), sketch.toImage(), sketch.getAngularVelocity());
		this.sketch = sketch;
	}

	public DamageBall(World world, String name, Vector2f position, Area entitySize, Script script,
					  GColor color, float opacity, CollisionBox collisionBox, EnumEntityCollision collisionType,
					  EnumEntityDrawPriority entityDrawPriority, double amplification, double velocity,
					  boolean xMovement, ITexture texture, double angularVelocity) {
		super(world, position, color, opacity, entitySize, entityDrawPriority, collisionBox, collisionType, null, script);
		setName(name);
		this.amplification = amplification;
		this.velocity = velocity / 1000000000f;
		this.xMovement = xMovement;
		rectangle.setTexture(texture);
		this.origin = new Vector2f(position);
		this.angularVelocity = angularVelocity / 1000000000f;
		if (rectangle.getTexture() != null)
			world.addGObject((TextureAbsImpl) rectangle.getTexture());
	}

	public double getAngularVelocity() {
		return angularVelocity;
	}

	public void setAngularVelocity(float angularVelocity) {
		this.angularVelocity = angularVelocity / 1000000000f;
	}

	@Override
	public void tick(long dif) {
		super.tick(dif);
		if (slept) return;
		nanos += dif;
		rectangle.setRotation((float) (rectangle.getRotation() + angularVelocity * dif));
		float cos = (float) Math.cos(nanos * velocity) * (float) amplification;
		Vector2f to = new Vector2f(origin).add(xMovement ? cos : 0, xMovement ? 0 : cos).sub(getPosition());
		move(to, false);
	}

	@Override
	public void restoreToSketch() {
		super.restoreToSketch();
		if (sketch == null) return;
		nanos = 0;
		this.origin = new Vector2f(getPosition());
		this.amplification = ((DamageBallEntitySketch) sketch).getAmplification();
		this.xMovement = ((DamageBallEntitySketch) sketch).isxMovement();
		this.velocity = ((DamageBallEntitySketch) sketch).getVelocity() / 1000000000f;
		this.angularVelocity = ((DamageBallEntitySketch) sketch).getAngularVelocity() / 1000000000f;
		rectangle.setTexture(((DamageBallEntitySketch) sketch).toImage());
		if (rectangle.getTexture() != null)
			world.addGObject((TextureAbsImpl) rectangle.getTexture());
	}

	@Override
	public void collideSelfEntity(Entity entity, Collision collision) {
		if (entity instanceof Player) {
			((Player) entity).getHealth().damage(0.07);
		}
	}
}
