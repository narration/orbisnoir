package com.degoos.orbisnoir.game.map.level1;

import com.degoos.orbisnoir.game.entity.level1.Level1Room4Fog;
import com.degoos.orbisnoir.world.EntitySketchContainer;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

public class Level1Room4 extends World {

	public Level1Room4(EntitySketchContainer entitySketchContainer, Vector2f playerPosition, boolean fromJar) {
		super("level1room4", entitySketchContainer, playerPosition, fromJar);
		Level1Room4Fog rectangle = new Level1Room4Fog("fog0");
		addGObject(rectangle);
		rectangle = new Level1Room4Fog("fog1");
		rectangle.setVelocity(new Vector2f(-0.0001f, -0.003f));
		addGObject(rectangle);
	}
}
