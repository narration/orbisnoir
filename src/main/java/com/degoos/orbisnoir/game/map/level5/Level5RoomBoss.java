package com.degoos.orbisnoir.game.map.level5;

import com.degoos.orbisnoir.game.entity.level5.boss.Level5Boss;
import com.degoos.orbisnoir.world.EntitySketchContainer;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

public class Level5RoomBoss extends World {

	public Level5RoomBoss(EntitySketchContainer entitySketchContainer, Vector2f playerPosition, boolean fromJar) {
		super("level5roomBoss", entitySketchContainer, playerPosition, fromJar);
		try {
			registerEntity(new Level5Boss(this, new Vector2f(player.getPosition()).sub(0, 50)));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
