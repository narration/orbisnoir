package com.degoos.orbisnoir.game.map.purgatorio;

import com.degoos.orbisnoir.entity.player.PlayerController;
import com.degoos.orbisnoir.game.entity.purgatorio.PurgatorioBackground;
import com.degoos.orbisnoir.world.EntitySketchContainer;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;


public class PurgatorioWorld extends World {

	public PurgatorioWorld(EntitySketchContainer entitySketchContainer, Vector2f playerPosition, boolean fromJar) {
		super("purgatorio", entitySketchContainer, playerPosition, fromJar);
		if (player.getController() instanceof PlayerController)
			((PlayerController) player.getController()).setCanBeControlled(false);
		getPlayer().getRectangle().setOpacity(0);
		addGObject(new PurgatorioBackground());
	}
}
