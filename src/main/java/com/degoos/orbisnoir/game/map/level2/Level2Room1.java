package com.degoos.orbisnoir.game.map.level2;

import com.degoos.orbisnoir.game.entity.level2.Level2Room1Fog;
import com.degoos.orbisnoir.world.EntitySketchContainer;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

public class Level2Room1 extends World {

	public Level2Room1(EntitySketchContainer entitySketchContainer, Vector2f playerPosition, boolean fromJar) {
		super("level2room1", entitySketchContainer, playerPosition, fromJar);
		addGObject(new Level2Room1Fog());
		addGObject(new Level2Room1Fog().setVelocity(new Vector2f(-0.0001f, -0.003f)));
	}
}
