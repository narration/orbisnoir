package com.degoos.orbisnoir.game.map.level3;

import com.degoos.orbisnoir.game.entity.level3.boss.Level3Boss;
import com.degoos.orbisnoir.world.EntitySketchContainer;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;
import org.jooq.tools.json.ParseException;

import java.io.IOException;

public class Level3Room5 extends World {

	public Level3Room5(EntitySketchContainer entitySketchContainer, Vector2f playerPosition, boolean fromJar) {
		super("level3room5", entitySketchContainer, playerPosition, fromJar);
		try {
			//24, 11
			registerEntity(new Level3Boss(this, new Vector2f(2000, 0)));
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
	}
}
