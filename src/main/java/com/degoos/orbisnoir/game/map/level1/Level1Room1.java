package com.degoos.orbisnoir.game.map.level1;

import com.degoos.orbisnoir.game.entity.level1.Level1Room1Fog;
import com.degoos.orbisnoir.world.EntitySketchContainer;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

public class Level1Room1 extends World {

	public Level1Room1(EntitySketchContainer entitySketchContainer, Vector2f playerPosition, boolean fromJar) {
		super("level1room1", entitySketchContainer, playerPosition, fromJar);
		addGObject(new Level1Room1Fog());
		getCamera().setBlockFollowXMovement(true);
	}
}
