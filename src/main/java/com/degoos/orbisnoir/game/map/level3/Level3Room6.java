package com.degoos.orbisnoir.game.map.level3;

import com.degoos.orbisnoir.game.entity.level2.Level2Room1Fog;
import com.degoos.orbisnoir.world.EntitySketchContainer;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

public class Level3Room6 extends World {

	public Level3Room6(EntitySketchContainer entitySketchContainer, Vector2f playerPosition, boolean fromJar) {
		super("level3room6", entitySketchContainer, playerPosition, fromJar);
		addGObject(new Level2Room1Fog());
		addGObject(new Level2Room1Fog().setVelocity(new Vector2f(-0.0001f, -0.003f)));
	}
}
