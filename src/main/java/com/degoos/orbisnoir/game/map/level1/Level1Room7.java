package com.degoos.orbisnoir.game.map.level1;

import com.degoos.orbisnoir.game.entity.level1.Level1Room4Fog;
import com.degoos.orbisnoir.game.entity.level1.boss.Level1BossFire;
import com.degoos.orbisnoir.world.EntitySketchContainer;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

import java.util.Random;

public class Level1Room7 extends World {

	public Level1Room7(EntitySketchContainer entitySketchContainer, Vector2f playerPosition, boolean fromJar) {
		super("level1room7", entitySketchContainer, playerPosition, fromJar);

		addGObject(new Level1Room4Fog("fog0"));
		addGObject(new Level1Room4Fog("fog1").setVelocity(new Vector2f(-0.0001f, -0.003f)));

		Random random = new Random();
		for (int x = 0; x < 70; x++) {
			for (int y = -60; y < 30; y++) {
				if (random.nextBoolean() &&
						random.nextBoolean() && random.nextBoolean()) {
					registerEntity(new Level1BossFire(this,
							new Vector2f(x + random.nextFloat(), y + random.nextFloat()), null).setDamage(0));
				}
			}
		}
	}
}
