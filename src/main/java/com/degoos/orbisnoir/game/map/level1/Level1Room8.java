package com.degoos.orbisnoir.game.map.level1;

import com.degoos.graphicengine2.object.texture.Palette;
import com.degoos.orbisnoir.game.entity.level1.FloatingBlock;
import com.degoos.orbisnoir.game.entity.level1.Level1Room1Fog;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.world.EntitySketchContainer;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2f;

public class Level1Room8 extends World {

	public Level1Room8(EntitySketchContainer entitySketchContainer, Vector2f playerPosition, boolean fromJar) {
		super("level1room8", entitySketchContainer, playerPosition, fromJar);
		addGObject(new Level1Room1Fog());

		try {
			Palette palette = PaletteManager.loadPalette("level1/bnw");
			registerEntity(new FloatingBlock(this, new Vector2f(-21, 15), new Vector2f(1 / 1000000000f, 0), 90 / 1000000000F, palette));
			registerEntity(new FloatingBlock(this, new Vector2f(-21, 0), new Vector2f(1 / 1000000000f, 1 / 1000000000f), -90 / 1000000000F, palette));
			registerEntity(new FloatingBlock(this, new Vector2f(24, 25), new Vector2f(-1 / 1000000000f, -0.5f / 1000000000f), 60 / 1000000000F, palette));
			registerEntity(new FloatingBlock(this, new Vector2f(24, 10), new Vector2f(-1 / 1000000000f, 0), 60 / 1000000000F, palette));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
