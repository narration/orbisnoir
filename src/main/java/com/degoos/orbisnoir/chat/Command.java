package com.degoos.orbisnoir.chat;

import com.degoos.graphicengine2.util.Validate;

import java.util.Objects;

public abstract class Command {

	private String root;

	public Command(String root) {
		Validate.notNull(root, "Root cannot be null!");
		this.root = root;
	}

	public String getRoot() {
		return root;
	}

	public abstract void execute(String args, Chat chat);

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Command command = (Command) o;
		return root.equals(command.root);
	}

	@Override
	public int hashCode() {
		return Objects.hash(root);
	}
}
