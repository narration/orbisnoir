package com.degoos.orbisnoir.chat;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import com.degoos.graphicengine2.event.drop.DropFilesEvent;
import com.degoos.graphicengine2.event.keyboard.KeyEvent;
import com.degoos.graphicengine2.event.keyboard.KeyHoldEvent;
import com.degoos.graphicengine2.event.keyboard.KeyPressEvent;
import com.degoos.graphicengine2.event.mouse.MouseEvent;
import com.degoos.graphicengine2.event.mouse.MousePressEvent;
import com.degoos.graphicengine2.event.window.WindowResizeEvent;
import com.degoos.graphicengine2.font.TrueTypeFont;
import com.degoos.graphicengine2.io.KeyboardCharParser;
import com.degoos.graphicengine2.object.*;
import com.degoos.graphicengine2.util.Area;
import com.degoos.orbisnoir.util.TextParser;
import org.joml.Vector3f;

import java.util.*;

public class Chat extends GObject {

	public static final TrueTypeFont FONT = Engine.getFontManager().getFontUnsafe("Game");

	private Room room;

	private boolean extended;
	private List<Text> messages;
	private Text input;
	private List<String> sentMessages;
	private String writtenMessage;
	private int sentMessageIndex;
	private Set<Command> commands;
	private Rectangle inputRectangle, chatRectangle;
	private boolean canUseKeyboard;

	public Chat(Room room) {
		this.room = room;

		extended = false;
		messages = new LinkedList<>();
		sentMessages = new LinkedList<>();
		sentMessageIndex = 0;
		canUseKeyboard = true;

		commands = new HashSet<>();

		input = new Text(FONT, new Vector3f(Engine.getRenderArea().getMin().x + 0.13f, -0.6f, 0.998f),
				new Area(0, 0, 0.9f, 0.05f), "", new GColor(1, 1, 0), 1);

		inputRectangle = new Rectangle(new Vector3f(Engine.getRenderArea().getMin().x + 0.13f, -0.6f, 0.997f),
				new Vector3f(-0.01f, -0.001f, 0), new Vector3f(new Vector3f(0.901f, 0.051f, 0)));

		chatRectangle = new Rectangle(new Vector3f(Engine.getRenderArea().getMin().x + 0.13f, -0.5f, 0.997f),
				new Vector3f(-0.01f, -0.001f, 0), new Vector3f(new Vector3f(0.901f, 0.251f, 0)));

		inputRectangle.setVisible(false);
		chatRectangle.setVisible(false);
		inputRectangle.setColor(new GColor(0.2f, 0.2f, 0.2f));
		inputRectangle.setOpacity(0.7f);
		chatRectangle.setColor(new GColor(0.2f, 0.2f, 0.2f));
		chatRectangle.setOpacity(0.7f);
		inputRectangle.setKey("input");
		chatRectangle.setKey("chat");
		room.addGObject(input);
		room.addGObject(inputRectangle);
		room.addGObject(chatRectangle);
	}

	public boolean isExtended() {
		return extended;
	}

	public void setExtended(boolean extended) {
		if (this.extended == extended) return;
		this.extended = extended;
		input.setVisible(extended);
		inputRectangle.setVisible(extended);
		chatRectangle.setVisible(extended);
		sentMessageIndex = -1;
		writtenMessage = "";
		if (!extended) input.setText("");
	}

	public List<Text> getMessages() {
		return messages;
	}

	public List<String> getSentMessages() {
		return sentMessages;
	}

	public Set<Command> getCommands() {
		return commands;
	}

	public void setCanUseKeyboard(boolean canUseKeyboard) {
		this.canUseKeyboard = canUseKeyboard;
	}

	public boolean canUseKeyboard() {
		return canUseKeyboard;
	}

	public void addText(String string) {
		Text text = new Text(FONT, new Vector3f(Engine.getRenderArea().getMin().x + 0.13f, -0.5f, 0.998f),
				new Area(0, 0, 0.9f, 0.05f), "", GColor.WHITE, 1);
		text.setVisible(true);
		TextParser.parseText(text, string);
		room.addGObject(text);
		refreshMessages();
		messages.add(0, text);
	}

	public void refreshMessages() {
		Text text;
		for (int i = 0; i < messages.size(); i++) {
			text = messages.get(i);
			text.setVisible(i < 4);
			text.setPosition(text.getPosition().add(0, 0.05f, 0));
		}
	}

	private void parseCommand(String command) {
		String[] array = command.split(" ");
		if (array.length == 0) return;
		String root = array[0];
		Optional<Command> optional = commands.stream().filter(target -> target.getRoot().equalsIgnoreCase(root)).findAny();
		if (optional.isPresent()) {
			String args = command.replaceFirst(root, "").trim();
			optional.get().execute(args, this);
		}
	}

	@Override
	public void onTick(long l, Room room) {
	}

	@Override
	public void onTick2(long l, Room room) {

	}

	@Override
	public void onAsyncTick(long l, Room room) {

	}

	@Override
	public void onKeyboardEvent(KeyEvent keyEvent) {
		if (!(keyEvent instanceof KeyPressEvent || keyEvent instanceof KeyHoldEvent)) return;
		EnumKeyboardKey key = keyEvent.getKeyboardKey();
		if (!extended) {
			if (canUseKeyboard && key == EnumKeyboardKey.T) {
				setExtended(true);
			}
		} else {
			if (key == EnumKeyboardKey.ESCAPE) {
				setExtended(false);
			} else {
				if (key == EnumKeyboardKey.LEFT_SHIFT || key == EnumKeyboardKey.RIGHT_SHIFT) return;

				if (key == EnumKeyboardKey.ARROW_UP) {
					if (sentMessageIndex == sentMessages.size() - 1) {
						return;
					}
					sentMessageIndex++;
					input.setText(sentMessages.get(sentMessageIndex));
				} else if (key == EnumKeyboardKey.ARROW_DOWN) {
					sentMessageIndex--;
					if (sentMessageIndex < -1) {
						sentMessageIndex = -1;

					} else if (sentMessageIndex == -1) {
						input.setText(writtenMessage);
					} else {
						input.setText(sentMessages.get(sentMessageIndex));
					}
				} else if (key == EnumKeyboardKey.BACKSPACE) {
					if (!input.getText().isEmpty())
						input.setText(input.getText().substring(0, input.getText().length() - 1));
				} else if (key == EnumKeyboardKey.ENTER) {
					if (!input.getText().isEmpty()) {
						if (sentMessages.isEmpty() || !input.getText().equals(sentMessages.get(0)))
							sentMessages.add(0, input.getText());
						if (input.getText().startsWith("/")) parseCommand(input.getText().substring(1));
						else addText(input.getText());
					}
					setExtended(false);
				} else {
					input.setText(input.getText() + KeyboardCharParser.toFormattedChar((char) key.getId()));
					writtenMessage = input.getText();
					sentMessageIndex = -1;
				}
			}
		}
	}

	@Override
	public void onMouseEvent(MouseEvent mouseEvent) {
		if (mouseEvent instanceof MousePressEvent) {
			setExtended(false);
		}
	}

	@Override
	public void onResize(WindowResizeEvent windowResizeEvent) {
		float x = windowResizeEvent.getNewRenderArea().getMin().x + 0.13f;
		chatRectangle.getPosition().x = x;
		inputRectangle.getPosition().x = x;
		input.getPosition().x = x;
		chatRectangle.setRequiresRecalculation(true);
		inputRectangle.setRequiresRecalculation(true);
		input.setRequiresRecalculation(true);

		for (Text text : messages) {
			text.getPosition().x = x;
			text.setRequiresRecalculation(true);
		}
	}

	@Override
	public void onDropFilesEvent(DropFilesEvent dropFilesEvent) {

	}
}
