package com.degoos.orbisnoir.chat.editorcommands;

import com.degoos.orbisnoir.chat.Command;
import com.degoos.orbisnoir.chat.editorcommands.script.ECSReloadScripts;
import com.degoos.orbisnoir.chat.editorcommands.texture.ECTCopy;
import com.degoos.orbisnoir.chat.editorcommands.texture.ECTFill;
import com.degoos.orbisnoir.chat.editorcommands.texture.ECTSetPalette;

import java.util.Set;

public class EditorCommands {

	public static void addGeneralCommands(Set<Command> commands) {
		commands.add(new ECSave());
		commands.add(new ECSReloadScripts());
	}

	public static void addTextureCommands(Set<Command> commands) {
		addGeneralCommands(commands);
		commands.add(new ECTFill());
		commands.add(new ECTCopy());
		commands.add(new ECTSetPalette());
	}

	public static void addCollisionCommands(Set<Command> commands) {
		addGeneralCommands(commands);
	}

	public static void addEntityCommands(Set<Command> commands) {
		addGeneralCommands(commands);
	}

	public static void addScriptCommands(Set<Command> commands) {
		addGeneralCommands(commands);
	}

}
