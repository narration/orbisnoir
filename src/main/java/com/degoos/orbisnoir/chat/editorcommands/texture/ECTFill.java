package com.degoos.orbisnoir.chat.editorcommands.texture;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.object.texture.Palette;
import com.degoos.orbisnoir.chat.Chat;
import com.degoos.orbisnoir.chat.Command;
import com.degoos.orbisnoir.editor.texture.EditorSelection;
import com.degoos.orbisnoir.editor.texture.TextureEditor;
import com.degoos.orbisnoir.enums.EnumBlockCollision;
import com.degoos.orbisnoir.util.VectorUtils;
import com.degoos.orbisnoir.world.Box;
import com.degoos.orbisnoir.world.Chunk;
import com.degoos.orbisnoir.world.World;
import org.joml.Vector2i;

import java.util.NoSuchElementException;

public class ECTFill extends Command {


	public ECTFill() {
		super("fill");
	}

	@Override
	public void execute(String args, Chat chat) {
		if (!(Engine.getRoom() instanceof TextureEditor)) return;
		TextureEditor editor = (TextureEditor) Engine.getRoom();
		Vector2i palette = editor.getHotbar().getSelectedIndex().getElement();

		if (palette == null) remove(editor);
		else fill(editor, editor.getBar().getPalette(), editor.getBar().getPaletteName(), palette);

	}

	private void remove(TextureEditor editor) {
		EditorSelection selection = editor.getSelection();
		for (Chunk chunk : editor.getWorld().getChunks().values()) {
			if (selection.intersects(chunk)) {
				Box[][] boxes = chunk.getBoxes();
				Box box;
				for (int x = 0; x < 32; x++) {
					for (int y = 0; y < 32; y++) {
						box = boxes[x][y];
						if (box != null && selection.intersects(box)) chunk.unregisterBox(box.getChunkPosition());
					}
				}
			}
		}
		editor.getChat().addText("&cBoxes removed.");
	}

	private void fill(TextureEditor editor, Palette palette, String paletteName, Vector2i paletteIndex) {
		EditorSelection selection = editor.getSelection();
		World world = editor.getWorld();

		Vector2i start = new Vector2i((int) Math.floor(selection.getMin().x), (int) Math.floor(selection.getMin().y));

		VectorUtils.shiftLeft(VectorUtils.shiftRight(start, 5, start), 5, start);

		for (int x = start.x; x < selection.getMax().x; x += 32) {
			for (int y = start.y; y < selection.getMax().y; y += 32) {
				fillChunk(getOrCreateChunk(world, new Vector2i(x >> 5, y >> 5), palette, paletteName), selection, paletteIndex);
			}
		}
		editor.getChat().addText("&bBoxes filled.");
	}

	private void fillChunk(Chunk chunk, EditorSelection selection, Vector2i paletteIndex) {
		Vector2i shift = VectorUtils.shiftLeft(chunk.getPosition(), 5, new Vector2i());
		Vector2i min = new Vector2i((int) Math.floor(selection.getMin().x), (int) Math.floor(selection.getMin().y))
				.sub(shift).max(new Vector2i(0));

		Vector2i max = new Vector2i((int) Math.floor(selection.getMax().x), (int) Math.floor(selection.getMax().y))
				.sub(shift).min(new Vector2i(31));

		Box box;
		for (int x = min.x; x <= max.x; x++) {
			for (int y = min.y; y <= max.y; y++) {
				box = chunk.getBox(x, y);
				if (box != null)
					box.setPaletteTexture(paletteIndex);
				else {
					box = new Box(new Vector2i(x, y), chunk, paletteIndex, EnumBlockCollision.LEVEL_0, null);
					chunk.registerBox(box);
				}
			}
		}
	}

	public static Chunk getOrCreateChunk(World world, Vector2i position, Palette palette, String paletteName) {
		try {
			return world.getChunk(position);
		} catch (NoSuchElementException ex) {
			Chunk chunk = new Chunk(world, position, palette, paletteName);
			world.registerChunk(chunk);
			return chunk;
		}
	}
}
