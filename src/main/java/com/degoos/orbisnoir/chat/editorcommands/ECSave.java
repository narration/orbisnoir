package com.degoos.orbisnoir.chat.editorcommands;

import com.degoos.graphicengine2.Engine;
import com.degoos.orbisnoir.chat.Chat;
import com.degoos.orbisnoir.chat.Command;
import com.degoos.orbisnoir.editor.Editor;
import com.degoos.orbisnoir.world.Chunk;
import com.degoos.orbisnoir.world.WorldManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ECSave extends Command {

	public ECSave() {
		super("save");
	}

	@Override
	public void execute(String args, Chat chat) {
		if (!(Engine.getRoom() instanceof Editor)) return;
		Editor editor = (Editor) Engine.getRoom();
		WorldManager.resetFolder(editor.getWorld());

		chat.addText("&bSaving and optimizing...");
		editor.getWorld().getEntitySketchContainer().saveSketches();
		List<Chunk> chunks = new ArrayList<>(editor.getWorld().getChunks().values());
		Chunk chunk;
		for (int i = 0; i < chunks.size(); i++) {
			chunk = chunks.get(i);
			if (chunk.isEmpty()) {
				editor.getWorld().unregisterChunk(chunk.getPosition());
			} else {
				try {
					WorldManager.saveChunk(chunk);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		chat.addText("&aSaved!");
	}
}
