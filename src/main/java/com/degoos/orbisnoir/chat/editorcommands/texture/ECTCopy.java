package com.degoos.orbisnoir.chat.editorcommands.texture;

import com.degoos.graphicengine2.Engine;
import com.degoos.orbisnoir.chat.Chat;
import com.degoos.orbisnoir.chat.Command;
import com.degoos.orbisnoir.editor.texture.TextureEditor;

public class ECTCopy extends Command {


	public ECTCopy() {
		super("copy");
	}

	@Override
	public void execute(String args, Chat chat) {
		if (!(Engine.getRoom() instanceof TextureEditor)) return;
		TextureEditor editor = (TextureEditor) Engine.getRoom();
		if (editor.getSelection().hasSelection()) {
			editor.getClipboard().copy(editor.getSelection());
			chat.addText("&bSelection copied to clipboard.");
		} else chat.addText("&cNo selection found.");
	}
}
