package com.degoos.orbisnoir.chat.editorcommands.script;

import com.degoos.graphicengine2.Engine;
import com.degoos.orbisnoir.chat.Chat;
import com.degoos.orbisnoir.chat.Command;
import com.degoos.orbisnoir.editor.Editor;
import com.degoos.orbisnoir.editor.script.ScriptEditor;
import com.degoos.orbisnoir.script.ScriptManager;
import com.degoos.orbisnoir.world.Box;
import com.degoos.orbisnoir.world.World;

public class ECSReloadScripts extends Command {

	public ECSReloadScripts() {
		super("reloadScripts");
	}

	@Override
	public void execute(String args, Chat chat) {
		if (!(Engine.getRoom() instanceof Editor)) return;
		Editor editor = (Editor) Engine.getRoom();
		chat.addText("&6Reloading...");
		chat.addText("&6Refreshing boxes and entities...");
		reloadScripts(editor.getWorld());
		if (editor instanceof ScriptEditor)
			((ScriptEditor) editor).getBar().loadScripts();
		chat.addText("&2All scripts has been reloaded!");
	}

	public static void reloadScripts (World world) {
		ScriptManager.load();
		world.getChunks().values().forEach(chunk -> {
			for (Box[] boxes : chunk.getBoxes()) {
				for (Box box : boxes) {
					if (box != null && box.getScript() != null) {
						box.setScript(ScriptManager.getScriptUnsafe(box.getScript().getName()));
					}
				}
			}
		});
		world.getEntities().values().forEach(entity -> {
			if (entity.getScript() != null)
				entity.setScript(ScriptManager.getScriptUnsafe(entity.getScript().getName()));
		});
	}
}
