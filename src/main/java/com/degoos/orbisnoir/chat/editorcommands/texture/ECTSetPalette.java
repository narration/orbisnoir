package com.degoos.orbisnoir.chat.editorcommands.texture;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.object.texture.Palette;
import com.degoos.orbisnoir.chat.Chat;
import com.degoos.orbisnoir.chat.Command;
import com.degoos.orbisnoir.editor.texture.TextureEditor;
import com.degoos.orbisnoir.texture.PaletteManager;
import com.degoos.orbisnoir.world.Chunk;

public class ECTSetPalette extends Command {


	public ECTSetPalette() {
		super("setPalette");
	}

	@Override
	public void execute(String args, Chat chat) {
		if (!(Engine.getRoom() instanceof TextureEditor)) return;
		TextureEditor editor = (TextureEditor) Engine.getRoom();
		try {
			Palette palette = PaletteManager.loadPalette(args);
			Chunk chunk = editor.getBar().getChunk();
			chunk.setPalette(palette, args);
		} catch (Exception e) {
			chat.addText("&cError loading palette " + args + ".");
			chat.addText("&c" + e.getMessage());
			e.printStackTrace();
		}
	}
}
