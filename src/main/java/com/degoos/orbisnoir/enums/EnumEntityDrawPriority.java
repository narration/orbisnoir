package com.degoos.orbisnoir.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumEntityDrawPriority {

	ALWAYS_TOP, ALWAYS_TOP_2, ALWAYS_TOP_3, ALWAYS_BOTTOM, ALWAYS_BOTTOM_2, ALWAYS_BOTTOM_3, RELATIVE;

	public static Optional<EnumEntityDrawPriority> getByName(String name) {
		return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
	}

}
