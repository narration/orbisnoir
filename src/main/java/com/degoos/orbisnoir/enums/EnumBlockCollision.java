package com.degoos.orbisnoir.enums;

import com.degoos.graphicengine2.object.GColor;

public enum EnumBlockCollision {

	LEVEL_0("LVL0", GColor.GREEN),
	COLLIDE("COLL", GColor.RED),
	LEVEL_1("LVL1", GColor.BLUE),
	TRANSITION_0_1("TR01", new GColor(1, 0, 1));

	private String abbreviation;
	private GColor color;


	EnumBlockCollision(String abbreviation, GColor color) {
		this.abbreviation = abbreviation;
		this.color = color;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public GColor getColor() {
		return color;
	}

	public boolean canPassThrough(EnumBlockCollision blockCollision) {
		if (blockCollision == COLLIDE) return false;
		if (blockCollision == LEVEL_0) return this == LEVEL_0 || this == COLLIDE || this == TRANSITION_0_1;
		if (blockCollision == LEVEL_1) return this == LEVEL_1 || this == TRANSITION_0_1;
		if (blockCollision == TRANSITION_0_1) return true;
		return true;
	}


}
