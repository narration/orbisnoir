package com.degoos.orbisnoir.enums;

import com.degoos.graphicengine2.enums.EnumKeyboardKey;
import org.joml.Vector2i;

import java.util.Arrays;
import java.util.Optional;

public enum EnumDirection {

	NONE(0, 0), REFRESH(0, 0), UP(0, 1), DOWN(0, -1), LEFT(-1, 0), RIGHT(1, 0), UP_LEFT(-1, 1), UP_RIGHT(1, 1), DOWN_LEFT(-1, -1), DOWN_RIGHT(1, -1);

	private Vector2i relative;

	EnumDirection(int x, int y) {
		relative = new Vector2i(x, y);
	}

	public Vector2i getRelative() {
		return new Vector2i(relative);
	}

	public EnumDirection getOpposite() {
		switch (this) {
			case UP:
				return DOWN;
			case DOWN:
				return UP;
			case LEFT:
				return RIGHT;
			case RIGHT:
				return LEFT;
			case UP_LEFT:
				return DOWN_RIGHT;
			case UP_RIGHT:
				return DOWN_LEFT;
			case DOWN_LEFT:
				return UP_RIGHT;
			case DOWN_RIGHT:
				return UP_LEFT;
			default:
				return NONE;
		}
	}

	public static Optional<EnumDirection> getByRelative(Vector2i relative) {
		return Arrays.stream(values()).filter(target -> target.getRelative().equals(relative)).findAny();
	}

	public static Optional<EnumDirection> getByName(String name) {
		return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
	}

	public static Optional<EnumDirection> getByKeys(EnumKeyboardKey primary, EnumKeyboardKey secondary) {
		if (secondary == null) return getByName(primary.name().substring(6));
		else
			return getByName(primary == EnumKeyboardKey.ARROW_UP || primary == EnumKeyboardKey.ARROW_DOWN ?
					primary.name().substring(6) + "_" + secondary.name().substring(6) : secondary.name().substring(6) + "_" + primary.name().substring(6));
	}
}
