package com.degoos.orbisnoir.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumEntityCollision {

	PASS, COLLIDE;


	public static Optional<EnumEntityCollision> getByName(String name) {
		return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
	}
}
