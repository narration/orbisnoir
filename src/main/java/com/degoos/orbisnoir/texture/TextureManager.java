package com.degoos.orbisnoir.texture;

import com.degoos.graphicengine2.object.texture.Animation;
import com.degoos.graphicengine2.object.texture.ITexture;
import com.degoos.graphicengine2.object.texture.Texture;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TextureManager {

	private static Map<String, ITexture> images = new HashMap<>();

	public static ITexture loadImageOrNull(String url) {
		try {
			return loadImage(url);
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static ITexture loadImage(String url) throws IOException {
		if (images.containsKey(url)) return images.get(url);
		Texture image = Texture.fromResource(url);
		images.put(url, image);
		return image;
	}

	public static ITexture loadAnimation(String url) throws IOException {
		if (images.containsKey(url)) return images.get(url);
		Animation animation = new Animation(url, ".png");
		images.put(url, animation);
		return animation;
	}

}
