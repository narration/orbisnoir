package com.degoos.orbisnoir.texture;

import com.degoos.graphicengine2.Engine;
import com.degoos.graphicengine2.object.texture.Palette;
import org.joml.Vector2i;
import org.jooq.tools.json.JSONValue;
import org.jooq.tools.json.ParseException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class PaletteManager {

	private static Map<String, Palette> palettes = new HashMap<>();

	public static Palette loadPalette(String name) throws IOException, ParseException {
		try {
			if (palettes.containsKey(name)) return palettes.get(name);

			Scanner scanner = new Scanner(Engine.getResourceManager().getResourceInputStream("palette/" + name + "/data.txt"));
			String data = scanner.nextLine();

			Map map = (Map) JSONValue.parseWithException(data);

			Palette palette = Palette.fromResource("palette/" + name + "/palette.png", new Vector2i((int) (long) map.get("width"),
					(int) (long) map.get("height")), "palette/" + name + "/animation");
			palettes.put(name, palette);
			return palette;
		} catch (Exception ex) {
			throw new RuntimeException("Error while loading palette " + name + ".", ex);
		}
	}

	public static boolean unloadPalette(String name) {
		return palettes.remove(name) != null;
	}

}
