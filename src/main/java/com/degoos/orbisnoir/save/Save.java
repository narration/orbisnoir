package com.degoos.orbisnoir.save;

import org.joml.Vector2f;
import org.jooq.tools.json.JSONValue;

import java.io.*;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class Save {

	private Map map;
	private File file;

	public String player_name = "Clara";
	public Vector2f player_position = new Vector2f(0, 0);
	public String player_room = "purgatorio";
	public String hotbar = "";

	public boolean spanish_controller_unlocked = false;
	public boolean french_controller_unlocked = false;
	public boolean shield_unlocked = false;

	public boolean diary_unlocked = false;
	public boolean diary_page_0 = false;
	public boolean diary_page_1 = false;
	public boolean diary_page_2 = false;
	public boolean diary_page_3 = false;
	public boolean diary_page_4 = false;
	public boolean diary_page_5 = false;
	public boolean diary_page_6 = false;
	public boolean diary_page_7 = false;
	public boolean diary_page_8 = false;
	public boolean diary_page_9 = false;
	public boolean diary_page_10 = false;
	public boolean diary_page_11 = false;
	public boolean diary_page_12 = false;
	public boolean diary_page_0_read = false;
	public boolean diary_page_1_read = false;
	public boolean diary_page_2_read = false;
	public boolean diary_page_3_read = false;
	public boolean diary_page_4_read = false;
	public boolean diary_page_5_read = false;
	public boolean diary_page_6_read = false;
	public boolean diary_page_7_read = false;
	public boolean diary_page_8_read = false;
	public boolean diary_page_9_read = false;
	public boolean diary_page_10_read = false;
	public boolean diary_page_11_read = false;
	public boolean diary_page_12_read = false;

	public boolean tutorial_started = false;
	public boolean tutorial_flower_collected = false;
	public boolean tutorial_mop_collected = false;
	public int tutorial_watch_picture = 1;

	public boolean level_1_room_1_started = false;
	public boolean level_1_room_1_house_text = false;
	public boolean level_1_room_2_flashback = false;
	public boolean level_1_room_2_started = false;
	public boolean level_1_room_2_diary_close = false;
	public boolean level_1_room_3_chimney_room_text = false;
	public boolean level_1_room_3_fire_checked = false;
	public boolean level_1_room_3_bucket = false;
	public boolean level_1_room_3_bucket_filled = false;
	public boolean level_1_room_3_fire_extinguished = false;
	public boolean level_1_room_4_join_room_0 = false;
	public boolean level_1_room_4_join_room_2 = false;
	public boolean level_1_room_4_run_button = false;
	public boolean level_1_room_5_started = false;
	public boolean level_1_room_5_open_jeweler = false;

	public int level_2_clocks = -1;
	public boolean level_2_ring_collected = false;
	public boolean level_2_tree_repaired = false;
	public boolean level_2_mark1 = false;
	public boolean level_2_mark2 = false;
	public boolean level_2_mark3 = false;
	public boolean level_2_text1 = false;
	public boolean level_2_text2 = false;
	public boolean level_2_text3 = false;
	public boolean level_2_text4 = false;
	public boolean level_2_text5 = false;
	public boolean level_2_diary_close = false;
	public boolean level_2_picture_fixed = false;

	public boolean level_3_room_1_started = false;
	public boolean level_3_room_1_warrior = false;
	public boolean level_3_room_2_warning = false;
	public boolean level_3_room_3_started = false;
	public boolean level_3_room_4_french = false;
	public boolean level_3_room_5_started = false;
	public boolean level_3_room_5_key_picked = false;
	public boolean level_3_room_5_finished = false;

	public boolean level_4_room_2_mark_1 = false;
	public boolean level_4_room_2_mark_2 = false;
	public boolean level_4_room_2_mark_3 = false;
	public boolean level4_room_3_text = false;
	public boolean level_4_first = true;
	public int level_4_room_2_red_1 = 0;
	public int level_4_room_2_blue_1 = 0;

	public boolean level_5_room_1_hasEphigee = false;
	public boolean level_5_room_1_init = false;
	public boolean level_5_room_1_text_1 = false;
	public boolean level_5_room_1_text_2 = false;
	public boolean level_5_room_2_hasTalkedToMiguel = false;
	public boolean level_5_room_2_hasTalkedToMiguel2 = false;
	public boolean level_5_room_2_text_1 = false;
	public boolean level_5_room_3_nightmare_ = false;
	public boolean level_5_room_4_golem_warning = false;
	public boolean level_5_room_4_turret = false;
	public boolean level_5_room_4_town = false;
	public boolean level_5_room_4_text_1 = false;
	public boolean level_5_room_4_mark_1 = false;
	public boolean level_5_room_4_mark_2 = false;
	public boolean level_5_room_4_mark_3 = false;

	public Save(File file, boolean load) {
		this.file = file;
		try {
			if (!file.isFile()) {
				if (file.exists()) file.delete();
				file.createNewFile();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (load) load();
		else map = new HashMap();
	}

	//region injection

	private void load() {
		try (InputStream is = new FileInputStream(file)) {
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			int data;
			while ((data = is.read()) > -1) byteArrayOutputStream.write(data);
			byteArrayOutputStream.close();
			is.close();
			String info = new String(byteArrayOutputStream.toByteArray());
			if (info.isEmpty()) map = new HashMap();
			else
				map = (Map) JSONValue.parseWithException(info);
		} catch (Exception ex) {
			ex.printStackTrace();
			map = new HashMap();
		}
		injectFields();
	}

	private void injectFields() {
		try {
			Field[] fields = Save.class.getFields();
			for (Field field : fields) {
				String name = field.getName();
				if (name.equals("map") || name.equals("file")) continue;
				if (map.containsKey(name)) {
					injectDataToField(field, map.get(name));
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void injectDataToField(String fieldName, Object data) throws NoSuchFieldException {
		injectDataToField(Save.class.getField(fieldName), data);
	}

	public void injectDataToField(Field field, Object data) {
		try {
			Class<?> clazz = field.getType();
			if (clazz.equals(int.class)) {
				if (data instanceof Long) {
					field.setInt(this, (int) (long) data);
				} else if (data instanceof Integer) {
					field.setInt(this, (int) data);
				} else field.setInt(this, Integer.valueOf(data.toString()));
			} else if (clazz.equals(long.class)) {
				if (data instanceof Long) {
					field.setLong(this, (long) data);
				} else if (data instanceof Integer) {
					field.setLong(this, (long) (int) data);
				} else field.setLong(this, Long.valueOf(data.toString()));
			} else if (clazz.equals(float.class)) {
				if (data instanceof Long) {
					field.setFloat(this, (float) (long) data);
				} else if (data instanceof Integer) {
					field.setFloat(this, (float) (int) data);
				} else if (data instanceof Double) {
					field.setFloat(this, (float) (double) data);
				} else if (data instanceof Float) {
					field.setFloat(this, (float) data);
				} else field.setFloat(this, Float.valueOf(data.toString()));
			} else if (clazz.equals(double.class)) {
				if (data instanceof Long) {
					field.setDouble(this, (float) (long) data);
				} else if (data instanceof Integer) {
					field.setDouble(this, (float) (int) data);
				} else if (data instanceof Double) {
					field.setDouble(this, (double) data);
				} else if (data instanceof Float) {
					field.setDouble(this, (double) (float) data);
				} else field.setDouble(this, Double.valueOf(data.toString()));
			} else if (clazz.equals(boolean.class)) {
				if (data instanceof Boolean)
					field.setBoolean(this, (boolean) data);
				else field.setBoolean(this, Boolean.valueOf(data.toString()));
			} else if (clazz.equals(String.class)) {
				field.set(this, data.toString());
			} else if (clazz.equals(Vector2f.class)) {
				String aux = data.toString();
				aux = aux.substring(1, aux.length() - 1).replace(" ", "");
				String[] sl = aux.split(",");
				field.set(this, new Vector2f(Float.valueOf(sl[0]), Float.valueOf(sl[1])));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	//endregion


	public Object getValue(String key) {
		try {
			return Save.class.getField(key).get(this);
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public void save() {
		try {
			map.clear();
			Field[] fields = Save.class.getFields();
			for (Field field : fields) {
				String name = field.getName();
				if (name.equals("map") || name.equals("file")) continue;
				Object o = field.get(this);
				if (o instanceof Vector2f) o = "(" + ((Vector2f) o).x + "," + ((Vector2f) o).y + ")";
				map.put(name, o);
			}
			BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file));
			outputStream.write(JSONValue.toJSONString(map).getBytes());
			outputStream.flush();
			outputStream.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
